// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_H
#define pRC_H

#include <prc/config.hpp>

#include <prc/std.hpp>

#include <prc/pragma.hpp>

#include <prc/core.hpp>

#include <prc/algorithms.hpp>

#include <prc/tensor_train.hpp>

#endif // pRC_H
