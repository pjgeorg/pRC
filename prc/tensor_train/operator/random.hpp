// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_OPERATOR_RANDOM_H
#define pRC_TENSOR_TRAIN_OPERATOR_RANDOM_H

#include <prc/core/basic/random.hpp>
#include <prc/tensor_train/operator/views/random.hpp>

namespace pRC
{
    template<class T, class M, class N, class Ranks, template<class...> class D>
    struct Random<TensorTrain::Operator<T, M, N, Ranks>, D<typename T::Value>,
        If<IsDistribution<D<typename T::Value>>>>
    {
    public:
        Random(RandomEngine &rng, D<typename T::Value> &distribution)
            : mRNG(rng)
            , mDistribution(distribution)
        {
        }

        constexpr auto operator()()
        {
            return TensorTrain::OperatorViews::Random<T, M, N, Ranks, D>(
                mRNG, mDistribution);
        }

    private:
        RandomEngine &mRNG;
        D<typename T::Value> &mDistribution;
    };
}
#endif // pRC_TENSOR_TRAIN_OPERATOR_RANDOM_H
