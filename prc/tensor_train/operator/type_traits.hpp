// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_OPERATOR_TYPE_TRAITS_H
#define pRC_TENSOR_TRAIN_OPERATOR_TYPE_TRAITS_H

#include <prc/core/basic/common.hpp>
#include <prc/core/basic/sequence.hpp>
#include <prc/core/basic/type_traits.hpp>
#include <prc/core/container/type_traits.hpp>

namespace pRC::TensorTrain
{
    template<class T, class M, class N,
        class R = decltype(Sizes(makeConstantSequence<Index,
            Common<typename M::Dimension, typename N::Dimension>() - 1, 1>())),
        class = If<>>
    class Operator;

    template<class>
    struct IsOperator : False<>
    {
    };

    template<class T>
    struct IsOperator<T const> : IsOperator<T>
    {
    };

    template<class T, class M, class N, class R>
    struct IsOperator<Operator<T, M, N, R>> : True<>
    {
    };

    namespace OperatorViews
    {
        template<class T, class M, class N, class R, class F>
        class View;

        template<class T, class M, class N, class R, class F>
        class Assignable;
    }

    template<class X, class T, class M, class N, class R, class F>
    static inline constexpr auto isOperatorView(
        OperatorViews::View<T, M, N, R, F> const &&)
    {
        if constexpr(IsReference<X>{})
        {
            return False<>();
        }
        else
        {
            return True<>();
        }
    }

    template<class>
    static inline constexpr auto isOperatorView(...)
    {
        return False<>();
    }

    template<class T>
    struct IsOperatorView
        : decltype(pRC::TensorTrain::isOperatorView<T>(declval<T>()))
    {
    };

    template<class T>
    using IsOperatorish = Any<IsOperator<T>, IsOperatorView<T>>;
}
#endif // pRC_TENSOR_TRAIN_OPERATOR_TYPE_TRAITS_H
