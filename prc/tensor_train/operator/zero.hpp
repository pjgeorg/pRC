// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_OPERATOR_ZERO_H
#define pRC_TENSOR_TRAIN_OPERATOR_ZERO_H

#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/functors/eval.hpp>
#include <prc/tensor_train/operator/views/enumerate.hpp>

namespace pRC
{
    template<class T>
    struct Zero<T, If<TensorTrain::IsOperatorView<T>>> : Zero<ResultOf<Eval, T>>
    {
    };

    template<class T>
    struct Zero<T, If<TensorTrain::IsOperator<T>>>
    {
        constexpr auto operator()()
        {
            auto const f = []<Index C>()
            {
                return zero<typename T::template Cores<C>>();
            };

            using F = RemoveConstReference<decltype(f)>;
            using M = typename T::M;
            using N = typename T::N;
            using Ranks = typename T::Ranks;

            return TensorTrain::OperatorViews::Enumerate<typename T::Type, M, N,
                Ranks, F>(f);
        }
    };
}
#endif // pRC_TENSOR_TRAIN_OPERATOR_ZERO_H
