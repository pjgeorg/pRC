// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_OPERATOR_VIEWS_REFERENCE_TO_CONST_H
#define pRC_TENSOR_TRAIN_OPERATOR_VIEWS_REFERENCE_TO_CONST_H

#include <prc/tensor_train/operator/type_traits.hpp>

namespace pRC::TensorTrain::OperatorViews
{
    template<class T, class M, class N, class Ranks>
    class ReferenceToConst;

    template<class T, class M, class N, class Ranks>
    ReferenceToConst(Operator<T, M, N, Ranks> const &)
        -> ReferenceToConst<T, M, N, Ranks>;

    template<class T, class M, class N, class Ranks>
    class ReferenceToConst
        : public View<T, M, N, Ranks, ReferenceToConst<T, M, N, Ranks>>
    {
    private:
        using Base = View<T, M, N, Ranks, ReferenceToConst>;

    public:
        ReferenceToConst(Operator<T, M, N, Ranks> const &a)
            : mA(a)
        {
        }

        ReferenceToConst(Operator<T, M, N, Ranks> const &&) = delete;

        template<Index C>
        constexpr decltype(auto) core() const
        {
            return mA.template core<C>();
        }

    private:
        Operator<T, M, N, Ranks> const &mA;
    };
}
#endif // pRC_TENSOR_TRAIN_OPERATOR_VIEWS_REFERENCE_TO_CONST_H
