// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_OPERATOR_VIEWS_ENUMERATE_H
#define pRC_TENSOR_TRAIN_OPERATOR_VIEWS_ENUMERATE_H

#include <prc/core/basic/sequence.hpp>
#include <prc/tensor_train/operator/type_traits.hpp>
#include <prc/tensor_train/tensor/type_traits.hpp>

namespace pRC::TensorTrain::OperatorViews
{
    template<class T, class M, class N, class Ranks, class F, class... Vs>
    class Enumerate
        : public Conditional<
              IsAssignable<decltype(declval<F>().template operator()<0>(
                  declval<Vs>().template core<0>()...))>,
              Assignable<T, M, N, Ranks, Enumerate<T, M, N, Ranks, F, Vs...>>,
              View<T, M, N, Ranks, Enumerate<T, M, N, Ranks, F, Vs...>>>
    {
        static_assert(All<Any<IsTensorView<Vs>, IsOperatorView<Vs>>...>());

    private:
        using Base = Conditional<
            IsAssignable<decltype(declval<F>().template operator()<0>(
                declval<Vs>().template core<0>()...))>,
            Assignable<T, M, N, Ranks, Enumerate>,
            View<T, M, N, Ranks, Enumerate>>;

    public:
        template<class... Xs, If<All<IsSame<Vs, RemoveReference<Xs>>...>> = 0>
        Enumerate(F f, Xs &&...args)
            : mF(forward<F>(f))
            , mArgs(forward<Xs>(args)...)
        {
        }

        using Base::operator=;

        template<Index C>
        constexpr decltype(auto) core()
        {
            return expand(makeSeriesFor<Index, Vs...>(),
                [this](auto const... ops) -> decltype(auto)
                {
                    return mF.template operator()<C>(
                        get<ops>(mArgs).template core<C>()...);
                });
        }

        template<Index C>
        constexpr decltype(auto) core() const
        {
            return expand(makeSeriesFor<Index, Vs...>(),
                [this](auto const... ops) -> decltype(auto)
                {
                    return mF.template operator()<C>(
                        get<ops>(mArgs).template core<C>()...);
                });
        }

    private:
        F mF;
        tuple<Vs...> mArgs;
    };
}
#endif // pRC_TENSOR_TRAIN_OPERATOR_VIEWS_ENUMERATE_H
