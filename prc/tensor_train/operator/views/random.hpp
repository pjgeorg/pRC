// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_OPERATOR_VIEWS_RANDOM_H
#define pRC_TENSOR_TRAIN_OPERATOR_VIEWS_RANDOM_H

#include <prc/core/basic/functions/random.hpp>
#include <prc/tensor_train/operator/type_traits.hpp>

namespace pRC::TensorTrain::OperatorViews
{
    template<class T, class M, class N, class Ranks, template<class...> class D>
    class Random : public View<T, M, N, Ranks, Random<T, M, N, Ranks, D>>
    {
    private:
        using Base = View<T, M, N, Ranks, Random>;

    public:
        Random(RandomEngine &rng, D<typename T::Value> &distribution)
            : mRNG(rng)
            , mDistribution(distribution)
        {
        }

        template<Index C>
        constexpr decltype(auto) core()
        {
            using Core = typename Base::template Cores<C>;
            return random<Core>(mRNG, mDistribution);
        }

    private:
        RandomEngine &mRNG;
        D<typename T::Value> &mDistribution;
    };
}
#endif // pRC_TENSOR_TRAIN_OPERATOR_VIEWS_RANDOM_H
