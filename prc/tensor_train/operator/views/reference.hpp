// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_OPERATOR_VIEWS_REFERENCE_H
#define pRC_TENSOR_TRAIN_OPERATOR_VIEWS_REFERENCE_H

#include <prc/tensor_train/operator/type_traits.hpp>

namespace pRC::TensorTrain::OperatorViews
{
    template<class T, class M, class N, class Ranks>
    class Reference;

    template<class T, class M, class N, class Ranks>
    Reference(Operator<T, M, N, Ranks> &) -> Reference<T, M, N, Ranks>;

    template<class T, class M, class N, class Ranks>
    class Reference
        : public Assignable<T, M, N, Ranks, Reference<T, M, N, Ranks>>
    {
    private:
        using Base = Assignable<T, M, N, Ranks, Reference>;

    public:
        Reference(Operator<T, M, N, Ranks> &a)
            : mA(a)
        {
        }

        using Base::operator=;

        template<Index C>
        constexpr decltype(auto) core()
        {
            return mA.template core<C>();
        }

        template<Index C>
        constexpr decltype(auto) core() const
        {
            return asConst(mA).template core<C>();
        }

    private:
        Operator<T, M, N, Ranks> &mA;
    };
}
#endif // pRC_TENSOR_TRAIN_OPERATOR_VIEWS_REFERENCE_H
