// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_OPERATOR_VIEWS_ASSIGNABLE_H
#define pRC_TENSOR_TRAIN_OPERATOR_VIEWS_ASSIGNABLE_H

#include <prc/core/basic/crtp.hpp>
#include <prc/core/basic/functions/identity.hpp>
#include <prc/core/basic/functions/unit.hpp>
#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/basic/range.hpp>
#include <prc/core/basic/sequence.hpp>
#include <prc/core/functors/add.hpp>
#include <prc/core/functors/mul.hpp>
#include <prc/core/tensor/functions/chip.hpp>
#include <prc/core/tensor/type_traits.hpp>
#include <prc/core/value/type_traits.hpp>
#include <prc/tensor_train/common/functions/round.hpp>
#include <prc/tensor_train/operator/type_traits.hpp>

namespace pRC::TensorTrain::OperatorViews
{
    template<class T, Size... Ms, Size... Ns, Size... Rs, class F>
    class Assignable<T, Sizes<Ms...>, Sizes<Ns...>, Sizes<Rs...>, F>
        : public View<T, Sizes<Ms...>, Sizes<Ns...>, Sizes<Rs...>, F>
    {
    private:
        using Base = View<T, Sizes<Ms...>, Sizes<Ns...>, Sizes<Rs...>, F>;

    public:
        constexpr auto &operator=(Zero<> const)
        {
            return *this = zero<F>();
        }

        constexpr auto &operator=(Unit<> const)
        {
            return *this = unit<F>();
        }

        constexpr auto &operator=(Identity<> const)
        {
            return *this = identity<F>();
        }

        template<class X, class R = RemoveReference<X>,
            If<IsOperatorish<R>> = 0,
            If<IsSame<typename Base::Sizes, typename R::Sizes>> = 0,
            If<IsSame<typename Base::Ranks, typename R::Ranks>> = 0,
            If<IsConvertible<typename R::Type, T>> = 0>
        constexpr auto &operator=(X &&rhs)
        {
            range<Context::CompileTime, typename Base::Dimension{}>(
                [this, &rhs](auto const i)
                {
                    this->self().template core<i>() =
                        forward<X>(rhs).template core<i>();
                });
            return this->self();
        }

        template<class X, class R = RemoveReference<X>,
            If<IsOperatorish<R>> = 0,
            If<IsSame<typename Base::Sizes, typename R::Sizes>> = 0,
            If<Not<IsSame<typename Base::Ranks, typename R::Ranks>>> = 0,
            If<IsSame<typename Base::Ranks,
                typename decltype(round<Rs...>(declval<X>()))::Ranks>> = 0,
            If<IsConvertible<typename R::Type, T>> = 0>
        constexpr auto &operator=(X &&rhs)
        {
            this->self() = round<Rs...>(forward<X>(rhs));
            return this->self();
        }

        template<class X, If<IsInvocable<Add, F, X>> = 0>
        constexpr auto operator+=(X &&rhs)
        {
            this->self() = this->self() + forward<X>(rhs);
            return this->self();
        }

        template<class X, If<IsInvocable<Sub, F, X>> = 0>
        constexpr auto operator-=(X &&rhs)
        {
            this->self() = this->self() - forward<X>(rhs);
            return this->self();
        }

        template<class X, If<IsInvocable<Mul, X, F>> = 0>
        constexpr auto applyOnTheLeft(X &&lhs)
        {
            this->self() = eval(forward<X>(lhs) * this->self());
            return this->self();
        }

        template<class X, If<IsInvocable<Mul, F, X>> = 0>
        constexpr auto applyOnTheRight(X &&rhs)
        {
            this->self() = eval(this->self() * forward<X>(rhs));
            return this->self();
        }

        template<class X, If<IsInvocable<Mul, F, X>> = 0>
        constexpr auto operator*=(X &&rhs)
        {
            return applyOnTheRight(forward<X>(rhs));
        }

        template<class X, If<IsInvocable<Div, F, X>> = 0>
        constexpr auto operator/=(X &&rhs)
        {
            this->self() = this->self() / forward<X>(rhs);
            return this->self();
        }

    protected:
        ~Assignable() = default;
        constexpr Assignable(Assignable const &) = default;
        constexpr Assignable(Assignable &&) = default;
        constexpr Assignable &operator=(Assignable const &) = delete;
        constexpr Assignable &operator=(Assignable &&) = delete;
        constexpr Assignable() = default;
    };
}
#endif // pRC_TENSOR_TRAIN_OPERATOR_VIEWS_ASSIGNABLE_H
