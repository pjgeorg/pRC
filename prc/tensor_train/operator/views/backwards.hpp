// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_OPERATOR_VIEWS_BACKWARDS_H
#define pRC_TENSOR_TRAIN_OPERATOR_VIEWS_BACKWARDS_H

#include <prc/core/basic/sequence.hpp>
#include <prc/tensor_train/operator/type_traits.hpp>
#include <prc/tensor_train/tensor/type_traits.hpp>

namespace pRC::TensorTrain::OperatorViews
{
    template<class T, class M, class N, class Ranks, class V>
    class Backwards
        : public Conditional<IsAssignable<V>,
              Assignable<T, M, N, Ranks, Backwards<T, M, N, Ranks, V>>,
              View<T, M, N, Ranks, Backwards<T, M, N, Ranks, V>>>
    {
        static_assert(IsOperatorView<V>());

    private:
        using Base =
            Conditional<IsAssignable<V>, Assignable<T, M, N, Ranks, Backwards>,
                View<T, M, N, Ranks, Backwards>>;

    public:
        template<class X, If<IsSame<V, RemoveReference<X>>> = 0>
        Backwards(X &&a)
            : mA(forward<X>(a))
        {
        }

        using Base::operator=;

        template<Index C>
        constexpr decltype(auto) core()
        {
            constexpr auto B = typename Base::Dimension() - C - 1;

            return permute<3, 1, 2, 0>(mA.template core<B>());
        }

        template<Index C>
        constexpr decltype(auto) core() const
        {
            constexpr auto B = typename Base::Dimension() - C - 1;

            return permute<3, 1, 2, 0>(mA.template core<B>());
        }

    private:
        V mA;
    };
}
#endif // pRC_TENSOR_TRAIN_OPERATOR_VIEWS_BACKWARDS_H
