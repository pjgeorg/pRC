// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_EVAL_H
#define pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_EVAL_H

#include <prc/tensor_train/operator/type_traits.hpp>

namespace pRC
{
    template<class X, If<TensorTrain::IsOperator<RemoveReference<X>>> = 0>
    static inline constexpr X eval(X &&a)
    {
        return forward<X>(a);
    }

    template<class X, If<TensorTrain::IsOperatorView<RemoveReference<X>>> = 0>
    static inline constexpr auto eval(X &&a)
    {
        return TensorTrain::Operator(forward<X>(a));
    }
}
#endif // pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_EVAL_H
