// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_BACKWARDS_H
#define pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_BACKWARDS_H

#include <prc/core/functors/backwards.hpp>
#include <prc/core/functors/view.hpp>
#include <prc/tensor_train/operator/views/backwards.hpp>

namespace pRC
{
    template<class X, class R = RemoveReference<X>,
        If<TensorTrain::IsOperatorish<R>> = 0, If<IsInvocable<View, X>> = 0>
    static inline constexpr auto backwards(X &&a)
    {
        using V = RemoveReference<decltype(view(forward<X>(a)))>;
        using M = decltype(reverse(typename R::M()));
        using N = decltype(reverse(typename R::N()));
        using Ranks = decltype(reverse(typename R::Ranks()));

        return TensorTrain::OperatorViews::Backwards<typename R::Type, M, N,
            Ranks, V>(view(forward<X>(a)));
    }

    template<class X, class R = RemoveReference<X>,
        If<TensorTrain::IsOperatorish<R>> = 0,
        If<Not<IsInvocable<View, X>>> = 0>
    static inline constexpr auto backwards(X &&a)
    {
        return eval(backwards(a));
    }
}
#endif // pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_BACKWARDS_H
