// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_VIEW_H
#define pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_VIEW_H

#include <prc/tensor_train/operator/views/reference.hpp>
#include <prc/tensor_train/operator/views/reference_to_const.hpp>

namespace pRC
{
    template<class X, If<TensorTrain::IsOperatorView<RemoveReference<X>>> = 0>
    static inline constexpr X view(X &&a)
    {
        return forward<X>(a);
    }

    template<class T, class M, class N, class R>
    static inline constexpr auto view(
        TensorTrain::Operator<T, M, N, R> const &a)
    {
        return TensorTrain::OperatorViews::ReferenceToConst(a);
    }

    template<class T, class M, class N, class R>
    static inline constexpr auto view(TensorTrain::Operator<T, M, N, R> &a)
    {
        return TensorTrain::OperatorViews::Reference(a);
    }

    template<class T, class M, class N, class R>
    static inline constexpr auto view(
        TensorTrain::Operator<T, M, N, R> const &&) = delete;
}
#endif // pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_VIEW_H
