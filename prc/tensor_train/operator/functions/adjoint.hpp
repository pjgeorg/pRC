// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_ADJOINT_H
#define pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_ADJOINT_H

#include <prc/core/functors/adjoint.hpp>
#include <prc/core/functors/conj.hpp>
#include <prc/core/functors/transpose.hpp>
#include <prc/tensor_train/common/functions/conj.hpp>
#include <prc/tensor_train/operator/functions/transpose.hpp>

namespace pRC
{
    template<class X, If<TensorTrain::IsOperatorish<RemoveReference<X>>> = 0,
        If<IsInvocable<View, X>> = 0, If<IsInvocable<Transpose, X>> = 0,
        If<IsInvocable<Conj, ResultOf<Transpose, X>>> = 0>
    static inline constexpr auto adjoint(X &&a)
    {
        return conj(transpose(forward<X>(a)));
    }

    template<class X, If<TensorTrain::IsOperatorish<RemoveReference<X>>> = 0,
        If<Not<IsInvocable<View, X>>> = 0, If<IsInvocable<Adjoint, X &>> = 0>
    static inline constexpr auto adjoint(X &&a)
    {
        return eval(adjoint(a));
    }
}
#endif // pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_ADJOINT_H
