// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_TRANSFORM_H
#define pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_TRANSFORM_H

#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/operator/transform.hpp>
#include <prc/tensor_train/operator/type_traits.hpp>

namespace pRC
{
    template<Operator::Transform OT = Operator::Transform::None, class X,
        class R = RemoveReference<X>, If<TensorTrain::IsOperatorish<R>> = 0>
    static inline constexpr decltype(auto) transform(X &&a)
    {
        if constexpr(OT == Operator::Transform::Transpose)
        {
            return transpose(forward<X>(a));
        }
        else if constexpr(OT == Operator::Transform::Adjoint)
        {
            return adjoint(forward<X>(a));
        }
        else if constexpr(OT == Operator::Transform::None)
        {
            if constexpr(TensorTrain::IsOperator<R>() &&
                !IsInvocable<View, X>())
            {
                return eval(forward<X>(a));
            }
            else
            {
                return view(forward<X>(a));
            }
        }
        else
        {
            static_assert(OT != OT, "Unsupported operator transformation.");
        }
    }
}
#endif // pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_TRANSFORM_H
