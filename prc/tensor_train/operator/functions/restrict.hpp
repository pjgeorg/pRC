// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_RESTRICT_H
#define pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_RESTRICT_H

#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/operator/restrict.hpp>
#include <prc/tensor_train/operator/type_traits.hpp>

namespace pRC
{
    template<Operator::Restrict OR = Operator::Restrict::None, class X,
        class R = RemoveReference<X>, If<TensorTrain::IsOperatorish<R>> = 0>
    static inline constexpr decltype(auto) restrict(X &&a)
    {
        if constexpr(OR == Operator::Restrict::None)
        {
            if constexpr(TensorTrain::IsOperator<R>() &&
                !IsInvocable<View, X>())
            {
                return eval(forward<X>(a));
            }
            else
            {
                return view(forward<X>(a));
            }
        }
        else
        {
            static_assert(OR != OR, "Unsupported operator restriction.");
        }
    }
}
#endif // pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_RESTRICT_H
