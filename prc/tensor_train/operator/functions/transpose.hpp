// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_TRANSPOSE_H
#define pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_TRANSPOSE_H

#include <prc/core/functors/loop.hpp>
#include <prc/core/functors/permute.hpp>
#include <prc/tensor_train/common/functions/loop.hpp>

namespace pRC
{
    template<class X, class R = RemoveReference<X>,
        If<TensorTrain::IsOperatorish<R>> = 0,
        If<IsInvocable<Loop<Permute<0, 2, 1, 3>>, X>> = 0>
    static inline constexpr auto transpose(X &&a)
    {
        return loop<Permute<0, 2, 1, 3>>(forward<X>(a));
    }
}
#endif // pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_TRANSPOSE_H
