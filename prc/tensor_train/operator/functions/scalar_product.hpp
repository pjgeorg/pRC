// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_SCALAR_PRODUCT_H
#define pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_SCALAR_PRODUCT_H

#include <prc/core/functors/hadamard_product.hpp>
#include <prc/core/tensor/functions/chip.hpp>
#include <prc/core/tensor/functions/contract.hpp>
#include <prc/core/tensor/functions/reshape.hpp>
#include <prc/tensor_train/operator/type_traits.hpp>

namespace pRC
{
    template<class XA, class XB,
        If<TensorTrain::IsOperatorish<RemoveReference<XA>>> = 0,
        If<TensorTrain::IsOperatorish<RemoveReference<XB>>> = 0,
        If<IsInvocable<HadamardProduct, XA, XB>> = 0>
    static inline constexpr auto scalarProduct(XA &&a, XB &&b)
    {
        return expand(
            makeRange<Index, 1, typename RemoveReference<XA>::Dimension{}>(),
            [&a, &b](auto const... seq)
            {
                return RecursiveLambda(
                    [](auto const &self, auto &&r, auto &&t, auto &&...args)
                    {
                        auto const f = [&r, &t]()
                        {
                            return contract<0, 1, 2, 0, 1, 2>(
                                eval(contract<0, 0>(forward<decltype(r)>(r),
                                    get<0>(forward<decltype(t)>(t)))),
                                get<1>(forward<decltype(t)>(t)));
                        };

                        if constexpr(sizeof...(args) == 0)
                        {
                            return reshape<>(f());
                        }
                        else
                        {
                            return self(f(), forward<decltype(args)>(args)...);
                        }
                    })(
                    eval(contract<0, 1, 0, 1>(
                        chip<0>(forward<XA>(a).template core<0>(), 0),
                        chip<0>(forward<XB>(b).template core<0>(), 0))),
                    forwardAsTuple(forward<XA>(a).template core<seq>(),
                        forward<XB>(b).template core<seq>())...);
            });
    }
}
#endif // pRC_TENSOR_TRAIN_OPERATOR_FUNCTIONS_SCALAR_PRODUCT_H
