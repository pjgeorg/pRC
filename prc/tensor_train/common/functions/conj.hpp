// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_CONJ_H
#define pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_CONJ_H

#include <prc/core/functors/conj.hpp>
#include <prc/core/functors/loop.hpp>
#include <prc/tensor_train/common/functions/loop.hpp>

namespace pRC
{
    template<class X, class R = RemoveReference<X>,
        If<Any<TensorTrain::IsTensorish<R>, TensorTrain::IsOperatorish<R>>> = 0,
        If<IsInvocable<Loop<Conj>, X>> = 0>
    static inline constexpr auto conj(X &&a)
    {
        return loop<Conj>(forward<X>(a));
    }
}
#endif // pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_CONJ_H
