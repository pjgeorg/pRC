// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_PLUS_H
#define pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_PLUS_H

#include <prc/core/functors/loop.hpp>
#include <prc/core/functors/plus.hpp>
#include <prc/tensor_train/common/functions/loop.hpp>

namespace pRC
{
    template<class X, class R = RemoveReference<X>,
        If<Any<TensorTrain::IsTensorish<R>, TensorTrain::IsOperatorish<R>>> = 0,
        If<IsInvocable<Loop<Plus>, X>> = 0>
    static inline constexpr auto operator+(X &&a)
    {
        return loop<Plus>(forward<X>(a));
    }
}
#endif // pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_PLUS_H
