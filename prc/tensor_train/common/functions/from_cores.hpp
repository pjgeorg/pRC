// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_FROM_CORES_H
#define pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_FROM_CORES_H

#include <prc/core/tensor/type_traits.hpp>
#include <prc/tensor_train/operator/type_traits.hpp>
#include <prc/tensor_train/tensor/type_traits.hpp>

namespace pRC::TensorTrain
{
    template<class... Xs, If<All<pRC::IsTensorish<RemoveReference<Xs>>...>> = 0>
    static inline constexpr auto fromCores(Xs &&...cores)
    {
        constexpr auto Dimension = sizeof...(Xs);
        using Type = Common<typename RemoveReference<Xs>::Type...>;

        auto ret = []()
        {
            if constexpr(Common<typename RemoveReference<Xs>::Dimension...>() ==
                3)
            {
                using N = Sizes<RemoveReference<Xs>::size(1)...>;
                using Ranks =
                    Common<decltype(chip<0>(
                               Sizes<RemoveReference<Xs>::size(0)...>())),
                        decltype(chip<Dimension - 1>(
                            Sizes<RemoveReference<Xs>::size(2)...>()))>;

                return TensorTrain::Tensor<Type, N, Ranks>();
            }
            if constexpr(Common<typename RemoveReference<Xs>::Dimension...>() ==
                4)
            {
                using M = Sizes<RemoveReference<Xs>::size(1)...>;
                using N = Sizes<RemoveReference<Xs>::size(2)...>;
                using Ranks =
                    Common<decltype(chip<0>(
                               Sizes<RemoveReference<Xs>::size(0)...>())),
                        decltype(chip<Dimension - 1>(
                            Sizes<RemoveReference<Xs>::size(3)...>()))>;

                return TensorTrain::Operator<Type, M, N, Ranks>();
            }
        }();

        expand(makeSeries<Index, Dimension>(),
            [&ret, &cores...](auto const... seq)
            {
                ((ret.template core<seq>() = forward<Xs>(cores)), ...);
            });

        return ret;
    }
}
#endif // pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_FROM_CORES_H
