// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_INNER_PRODUCT_H
#define pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_INNER_PRODUCT_H

#include <prc/core/functors/conj.hpp>
#include <prc/core/functors/inner_product.hpp>
#include <prc/core/functors/scalar_product.hpp>
#include <prc/tensor_train/common/functions/conj.hpp>
#include <prc/tensor_train/operator/functions/scalar_product.hpp>
#include <prc/tensor_train/tensor/functions/scalar_product.hpp>

namespace pRC
{
    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>,
        If<Any<All<TensorTrain::IsTensorish<RA>, TensorTrain::IsTensorish<RB>>,
            All<TensorTrain::IsOperatorish<RA>,
                TensorTrain::IsOperatorish<RB>>>> = 0,
        If<IsInvocable<Conj, XA>> = 0,
        If<IsInvocable<ScalarProduct, ResultOf<Conj, XA>, XB>> = 0>
    static inline constexpr auto innerProduct(XA &&a, XB &&b)
    {
        return scalarProduct(conj(forward<XA>(a)), forward<XB>(b));
    }
}
#endif // pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_INNER_PRODUCT_H
