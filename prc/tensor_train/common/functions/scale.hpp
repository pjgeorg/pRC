// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_SCALE_H
#define pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_SCALE_H

#include <prc/core/complex/type_traits.hpp>
#include <prc/core/functors/div.hpp>
#include <prc/core/functors/mul.hpp>
#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/type_traits.hpp>
#include <prc/core/value/functions/cast.hpp>
#include <prc/core/value/functions/rcp.hpp>
#include <prc/core/value/type_traits.hpp>
#include <prc/tensor_train/common/functions/enumerate.hpp>

namespace pRC
{
    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>,
        If<Any<TensorTrain::IsTensorish<RA>, TensorTrain::IsOperatorish<RA>>> =
            0,
        If<IsInvocable<View, XA>> = 0, If<Any<IsValue<RB>, IsComplex<RB>>> = 0,
        If<IsInvocable<Mul, typename RA::Type, RB const &>> = 0>
    static inline constexpr auto operator*(XA &&a, XB &&b)
    {
        return enumerate(
            [lambda = pow(abs(b), rcp(unit<RB>(typename RA::Dimension()))),
                sgn = sign(b)]<Index C>(auto &&a)
            {
                if constexpr(C == 0)
                {
                    return forward<decltype(a)>(a) * sgn * lambda;
                }
                else
                {
                    return forward<decltype(a)>(a) * lambda;
                }
            },
            forward<XA>(a));
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<Any<IsValue<RA>, IsComplex<RA>>> = 0,
        If<Any<TensorTrain::IsTensorish<RB>, TensorTrain::IsOperatorish<RB>>> =
            0,
        If<IsInvocable<View, XB>> = 0,
        If<IsInvocable<Mul, RA const &, typename RB::Type>> = 0>
    static inline constexpr auto operator*(XA &&a, XB &&b)
    {
        return forward<XB>(b) * forward<XA>(a);
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>,
        If<Any<TensorTrain::IsTensorish<RA>, TensorTrain::IsOperatorish<RA>>> =
            0,
        If<IsInvocable<View, XA>> = 0, If<Any<IsValue<RB>, IsComplex<RB>>> = 0,
        If<IsInvocable<Div, typename RA::Type, RB const &>> = 0>
    static inline constexpr auto operator/(XA &&a, XB &&b)
    {
        using T = ResultOf<Div, typename RA::Type, RB const &>;

        return forward<XA>(a) * rcp(cast<typename T::Value>(forward<XB>(b)));
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>,
        If<Any<TensorTrain::IsTensorish<RA>, TensorTrain::IsOperatorish<RA>>> =
            0,
        If<Not<IsInvocable<View, XA>>> = 0,
        If<Any<IsValue<RB>, IsComplex<RB>>> = 0,
        If<IsInvocable<Mul, XA &, RB const &>> = 0>
    static inline constexpr auto operator*(XA &&a, XB &&b)
    {
        return eval(a * forward<XB>(b));
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<Any<IsValue<RA>, IsComplex<RA>>> = 0,
        If<Any<TensorTrain::IsTensorish<RB>, TensorTrain::IsOperatorish<RB>>> =
            0,
        If<Not<IsInvocable<View, XB>>> = 0,
        If<IsInvocable<Mul, RA const &, XB &>> = 0>
    static inline constexpr auto operator*(XA &&a, XB &&b)
    {
        return eval(forward<XA>(a) * b);
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>,
        If<Any<TensorTrain::IsTensorish<RA>, TensorTrain::IsOperatorish<RA>>> =
            0,
        If<Not<IsInvocable<View, XA>>> = 0,
        If<Any<IsValue<RB>, IsComplex<RB>>> = 0,
        If<IsInvocable<Div, XA &, RB const &>> = 0>
    static inline constexpr auto operator/(XA &&a, XB &&b)
    {
        return eval(a / forward<XB>(b));
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>,
        If<Any<TensorTrain::IsTensorish<RA>, TensorTrain::IsOperatorish<RA>>> =
            0,
        If<IsTensorish<RB>> = 0,
        If<IsInvocable<Mul, XA, typename RB::Type>> = 0>
    static inline constexpr auto operator*(XA &&a, XB &&b)
    {
        return forward<XA>(a) * forward<XB>(b)();
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<Any<TensorTrain::IsTensorish<RB>, TensorTrain::IsOperatorish<RB>>> =
            0,
        If<IsInvocable<Mul, typename RA::Type, XB>> = 0>
    static inline constexpr auto operator*(XA &&a, XB &&b)
    {
        return forward<XA>(a)() * forward<XB>(b);
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>,
        If<Any<TensorTrain::IsTensorish<RA>, TensorTrain::IsOperatorish<RA>>> =
            0,
        If<IsTensorish<RB>> = 0,
        If<IsInvocable<Div, XA, typename RB::Type>> = 0>
    static inline constexpr auto operator/(XA &&a, XB &&b)
    {
        return forward<XA>(a) / forward<XB>(b)();
    }
}
#endif // pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_SCALE_H
