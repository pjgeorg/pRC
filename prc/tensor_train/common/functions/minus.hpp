// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_MINUS_H
#define pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_MINUS_H

#include <prc/core/functors/view.hpp>
#include <prc/tensor_train/common/functions/enumerate.hpp>

namespace pRC
{
    template<class X, class R = RemoveReference<X>,
        If<Any<TensorTrain::IsTensorish<R>, TensorTrain::IsOperatorish<R>>> = 0>
    static inline constexpr auto operator-(X &&a)
    {
        return enumerate(
            []<Index C>(auto &&a)
            {
                if constexpr(C == 0)
                {
                    return -forward<decltype(a)>(a);
                }
                else
                {
                    return forward<decltype(a)>(a);
                }
            },
            forward<X>(a));
    }
}
#endif // pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_MINUS_H
