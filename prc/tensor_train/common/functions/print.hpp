// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_PRINT_H
#define pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_PRINT_H

#include <prc/core/basic/range.hpp>
#include <prc/core/functors/eval.hpp>
#include <prc/tensor_train/operator/type_traits.hpp>
#include <prc/tensor_train/tensor/type_traits.hpp>

namespace pRC
{
    template<class X, class S, class R = RemoveReference<X>,
        If<Any<TensorTrain::IsTensorish<R>, TensorTrain::IsOperatorish<R>>> = 0>
    static inline void print(X &&arg, S &&stream)
    {
        print(name<ResultOf<Eval, R>>(), stream);
        print(":\n", stream);

        range<Context::CompileTime, typename R::Dimension{}>(
            [&arg, &stream](auto const c)
            {
                print(arg.template core<c>(), stream);
            });
    }
}
#endif // pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_PRINT_H
