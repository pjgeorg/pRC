// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_SUB_H
#define pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_SUB_H

#include <prc/core/functors/add.hpp>
#include <prc/core/functors/minus.hpp>
#include <prc/tensor_train/common/functions/minus.hpp>
#include <prc/tensor_train/operator/functions/add.hpp>
#include <prc/tensor_train/tensor/functions/add.hpp>

namespace pRC
{
    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>,
        If<Any<All<TensorTrain::IsTensorish<RA>, TensorTrain::IsTensorish<RB>>,
            All<TensorTrain::IsOperatorish<RA>,
                TensorTrain::IsOperatorish<RB>>>> = 0,
        If<IsInvocable<Minus, XB>> = 0,
        If<IsInvocable<Add, XA, ResultOf<Minus, XB>>> = 0>
    static inline constexpr auto operator-(XA &&a, XB &&b)
    {
        return forward<XA>(a) + (-forward<XB>(b));
    }
}
#endif // pRC_TENSOR_TRAIN_COMMON_FUNCTIONS_SUB_H
