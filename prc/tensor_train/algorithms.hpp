// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_ALGORITHMS_H
#define pRC_TENSOR_TRAIN_ALGORITHMS_H

#include <prc/tensor_train/algorithms/orthogonalize.hpp>
#include <prc/tensor_train/algorithms/truncate.hpp>

#endif // pRC_TENSOR_TRAIN_ALGORITHMS_H
