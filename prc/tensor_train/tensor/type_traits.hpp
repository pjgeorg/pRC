// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_TENSOR_TYPE_TRAITS_H
#define pRC_TENSOR_TRAIN_TENSOR_TYPE_TRAITS_H

#include <prc/core/basic/common.hpp>
#include <prc/core/basic/sequence.hpp>
#include <prc/core/basic/type_traits.hpp>
#include <prc/core/container/type_traits.hpp>

namespace pRC::TensorTrain
{
    template<class T, class N,
        class R = decltype(Sizes(
            makeConstantSequence<Index, typename N::Dimension() - 1, 1>())),
        class = If<>>
    class Tensor;

    template<class>
    struct IsTensor : False<>
    {
    };

    template<class T>
    struct IsTensor<T const> : IsTensor<T>
    {
    };

    template<class T, class N, class R>
    struct IsTensor<Tensor<T, N, R>> : True<>
    {
    };

    namespace TensorViews
    {
        template<class T, class N, class R, class F>
        class View;

        template<class T, class N, class R, class F>
        class Assignable;
    }

    template<class X, class T, class N, class R, class F>
    static inline constexpr auto isTensorView(
        TensorViews::View<T, N, R, F> const &&)
    {
        if constexpr(IsReference<X>{})
        {
            return False<>();
        }
        else
        {
            return True<>();
        }
    }

    template<class>
    static inline constexpr auto isTensorView(...)
    {
        return False<>();
    }

    template<class T>
    struct IsTensorView
        : decltype(pRC::TensorTrain::isTensorView<T>(declval<T>()))
    {
    };

    template<class T>
    using IsTensorish = Any<IsTensor<T>, IsTensorView<T>>;
}
#endif // pRC_TENSOR_TRAIN_TENSOR_TYPE_TRAITS_H
