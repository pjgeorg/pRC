// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_TENSOR_RANDOM_H
#define pRC_TENSOR_TRAIN_TENSOR_RANDOM_H

#include <prc/core/basic/random.hpp>
#include <prc/tensor_train/tensor/views/random.hpp>

namespace pRC
{
    template<class T, class N, class Ranks, template<class...> class D>
    struct Random<TensorTrain::Tensor<T, N, Ranks>, D<typename T::Value>,
        If<IsDistribution<D<typename T::Value>>>>
    {
    public:
        Random(RandomEngine &rng, D<typename T::Value> &distribution)
            : mRNG(rng)
            , mDistribution(distribution)
        {
        }

        constexpr auto operator()()
        {
            return TensorTrain::TensorViews::Random<T, N, Ranks, D>(
                mRNG, mDistribution);
        }

    private:
        RandomEngine &mRNG;
        D<typename T::Value> &mDistribution;
    };
}
#endif // pRC_TENSOR_TRAIN_TENSOR_RANDOM_H
