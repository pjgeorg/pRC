// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_TENSOR_ZERO_H
#define pRC_TENSOR_TRAIN_TENSOR_ZERO_H

#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/functors/eval.hpp>
#include <prc/tensor_train/tensor/views/enumerate.hpp>

namespace pRC
{
    template<class T>
    struct Zero<T, If<TensorTrain::IsTensorView<T>>> : Zero<ResultOf<Eval, T>>
    {
    };

    template<class T>
    struct Zero<T, If<TensorTrain::IsTensor<T>>>
    {
        constexpr auto operator()()
        {
            auto const f = []<Index C>()
            {
                return zero<typename T::template Cores<C>>();
            };

            using F = RemoveConstReference<decltype(f)>;
            using N = typename T::N;
            using Ranks = typename T::Ranks;

            return TensorTrain::TensorViews::Enumerate<typename T::Type, N,
                Ranks, F>(f);
        }
    };
}
#endif // pRC_TENSOR_TRAIN_TENSOR_ZERO_H
