// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_TENSOR_FUNCTIONS_SCALAR_PRODUCT_H
#define pRC_TENSOR_TRAIN_TENSOR_FUNCTIONS_SCALAR_PRODUCT_H

#include <prc/core/functors/hadamard_product.hpp>
#include <prc/core/tensor/functions/chip.hpp>
#include <prc/core/tensor/functions/contract.hpp>
#include <prc/core/tensor/functions/reshape.hpp>
#include <prc/tensor_train/tensor/type_traits.hpp>

namespace pRC
{
    template<class XA, class XB,
        If<TensorTrain::IsTensorish<RemoveReference<XA>>> = 0,
        If<TensorTrain::IsTensorish<RemoveReference<XB>>> = 0,
        If<IsInvocable<HadamardProduct, XA, XB>> = 0>
    static inline constexpr auto scalarProduct(XA &&a, XB &&b)
    {
        using T = Common<typename RemoveReference<XA>::Value,
            typename RemoveReference<XB>::Value>;
        typename T::NonComplex lambda = zero();

        return expand(
            makeSeries<Index, typename RemoveReference<XA>::Dimension{}>(),
            [&a, &b, &lambda](auto const... seq)
            {
                return RecursiveLambda(
                    [&lambda](
                        auto const &self, auto &&p, auto &&t, auto &&...args)
                    {
                        auto const f = [&p, &t, &lambda]()
                        {
                            auto s = contract<0, 1, 0, 1>(
                                eval(contract<0, 0>(forward<decltype(p)>(p),
                                    get<0>(forward<decltype(t)>(t)))),
                                get<1>(forward<decltype(t)>(t)));

                            auto const sNorm = norm(s)();
                            s = s / sNorm;
                            lambda += log(sNorm);

                            return s;
                        };

                        if constexpr(sizeof...(args) == 0)
                        {
                            return exp(lambda) * reshape<>(f());
                        }
                        else
                        {
                            return self(f(), forward<decltype(args)>(args)...);
                        }
                    })(identity<Tensor<T, 1, 1>>(),
                    forwardAsTuple(forward<XA>(a).template core<seq>(),
                        forward<XB>(b).template core<seq>())...);
            });
    }
}
#endif // pRC_TENSOR_TRAIN_TENSOR_FUNCTIONS_SCALAR_PRODUCT_H
