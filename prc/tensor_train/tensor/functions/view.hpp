// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_TENSOR_FUNCTIONS_VIEW_H
#define pRC_TENSOR_TRAIN_TENSOR_FUNCTIONS_VIEW_H

#include <prc/tensor_train/tensor/views/reference.hpp>
#include <prc/tensor_train/tensor/views/reference_to_const.hpp>

namespace pRC
{
    template<class X, If<TensorTrain::IsTensorView<RemoveReference<X>>> = 0>
    static inline constexpr X view(X &&a)
    {
        return forward<X>(a);
    }

    template<class T, class N, class R>
    static inline constexpr auto view(TensorTrain::Tensor<T, N, R> const &a)
    {
        return TensorTrain::TensorViews::ReferenceToConst(a);
    }

    template<class T, class N, class R>
    static inline constexpr auto view(TensorTrain::Tensor<T, N, R> &a)
    {
        return TensorTrain::TensorViews::Reference(a);
    }

    template<class T, class N, class R>
    static inline constexpr auto view(
        TensorTrain::Tensor<T, N, R> const &&) = delete;
}
#endif // pRC_TENSOR_TRAIN_TENSOR_FUNCTIONS_VIEW_H
