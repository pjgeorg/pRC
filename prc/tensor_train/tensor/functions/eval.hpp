// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_TENSOR_FUNCTIONS_EVAL_H
#define pRC_TENSOR_TRAIN_TENSOR_FUNCTIONS_EVAL_H

#include <prc/tensor_train/tensor/type_traits.hpp>

namespace pRC
{
    template<class X, If<TensorTrain::IsTensor<RemoveReference<X>>> = 0>
    static inline constexpr X eval(X &&a)
    {
        return forward<X>(a);
    }

    template<class X, If<TensorTrain::IsTensorView<RemoveReference<X>>> = 0>
    static inline constexpr auto eval(X &&a)
    {
        return TensorTrain::Tensor(forward<X>(a));
    }
}
#endif // pRC_TENSOR_TRAIN_TENSOR_FUNCTIONS_EVAL_H
