// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_TENSOR_FUNCTIONS_HADAMARD_PRODUCT_H
#define pRC_TENSOR_TRAIN_TENSOR_FUNCTIONS_HADAMARD_PRODUCT_H

#include <prc/core/functors/exclude.hpp>
#include <prc/core/functors/kronecker_product.hpp>
#include <prc/core/functors/loop.hpp>
#include <prc/core/tensor/functions/permute.hpp>
#include <prc/tensor_train/common/functions/loop.hpp>

namespace pRC
{
    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<TensorTrain::IsTensorish<RA>> = 0,
        If<TensorTrain::IsTensorish<RB>> = 0,
        If<IsSame<typename RA::Sizes, typename RB::Sizes>> = 0,
        If<IsInvocable<Mul, typename RA::Type, typename RB::Type>> = 0>
    static inline constexpr auto hadamardProduct(XA &&a, XB &&b)
    {
        return loop(
            [](auto &&...args)
            {
                return permute<1, 0, 2>(exclude<KroneckerProduct, 1>(
                    forward<decltype(args)>(args)...));
            },
            forward<XA>(a), forward<XB>(b));
    }
}
#endif // pRC_TENSOR_TRAIN_TENSOR_FUNCTIONS_HADAMARD_PRODUCT_H
