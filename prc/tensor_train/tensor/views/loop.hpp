// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_TENSOR_VIEWS_LOOP_H
#define pRC_TENSOR_TRAIN_TENSOR_VIEWS_LOOP_H

#include <prc/core/basic/sequence.hpp>
#include <prc/tensor_train/operator/type_traits.hpp>
#include <prc/tensor_train/tensor/type_traits.hpp>

namespace pRC::TensorTrain::TensorViews
{
    template<class T, class N, class Ranks, class F, class... Vs>
    class Loop
        : public Conditional<
              IsAssignable<
                  ResultOf<F, decltype(declval<Vs>().template core<0>())...>>,
              Assignable<T, N, Ranks, Loop<T, N, Ranks, F, Vs...>>,
              View<T, N, Ranks, Loop<T, N, Ranks, F, Vs...>>>
    {
        static_assert(All<Any<IsTensorView<Vs>, IsOperatorView<Vs>>...>());

    private:
        using Base =
            Conditional<IsAssignable<ResultOf<F,
                            decltype(declval<Vs>().template core<0>())...>>,
                Assignable<T, N, Ranks, Loop>, View<T, N, Ranks, Loop>>;

    public:
        template<class... Xs, If<All<IsSame<Vs, RemoveReference<Xs>>...>> = 0>
        Loop(F f, Xs &&...args)
            : mF(forward<F>(f))
            , mArgs(forward<Xs>(args)...)
        {
        }

        using Base::operator=;

        template<Index C>
        constexpr decltype(auto) core()
        {
            return expand(makeSeriesFor<Index, Vs...>(),
                [this](auto const... ops) -> decltype(auto)
                {
                    return mF(get<ops>(mArgs).template core<C>()...);
                });
        }

        template<Index C>
        constexpr decltype(auto) core() const
        {
            return expand(makeSeriesFor<Index, Vs...>(),
                [this](auto const... ops) -> decltype(auto)
                {
                    return mF(get<ops>(mArgs).template core<C>()...);
                });
        }

    private:
        F mF;
        tuple<Vs...> mArgs;
    };
}
#endif // pRC_TENSOR_TRAIN_TENSOR_VIEWS_LOOP_H
