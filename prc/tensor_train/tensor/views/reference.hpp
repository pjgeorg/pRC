// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_TENSOR_VIEWS_REFERENCE_H
#define pRC_TENSOR_TRAIN_TENSOR_VIEWS_REFERENCE_H

#include <prc/tensor_train/tensor/type_traits.hpp>

namespace pRC::TensorTrain::TensorViews
{
    template<class T, class N, class Ranks>
    class Reference;

    template<class T, class N, class Ranks>
    Reference(Tensor<T, N, Ranks> &) -> Reference<T, N, Ranks>;

    template<class T, class N, class Ranks>
    class Reference : public Assignable<T, N, Ranks, Reference<T, N, Ranks>>
    {
    private:
        using Base = Assignable<T, N, Ranks, Reference>;

    public:
        Reference(Tensor<T, N, Ranks> &a)
            : mA(a)
        {
        }

        using Base::operator=;

        template<Index C>
        constexpr decltype(auto) core()
        {
            return mA.template core<C>();
        }

        template<Index C>
        constexpr decltype(auto) core() const
        {
            return asConst(mA).template core<C>();
        }

    private:
        Tensor<T, N, Ranks> &mA;
    };
}
#endif // pRC_TENSOR_TRAIN_TENSOR_VIEWS_REFERENCE_H
