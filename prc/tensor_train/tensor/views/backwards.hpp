// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_TENSOR_VIEWS_BACKWARDS_H
#define pRC_TENSOR_TRAIN_TENSOR_VIEWS_BACKWARDS_H

#include <prc/core/basic/sequence.hpp>
#include <prc/tensor_train/operator/type_traits.hpp>
#include <prc/tensor_train/tensor/type_traits.hpp>

namespace pRC::TensorTrain::TensorViews
{
    template<class T, class N, class Ranks, class V>
    class Backwards
        : public Conditional<IsAssignable<V>,
              Assignable<T, N, Ranks, Backwards<T, N, Ranks, V>>,
              View<T, N, Ranks, Backwards<T, N, Ranks, V>>>
    {
        static_assert(IsTensorView<V>());

    private:
        using Base = Conditional<IsAssignable<V>,
            Assignable<T, N, Ranks, Backwards>, View<T, N, Ranks, Backwards>>;

    public:
        template<class X, If<IsSame<V, RemoveReference<X>>> = 0>
        Backwards(X &&a)
            : mA(forward<X>(a))
        {
        }

        using Base::operator=;

        template<Index C>
        constexpr decltype(auto) core()
        {
            constexpr auto B = typename Base::Dimension() - C - 1;

            return permute<2, 1, 0>(mA.template core<B>());
        }

        template<Index C>
        constexpr decltype(auto) core() const
        {
            constexpr auto B = typename Base::Dimension() - C - 1;

            return permute<2, 1, 0>(mA.template core<B>());
        }

    private:
        V mA;
    };
}
#endif // pRC_TENSOR_TRAIN_TENSOR_VIEWS_BACKWARDS_H
