// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_TENSOR_VIEWS_VIEW_H
#define pRC_TENSOR_TRAIN_TENSOR_VIEWS_VIEW_H

#include <prc/core/basic/crtp.hpp>
#include <prc/core/basic/limits.hpp>
#include <prc/core/basic/range.hpp>
#include <prc/core/basic/sequence.hpp>
#include <prc/core/complex/type_traits.hpp>
#include <prc/core/functors/mul.hpp>
#include <prc/core/tensor/functions/chip.hpp>
#include <prc/core/tensor/type_traits.hpp>
#include <prc/core/value/functions/fold.hpp>
#include <prc/core/value/type_traits.hpp>
#include <prc/tensor_train/tensor/type_traits.hpp>

namespace pRC::TensorTrain::TensorViews
{
    template<class T, Size... Ns, Size... Rs, class F>
    class View<T, Sizes<Ns...>, Sizes<Rs...>, F> : public CRTP<F>
    {
        static_assert(sizeof...(Ns) - 1 == sizeof...(Rs));
        static_assert(IsValue<T>() || IsComplex<T>(),
            "Tensor<T, Sizes<Ns...>>: T has to be of type Value or Complex.");

    public:
        using N = pRC::Sizes<Ns...>;
        using L = N;
        using Sizes = N;

        using SubscriptsN = pRC::Subscripts<Ns...>;
        using SubscriptsL = SubscriptsN;
        using Subscripts = SubscriptsN;

        using Dimension = typename N::Dimension;

        using Ranks = pRC::Sizes<Rs...>;
        template<class S,
            If<IsSame<typename S::Dimension, typename Ranks::Dimension>> = 0>
        using ChangeRanks = Tensor<T, N, S>;

        template<Index C>
        using Cores = pRC::Tensor<T, pRC::Sizes<1, Rs..., 1>::size(C),
            N::size(C), pRC::Sizes<1, Rs..., 1>::size(C + 1)>;

        using Type = T;
        template<class C>
        using ChangeType = Tensor<C, N, Ranks>;

        using Value = typename T::Value;
        template<class V, If<IsValue<V>> = 0>
        using ChangeValue =
            Tensor<typename T::template ChangeValue<V>, N, Ranks>;

        using Signed = typename T::Signed;
        template<Bool R>
        using ChangeSigned =
            Tensor<typename T::template ChangeSigned<R>, N, Ranks>;

        using Width = typename T::Width;
        template<Size Q>
        using ChangeWidth =
            Tensor<typename T::template ChangeWidth<Q>, N, Ranks>;

        using IsComplexified = typename T::IsComplexified;
        using Complexify = Tensor<typename T::Complexify, N, Ranks>;
        using NonComplex = Tensor<typename T::NonComplex, N, Ranks>;

        template<class E = typename N::IsLinearizable, If<E> = 0>
        static constexpr auto n()
        {
            return N::size();
        }

        static constexpr auto n(Index const dimension)
        {
            return N::size(dimension);
        }

        template<class E = typename Sizes::IsLinearizable, If<E> = 0>
        static constexpr auto size()
        {
            return Sizes::size();
        }

        static constexpr auto size(Index const dimension)
        {
            return Sizes::size(dimension);
        }

        template<class X, class... Is, If<IsConstructible<T, X>> = 0,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0>
        static inline constexpr auto Single(X &&value, Is const... indices)
        {
            return Tensor<T, N, Ranks>::Single(forward<X>(value), indices...);
        }

        template<class X, If<IsConstructible<T, X>> = 0>
        static inline constexpr auto Single(
            X &&value, Subscripts const &subscripts)
        {
            return Tensor<T, N, Ranks>::Single(forward<X>(value), subscripts);
        }

    public:
        template<Index C>
        constexpr decltype(auto) core()
        {
            return this->self().template core<C>();
        }

        template<Index C>
        constexpr decltype(auto) core() const
        {
            return this->self().template core<C>();
        }

        template<class... Is,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0>
        constexpr auto operator()(Is const... indices)
        {
            return expand(makeSeries<Index, Dimension{}>(),
                [this, &indices...](auto const... seq)
                {
                    return fold<Mul, true, Direction::Backwards>(
                        chip<1>(this->template core<seq>(), indices)...)(0, 0);
                });
        }

        template<class... Is,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0>
        constexpr auto operator()(Is const... indices) const
        {
            return expand(makeSeries<Index, Dimension{}>(),
                [this, &indices...](auto const... seq)
                {
                    return fold<Mul, true, Direction::Backwards>(
                        chip<1>(this->template core<seq>(), indices)...)(0, 0);
                });
        }

        constexpr decltype(auto) operator()(Subscripts const &subscripts)
        {
            return expand(makeSeries<Index, Dimension{}>(),
                [this, &subscripts](auto const... seq) -> decltype(auto)
                {
                    return (*this)(subscripts[seq]...);
                });
        }

        constexpr decltype(auto) operator()(Subscripts const &subscripts) const
        {
            return expand(makeSeries<Index, Dimension{}>(),
                [this, &subscripts](auto const... seq) -> decltype(auto)
                {
                    return (*this)(subscripts[seq]...);
                });
        }

        template<class E =
                     IsSatisfied<(Ns * ... * 1) <= NumericLimits<Size>::max() &&
                         typename Sizes::IsLinearizable()>,
            If<E> = 0>
        explicit constexpr operator pRC::Tensor<T, Ns...>()
        {
            pRC::Tensor<T, Ns...> full;

            range<Sizes>(
                [this, &full](auto const... indices)
                {
                    full(indices...) = (*this)(indices...);
                });

            return full;
        }

        template<class E =
                     IsSatisfied<(Ns * ... * 1) <= NumericLimits<Size>::max() &&
                         typename Sizes::IsLinearizable()>,
            If<E> = 0>
        explicit constexpr operator pRC::Tensor<T, Ns...>() const
        {
            pRC::Tensor<T, Ns...> full;

            range<Sizes>(
                [this, &full](auto const... indices)
                {
                    full(indices...) = (*this)(indices...);
                });

            return full;
        }

    protected:
        ~View() = default;
        constexpr View(View const &) = default;
        constexpr View(View &&) = default;
        constexpr View &operator=(View const &) = delete;
        constexpr View &operator=(View &&) = delete;
        constexpr View()
        {
            range<Context::CompileTime, Dimension{}>(
                [](auto const i)
                {
                    static_assert(
                        True<decltype(declval<F>().template core<i>())>());
                    static_assert(
                        IsConvertible<decltype(declval<F>().template core<i>()),
                            Cores<i>>());
                });
            static_assert(IsBaseOf<View, F>());
        }
    };
}

namespace pRC
{
    template<class T, Size... Ns, class R, class F>
    Tensor(TensorTrain::TensorViews::View<T, Sizes<Ns...>, R, F> const &)
        -> Tensor<T, Ns...>;
}
#endif // pRC_TENSOR_TRAIN_TENSOR_VIEWS_VIEW_H
