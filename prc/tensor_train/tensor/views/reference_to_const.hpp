// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_TENSOR_VIEWS_REFERENCE_TO_CONST_H
#define pRC_TENSOR_TRAIN_TENSOR_VIEWS_REFERENCE_TO_CONST_H

#include <prc/tensor_train/tensor/type_traits.hpp>

namespace pRC::TensorTrain::TensorViews
{
    template<class T, class N, class Ranks>
    class ReferenceToConst;

    template<class T, class N, class Ranks>
    ReferenceToConst(Tensor<T, N, Ranks> const &)
        -> ReferenceToConst<T, N, Ranks>;

    template<class T, class N, class Ranks>
    class ReferenceToConst
        : public View<T, N, Ranks, ReferenceToConst<T, N, Ranks>>
    {
    private:
        using Base = View<T, N, Ranks, ReferenceToConst>;

    public:
        ReferenceToConst(Tensor<T, N, Ranks> const &a)
            : mA(a)
        {
        }

        ReferenceToConst(Tensor<T, N, Ranks> const &&) = delete;

        template<Index C>
        constexpr decltype(auto) core() const
        {
            return mA.template core<C>();
        }

    private:
        Tensor<T, N, Ranks> const &mA;
    };
}
#endif // pRC_TENSOR_TRAIN_TENSOR_VIEWS_REFERENCE_TO_CONST_H
