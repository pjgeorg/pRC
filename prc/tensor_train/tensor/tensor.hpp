// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_TENSOR_TENSOR_H
#define pRC_TENSOR_TRAIN_TENSOR_TENSOR_H

#include <prc/core/basic/limits.hpp>
#include <prc/core/basic/range.hpp>
#include <prc/core/basic/sequence.hpp>
#include <prc/core/complex/type_traits.hpp>
#include <prc/core/functors/mul.hpp>
#include <prc/core/tensor/type_traits.hpp>
#include <prc/core/value/type_traits.hpp>
#include <prc/tensor_train/tensor/type_traits.hpp>
#include <prc/tensor_train/tensor/views/enumerate.hpp>
#include <prc/tensor_train/tensor/views/reference.hpp>

namespace pRC::TensorTrain
{
    template<class T, class N, class Ranks, class F>
    Tensor(TensorViews::View<T, N, Ranks, F> const &) -> Tensor<T, N, Ranks>;

    template<class T, Size... Ns, Size... Rs>
    class Tensor<T, Sizes<Ns...>, Sizes<Rs...>,
        If<All<IsSatisfied<(sizeof...(Ns) - 1 == sizeof...(Rs))>,
            Any<IsValue<T>, IsComplex<T>>>>>
    {
    public:
        using N = pRC::Sizes<Ns...>;
        using L = N;
        using Sizes = N;

        using SubscriptsN = pRC::Subscripts<Ns...>;
        using SubscriptsL = SubscriptsN;
        using Subscripts = SubscriptsN;

        using Dimension = typename N::Dimension;

        using Ranks = pRC::Sizes<Rs...>;
        template<class S,
            If<IsSame<typename S::Dimension, typename Ranks::Dimension>> = 0>
        using ChangeRanks = Tensor<T, N, S>;

        template<Index C>
        using Cores = pRC::Tensor<T, pRC::Sizes<1, Rs..., 1>::size(C),
            N::size(C), pRC::Sizes<1, Rs..., 1>::size(C + 1)>;

        using Type = T;
        template<class C>
        using ChangeType = Tensor<C, N, Ranks>;

        using Value = typename T::Value;
        template<class V, If<IsValue<V>> = 0>
        using ChangeValue =
            Tensor<typename T::template ChangeValue<V>, N, Ranks>;

        using Signed = typename T::Signed;
        template<Bool R>
        using ChangeSigned =
            Tensor<typename T::template ChangeSigned<R>, N, Ranks>;

        using Width = typename T::Width;
        template<Size Q>
        using ChangeWidth =
            Tensor<typename T::template ChangeWidth<Q>, N, Ranks>;

        using IsComplexified = typename T::IsComplexified;
        using Complexify = Tensor<typename T::Complexify, N, Ranks>;
        using NonComplex = Tensor<typename T::NonComplex, N, Ranks>;

        template<class E = typename N::IsLinearizable, If<E> = 0>
        static constexpr auto n()
        {
            return N::size();
        }

        static constexpr auto n(Index const dimension)
        {
            return N::size(dimension);
        }

        template<class E = typename Sizes::IsLinearizable, If<E> = 0>
        static constexpr auto size()
        {
            return Sizes::size();
        }

        static constexpr auto size(Index const dimension)
        {
            return Sizes::size(dimension);
        }

        template<class X, class... Is, If<IsConstructible<T, X>> = 0,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0>
        static inline constexpr auto Single(X &&value, Is const... indices)
        {
            return Single(forward<X>(value), Subscripts(indices...));
        }

        template<class X, If<IsConstructible<T, X>> = 0>
        static inline constexpr auto Single(
            X &&value, Subscripts const &subscripts)
        {
            auto const f = [subscripts, value = T(forward<X>(value))]<Index C>()
            {
                if constexpr(C == 0)
                {
                    return Cores<C>::Single(value, 0, subscripts[C], 0);
                }
                else
                {
                    return Cores<C>::Single(identity<T>(), 0, subscripts[C], 0);
                }
            };
            using F = RemoveConstReference<decltype(f)>;
            return TensorTrain::TensorViews::Enumerate<T, N, Ranks, F>(f);
        }

    public:
        ~Tensor() = default;
        constexpr Tensor(Tensor const &) = default;
        constexpr Tensor(Tensor &&) = default;
        constexpr Tensor &operator=(Tensor const &) & = default;
        constexpr Tensor &operator=(Tensor &&) & = default;
        constexpr Tensor() = default;

        template<class X,
            If<IsAssignable<TensorViews::Reference<T, N, Ranks>, X>> = 0>
        constexpr Tensor(X &&other)
        {
            *this = forward<X>(other);
        }

        template<class X,
            If<IsAssignable<TensorViews::Reference<T, N, Ranks>, X>> = 0>
        constexpr auto &operator=(X &&rhs) &
        {
            view(*this) = forward<X>(rhs);
            return *this;
        }

        template<Index C>
        constexpr decltype(auto) core() &&
        {
            return get<C>(move(mCores));
        }

        template<Index C>
        constexpr decltype(auto) core() const &&
        {
            return get<C>(move(mCores));
        }

        template<Index C>
        constexpr decltype(auto) core() &
        {
            return get<C>(mCores);
        }

        template<Index C>
        constexpr decltype(auto) core() const &
        {
            return get<C>(mCores);
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            return view(*this)(indices...);
        }

        constexpr decltype(auto) operator()(Subscripts const &subscripts) const
        {
            return view(*this)(subscripts);
        }

        template<class X, If<IsInvocable<Add, Tensor &, X>> = 0>
        constexpr auto &operator+=(X &&rhs) &
        {
            return *this = *this + forward<X>(rhs);
        }

        template<class X, If<IsInvocable<Sub, Tensor &, X>> = 0>
        constexpr auto &operator-=(X &&rhs) &
        {
            return *this = *this - forward<X>(rhs);
        }

        template<class X, If<IsInvocable<Mul, X, Tensor &>> = 0>
        constexpr auto &applyOnTheLeft(X &&lhs) &
        {
            view(*this).applyOnTheLeft(forward<X>(lhs));
            return *this;
        }

        template<class X, If<IsInvocable<Mul, Tensor &, X>> = 0>
        constexpr auto &applyOnTheRight(X &&rhs) &
        {
            view(*this).applyOnTheRight(forward<X>(rhs));
            return *this;
        }

        template<class X, If<IsInvocable<Mul, Tensor &, X>> = 0>
        constexpr auto &operator*=(X &&rhs) &
        {
            view(*this) *= forward<X>(rhs);
            return *this;
        }

        template<class X, If<IsInvocable<Div, Tensor &, X>> = 0>
        constexpr auto &operator/=(X &&rhs) &
        {
            return *this = *this / forward<X>(rhs);
        }

        template<class E =
                     IsSatisfied<(Ns * ... * 1) <= NumericLimits<Size>::max() &&
                         typename Sizes::IsLinearizable()>,
            If<E> = 0>
        explicit constexpr operator pRC::Tensor<T, Ns...>() const
        {
            return pRC::Tensor(view(*this));
        }

    private:
        template<Index... seq>
        static constexpr auto coreTypes(Sequence<Index, seq...>)
        {
            return tuple<Cores<seq>...>{};
        }

        using CoreTypes = decltype(coreTypes(makeSeries<Index, Dimension{}>()));

    private:
        CoreTypes mCores;
    };
}

namespace pRC
{
    template<class T, Size... Ns, class R>
    Tensor(TensorTrain::Tensor<T, Sizes<Ns...>, R> const &) -> Tensor<T, Ns...>;
}
#endif // pRC_TENSOR_TRAIN_TENSOR_TENSOR_H
