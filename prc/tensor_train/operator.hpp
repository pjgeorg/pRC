// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_OPERATOR_H
#define pRC_TENSOR_TRAIN_OPERATOR_H

#include <prc/tensor_train/operator/functions/add.hpp>
#include <prc/tensor_train/operator/functions/adjoint.hpp>
#include <prc/tensor_train/operator/functions/apply.hpp>
#include <prc/tensor_train/operator/functions/backwards.hpp>
#include <prc/tensor_train/operator/functions/eval.hpp>
#include <prc/tensor_train/operator/functions/hadamard_product.hpp>
#include <prc/tensor_train/operator/functions/mul.hpp>
#include <prc/tensor_train/operator/functions/restrict.hpp>
#include <prc/tensor_train/operator/functions/scalar_product.hpp>
#include <prc/tensor_train/operator/functions/transform.hpp>
#include <prc/tensor_train/operator/functions/transpose.hpp>
#include <prc/tensor_train/operator/functions/view.hpp>
#include <prc/tensor_train/operator/identity.hpp>
#include <prc/tensor_train/operator/operator.hpp>
#include <prc/tensor_train/operator/random.hpp>
#include <prc/tensor_train/operator/type_traits.hpp>
#include <prc/tensor_train/operator/unit.hpp>
#include <prc/tensor_train/operator/views/assignable.hpp>
#include <prc/tensor_train/operator/views/backwards.hpp>
#include <prc/tensor_train/operator/views/enumerate.hpp>
#include <prc/tensor_train/operator/views/loop.hpp>
#include <prc/tensor_train/operator/views/random.hpp>
#include <prc/tensor_train/operator/views/reference.hpp>
#include <prc/tensor_train/operator/views/reference_to_const.hpp>
#include <prc/tensor_train/operator/views/view.hpp>
#include <prc/tensor_train/operator/zero.hpp>

#endif // pRC_TENSOR_TRAIN_OPERATOR_H
