// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_ALGORITHMS_H
#define pRC_ALGORITHMS_H

#include <prc/config.hpp>
#include <prc/core.hpp>
#include <prc/std.hpp>

#include <prc/algorithms/cholesky.hpp>
#include <prc/algorithms/jacobi_rotation.hpp>
#include <prc/algorithms/lq.hpp>
#include <prc/algorithms/optimize.hpp>
#include <prc/algorithms/optimizer/bfgs.hpp>
#include <prc/algorithms/optimizer/lbfgs.hpp>
#include <prc/algorithms/optimizer/lbfgsb.hpp>
#include <prc/algorithms/optimizer/line_search/bracketing.hpp>
#include <prc/algorithms/optimizer/line_search/more_thuente.hpp>
#include <prc/algorithms/qr.hpp>
#include <prc/algorithms/solve.hpp>
#include <prc/algorithms/solver/backward_substitution.hpp>
#include <prc/algorithms/solver/forward_substitution.hpp>
#include <prc/algorithms/solver/gmres.hpp>
#include <prc/algorithms/sort.hpp>
#include <prc/algorithms/svd.hpp>

#endif // pRC_ALGORITHMS_H
