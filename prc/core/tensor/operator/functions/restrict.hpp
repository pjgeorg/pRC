// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_RESTRICT_H
#define pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_RESTRICT_H

#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/operator/restrict.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC
{
    template<Operator::Restrict OR = Operator::Restrict::None, class X,
        class R = RemoveReference<X>, If<IsTensorish<R>> = 0>
    static inline constexpr decltype(auto) restrict(X &&a)
    {
        if constexpr(OR == Operator::Restrict::Diagonal)
        {
            return diagonal(forward<X>(a));
        }
        else if constexpr(OR == Operator::Restrict::LowerTriangular)
        {
            return lowerTriangular(forward<X>(a));
        }
        else if constexpr(OR == Operator::Restrict::StrictlyLowerTriangular)
        {
            return strictlyLowerTriangular(forward<X>(a));
        }
        else if constexpr(OR == Operator::Restrict::UpperTriangular)
        {
            return upperTriangular(forward<X>(a));
        }
        else if constexpr(OR == Operator::Restrict::StrictlyUpperTriangular)
        {
            return strictlyUpperTriangular(forward<X>(a));
        }
        else if constexpr(OR == Operator::Restrict::OffDiagonal)
        {
            return offDiagonal(forward<X>(a));
        }
        else if constexpr(OR == Operator::Restrict::None)
        {
            if constexpr(IsTensor<R>() && !IsInvocable<View, X>())
            {
                return eval(forward<X>(a));
            }
            else
            {
                return view(forward<X>(a));
            }
        }
        else
        {
            static_assert(OR != OR, "Unsupported operator restriction.");
        }
    }
}
#endif // pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_RESTRICT_H
