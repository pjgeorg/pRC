// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_PRINT_H
#define pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_PRINT_H

#include <prc/core/tensor/operator/hint.hpp>
#include <prc/core/tensor/operator/restrict.hpp>
#include <prc/core/tensor/operator/transform.hpp>

namespace pRC::Operator
{
    template<class S>
    static inline auto print(Hint const dir, S &&stream)
    {
        switch(dir)
        {
            case Hint::None:
                print("pRC::Operator::Hint::None", stream);
                break;
            case Hint::Symmetric:
                print("pRC::Operator::Hint::Symmetric", stream);
                break;
            case Hint::SelfAdjoint:
                print("pRC::Operator::Hint::SelfAdjoint", stream);
                break;
            case Hint::Diagonal:
                print("pRC::Operator::Hint::Diagonal", stream);
                break;
            case Hint::LowerTriangular:
                print("pRC::Operator::Hint::LowerTriangular", stream);
                break;
            case Hint::StrictlyLowerTriangular:
                print("pRC::Operator::Hint::StrictlyLowerTriangular", stream);
                break;
            case Hint::UnitLowerTriangular:
                print("pRC::Operator::Hint::UnitLowerTriangular", stream);
                break;
            case Hint::UpperTriangular:
                print("pRC::Operator::Hint::UpperTriangular", stream);
                break;
            case Hint::StrictlyUpperTriangular:
                print("pRC::Operator::Hint::StrictlyUpperTriangular", stream);
                break;
            case Hint::UnitUpperTriangular:
                print("pRC::Operator::Hint::UnitUpperTriangular", stream);
                break;
            case Hint::OffDiagonal:
                print("pRC::Operator::Hint::OffDiagonal", stream);
                break;
            default:
                print("pRC::Operator::Hint::Unknown", stream);
                break;
        }
    }

    template<class S>
    static inline auto print(Operator::Restrict const dir, S &&stream)
    {
        switch(dir)
        {
            case Restrict::None:
                print("pRC::Operator::Restrict::None", stream);
                break;
            case Restrict::Diagonal:
                print("pRC::Operator::Restrict::Diagonal", stream);
                break;
            case Restrict::LowerTriangular:
                print("pRC::Operator::Restrict::LowerTriangular", stream);
                break;
            case Restrict::StrictlyLowerTriangular:
                print(
                    "pRC::Operator::Restrict::StrictlyLowerTriangular", stream);
                break;
            case Restrict::UpperTriangular:
                print("pRC::Operator::Restrict::UpperTriangular", stream);
                break;
            case Restrict::StrictlyUpperTriangular:
                print(
                    "pRC::Operator::Restrict::StrictlyUpperTriangular", stream);
                break;
            case Restrict::OffDiagonal:
                print("pRC::Operator::Restrict::OffDiagonal", stream);
                break;
            default:
                print("pRC::Operator::Restrict::Unknown", stream);
                break;
        }
    }

    template<class S>
    static inline auto print(Transform const dir, S &&stream)
    {
        switch(dir)
        {
            case Transform::None:
                print("pRC::Operator::Transform::None", stream);
                break;
            case Transform::Transpose:
                print("pRC::Operator::Transform::Transpose", stream);
                break;
            case Transform::Adjoint:
                print("pRC::Operator::Transform::Adjoint", stream);
                break;
            default:
                print("pRC::Operator::Transform::Unknown", stream);
                break;
        }
    }
}
#endif // pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_PRINT_H
