// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_FROM_DIAGONAL_H
#define pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_FROM_DIAGONAL_H

#include <prc/core/basic/functions/is_even.hpp>
#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/operator/views/from_diagonal.hpp>

namespace pRC
{
    template<Size... Ds>
    struct FromDiagonal;

    template<Size... Ds, class X, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<IsInvocable<View, X>> = 0,
        If<IsSatisfied<(isEven(sizeof...(Ds)))>> = 0,
        If<IsSatisfied<(sizeof...(Ds) / 2 == typename R::Dimension())>> = 0,
        If<IsSatisfied<(cut<2, 0>(Sizes<Ds...>()) >= typename R::Sizes())>> = 0,
        If<IsSatisfied<(cut<2, 1>(Sizes<Ds...>()) >= typename R::Sizes())>> = 0>
    static inline constexpr auto fromDiagonal(X &&a)
    {
        using V = RemoveReference<decltype(view(forward<X>(a)))>;
        return TensorViews::FromDiagonal<typename R::Type, Sizes<Ds...>, V>(
            view(forward<X>(a)));
    }

    template<Size... Ds, class X, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<Not<IsInvocable<View, X>>> = 0,
        If<IsInvocable<FromDiagonal<Ds...>, X &>> = 0>
    static inline constexpr auto fromDiagonal(X &&a)
    {
        return eval(fromDiagonal<Ds...>(a));
    }

    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0>
    static inline constexpr auto fromDiagonal(X &&a)
    {
        return expand(makeSeries<Index, typename R::Dimension{}>(),
            [&a](auto const... seq)
            {
                return fromDiagonal<R::size(seq)..., R::size(seq)...>(
                    forward<X>(a));
            });
    }
}
#endif // pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_FROM_DIAGONAL_H
