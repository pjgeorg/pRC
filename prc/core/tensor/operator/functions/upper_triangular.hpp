// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_UPPER_TRIANGULAR_H
#define pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_UPPER_TRIANGULAR_H

#include <prc/core/functors/upper_triangular.hpp>
#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/operator/views/upper_triangular.hpp>

namespace pRC
{
    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
        If<IsInvocable<View, X>> = 0,
        If<IsSatisfied<(typename R::Dimension() == 2)>> = 0>
    static inline constexpr auto upperTriangular(X &&a)
    {
        using V = RemoveReference<decltype(view(forward<X>(a)))>;
        return TensorViews::UpperTriangular<typename R::Type, typename R::Sizes,
            V>(view(forward<X>(a)));
    }

    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
        If<Not<IsInvocable<View, X>>> = 0,
        If<IsInvocable<UpperTriangular, X &>> = 0>
    static inline constexpr auto upperTriangular(X &&a)
    {
        return eval(upperTriangular(a));
    }
}
#endif // pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_UPPER_TRIANGULAR_H
