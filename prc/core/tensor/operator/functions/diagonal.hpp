// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_DIAGONAL_H
#define pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_DIAGONAL_H

#include <prc/core/basic/functions/is_even.hpp>
#include <prc/core/functors/diagonal.hpp>
#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/operator/views/diagonal.hpp>

namespace pRC
{
    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
        If<IsInvocable<View, X>> = 0,
        If<IsSatisfied<(isEven(typename R::Dimension()))>> = 0>
    static inline constexpr auto diagonal(X &&a)
    {
        using V = RemoveReference<decltype(view(forward<X>(a)))>;
        return TensorViews::Diagonal<typename R::Type, typename R::Sizes, V>(
            view(forward<X>(a)));
    }

    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
        If<Not<IsInvocable<View, X>>> = 0, If<IsInvocable<Diagonal, X &>> = 0>
    static inline constexpr auto diagonal(X &&a)
    {
        return eval(diagonal(a));
    }
}
#endif // pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_DIAGONAL_H
