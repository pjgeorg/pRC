// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_MATRICIZE_H
#define pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_MATRICIZE_H

#include <prc/core/functors/reshape.hpp>
#include <prc/core/tensor/functions/reshape.hpp>

namespace pRC
{
    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
        If<IsSatisfied<(typename R::Dimension{} > 0)>> = 0,
        If<IsSatisfied<(isEven(typename R::Dimension()))>> = 0>
    static inline constexpr auto matricize(X &&a)
    {
        return reshape<decltype(cut<2, 0>(typename R::Sizes()))::size(),
            decltype(cut<2, 1>(typename R::Sizes()))::size()>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_MATRICIZE_H
