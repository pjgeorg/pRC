// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_IS_SELF_ADJOINT_H
#define pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_IS_SELF_ADJOINT_H

#include <prc/core/functors/adjoint.hpp>
#include <prc/core/functors/is_approx.hpp>
#include <prc/core/tensor/operator/functions/adjoint.hpp>
#include <prc/core/tensor/type_traits.hpp>
#include <prc/core/value/limits.hpp>

namespace pRC
{
    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
        If<IsInvocable<Adjoint, X>> = 0,
        If<IsInvocable<IsApprox, ResultOf<Adjoint, X>, X>> = 0,
        class TT = typename R::Value>
    static inline constexpr auto isSelfAdjoint(
        X &&a, TT const &tolerance = NumericLimits<TT>::tolerance())
    {
        return isApprox(adjoint(a), a, tolerance);
    }
}
#endif // pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_IS_SELF_ADJOINT_H
