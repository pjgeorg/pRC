// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_TRACE_H
#define pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_TRACE_H

#include <prc/core/functors/add.hpp>
#include <prc/core/functors/extract_diagonal.hpp>
#include <prc/core/functors/reduce.hpp>
#include <prc/core/functors/trace.hpp>
#include <prc/core/tensor/functions/reduce.hpp>
#include <prc/core/tensor/operator/functions/extract_diagonal.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<View, X>> = 0, If<IsInvocable<ExtractDiagonal, X>> = 0,
        If<IsInvocable<Reduce<Add>, ResultOf<ExtractDiagonal, X>>> = 0>
    static inline constexpr auto trace(X &&a)
    {
        return reduce<Add>(extractDiagonal(forward<X>(a)));
    }

    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<Not<IsInvocable<View, X>>> = 0, If<IsInvocable<Trace, X &>> = 0>
    static inline constexpr auto trace(X &&a)
    {
        return eval(trace(a));
    }
}
#endif // pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_TRACE_H
