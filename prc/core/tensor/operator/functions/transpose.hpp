// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_TRANSPOSE_H
#define pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_TRANSPOSE_H

#include <prc/core/tensor/functions/permute.hpp>

namespace pRC
{
    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
        If<IsSatisfied<(isEven(typename R::Dimension()))>> = 0>
    static inline constexpr auto transpose(X &&a)
    {
        constexpr auto H = typename R::Dimension() / 2;
        return expand((makeRange<Index, H, H + H>(), makeSeries<Index, H>()),
            [&a](auto const... seq)
            {
                return permute<seq...>(forward<X>(a));
            });
    }
}
#endif // pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_TRANSPOSE_H
