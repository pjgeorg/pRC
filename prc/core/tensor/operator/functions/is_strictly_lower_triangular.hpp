// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_IS_STRICTLY_LOWER_TRIANGULAR_H
#define pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_IS_STRICTLY_LOWER_TRIANGULAR_H

#include <prc/core/functors/is_approx.hpp>
#include <prc/core/functors/strictly_lower_triangular.hpp>
#include <prc/core/tensor/operator/functions/strictly_lower_triangular.hpp>
#include <prc/core/tensor/type_traits.hpp>
#include <prc/core/value/limits.hpp>

namespace pRC
{
    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
        If<IsInvocable<StrictlyLowerTriangular, X>> = 0,
        If<IsInvocable<IsApprox, ResultOf<StrictlyLowerTriangular, X>, X>> = 0,
        class TT = typename R::Value>
    static inline constexpr auto isStrictlyLowerTriangular(
        X &&a, TT const &tolerance = NumericLimits<TT>::tolerance())
    {
        return isApprox(strictlyLowerTriangular(a), a, tolerance);
    }
}
#endif // pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_IS_STRICTLY_LOWER_TRIANGULAR_H
