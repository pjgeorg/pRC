// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_APPLY_H
#define pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_APPLY_H

#include <prc/core/functors/mul.hpp>
#include <prc/core/functors/restrict.hpp>
#include <prc/core/functors/transform.hpp>
#include <prc/core/tensor/operator/functions/mul.hpp>
#include <prc/core/tensor/operator/hint.hpp>

namespace pRC
{
    template<Operator::Transform T, Operator::Restrict R, Operator::Hint H>
    struct Apply;

    template<Operator::Transform OT = Operator::Transform::None,
        Operator::Restrict OR = Operator::Restrict::None,
        Operator::Hint OH = Operator::Hint::None, class XA, class XB,
        class RA = RemoveReference<XA>, class RB = RemoveReference<XB>,
        If<IsTensorish<RA>> = 0, If<IsInvocable<View, XA>> = 0,
        If<IsTensorish<RB>> = 0, If<IsInvocable<View, XB>> = 0,
        class DA = typename RA::Dimension, class DB = typename RB::Dimension,
        If<IsSatisfied<(isEven(DA()) && (DA() / 2 == DB() || DA() == DB()))>> =
            0,
        If<IsInvocable<Transform<OT>, XA>> = 0,
        If<IsInvocable<Mul, ResultOf<Restrict<OR>, ResultOf<Transform<OT>, XA>>,
            XB>> = 0>
    static inline constexpr auto apply(XA &&a, XB &&b)
    {
        return restrict<OR>(transform<OT>(forward<XA>(a))) * forward<XB>(b);
    }

    template<Operator::Transform OT = Operator::Transform::None,
        Operator::Restrict OR = Operator::Restrict::None,
        Operator::Hint OH = Operator::Hint::None, class XA, class XB,
        class RA = RemoveReference<XA>, class RB = RemoveReference<XB>,
        If<IsTensorish<RA>> = 0, If<IsTensorish<RB>> = 0,
        If<Not<All<IsInvocable<View, XA>, IsInvocable<View, XB>>>> = 0,
        If<IsInvocable<Apply<OT, OR, OH>, XA &, XB &>> = 0>
    static inline constexpr auto apply(XA &&a, XB &&b)
    {
        return eval(apply<OT, OR, OH>(a, b));
    }
}
#endif // pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_APPLY_H
