// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_MUL_H
#define pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_MUL_H

#include <prc/core/basic/functions/is_even.hpp>
#include <prc/core/functors/contract.hpp>
#include <prc/core/tensor/functions/contract.hpp>

namespace pRC
{
    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<IsTensorish<RB>> = 0, class DA = typename RA::Dimension,
        class DB = typename RB::Dimension, class SA = typename RA::Sizes,
        class SB = typename RB::Sizes,
        If<IsSatisfied<(isEven(DA()) && DA() / 2 == DB())>> = 0,
        If<IsSame<decltype(cut<2, 1>(SA())), SB>> = 0,
        If<IsInvocable<Contract<DB{}, 0>, XA, XB>> = 0>
    static inline constexpr auto operator*(XA &&a, XB &&b)
    {
        return expand(makeSeries<Index, DB{}>(),
            [&a, &b](auto const... seq)
            {
                return contract<(seq + DB())..., seq...>(
                    forward<XA>(a), forward<XB>(b));
            });
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<IsTensorish<RB>> = 0, class DA = typename RA::Dimension,
        class DB = typename RB::Dimension, class SA = typename RA::Sizes,
        class SB = typename RB::Sizes,
        If<IsSatisfied<(isEven(DB()) && DA() == DB() / 2)>> = 0,
        If<IsSame<SA, decltype(cut<2, 0>(SB()))>> = 0,
        If<IsInvocable<Contract<0, 0>, XA, XB>> = 0>
    static inline constexpr auto operator*(XA &&a, XB &&b)
    {
        return expand(makeSeries<Index, DA{}>(),
            [&a, &b](auto const... seq)
            {
                return contract<seq..., seq...>(forward<XA>(a), forward<XB>(b));
            });
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<IsTensorish<RB>> = 0, class DA = typename RA::Dimension,
        class DB = typename RB::Dimension, class SA = typename RA::Sizes,
        class SB = typename RB::Sizes,
        If<IsSatisfied<(isEven(DA()) && isEven(DB()) && DA() == DB())>> = 0,
        If<IsSame<decltype(cut<2, 1>(SA())), decltype(cut<2, 0>(SB()))>> = 0,
        If<IsInvocable<Contract<DA() / 2, 0>, XA, XB>> = 0>
    static inline constexpr auto operator*(XA &&a, XB &&b)
    {
        return expand(makeSeries<Index, DA() / 2>(),
            [&a, &b](auto const... seq)
            {
                return contract<(seq + DA() / 2)..., seq...>(
                    forward<XA>(a), forward<XB>(b));
            });
    }
}
#endif // pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_MUL_H
