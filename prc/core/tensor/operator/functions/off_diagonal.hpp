// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_OFF_DIAGONAL_H
#define pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_OFF_DIAGONAL_H

#include <prc/core/functors/off_diagonal.hpp>
#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/operator/views/off_diagonal.hpp>

namespace pRC
{
    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
        If<IsInvocable<View, X>> = 0,
        If<IsSatisfied<(typename R::Dimension() == 2)>> = 0>
    static inline constexpr auto offDiagonal(X &&a)
    {
        using V = RemoveReference<decltype(view(forward<X>(a)))>;
        return TensorViews::OffDiagonal<typename R::Type, typename R::Sizes, V>(
            view(forward<X>(a)));
    }

    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
        If<Not<IsInvocable<View, X>>> = 0,
        If<IsInvocable<OffDiagonal, X &>> = 0>
    static inline constexpr auto offDiagonal(X &&a)
    {
        return eval(offDiagonal(a));
    }
}
#endif // pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_OFF_DIAGONAL_H
