// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_IS_SYMMETRIC_H
#define pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_IS_SYMMETRIC_H

#include <prc/core/functors/adjoint.hpp>
#include <prc/core/functors/is_approx.hpp>
#include <prc/core/functors/transpose.hpp>
#include <prc/core/tensor/operator/functions/transpose.hpp>
#include <prc/core/tensor/type_traits.hpp>
#include <prc/core/value/limits.hpp>

namespace pRC
{
    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
        If<IsInvocable<Transpose, X>> = 0,
        If<IsInvocable<IsApprox, ResultOf<Transpose, X>, X>> = 0,
        class TT = typename R::Value>
    static inline constexpr auto isSymmetric(
        X &&a, TT const &tolerance = NumericLimits<TT>::tolerance())
    {
        return isApprox(transpose(a), a, tolerance);
    }
}
#endif // pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_IS_SYMMETRIC_H
