// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_IS_STRICTLY_UPPER_TRIANGULAR_H
#define pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_IS_STRICTLY_UPPER_TRIANGULAR_H

#include <prc/core/functors/is_approx.hpp>
#include <prc/core/functors/strictly_upper_triangular.hpp>
#include <prc/core/tensor/operator/functions/strictly_upper_triangular.hpp>
#include <prc/core/tensor/type_traits.hpp>
#include <prc/core/value/limits.hpp>

namespace pRC
{
    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
        If<IsInvocable<StrictlyUpperTriangular, X>> = 0,
        If<IsInvocable<IsApprox, ResultOf<StrictlyUpperTriangular, X>, X>> = 0,
        class TT = typename R::Value>
    static inline constexpr auto isStrictlyUpperTriangular(
        X &&a, TT const &tolerance = NumericLimits<TT>::tolerance())
    {
        return isApprox(strictlyUpperTriangular(a), a, tolerance);
    }
}
#endif // pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_IS_STRICTLY_UPPER_TRIANGULAR_H
