// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_EXTRACT_DIAGONAL_H
#define pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_EXTRACT_DIAGONAL_H

#include <prc/core/basic/functions/is_even.hpp>
#include <prc/core/functors/extract_diagonal.hpp>
#include <prc/core/functors/min.hpp>
#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/operator/views/extract_diagonal.hpp>

namespace pRC
{
    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
        If<IsInvocable<View, X>> = 0,
        If<IsSatisfied<(isEven(typename R::Dimension()))>> = 0>
    static inline constexpr auto extractDiagonal(X &&a)
    {
        using V = RemoveReference<decltype(view(forward<X>(a)))>;
        return TensorViews::ExtractDiagonal<typename R::Type,
            decltype(pick<Min>(cut<2, 0>(typename R::Sizes()),
                cut<2, 1>(typename R::Sizes()))),
            V>(view(forward<X>(a)));
    }

    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
        If<Not<IsInvocable<View, X>>> = 0,
        If<IsInvocable<ExtractDiagonal, X &>> = 0>
    static inline constexpr auto extractDiagonal(X &&a)
    {
        return eval(extractDiagonal(a));
    }
}
#endif // pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_EXTRACT_DIAGONAL_H
