// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_IS_OFF_DIAGONAL_H
#define pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_IS_OFF_DIAGONAL_H

#include <prc/core/functors/is_approx.hpp>
#include <prc/core/functors/off_diagonal.hpp>
#include <prc/core/tensor/operator/functions/off_diagonal.hpp>
#include <prc/core/tensor/type_traits.hpp>
#include <prc/core/value/limits.hpp>

namespace pRC
{
    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
        If<IsInvocable<OffDiagonal, X>> = 0,
        If<IsInvocable<IsApprox, ResultOf<OffDiagonal, X>, X>> = 0,
        class TT = typename R::Value>
    static inline constexpr auto isOffDiagonal(
        X &&a, TT const &tolerance = NumericLimits<TT>::tolerance())
    {
        return isApprox(offDiagonal(a), a, tolerance);
    }
}
#endif // pRC_CORE_TENSOR_OPERATOR_FUNCTIONS_IS_OFF_DIAGONAL_H
