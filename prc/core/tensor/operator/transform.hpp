// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_TRANSFORM_H
#define pRC_CORE_TENSOR_OPERATOR_TRANSFORM_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC::Operator
{
    enum class Transform
    {
        None,
        Transpose,
        Adjoint,
    };
}
#endif // pRC_CORE_TENSOR_OPERATOR_TRANSFORM_H
