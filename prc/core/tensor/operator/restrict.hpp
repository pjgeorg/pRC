// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_RESTRICT_H
#define pRC_CORE_TENSOR_OPERATOR_RESTRICT_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC::Operator
{
    enum class Restrict
    {
        None,
        Diagonal,
        LowerTriangular,
        StrictlyLowerTriangular,
        UpperTriangular,
        StrictlyUpperTriangular,
        OffDiagonal,
    };
}
#endif // pRC_CORE_TENSOR_OPERATOR_RESTRICT_H
