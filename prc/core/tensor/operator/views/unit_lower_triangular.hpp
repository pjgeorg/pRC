// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_VIEWS_UNIT_LOWER_TRIANGULAR_H
#define pRC_CORE_TENSOR_OPERATOR_VIEWS_UNIT_LOWER_TRIANGULAR_H

#include <prc/core/basic/functions/identity.hpp>
#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, class N, class V>
    class UnitLowerTriangular : public View<T, N, UnitLowerTriangular<T, N, V>>
    {
        static_assert(IsTensorView<V>());
        static_assert(typename N::Dimension() == 2);

    private:
        using Base = View<T, N, UnitLowerTriangular>;

    public:
        template<class X, If<IsConstructible<V, X>> = 0>
        UnitLowerTriangular(X &&a)
            : mA(forward<X>(a))
        {
        }

        constexpr T operator()(Index const i, Index const j)
        {
            if(i > j)
            {
                return mA(i, j);
            }

            if(i == j)
            {
                return identity();
            }

            return zero();
        }

        constexpr T operator()(Index const i, Index const j) const
        {
            if(i > j)
            {
                return mA(i, j);
            }

            if(i == j)
            {
                return identity();
            }

            return zero();
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts)
        {
            return this->call(subscripts);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts) const
        {
            return this->call(subscripts);
        }

    private:
        V mA;
    };
}
#endif // pRC_CORE_TENSOR_OPERATOR_VIEWS_UNIT_LOWER_TRIANGULAR_H
