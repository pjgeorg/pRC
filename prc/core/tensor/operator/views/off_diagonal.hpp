// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_VIEWS_OFF_DIAGONAL_H
#define pRC_CORE_TENSOR_OPERATOR_VIEWS_OFF_DIAGONAL_H

#include <prc/core/basic/sequence.hpp>
#include <prc/core/basic/zero.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, class N, class V>
    class OffDiagonal : public View<T, N, OffDiagonal<T, N, V>>
    {
        static_assert(IsTensorView<V>());

    private:
        using Base = View<T, N, OffDiagonal>;

    public:
        template<class X, If<IsConstructible<V, X>> = 0>
        OffDiagonal(X &&a)
            : mA(forward<X>(a))
        {
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices)
        {
            return expand(
                makeSeries<Index, typename Base::Dimension() / 2>(),
                [this](auto const &indices, auto const... seq) -> T
                {
                    if(((indices[seq] ==
                            indices[typename Base::Dimension() / 2 + seq]) &&
                           ...))
                    {
                        return zero();
                    }

                    return mA(indices[seq]...,
                        indices[typename Base::Dimension() / 2 + seq]...);
                },
                Indices<typename Base::Dimension{}>(indices...));
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            return expand(
                makeSeries<Index, typename Base::Dimension() / 2>(),
                [this](auto const &indices, auto const... seq) -> T
                {
                    if(((indices[seq] ==
                            indices[typename Base::Dimension() / 2 + seq]) &&
                           ...))
                    {
                        return zero();
                    }

                    return mA(indices[seq]...,
                        indices[typename Base::Dimension() / 2 + seq]...);
                },
                Indices<typename Base::Dimension{}>(indices...));
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts)
        {
            return this->call(subscripts);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts) const
        {
            return this->call(subscripts);
        }

    private:
        V mA;
    };
}
#endif // pRC_CORE_TENSOR_OPERATOR_VIEWS_OFF_DIAGONAL_H
