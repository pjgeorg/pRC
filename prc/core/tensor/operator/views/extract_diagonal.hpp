// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_VIEWS_EXTRACT_DIAGONAL_H
#define pRC_CORE_TENSOR_OPERATOR_VIEWS_EXTRACT_DIAGONAL_H

#include <prc/core/basic/sequence.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, class N, class V>
    class ExtractDiagonal
        : public Conditional<IsAssignable<V>,
              Assignable<T, N, ExtractDiagonal<T, N, V>>,
              View<T, N, ExtractDiagonal<T, N, V>>>
    {
        static_assert(IsTensorView<V>());

    private:
        using Base = Conditional<IsAssignable<V>,
            Assignable<T, N, ExtractDiagonal<T, N, V>>,
            View<T, N, ExtractDiagonal<T, N, V>>>;

    public:
        template<class X, If<IsConstructible<V, X>> = 0>
        ExtractDiagonal(X &&a)
            : mA(forward<X>(a))
        {
        }

        using Base::operator=;

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices)
        {
            return mA(indices..., indices...);
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            return mA(indices..., indices...);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts)
        {
            return this->call(subscripts);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts) const
        {
            return this->call(subscripts);
        }

    private:
        V mA;
    };
}
#endif // pRC_CORE_TENSOR_OPERATOR_VIEWS_EXTRACT_DIAGONAL_H
