// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_VIEWS_FROM_DIAGONAL_H
#define pRC_CORE_TENSOR_OPERATOR_VIEWS_FROM_DIAGONAL_H

#include <prc/core/basic/sequence.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, class N, class V>
    class FromDiagonal : public View<T, N, FromDiagonal<T, N, V>>
    {
        static_assert(IsTensorView<V>());

    private:
        using Base = View<T, N, FromDiagonal>;

    public:
        template<class X, If<IsConstructible<V, X>> = 0>
        FromDiagonal(X &&a)
            : mA(forward<X>(a))
        {
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices)
        {
            return expand(
                makeSeries<Index, typename Base::Dimension() / 2>(),
                [this](auto const &indices, auto const... seq) -> T
                {
                    if(((indices[seq] ==
                            indices[typename Base::Dimension() / 2 + seq]) &&
                           ...))
                    {
                        return mA(indices[seq]...);
                    }

                    return zero();
                },
                Indices<typename Base::Dimension{}>(indices...));
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            return expand(
                makeSeries<Index, typename Base::Dimension() / 2>(),
                [this](auto const &indices, auto const... seq) -> T
                {
                    if(((indices[seq] ==
                            indices[typename Base::Dimension() / 2 + seq]) &&
                           ...))
                    {
                        return mA(indices[seq]...);
                    }

                    return zero();
                },
                Indices<typename Base::Dimension{}>(indices...));
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts)
        {
            return this->call(subscripts);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts) const
        {
            return this->call(subscripts);
        }

    private:
        V mA;
    };
}
#endif // pRC_CORE_TENSOR_OPERATOR_VIEWS_FROM_DIAGONAL_H
