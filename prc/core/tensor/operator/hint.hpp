// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_OPERATOR_HINT_H
#define pRC_CORE_TENSOR_OPERATOR_HINT_H

namespace pRC::Operator
{
    enum class Hint
    {
        None,
        Symmetric,
        SelfAdjoint,
        Diagonal,
        LowerTriangular,
        StrictlyLowerTriangular,
        UnitLowerTriangular,
        UpperTriangular,
        StrictlyUpperTriangular,
        UnitUpperTriangular,
        OffDiagonal,
    };
}
#endif // pRC_CORE_TENSOR_OPERATOR_HINT_H
