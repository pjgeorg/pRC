// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_COMMON_H
#define pRC_CORE_TENSOR_COMMON_H

#include <prc/core/basic/common.hpp>
#include <prc/core/functors/eval.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC
{
    template<class TA, class TB, Index... Ns>
    struct CommonType<Tensor<TA, Ns...>, Tensor<TB, Ns...>,
        If<HasCommon<TA, TB>>>
    {
        using Type = Tensor<Common<TA, TB>, Ns...>;
    };

    template<class VA, class VB>
    struct CommonType<VA, VB,
        If<All<IsTensorView<VA>, IsTensorView<VB>,
            HasCommon<ResultOf<Eval, VA>, ResultOf<Eval, VB>>>>>
        : CommonType<ResultOf<Eval, VA>, ResultOf<Eval, VB>>
    {
    };

    template<class VA, class TB>
    struct CommonType<VA, TB,
        If<All<IsTensorView<VA>, IsTensor<TB>,
            HasCommon<ResultOf<Eval, VA>, TB>>>>
        : CommonType<ResultOf<Eval, VA>, TB>
    {
    };

    template<class TA, class VB>
    struct CommonType<TA, VB,
        If<All<IsTensor<TA>, IsTensorView<VB>,
            HasCommon<TA, ResultOf<Eval, VB>>>>>
        : CommonType<TA, ResultOf<Eval, VB>>
    {
    };
}
#endif // pRC_CORE_TENSOR_COMMON_H
