// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_IDENTITY_H
#define pRC_CORE_TENSOR_IDENTITY_H

#include <prc/core/basic/functions/unit.hpp>
#include <prc/core/tensor/operator/functions/diagonal.hpp>
#include <prc/core/tensor/unit.hpp>

namespace pRC
{
    template<class T>
    struct Identity<T, If<IsTensorView<T>>> : Identity<ResultOf<Eval, T>>
    {
    };

    template<class T>
    struct Identity<T, If<IsTensor<T>>>
    {
        constexpr auto operator()()
        {
            return diagonal(unit<T>());
        }

        template<class X, If<IsConstructible<typename T::Type, X>> = 0>
        constexpr auto operator()(X &&value)
        {
            return diagonal(unit<T>(forward<X>(value)));
        }
    };
}
#endif // pRC_CORE_TENSOR_IDENTITY_H
