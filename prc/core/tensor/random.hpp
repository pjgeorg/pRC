// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_RANDOM_H
#define pRC_CORE_TENSOR_RANDOM_H

#include <prc/core/basic/random.hpp>
#include <prc/core/tensor/views/random.hpp>

namespace pRC
{
    template<class T, Size... Ns, template<class...> class D>
    struct Random<Tensor<T, Ns...>, D<typename T::Value>,
        If<IsDistribution<D<typename T::Value>>>>
    {
    public:
        Random(RandomEngine &rng, D<typename T::Value> &distribution)
            : mRNG(rng)
            , mDistribution(distribution)
        {
        }

        constexpr auto operator()()
        {
            return TensorViews::Random<T, Sizes<Ns...>, D>(mRNG, mDistribution);
        }

    private:
        RandomEngine &mRNG;
        D<typename T::Value> &mDistribution;
    };
}
#endif // pRC_CORE_TENSOR_RANDOM_H
