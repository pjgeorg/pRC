// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_WHERE_H
#define pRC_CORE_TENSOR_FUNCTIONS_WHERE_H

#include <prc/core/functors/loop.hpp>
#include <prc/core/functors/where.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class XE, class XA, class XB,
        If<IsTensorish<RemoveReference<XE>>> = 0,
        If<IsTensorish<RemoveReference<XA>>> = 0,
        If<IsTensorish<RemoveReference<XB>>> = 0,
        If<IsInvocable<Loop<Where>, XE, XA, XB>> = 0>
    static inline constexpr auto where(XE &&e, XA &&a, XB &&b)
    {
        return loop<Where>(forward<XE>(e), forward<XA>(a), forward<XB>(b));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_WHERE_H
