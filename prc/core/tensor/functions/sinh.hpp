// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_SINH_H
#define pRC_CORE_TENSOR_FUNCTIONS_SINH_H

#include <prc/core/functors/loop.hpp>
#include <prc/core/functors/sinh.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Sinh>, X>> = 0>
    static inline constexpr auto sinh(X &&a)
    {
        return loop<Sinh>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_SINH_H
