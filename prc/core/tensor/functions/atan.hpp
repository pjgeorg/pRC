// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_ATAN_H
#define pRC_CORE_TENSOR_FUNCTIONS_ATAN_H

#include <prc/core/functors/atan.hpp>
#include <prc/core/functors/loop.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Atan>, X>> = 0>
    static inline constexpr auto atan(X &&a)
    {
        return loop<Atan>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_ATAN_H
