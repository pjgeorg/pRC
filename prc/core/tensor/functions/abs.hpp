// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_ABS_H
#define pRC_CORE_TENSOR_FUNCTIONS_ABS_H

#include <prc/core/functors/abs.hpp>
#include <prc/core/functors/loop.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Abs>, X>> = 0>
    static inline constexpr auto abs(X &&a)
    {
        return loop<Abs>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_ABS_H
