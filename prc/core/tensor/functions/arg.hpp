// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_ARG_H
#define pRC_CORE_TENSOR_FUNCTIONS_ARG_H

#include <prc/core/functors/arg.hpp>
#include <prc/core/functors/loop.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Arg>, X>> = 0>
    static inline constexpr auto arg(X &&a)
    {
        return loop<Arg>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_ARG_H
