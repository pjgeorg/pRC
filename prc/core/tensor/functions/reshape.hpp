// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_RESHAPE_H
#define pRC_CORE_TENSOR_FUNCTIONS_RESHAPE_H

#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/views/reshape.hpp>

namespace pRC
{
    template<Size... Ns>
    struct Reshape;

    template<Size... Ns, class X, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<IsInvocable<View, X>> = 0,
        If<IsSatisfied<(Sizes<Ns...>::size() == R::size())>> = 0>
    static inline constexpr auto reshape(X &&a)
    {
        using V = RemoveReference<decltype(view(forward<X>(a)))>;
        return TensorViews::Reshape<typename R::Type, Sizes<Ns...>, V>(
            view(forward<X>(a)));
    }

    template<Size... Ns, class X, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<Not<IsInvocable<View, X>>> = 0,
        If<IsInvocable<Reshape<Ns...>, X &>> = 0>
    static inline constexpr auto reshape(X &&a)
    {
        return eval(reshape<Ns...>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_RESHAPE_H
