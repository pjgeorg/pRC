// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_TANH_H
#define pRC_CORE_TENSOR_FUNCTIONS_TANH_H

#include <prc/core/functors/loop.hpp>
#include <prc/core/functors/tanh.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Tanh>, X>> = 0>
    static inline constexpr auto tanh(X &&a)
    {
        return loop<Tanh>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_TANH_H
