// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_DIRECT_SUM_H
#define pRC_CORE_TENSOR_FUNCTIONS_DIRECT_SUM_H

#include <prc/core/basic/common.hpp>
#include <prc/core/functors/direct_sum.hpp>
#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/views/direct_sum.hpp>

namespace pRC
{
    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<IsInvocable<View, XA>> = 0, If<IsTensorish<RB>> = 0,
        If<IsInvocable<View, XB>> = 0,
        If<IsSame<typename RA::Dimension, typename RB::Dimension>> = 0,
        If<HasCommon<typename RA::Type, typename RB::Type>> = 0>
    static inline constexpr auto directSum(XA &&a, XB &&b)
    {
        using VA = RemoveReference<decltype(view(forward<XA>(a)))>;
        using VB = RemoveReference<decltype(view(forward<XB>(b)))>;
        using T = Common<typename RA::Type, typename RB::Type>;
        return TensorViews::DirectSum<T,
            decltype(typename RA::Sizes() + typename RB::Sizes()), VA, VB>(
            view(forward<XA>(a)), view(forward<XB>(b)));
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<IsTensorish<RB>> = 0,
        If<Not<All<IsInvocable<View, XA>, IsInvocable<View, XB>>>> = 0,
        If<IsInvocable<DirectSum, XA &, XB &>> = 0>
    static inline constexpr auto directSum(XA &&a, XB &&b)
    {
        return eval(directSum(a, b));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_DIRECT_SUM_H
