// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_SQUARE_H
#define pRC_CORE_TENSOR_FUNCTIONS_SQUARE_H

#include <prc/core/functors/loop.hpp>
#include <prc/core/functors/square.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Square>, X>> = 0>
    static inline constexpr auto square(X &&a)
    {
        return loop<Square>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_SQUARE_H
