// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_SLICE_H
#define pRC_CORE_TENSOR_FUNCTIONS_SLICE_H

#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/views/slice.hpp>

namespace pRC
{
    template<Size... Es>
    struct Slice;

    template<Size... Es, class X, class... Os, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<IsInvocable<View, X>> = 0,
        If<All<IsConvertible<Os, Index>...>> = 0,
        If<IsSatisfied<(sizeof...(Es) == typename R::Dimension())>> = 0,
        If<IsSatisfied<(sizeof...(Os) == typename R::Dimension())>> = 0,
        If<IsSatisfied<(Sizes<Es...>() <= typename R::Sizes())>> = 0>
    static inline constexpr auto slice(X &&a, Os const... offsets)
    {
        using V = RemoveReference<decltype(view(forward<X>(a)))>;
        return expand(makeSeries<Index, typename R::Dimension{}>(),
            [&a, &offsets...](auto const... seq)
            {
                return TensorViews::Slice<typename R::Type, Sizes<Es...>,
                    Sizes<(R::size(seq) - Es + 1)...>, V>(view(forward<X>(a)),
                    Subscripts<(R::size(seq) - Es + 1)...>(offsets...));
            });
    }

    template<Size... Es, class X, class... Os, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<Not<IsInvocable<View, X>>> = 0,
        If<IsInvocable<Slice<Es...>, X &, Os...>> = 0>
    static inline constexpr auto slice(X &&a, Os const... offsets)
    {
        return eval(slice<Es...>(a, offsets...));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_SLICE_H
