// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_REAL_H
#define pRC_CORE_TENSOR_FUNCTIONS_REAL_H

#include <prc/core/functors/loop.hpp>
#include <prc/core/functors/real.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Real>, X>> = 0>
    static inline constexpr auto real(X &&a)
    {
        return loop<Real>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_REAL_H
