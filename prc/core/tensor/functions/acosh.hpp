// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_ACOSH_H
#define pRC_CORE_TENSOR_FUNCTIONS_ACOSH_H

#include <prc/core/functors/acosh.hpp>
#include <prc/core/functors/loop.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Acosh>, X>> = 0>
    static inline constexpr auto acosh(X &&a)
    {
        return loop<Acosh>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_ACOSH_H
