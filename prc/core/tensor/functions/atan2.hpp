// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_ATAN2_H
#define pRC_CORE_TENSOR_FUNCTIONS_ATAN2_H

#include <prc/core/functors/atan2.hpp>
#include <prc/core/functors/loop.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class XA, class XB, If<IsTensorish<RemoveReference<XA>>> = 0,
        If<IsTensorish<RemoveReference<XB>>> = 0,
        If<IsInvocable<Loop<Atan2>, XA, XB>> = 0>
    static inline constexpr auto atan2(XA &&a, XB &&b)
    {
        return loop<Atan2>(forward<XA>(a), forward<XB>(b));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_ATAN2_H
