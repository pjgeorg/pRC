// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_OUTER_PRODUCT_H
#define pRC_CORE_TENSOR_FUNCTIONS_OUTER_PRODUCT_H

#include <prc/core/functors/conj.hpp>
#include <prc/core/functors/outer_product.hpp>
#include <prc/core/functors/tensor_product.hpp>
#include <prc/core/tensor/functions/conj.hpp>
#include <prc/core/tensor/functions/tensor_product.hpp>

namespace pRC
{
    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<IsInvocable<View, XA>> = 0, If<IsTensorish<RB>> = 0,
        If<IsInvocable<View, XB>> = 0, If<IsInvocable<Conj, XA>> = 0,
        If<IsInvocable<TensorProduct, XA, ResultOf<Conj, XB>>> = 0>
    static inline constexpr auto outerProduct(XA &&a, XB &&b)
    {
        return tensorProduct(forward<XA>(a), conj(forward<XB>(b)));
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<IsTensorish<RB>> = 0,
        If<Not<All<IsInvocable<View, XA>, IsInvocable<View, XB>>>> = 0,
        If<IsInvocable<OuterProduct, XA &, XB &>> = 0>
    static inline constexpr auto outerProduct(XA &&a, XB &&b)
    {
        return eval(outerProduct(a, b));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_OUTER_PRODUCT_H
