// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_NORM_H
#define pRC_CORE_TENSOR_FUNCTIONS_NORM_H

#include <prc/core/functors/add.hpp>
#include <prc/core/functors/loop.hpp>
#include <prc/core/functors/max.hpp>
#include <prc/core/functors/norm.hpp>
#include <prc/core/functors/reduce.hpp>
#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/functions/loop.hpp>
#include <prc/core/tensor/functions/reduce.hpp>
#include <prc/core/tensor/functions/sqrt.hpp>

namespace pRC
{
    template<Index P = 2, Index Q = P, class X,
        If<IsTensorish<RemoveReference<X>>> = 0, If<IsInvocable<View, X>> = 0,
        If<Any<All<IsSatisfied<(P == 1 && Q == 1)>,
                   IsInvocable<Loop<Norm<1, 1>>, X>,
                   IsInvocable<Reduce<Add>, ResultOf<Loop<Norm<1, 1>>, X>>>,
            All<IsSatisfied<(P == 2 && Q == 1)>,
                IsInvocable<Loop<Norm<2, 1>>, X>,
                IsInvocable<Reduce<Add>, ResultOf<Loop<Norm<2, 1>>, X>>>,
            All<IsSatisfied<(P == 2 && Q == 2)>,
                IsInvocable<Loop<Norm<2, 1>>, X>,
                IsInvocable<Reduce<Add>, ResultOf<Loop<Norm<2, 1>>, X>>,
                IsInvocable<Sqrt,
                    ResultOf<Reduce<Add>, ResultOf<Loop<Norm<2, 1>>, X>>>>,
            All<IsSatisfied<(P != Q && Q == 0)>,
                IsInvocable<Loop<Norm<P, P>>, X>,
                IsInvocable<Reduce<Max>, ResultOf<Loop<Norm<P, P>>, X>>>>> = 0>
    static inline constexpr auto norm(X &&a)
    {
        if constexpr(Q == 1)
        {
            return reduce<Add>(loop<Norm<P, 1>>(forward<X>(a)));
        }
        else if constexpr(Q == 2)
        {
            return sqrt(norm<P, 1>(forward<X>(a)));
        }
        else if constexpr(P != Q && Q == 0)
        {
            return reduce<Max>(loop<Norm<P, P>>(forward<X>(a)));
        }
        else
        {
            static_assert(P != P, "Unsupported p-norm.");
        }
    }

    template<Index P = 2, Index Q = P, class X,
        If<IsTensorish<RemoveReference<X>>> = 0,
        If<Not<IsInvocable<View, X>>> = 0, If<IsInvocable<Norm<P, Q>, X &>> = 0>
    static inline constexpr auto norm(X &&a)
    {
        return eval(norm<P, Q>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_NORM_H
