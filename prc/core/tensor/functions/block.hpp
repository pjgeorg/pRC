// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_BLOCK_H
#define pRC_CORE_TENSOR_FUNCTIONS_BLOCK_H

#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/views/block.hpp>

namespace pRC
{
    template<Size... Bs>
    struct Block;

    template<Size... Bs, class X, class... Os, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<IsInvocable<View, X>> = 0,
        If<All<IsConvertible<Os, Index>...>> = 0,
        If<IsSatisfied<(sizeof...(Bs) == typename R::Dimension())>> = 0,
        If<IsSatisfied<(sizeof...(Os) == typename R::Dimension())>> = 0,
        If<IsSatisfied<(
            typename R::Sizes() % Sizes<Bs...>() == Constant<Size, 0>())>> = 0>
    static inline constexpr auto block(X &&a, Os const... offsets)
    {
        using V = RemoveReference<decltype(view(forward<X>(a)))>;
        return TensorViews::Block<typename R::Type,
            decltype(typename R::Sizes() / Sizes<Bs...>()), Sizes<Bs...>, V>(
            view(forward<X>(a)), Subscripts<Bs...>(offsets...));
    }

    template<Size... Bs, class X, class... Os, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<Not<IsInvocable<View, X>>> = 0,
        If<IsInvocable<Block<Bs...>, X &, Os...>> = 0>
    static inline constexpr auto block(X &&a, Os const... offsets)
    {
        return eval(block<Bs...>(a, offsets...));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_BLOCK_H
