// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_SCALAR_PRODUCT_H
#define pRC_CORE_TENSOR_FUNCTIONS_SCALAR_PRODUCT_H

#include <prc/core/functors/add.hpp>
#include <prc/core/functors/hadamard_product.hpp>
#include <prc/core/functors/reduce.hpp>
#include <prc/core/functors/scalar_product.hpp>
#include <prc/core/tensor/functions/hadamard_product.hpp>
#include <prc/core/tensor/functions/reduce.hpp>

namespace pRC
{
    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<IsInvocable<View, XA>> = 0, If<IsTensorish<RB>> = 0,
        If<IsInvocable<View, XB>> = 0,
        If<IsSame<typename RA::Sizes, typename RB::Sizes>> = 0,
        If<IsInvocable<HadamardProduct, XA, XB>> = 0,
        If<IsInvocable<Reduce<Add>, ResultOf<HadamardProduct, XA, XB>>> = 0>
    static inline constexpr auto scalarProduct(XA &&a, XB &&b)
    {
        return reduce<Add>(hadamardProduct(forward<XA>(a), forward<XB>(b)));
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<IsTensorish<RB>> = 0,
        If<Not<All<IsInvocable<View, XA>, IsInvocable<View, XB>>>> = 0,
        If<IsInvocable<ScalarProduct, XA &, XB &>> = 0>
    static inline constexpr auto scalarProduct(XA &&a, XB &&b)
    {
        return eval(scalarProduct(a, b));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_SCALAR_PRODUCT_H
