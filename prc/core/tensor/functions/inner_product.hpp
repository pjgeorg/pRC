// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_INNER_PRODUCT_H
#define pRC_CORE_TENSOR_FUNCTIONS_INNER_PRODUCT_H

#include <prc/core/functors/conj.hpp>
#include <prc/core/functors/inner_product.hpp>
#include <prc/core/functors/scalar_product.hpp>
#include <prc/core/tensor/functions/conj.hpp>
#include <prc/core/tensor/functions/scalar_product.hpp>

namespace pRC
{
    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<IsInvocable<View, XA>> = 0, If<IsTensorish<RB>> = 0,
        If<IsInvocable<Conj, XA>> = 0,
        If<IsInvocable<ScalarProduct, ResultOf<Conj, XA>, XB>> = 0>
    static inline constexpr auto innerProduct(XA &&a, XB &&b)
    {
        return scalarProduct(conj(forward<XA>(a)), forward<XB>(b));
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<IsTensorish<RB>> = 0,
        If<Not<All<IsInvocable<View, XA>, IsInvocable<View, XB>>>> = 0,
        If<IsInvocable<InnerProduct, XA &, XB &>> = 0>
    static inline constexpr auto innerProduct(XA &&a, XB &&b)
    {
        return eval(innerProduct(a, b));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_INNER_PRODUCT_H
