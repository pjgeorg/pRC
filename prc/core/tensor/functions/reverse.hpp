// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_REVERSE_H
#define pRC_CORE_TENSOR_FUNCTIONS_REVERSE_H

#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/views/reverse.hpp>

namespace pRC
{
    template<Bool... Rs>
    struct Reverse;

    template<Bool... Rs, class X, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<IsInvocable<View, X>> = 0,
        If<IsSatisfied<(sizeof...(Rs) == typename R::Dimension())>> = 0>
    static inline constexpr auto reverse(X &&a)
    {
        using V = RemoveReference<decltype(view(forward<X>(a)))>;
        return TensorViews::Reverse<typename R::Type, typename R::Sizes,
            Sequence<Bool, Rs...>, V>(view(forward<X>(a)));
    }

    template<Bool... Rs, class X, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<Not<IsInvocable<View, X>>> = 0,
        If<IsInvocable<Reverse<Rs...>, X &>> = 0>
    static inline constexpr auto reverse(X &&a)
    {
        return eval(reverse<Rs...>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_REVERSE_H
