// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_FOLDING_H
#define pRC_CORE_TENSOR_FUNCTIONS_FOLDING_H

#include <prc/core/basic/position.hpp>
#include <prc/core/functors/unfolding.hpp>
#include <prc/core/tensor/functions/unfolding.hpp>

namespace pRC
{
    template<Position P, class X, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0,
        If<IsSatisfied<(typename R::Dimension{} > 1)>> = 0,
        If<IsSatisfied<(P == Position::Left || P == Position::Right)>> = 0,
        If<All<IsInvocable<Unfolding<typename R::Dimension() - 2>, X>,
            IsInvocable<Unfolding<0>, X>>> = 0>
    static inline constexpr auto folding(X &&a)
    {
        if constexpr(P == Position::Left)
        {
            return unfolding<typename R::Dimension() - 2>(forward<X>(a));
        }

        if constexpr(P == Position::Right)
        {
            return unfolding<0>(forward<X>(a));
        }
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_FOLDING_H
