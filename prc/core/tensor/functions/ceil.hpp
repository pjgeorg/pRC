// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_CEIL_H
#define pRC_CORE_TENSOR_FUNCTIONS_CEIL_H

#include <prc/core/functors/ceil.hpp>
#include <prc/core/functors/loop.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Ceil>, X>> = 0>
    static inline constexpr auto ceil(X &&a)
    {
        return loop<Ceil>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_CEIL_H
