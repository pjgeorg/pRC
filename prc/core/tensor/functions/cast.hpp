// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_CAST_H
#define pRC_CORE_TENSOR_FUNCTIONS_CAST_H

#include <prc/core/functors/cast.hpp>
#include <prc/core/functors/loop.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class C, class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Cast<C>>, X>> = 0>
    static inline constexpr auto cast(X &&a)
    {
        return loop<Cast<C>>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_CAST_H
