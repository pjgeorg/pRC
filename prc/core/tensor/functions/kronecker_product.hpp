// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_KRONECKER_PRODUCT_H
#define pRC_CORE_TENSOR_FUNCTIONS_KRONECKER_PRODUCT_H

#include <prc/core/functors/kronecker_product.hpp>
#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/functions/broadcast.hpp>
#include <prc/core/tensor/functions/hadamard_product.hpp>
#include <prc/core/tensor/functions/inflate.hpp>

namespace pRC
{
    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<IsInvocable<View, XA>> = 0, If<IsTensorish<RB>> = 0,
        If<IsInvocable<View, XB>> = 0,
        If<IsSame<typename RA::Dimension, typename RB::Dimension>> = 0,
        class EA = ResultOf<XA, typename RA::Subscripts>,
        class EB = ResultOf<XB, typename RB::Subscripts>,
        If<IsInvocable<Mul, EA, EB>> = 0>
    static inline constexpr auto kroneckerProduct(XA &&a, XB &&b)
    {
        return expand(makeSeries<Index, typename RA::Dimension{}>(),
            [&a, &b](auto const... seq)
            {
                return hadamardProduct(
                    inflate<RB::size(seq)...>(forward<XA>(a)),
                    broadcast<RA::size(seq)...>(forward<XB>(b)));
            });
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<IsTensorish<RB>> = 0,
        If<Not<All<IsInvocable<View, XA>, IsInvocable<View, XB>>>> = 0,
        If<IsInvocable<KroneckerProduct, XA &, XB &>> = 0>
    static inline constexpr auto kroneckerProduct(XA &&a, XB &&b)
    {
        return eval(kroneckerProduct(a, b));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_KRONECKER_PRODUCT_H
