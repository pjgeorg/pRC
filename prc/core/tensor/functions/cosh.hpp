// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_COSH_H
#define pRC_CORE_TENSOR_FUNCTIONS_COSH_H

#include <prc/core/functors/cosh.hpp>
#include <prc/core/functors/loop.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Cosh>, X>> = 0>
    static inline constexpr auto cosh(X &&a)
    {
        return loop<Cosh>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_COSH_H
