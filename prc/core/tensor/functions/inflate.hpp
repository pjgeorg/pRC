// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_INFLATE_H
#define pRC_CORE_TENSOR_FUNCTIONS_INFLATE_H

#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/views/inflate.hpp>

namespace pRC
{
    template<Size... Is>
    struct Inflate;

    template<Size... Is, class X, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<IsInvocable<View, X>> = 0,
        If<IsSatisfied<(sizeof...(Is) == typename R::Dimension())>> = 0>
    static inline constexpr auto inflate(X &&a)
    {
        using V = RemoveReference<decltype(view(forward<X>(a)))>;
        return TensorViews::Inflate<typename R::Type,
            decltype(typename R::Sizes() * Sizes<Is...>()), Sizes<Is...>, V>(
            view(forward<X>(a)));
    }

    template<Size... Is, class X, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<Not<IsInvocable<View, X>>> = 0,
        If<IsInvocable<Inflate<Is...>, X &>> = 0>
    static inline constexpr auto inflate(X &&a)
    {
        return eval(inflate<Is...>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_INFLATE_H
