// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_FLATTEN_H
#define pRC_CORE_TENSOR_FUNCTIONS_FLATTEN_H

#include <prc/core/basic/sequence.hpp>
#include <prc/core/functors/reshape.hpp>
#include <prc/core/tensor/functions/reshape.hpp>

namespace pRC
{
    template<Index K, class X, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<IsInvocable<View, X>> = 0,
        If<IsSatisfied<(typename R::Dimension{} > 1)>> = 0,
        If<IsSatisfied<(K < typename R::Dimension{})>> = 0>
    static inline constexpr auto flatten(X &&a)
    {
        return expand(chip<K>(makeSeries<Index, typename R::Dimension{}>()),
            [&a](auto const... seq)
            {
                return reshape<R::size(K),
                    decltype(chip<K>(typename R::Sizes()))::size()>(
                    permute<K, seq...>(forward<X>(a)));
            });
    }

    template<Index K, class X, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<Not<IsInvocable<View, X>>> = 0,
        If<IsSatisfied<(typename R::Dimension{} > 1)>> = 0,
        If<IsSatisfied<(K < typename R::Dimension{})>> = 0>
    static inline constexpr auto flatten(X &&a)
    {
        return eval(flatten<K>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_FLATTEN_H
