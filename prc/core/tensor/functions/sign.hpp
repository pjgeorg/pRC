// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_SIGN_H
#define pRC_CORE_TENSOR_FUNCTIONS_SIGN_H

#include <prc/core/functors/loop.hpp>
#include <prc/core/functors/sign.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Sign>, X>> = 0>
    static inline constexpr auto sign(X &&a)
    {
        return loop<Sign>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_SIGN_H
