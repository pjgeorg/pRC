// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_VIEW_H
#define pRC_CORE_TENSOR_FUNCTIONS_VIEW_H

#include <prc/core/tensor/views/reference.hpp>
#include <prc/core/tensor/views/reference_to_const.hpp>

namespace pRC
{
    template<class X, If<IsTensorView<RemoveReference<X>>> = 0>
    static inline constexpr X view(X &&a)
    {
        return forward<X>(a);
    }

    template<class T, Size... Ns>
    static inline constexpr auto view(Tensor<T, Ns...> const &a)
    {
        return TensorViews::ReferenceToConst(a);
    }

    template<class T, Size... Ns>
    static inline constexpr auto view(Tensor<T, Ns...> &a)
    {
        return TensorViews::Reference(a);
    }

    template<class T, Size... Ns>
    static inline constexpr auto view(Tensor<T, Ns...> const &&) = delete;
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_VIEW_H
