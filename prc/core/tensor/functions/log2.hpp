// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_LOG2_H
#define pRC_CORE_TENSOR_FUNCTIONS_LOG2_H

#include <prc/core/functors/log2.hpp>
#include <prc/core/functors/loop.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Log2>, X>> = 0>
    static inline constexpr auto log2(X &&a)
    {
        return loop<Log2>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_LOG2_H
