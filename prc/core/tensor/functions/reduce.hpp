// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_REDUCE_H
#define pRC_CORE_TENSOR_FUNCTIONS_REDUCE_H

#include <prc/core/functors/less.hpp>
#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/views/reduce.hpp>

namespace pRC
{
    template<class F, Index... Is>
    struct Reduce;

    template<class F, Index... Is, class X, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<IsInvocable<View, X>> = 0,
        If<IsSatisfied<(sizeof...(Is) <= typename R::Dimension())>> = 0,
        If<IsUnique<Constant<Index, Is>...>> = 0,
        If<IsSatisfied<(max(Is...) < typename R::Dimension())>> = 0,
        class E = ResultOf<X, typename R::Subscripts>,
        If<IsInvocable<F, E, E>> = 0,
        If<IsInvocable<F, decltype(F::template Identity<typename R::Type>()),
            E>> = 0>
    static inline constexpr auto reduce(X &&a)
    {
        using V = RemoveReference<decltype(view(forward<X>(a)))>;
        using T = RemoveConstReference<ResultOf<F, E, E>>;
        using Sizes = decltype(chip<Is...>(typename R::Sizes()));

        return TensorViews::Reduce<T, Sizes, F,
            decltype(sort(Sequence<Index, Is...>())), V>(view(forward<X>(a)));
    }

    template<class F, class X, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<IsInvocable<View, X>> = 0,
        class E = ResultOf<X, typename R::Subscripts>,
        If<IsInvocable<F, E, E>> = 0,
        If<IsInvocable<F, decltype(F::template Identity<typename R::Type>()),
            E>> = 0>
    static inline constexpr auto reduce(X &&a)
    {
        if constexpr(typename R::Dimension() == 0)
        {
            return view(forward<X>(a));
        }
        else
        {
            return expand(makeSeries<Index, typename R::Dimension{}>(),
                [&a](auto const... seq)
                {
                    return reduce<F, seq...>(forward<X>(a));
                });
        }
    }

    template<class F, Index... Is, class X, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<Not<IsInvocable<View, X>>> = 0,
        If<IsInvocable<Reduce<F, Is...>, X &>> = 0>
    static inline constexpr auto reduce(X &&a)
    {
        return eval(reduce<F, Is...>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_REDUCE_H
