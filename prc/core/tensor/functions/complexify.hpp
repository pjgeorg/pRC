// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_COMPLEXIFY_H
#define pRC_CORE_TENSOR_FUNCTIONS_COMPLEXIFY_H

#include <prc/core/functors/complexify.hpp>
#include <prc/core/functors/loop.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Complexify>, X>> = 0>
    static inline constexpr auto complexify(X &&a)
    {
        return loop<Complexify>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_COMPLEXIFY_H
