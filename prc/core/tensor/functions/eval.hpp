// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_EVAL_H
#define pRC_CORE_TENSOR_FUNCTIONS_EVAL_H

#include <prc/core/tensor/type_traits.hpp>

namespace pRC
{
    template<class X, If<IsTensor<RemoveReference<X>>> = 0>
    static inline constexpr X eval(X &&a)
    {
        return forward<X>(a);
    }

    template<class X, If<IsTensorView<RemoveReference<X>>> = 0>
    static inline constexpr auto eval(X &&a)
    {
        return Tensor(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_EVAL_H
