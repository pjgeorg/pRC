// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_ROUND_H
#define pRC_CORE_TENSOR_FUNCTIONS_ROUND_H

#include <prc/core/functors/loop.hpp>
#include <prc/core/functors/round.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<Size D = 0, class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Round<D>>, X>> = 0>
    static inline constexpr auto round(X &&a)
    {
        return loop<Round<D>>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_ROUND_H
