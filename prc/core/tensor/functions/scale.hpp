// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_SCALE_H
#define pRC_CORE_TENSOR_FUNCTIONS_SCALE_H

#include <prc/core/basic/functions/unit.hpp>
#include <prc/core/complex/type_traits.hpp>
#include <prc/core/functors/div.hpp>
#include <prc/core/functors/mul.hpp>
#include <prc/core/functors/tensor_product.hpp>
#include <prc/core/tensor/type_traits.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<IsTensorish<RB>> = 0, class DA = typename RA::Dimension,
        class DB = typename RB::Dimension,
        If<IsSatisfied<(DA() == 0 || DB() == 0)>> = 0,
        If<IsInvocable<TensorProduct, XA, XB>> = 0>
    static inline constexpr auto operator*(XA &&a, XB &&b)
    {
        return tensorProduct(forward<XA>(a), forward<XB>(b));
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<IsTensorish<RB>> = 0, class DA = typename RA::Dimension,
        class DB = typename RB::Dimension,
        If<IsSatisfied<(DA() == 0 && DB() != 0)>> = 0,
        If<IsInvocable<Div,
            decltype(unit<typename RB::template ChangeType<typename RA::Type>>(
                declval<XA>()())),
            XB>> = 0>
    static inline constexpr auto operator/(XA &&a, XB &&b)
    {
        return unit<typename RB::template ChangeType<typename RA::Type>>(
                   forward<XA>(a)()) /
            forward<XB>(b);
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<IsTensorish<RB>> = 0, class DA = typename RA::Dimension,
        class DB = typename RB::Dimension,
        If<IsSatisfied<(DA() != 0 && DB() == 0)>> = 0,
        If<IsInvocable<Div, XA,
            decltype(unit<typename RA::template ChangeType<typename RB::Type>>(
                declval<XB>()()))>> = 0>
    static inline constexpr auto operator/(XA &&a, XB &&b)
    {
        return forward<XA>(a) /
            unit<typename RA::template ChangeType<typename RB::Type>>(
                forward<XB>(b)());
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<Any<IsValue<RB>, IsComplex<RB>>> = 0,
        If<IsInvocable<Mul, XA, decltype(unit<Tensor<RB>>(declval<XB>()))>> = 0>
    static inline constexpr auto operator*(XA &&a, XB &&b)
    {
        return forward<XA>(a) * unit<Tensor<RB>>(forward<XB>(b));
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<Any<IsValue<RA>, IsComplex<RA>>> = 0,
        If<IsTensorish<RB>> = 0,
        If<IsInvocable<Mul, decltype(unit<Tensor<RA>>(declval<XA>())), XB>> = 0>
    static inline constexpr auto operator*(XA &&a, XB &&b)
    {
        return unit<Tensor<RA>>(forward<XA>(a)) * forward<XB>(b);
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<Any<IsValue<RB>, IsComplex<RB>>> = 0,
        If<IsInvocable<Div, XA, decltype(unit<Tensor<RB>>(declval<XB>()))>> = 0>
    static inline constexpr auto operator/(XA &&a, XB &&b)
    {
        return forward<XA>(a) / unit<Tensor<RB>>(forward<XB>(b));
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<Any<IsValue<RA>, IsComplex<RA>>> = 0,
        If<IsTensorish<RB>> = 0,
        If<IsInvocable<Div, decltype(unit<Tensor<RA>>(declval<XA>())), XB>> = 0>
    static inline constexpr auto operator/(XA &&a, XB &&b)
    {
        return unit<Tensor<RA>>(forward<XA>(a)) / forward<XB>(b);
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_SCALE_H
