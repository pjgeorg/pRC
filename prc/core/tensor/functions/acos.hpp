// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_ACOS_H
#define pRC_CORE_TENSOR_FUNCTIONS_ACOS_H

#include <prc/core/functors/acos.hpp>
#include <prc/core/functors/loop.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Acos>, X>> = 0>
    static inline constexpr auto acos(X &&a)
    {
        return loop<Acos>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_ACOS_H
