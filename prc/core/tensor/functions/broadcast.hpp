// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_BROADCAST_H
#define pRC_CORE_TENSOR_FUNCTIONS_BROADCAST_H

#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/views/broadcast.hpp>

namespace pRC
{
    template<Size... Bs>
    struct Broadcast;

    template<Size... Bs, class X, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<IsInvocable<View, X>> = 0,
        If<IsSatisfied<(sizeof...(Bs) == typename R::Dimension())>> = 0>
    static inline constexpr auto broadcast(X &&a)
    {
        using V = RemoveReference<decltype(view(forward<X>(a)))>;
        return TensorViews::Broadcast<typename R::Type,
            decltype(typename R::Sizes() * Sizes<Bs...>()), Sizes<Bs...>, V>(
            view(forward<X>(a)));
    }

    template<Size... Bs, class X, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<Not<IsInvocable<View, X>>> = 0,
        If<IsInvocable<Broadcast<Bs...>, X &>> = 0>
    static inline constexpr auto broadcast(X &&a)
    {
        return eval(broadcast<Bs...>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_BROADCAST_H
