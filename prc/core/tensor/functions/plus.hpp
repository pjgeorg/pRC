// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_PLUS_H
#define pRC_CORE_TENSOR_FUNCTIONS_PLUS_H

#include <prc/core/functors/loop.hpp>
#include <prc/core/functors/plus.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Plus>, X>> = 0>
    static inline constexpr auto operator+(X &&a)
    {
        return loop<Plus>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_PLUS_H
