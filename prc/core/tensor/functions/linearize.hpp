// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_LINEARIZE_H
#define pRC_CORE_TENSOR_FUNCTIONS_LINEARIZE_H

#include <prc/core/functors/reshape.hpp>
#include <prc/core/tensor/functions/reshape.hpp>

namespace pRC
{
    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
        If<IsSatisfied<(typename R::Dimension{} > 0)>> = 0,
        If<IsInvocable<Reshape<R::Sizes::size()>, X>> = 0>
    static inline constexpr auto linearize(X &&a)
    {
        return reshape<R::Sizes::size()>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_LINEARIZE_H
