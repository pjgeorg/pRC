// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_PERMUTE_H
#define pRC_CORE_TENSOR_FUNCTIONS_PERMUTE_H

#include <prc/core/basic/sequence.hpp>
#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/views/permute.hpp>

namespace pRC
{
    template<Index... Ps>
    struct Permute;

    template<Index... Ps, class X, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<IsInvocable<View, X>> = 0,
        If<IsSatisfied<(sizeof...(Ps) == typename R::Dimension())>> = 0,
        If<IsSatisfied<(max(Ps...) < typename R::Dimension())>> = 0,
        If<IsUnique<Constant<Index, Ps>...>> = 0>
    static inline constexpr auto permute(X &&a)
    {
        using V = RemoveReference<decltype(view(forward<X>(a)))>;
        return TensorViews::Permute<typename R::Type,
            decltype(permute<Ps...>(typename R::Sizes())),
            Sequence<Index, Ps...>, V>(view(forward<X>(a)));
    }

    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
        If<IsInvocable<View, X>> = 0,
        If<IsSatisfied<(0 == typename R::Dimension())>> = 0>
    static inline constexpr auto permute(X &&a)
    {
        return view(forward<X>(a));
    }

    template<Index... Ps, class X, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<Not<IsInvocable<View, X>>> = 0,
        If<IsInvocable<Permute<Ps...>, X &>> = 0>
    static inline constexpr auto permute(X &&a)
    {
        return eval(permute<Ps...>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_PERMUTE_H
