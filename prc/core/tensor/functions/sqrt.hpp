// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_SQRT_H
#define pRC_CORE_TENSOR_FUNCTIONS_SQRT_H

#include <prc/core/functors/loop.hpp>
#include <prc/core/functors/sqrt.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Sqrt>, X>> = 0>
    static inline constexpr auto sqrt(X &&a)
    {
        return loop<Sqrt>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_SQRT_H
