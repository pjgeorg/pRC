// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_SCALAR_H
#define pRC_CORE_TENSOR_FUNCTIONS_SCALAR_H

#include <prc/core/functors/reshape.hpp>
#include <prc/core/tensor/functions/reshape.hpp>

namespace pRC
{
    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
        If<IsSatisfied<(R::Sizes::size() == 1)>> = 0,
        If<IsInvocable<Reshape<>, X>> = 0>
    static inline constexpr auto scalar(X &&a)
    {
        return reshape<>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_SCALAR_H
