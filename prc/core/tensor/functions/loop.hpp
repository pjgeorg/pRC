// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_LOOP_H
#define pRC_CORE_TENSOR_FUNCTIONS_LOOP_H

#include <prc/core/basic/common.hpp>
#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/type_traits.hpp>
#include <prc/core/tensor/views/loop.hpp>

namespace pRC
{
    template<class F>
    struct Loop;

    template<class F, class... Xs,
        If<All<IsTensorish<RemoveReference<Xs>>...>> = 0,
        If<All<IsInvocable<View, Xs>...>> = 0,
        If<IsSame<typename RemoveReference<Xs>::Sizes...>> = 0,
        If<IsInvocable<F,
            ResultOf<Xs, typename RemoveReference<Xs>::Subscripts>...>> = 0>
    static inline constexpr auto loop(F &&f, Xs &&...args)
    {
        using T = RemoveConstReference<ResultOf<F,
            ResultOf<Xs, typename RemoveReference<Xs>::Subscripts>...>>;

        return TensorViews::Loop<T,
            Common<typename RemoveReference<Xs>::Sizes...>, F,
            RemoveReference<decltype(view(forward<Xs>(args)))>...>(
            forward<F>(f), view(forward<Xs>(args))...);
    }

    template<class F, class... Xs,
        If<All<IsTensorish<RemoveReference<Xs>>...>> = 0,
        If<All<IsInvocable<View, Xs>...>> = 0,
        If<IsSame<typename RemoveReference<Xs>::Sizes...>> = 0,
        If<IsInvocable<F,
            ResultOf<Xs, typename RemoveReference<Xs>::Subscripts>...>> = 0>
    static inline constexpr auto loop(Xs &&...args)
    {
        return loop(F(), forward<Xs>(args)...);
    }

    template<class F, class... Xs,
        If<All<IsTensorish<RemoveReference<Xs>>...>> = 0,
        If<Not<All<IsInvocable<View, Xs>...>>> = 0,
        If<IsInvocable<Loop<F>, F, Xs &...>> = 0>
    static inline constexpr auto loop(F &&f, Xs &&...args)
    {
        return eval(loop(forward<F>(f), args...));
    }

    template<class F, class... Xs,
        If<All<IsTensorish<RemoveReference<Xs>>...>> = 0,
        If<Not<All<IsInvocable<View, Xs>...>>> = 0,
        If<IsInvocable<Loop<F>, Xs &...>> = 0>
    static inline constexpr auto loop(Xs &&...args)
    {
        return eval(loop<F>(args...));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_LOOP_H
