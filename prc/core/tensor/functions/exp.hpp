// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_EXP_H
#define pRC_CORE_TENSOR_FUNCTIONS_EXP_H

#include <prc/core/functors/exp.hpp>
#include <prc/core/functors/loop.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Exp>, X>> = 0>
    static inline constexpr auto exp(X &&a)
    {
        return loop<Exp>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_EXP_H
