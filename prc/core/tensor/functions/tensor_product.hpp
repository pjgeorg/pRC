// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_TENSOR_PRODUCT_H
#define pRC_CORE_TENSOR_FUNCTIONS_TENSOR_PRODUCT_H

#include <prc/core/functors/tensor_product.hpp>
#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/views/tensor_product.hpp>

namespace pRC
{
    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<IsInvocable<View, XA>> = 0, If<IsTensorish<RB>> = 0,
        If<IsInvocable<View, XB>> = 0,
        class EA = ResultOf<XA, typename RA::Subscripts>,
        class EB = ResultOf<XB, typename RB::Subscripts>,
        If<IsInvocable<Mul, EA, EB>> = 0>
    static inline constexpr auto tensorProduct(XA &&a, XB &&b)
    {
        using VA = RemoveReference<decltype(view(forward<XA>(a)))>;
        using VB = RemoveReference<decltype(view(forward<XB>(b)))>;
        using T = RemoveConstReference<ResultOf<Mul, EA, EB>>;
        return TensorViews::TensorProduct<T,
            decltype(typename RA::Sizes(), typename RB::Sizes()), VA, VB>(
            view(forward<XA>(a)), view(forward<XB>(b)));
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<IsTensorish<RB>> = 0,
        If<Not<All<IsInvocable<View, XA>, IsInvocable<View, XB>>>> = 0,
        If<IsInvocable<TensorProduct, XA &, XB &>> = 0>
    static inline constexpr auto tensorProduct(XA &&a, XB &&b)
    {
        return eval(tensorProduct(a, b));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_TENSOR_PRODUCT_H
