// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_FMOD_H
#define pRC_CORE_TENSOR_FUNCTIONS_FMOD_H

#include <prc/core/functors/fmod.hpp>
#include <prc/core/functors/loop.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class XA, class XB, If<IsTensorish<RemoveReference<XA>>> = 0,
        If<IsTensorish<RemoveReference<XB>>> = 0,
        If<IsInvocable<Loop<FMod>, XA, XB>> = 0>
    static inline constexpr auto fmod(XA &&a, XB &&b)
    {
        return loop<FMod>(forward<XA>(a), forward<XB>(b));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_FMOD_H
