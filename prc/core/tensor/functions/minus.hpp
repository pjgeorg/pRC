// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_MINUS_H
#define pRC_CORE_TENSOR_FUNCTIONS_MINUS_H

#include <prc/core/functors/loop.hpp>
#include <prc/core/functors/minus.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Minus>, X>> = 0>
    static inline constexpr auto operator-(X &&a)
    {
        return loop<Minus>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_MINUS_H
