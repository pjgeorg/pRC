// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_TAN_H
#define pRC_CORE_TENSOR_FUNCTIONS_TAN_H

#include <prc/core/functors/loop.hpp>
#include <prc/core/functors/tan.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Tan>, X>> = 0>
    static inline constexpr auto tan(X &&a)
    {
        return loop<Tan>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_TAN_H
