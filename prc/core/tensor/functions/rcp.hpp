// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_RCP_H
#define pRC_CORE_TENSOR_FUNCTIONS_RCP_H

#include <prc/core/functors/loop.hpp>
#include <prc/core/functors/rcp.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Rcp>, X>> = 0>
    static inline constexpr auto rcp(X &&a)
    {
        return loop<Rcp>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_RCP_H
