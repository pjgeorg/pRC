// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_FLOOR_H
#define pRC_CORE_TENSOR_FUNCTIONS_FLOOR_H

#include <prc/core/functors/floor.hpp>
#include <prc/core/functors/loop.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Floor>, X>> = 0>
    static inline constexpr auto floor(X &&a)
    {
        return loop<Floor>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_FLOOR_H
