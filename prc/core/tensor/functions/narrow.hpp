// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_NARROW_H
#define pRC_CORE_TENSOR_FUNCTIONS_NARROW_H

#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/functions/exclude.hpp>

namespace pRC
{
    template<Index... Ns, class F, class... Xs,
        If<All<IsTensorish<RemoveReference<Xs>>...>> = 0,
        If<IsSame<decltype(
            chip<Ns...>(typename RemoveReference<Xs>::Sizes()))...>> = 0,
        If<True<decltype(chip<Ns...>(makeSeries<Index,
            Common<typename RemoveReference<Xs>::Dimension...>{}>()))>> = 0>
    static inline constexpr auto narrow(F &&f, Xs &&...args)
    {
        return expand(
            chip<Ns...>(makeSeries<Index,
                Common<typename RemoveReference<Xs>::Dimension...>{}>()),
            [&f, &args...](auto const... seq)
            {
                return exclude<seq...>(forward<F>(f), forward<Xs>(args)...);
            });
    }

    template<class F, Index... Ns, class... Xs,
        If<All<IsTensorish<RemoveReference<Xs>>...>> = 0,
        If<IsSame<decltype(
            chip<Ns...>(typename RemoveReference<Xs>::Sizes()))...>> = 0,
        If<True<decltype(chip<Ns...>(makeSeries<Index,
            Common<typename RemoveReference<Xs>::Dimension...>{}>()))>> = 0>
    static inline constexpr auto narrow(Xs &&...args)
    {
        return narrow<Ns...>(F(), forward<Xs>(args)...);
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_NARROW_H
