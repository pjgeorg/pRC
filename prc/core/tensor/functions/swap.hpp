// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_SWAP_H
#define pRC_CORE_TENSOR_FUNCTIONS_SWAP_H

#include <prc/core/basic/range.hpp>
#include <prc/core/functors/swap.hpp>
#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC
{
    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsTensorish<RA>> = 0,
        If<IsInvocable<View, XA>> = 0, If<IsTensorish<RB>> = 0,
        If<IsInvocable<View, XB>> = 0,
        If<IsSame<typename RA::Sizes, typename RB::Sizes>> = 0,
        class EA = ResultOf<XA, typename RA::Subscripts>,
        class EB = ResultOf<XB, typename RB::Subscripts>,
        If<IsInvocable<Swap, EA, EB>> = 0>
    static inline constexpr auto swap(XA &&a, XB &&b)
    {
        range<typename RA::Sizes>(
            [&a, &b](auto const... indices)
            {
                swap(forward<XA>(a)(indices...), forward<XB>(b)(indices...));
            });
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_SWAP_H
