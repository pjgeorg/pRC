// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_SQUEEZE_H
#define pRC_CORE_TENSOR_FUNCTIONS_SQUEEZE_H

#include <prc/core/basic/sequence.hpp>
#include <prc/core/functors/is_unit.hpp>
#include <prc/core/functors/not_fn.hpp>
#include <prc/core/tensor/functions/reshape.hpp>

namespace pRC
{
    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0>
    static inline constexpr auto squeeze(X &&a)
    {
        return expand(makeSeries<Index, typename R::Dimension{}>(),
            [&a](auto const... seq)
            {
                return expand(
                    filter<NotFn<IsUnit>>(Sequence<Size, R::size(seq)...>()),
                    [&a](auto const... sizes)
                    {
                        return reshape<sizes...>(forward<X>(a));
                    });
            });
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_SQUEEZE_H
