// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_ROTATE_H
#define pRC_CORE_TENSOR_FUNCTIONS_ROTATE_H

#include <prc/core/tensor/functions/permute.hpp>

namespace pRC
{
    template<Direction D, Size S = 1, class X, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0,
        If<IsSatisfied<(
            D == Direction::Leftwards || D == Direction::Rightwards)>> = 0>
    static inline constexpr auto rotate(X &&a)
    {
        return expand(
            rotate<D, S>(makeSeries<Index, typename R::Dimension{}>()),
            [&a](auto const... seq)
            {
                return permute<seq...>(forward<X>(a));
            });
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_ROTATE_H
