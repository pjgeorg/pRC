// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_CHIP_H
#define pRC_CORE_TENSOR_FUNCTIONS_CHIP_H

#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/views/chip.hpp>

namespace pRC
{
    template<Index... Ds>
    struct Chip;

    template<Index... Ds, class X, class... Is, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<IsInvocable<View, X>> = 0,
        If<All<IsConvertible<Is, Index>...>> = 0,
        If<IsSatisfied<(sizeof...(Is) == sizeof...(Ds))>> = 0,
        If<True<decltype(
            chip<Ds...>(makeSeries<Index, typename R::Dimension{}>()))>> = 0>
    static inline constexpr auto chip(X &&a, Is const... indices)
    {
        if constexpr(sizeof...(Ds) == 0)
        {
            return view(forward<X>(a));
        }
        else if constexpr(sizeof...(Ds) == 1)
        {
            using V = RemoveReference<decltype(view(forward<X>(a)))>;
            constexpr auto D = Sequence<Index, Ds...>::value(0);
            return TensorViews::Chip<typename R::Type,
                decltype(chip<D>(typename R::Sizes())), D, V>(
                view(forward<X>(a)), indices...);
        }
        else
        {
            return expand(
                makeRange<Index, 1, sizeof...(Ds)>(),
                [&a](auto const indices, auto const... seq)
                {
                    using S = Sequence<Index, Ds...>;
                    return chip<((S::value(seq) < S::value(0))
                            ? S::value(seq)
                            : S::value(seq) - 1)...>(
                        chip<S::value(0)>(a, indices[0]), indices[seq]...);
                },
                Indices<sizeof...(Ds)>(indices...));
        }
    }

    template<Index... Ds, class X, class... Is, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<Not<IsInvocable<View, X>>> = 0,
        If<IsInvocable<Chip<Ds...>, X &, Is...>> = 0>
    static inline constexpr auto chip(X &&a, Is const... indices)
    {
        return eval(chip<Ds...>(a, indices...));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_CHIP_H
