// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_ASIN_H
#define pRC_CORE_TENSOR_FUNCTIONS_ASIN_H

#include <prc/core/functors/asin.hpp>
#include <prc/core/functors/loop.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Asin>, X>> = 0>
    static inline constexpr auto asin(X &&a)
    {
        return loop<Asin>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_ASIN_H
