// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_CONJ_H
#define pRC_CORE_TENSOR_FUNCTIONS_CONJ_H

#include <prc/core/functors/conj.hpp>
#include <prc/core/functors/loop.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Conj>, X>> = 0>
    static inline constexpr auto conj(X &&a)
    {
        return loop<Conj>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_CONJ_H
