// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_PRINT_H
#define pRC_CORE_TENSOR_FUNCTIONS_PRINT_H

#include <prc/core/basic/range.hpp>
#include <prc/core/functors/eval.hpp>
#include <prc/core/tensor/functions/chip.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC
{
    template<class X, class S, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0>
    static inline void print(X &&arg, S &&stream)
    {
        if constexpr(typename R::Dimension() == 0)
        {
            return print(arg(), stream);
        }
        else
        {
            print(name<ResultOf<Eval, R>>(), stream);
            print(":\n", stream);
            range<typename R::Sizes>(
                [&arg, &stream](auto const... indices)
                {
                    print(typename R::Subscripts(indices...), stream);

                    print(": ", stream);
                    print(arg(indices...), stream);
                    print('\n', stream);
                });
        }
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_PRINT_H
