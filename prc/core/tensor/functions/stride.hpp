// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_STRIDE_H
#define pRC_CORE_TENSOR_FUNCTIONS_STRIDE_H

#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/views/stride.hpp>

namespace pRC
{
    template<Size... Ss>
    struct Stride;

    template<Size... Ss, class X, class... Os, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<IsInvocable<View, X>> = 0,
        If<All<IsConvertible<Os, Index>...>> = 0,
        If<IsSatisfied<(sizeof...(Ss) == typename R::Dimension())>> = 0,
        If<IsSatisfied<(sizeof...(Os) == typename R::Dimension())>> = 0,
        If<IsSatisfied<(
            typename R::Sizes() % Sizes<Ss...>() == Constant<Size, 0>())>> = 0>
    static inline constexpr auto stride(X &&a, Os const... offsets)
    {
        using V = RemoveReference<decltype(view(forward<X>(a)))>;
        return TensorViews::Stride<typename R::Type,
            decltype(typename R::Sizes() / Sizes<Ss...>()), Sizes<Ss...>, V>(
            view(forward<X>(a)), Subscripts<Ss...>(offsets...));
    }

    template<Size... Ss, class X, class... Os, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<Not<IsInvocable<View, X>>> = 0,
        If<IsInvocable<Stride<Ss...>, X &, Os...>> = 0>
    static inline constexpr auto stride(X &&a, Os const... offsets)
    {
        return eval(stride<Ss...>(a, offsets...));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_STRIDE_H
