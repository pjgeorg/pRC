// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_BACKWARDS_H
#define pRC_CORE_TENSOR_FUNCTIONS_BACKWARDS_H

#include <prc/core/tensor/functions/permute.hpp>

namespace pRC
{
    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
        If<IsInvocable<View, X>> = 0>
    static inline constexpr auto backwards(X &&a)
    {
        return expand(reverse(makeSeries<Index, typename R::Dimension{}>()),
            [&a](auto const... seq)
            {
                return permute<seq...>(forward<X>(a));
            });
    }

    template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
        If<Not<IsInvocable<View, X>>> = 0>
    static inline constexpr auto backwards(X &&a)
    {
        return eval(backwards(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_BACKWARDS_H
