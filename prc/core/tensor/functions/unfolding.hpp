// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_UNFOLDING_H
#define pRC_CORE_TENSOR_FUNCTIONS_UNFOLDING_H

#include <prc/core/functors/reshape.hpp>
#include <prc/core/tensor/functions/reshape.hpp>

namespace pRC
{
    template<Index K, class X, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0,
        If<IsSatisfied<(typename R::Dimension{} > 1)>> = 0,
        If<IsSatisfied<(K + 1 < typename R::Dimension{})>> = 0,
        If<IsInvocable<Reshape<R::size(K), R::Sizes::size() / R::size(K)>, X>> =
            0>
    static inline constexpr auto unfolding(X &&a)
    {
        return reshape<decltype(trim<0, K + 1>(typename R::Sizes()))::size(),
            decltype(trim<K + 1, typename R::Dimension{}>(
                typename R::Sizes{}))::size()>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_UNFOLDING_H
