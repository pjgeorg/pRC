// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_EXCLUDE_H
#define pRC_CORE_TENSOR_FUNCTIONS_EXCLUDE_H

#include <prc/core/basic/common.hpp>
#include <prc/core/functors/view.hpp>
#include <prc/core/tensor/views/exclude.hpp>

namespace pRC
{
    template<class F, Index... Es>
    struct Exclude;

    template<Index... Es, class F, class... Xs,
        If<All<IsTensorish<RemoveReference<Xs>>...>> = 0,
        If<All<IsInvocable<View, Xs>...>> = 0,
        If<IsSame<decltype(
            select<Es...>(typename RemoveReference<Xs>::Sizes()))...>> = 0,
        If<IsInvocable<F, ResultOf<pRC::Chip<Es...>, Xs, decltype(Es)...>...>> =
            0,
        If<True<decltype(select<Es...>(makeSeries<Index,
            typename RemoveReference<Xs>::Dimension{}>()))...>> = 0>
    static inline constexpr auto exclude(F &&f, Xs &&...args)
    {
        using ESizes = Common<decltype(
            select<Es...>(typename RemoveReference<Xs>::Sizes()))...>;
        using R = RemoveReference<
            ResultOf<F, ResultOf<pRC::Chip<Es...>, Xs, decltype(Es)...>...>>;
        using FSizes = typename R::Sizes;
        using Sizes = decltype(ESizes(), FSizes());
        using T = typename R::Type;

        return TensorViews::Exclude<T, Sizes, F, Sequence<Index, Es...>,
            RemoveReference<decltype(view(forward<Xs>(args)))>...>(
            forward<F>(f), view(forward<Xs>(args))...);
    }

    template<class F, Index... Es, class... Xs,
        If<All<IsTensorish<RemoveReference<Xs>>...>> = 0,
        If<All<IsInvocable<View, Xs>...>> = 0,
        If<IsSame<decltype(
            select<Es...>(typename RemoveReference<Xs>::Sizes()))...>> = 0,
        If<IsInvocable<F, ResultOf<pRC::Chip<Es...>, Xs, decltype(Es)...>...>> =
            0,
        If<True<decltype(select<Es...>(makeSeries<Index,
            typename RemoveReference<Xs>::Dimension{}>()))...>> = 0>
    static inline constexpr auto exclude(Xs &&...args)
    {
        return exclude<Es...>(F(), forward<Xs>(args)...);
    }

    template<Index... Es, class F, class... Xs,
        If<All<IsTensorish<RemoveReference<Xs>>...>> = 0,
        If<Not<All<IsInvocable<View, Xs>...>>> = 0,
        If<IsInvocable<Exclude<F, Es...>, Xs &...>> = 0>
    static inline constexpr auto exclude(F &&f, Xs &&...args)
    {
        return eval(exclude<Es...>(forward<F>(f), args...));
    }

    template<class F, Index... Es, class... Xs,
        If<All<IsTensorish<RemoveReference<Xs>>...>> = 0,
        If<Not<All<IsInvocable<View, Xs>...>>> = 0,
        If<IsInvocable<Exclude<F, Es...>, Xs &...>> = 0>
    static inline constexpr auto exclude(Xs &&...args)
    {
        return eval(exclude<F, Es...>(args...));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_EXCLUDE_H
