// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_HADAMARD_PRODUCT_H
#define pRC_CORE_TENSOR_FUNCTIONS_HADAMARD_PRODUCT_H

#include <prc/core/functors/loop.hpp>
#include <prc/core/functors/mul.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class XA, class XB, If<IsTensorish<RemoveReference<XA>>> = 0,
        If<IsTensorish<RemoveReference<XB>>> = 0,
        If<IsInvocable<Loop<Mul>, XA, XB>> = 0>
    static inline constexpr auto hadamardProduct(XA &&a, XB &&b)
    {
        return loop<Mul>(forward<XA>(a), forward<XB>(b));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_HADAMARD_PRODUCT_H
