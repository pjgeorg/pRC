// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_COS_H
#define pRC_CORE_TENSOR_FUNCTIONS_COS_H

#include <prc/core/functors/cos.hpp>
#include <prc/core/functors/loop.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Cos>, X>> = 0>
    static inline constexpr auto cos(X &&a)
    {
        return loop<Cos>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_COS_H
