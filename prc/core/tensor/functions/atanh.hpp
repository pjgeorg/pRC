// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_FUNCTIONS_ATANH_H
#define pRC_CORE_TENSOR_FUNCTIONS_ATANH_H

#include <prc/core/functors/atanh.hpp>
#include <prc/core/functors/loop.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class X, If<IsTensorish<RemoveReference<X>>> = 0,
        If<IsInvocable<Loop<Atanh>, X>> = 0>
    static inline constexpr auto atanh(X &&a)
    {
        return loop<Atanh>(forward<X>(a));
    }
}
#endif // pRC_CORE_TENSOR_FUNCTIONS_ATANH_H
