// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_UNIT_H
#define pRC_CORE_TENSOR_UNIT_H

#include <prc/core/basic/functions/unit.hpp>
#include <prc/core/functors/eval.hpp>
#include <prc/core/tensor/views/loop.hpp>

namespace pRC
{
    template<class T>
    struct Unit<T, If<IsTensorView<T>>> : Unit<ResultOf<Eval, T>>
    {
    };

    template<class T>
    struct Unit<T, If<IsTensor<T>>>
    {
        constexpr auto operator()()
        {
            auto const f = []()
            {
                return unit<typename T::Type>();
            };
            using F = RemoveConstReference<decltype(f)>;
            using Sizes = typename T::Sizes;
            return TensorViews::Loop<typename T::Type, Sizes, F>(f);
        }

        template<class X, If<IsConstructible<typename T::Type, X>> = 0>
        constexpr auto operator()(X &&value)
        {
            auto const f = [value = typename T::Type(forward<X>(value))]()
            {
                return value;
            };
            using F = RemoveConstReference<decltype(f)>;
            using Sizes = typename T::Sizes;
            return TensorViews::Loop<typename T::Type, Sizes, F>(f);
        }
    };
}
#endif // pRC_CORE_TENSOR_UNIT_H
