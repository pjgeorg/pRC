// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_BOOL_H
#define pRC_CORE_TENSOR_BOOL_H

#include <prc/core/basic/functions/identity.hpp>
#include <prc/core/basic/functions/unit.hpp>
#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/basic/sequence.hpp>
#include <prc/core/container/array.hpp>
#include <prc/core/functors/logical_and.hpp>
#include <prc/core/tensor/functions/reduce.hpp>
#include <prc/core/tensor/operator/functions/diagonal.hpp>
#include <prc/core/tensor/type_traits.hpp>
#include <prc/core/tensor/views/single.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<Size... Ns>
    class Tensor<Bool, Ns...>
    {
        static_assert((Ns * ... * 1) <= NumericLimits<Size>::max() &&
            typename pRC::Sizes<Ns...>::IsLinearizable());

    public:
        using Type = Bool;
        template<class C>
        using ChangeType = Tensor<C, Ns...>;

        using Subscripts = pRC::Subscripts<Ns...>;

        using Sizes = pRC::Sizes<Ns...>;
        template<Size... Ss>
        using ChangeSizes = Tensor<Bool, Ss...>;

        using Dimension = typename Sizes::Dimension;

        template<class E = typename Sizes::IsLinearizable, If<E> = 0>
        static constexpr auto size()
        {
            return Sizes::size();
        }

        static constexpr auto size(Index const dimension)
        {
            return Sizes::size(dimension);
        }

        template<class X, class... Is, If<IsConstructible<Bool, X>> = 0,
            If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0>
        static constexpr auto Single(X &&value, Is const... indices)
        {
            return Single(forward<X>(value), Subscripts(indices...));
        }

        template<class X, If<IsConstructible<Bool, X>> = 0>
        static constexpr auto Single(X &&value, Subscripts const &subscripts)
        {
            return TensorViews::Single<Bool, Sizes>(
                forward<X>(value), subscripts);
        }

    public:
        ~Tensor() = default;
        constexpr Tensor(Tensor const &) = default;
        constexpr Tensor(Tensor &&) = default;
        constexpr Tensor &operator=(Tensor const &) & = default;
        constexpr Tensor &operator=(Tensor &&) & = default;
        constexpr Tensor() = default;

        template<class X,
            If<IsAssignable<TensorViews::Reference<Bool, Sizes>, X>> = 0>
        constexpr Tensor(X &&other)
        {
            *this = forward<X>(other);
        }

        template<class X,
            If<IsAssignable<TensorViews::Reference<Bool, Sizes>, X>> = 0>
        constexpr auto &operator=(X &&rhs) &
        {
            view(*this) = forward<X>(rhs);
            return *this;
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) &&
        {
            return move(mData)(indices...);
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const &&
        {
            return move(mData)(indices...);
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) &
        {
            return mData(indices...);
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const &
        {
            return mData(indices...);
        }

        constexpr decltype(auto) operator()(Subscripts const &subscripts) &&
        {
            return move(mData)(subscripts);
        }

        constexpr decltype(auto) operator()(
            Subscripts const &subscripts) const &&
        {
            return move(mData)(subscripts);
        }

        constexpr decltype(auto) operator()(Subscripts const &subscripts) &
        {
            return mData(subscripts);
        }

        constexpr decltype(auto) operator()(
            Subscripts const &subscripts) const &
        {
            return mData(subscripts);
        }

        constexpr decltype(auto) operator[](Index const index) &&
        {
            return move(mData)[index];
        }

        constexpr decltype(auto) operator[](Index const index) const &&
        {
            return move(mData)[index];
        }

        constexpr decltype(auto) operator[](Index const index) &
        {
            return mData[index];
        }

        constexpr decltype(auto) operator[](Index const index) const &
        {
            return mData[index];
        }

        explicit constexpr operator Bool() const
        {
            return reduce<LogicalAnd>(*this)();
        }

    private:
        BEGIN_IGNORE_DIAGNOSTIC_GCC("-Wpedantic")
        Array<Bool, Ns...> mData;
        END_IGNORE_DIAGNOSTIC_GCC
    };
}
#endif // pRC_CORE_TENSOR_BOOL_H
