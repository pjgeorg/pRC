// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_ZERO_H
#define pRC_CORE_TENSOR_ZERO_H

#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/functors/eval.hpp>
#include <prc/core/tensor/views/loop.hpp>

namespace pRC
{
    template<class T>
    struct Zero<T, If<IsTensorView<T>>> : Zero<ResultOf<Eval, T>>
    {
    };

    template<class T>
    struct Zero<T, If<IsTensor<T>>>
    {
        constexpr auto operator()()
        {
            auto const f = []()
            {
                return zero<typename T::Type>();
            };
            using F = RemoveConstReference<decltype(f)>;
            using Sizes = typename T::Sizes;
            return TensorViews::Loop<typename T::Type, Sizes, F>(f);
        }
    };
}
#endif // pRC_CORE_TENSOR_ZERO_H
