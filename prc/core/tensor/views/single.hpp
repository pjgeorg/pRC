// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_VIEWS_SINGLE_H
#define pRC_CORE_TENSOR_VIEWS_SINGLE_H

#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, class N>
    class Single : public View<T, N, Single<T, N>>
    {
    private:
        using Base = View<T, N, Single>;

    public:
        template<class X, If<IsConstructible<T, X>> = 0>
        Single(X &&value, typename Base::Subscripts const &subscripts)
            : mValue(forward<X>(value))
            , mSubscripts(subscripts)
        {
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            return (*this)(typename Base::Subscripts(indices...));
        }

        constexpr T operator()(
            typename Base::Subscripts const &subscripts) const
        {
            if(subscripts == mSubscripts)
            {
                return mValue;
            }
            else
            {
                return zero();
            }
        }

    private:
        T const mValue;
        typename Base::Subscripts const mSubscripts;
    };
}
#endif // pRC_CORE_TENSOR_VIEWS_SINGLE_H
