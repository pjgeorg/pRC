// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_VIEWS_EXCLUDE_H
#define pRC_CORE_TENSOR_VIEWS_EXCLUDE_H

#include <prc/core/basic/sequence.hpp>
#include <prc/core/functors/chip.hpp>
#include <prc/core/functors/subscript.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, class N, class F, class E, class... Vs>
    class Exclude;

    template<class T, class N, class F, Index... Es, class... Vs>
    class Exclude<T, N, F, Sequence<Index, Es...>, Vs...>
        : public Conditional<
              IsAssignable<ResultOf<F,
                  ResultOf<pRC::Chip<Es...>, Vs, decltype(Es)...>...>>,
              Assignable<T, N, Exclude<T, N, F, Sequence<Index, Es...>, Vs...>>,
              View<T, N, Exclude<T, N, F, Sequence<Index, Es...>, Vs...>>>
    {
        static_assert(All<IsTensorView<Vs>...>());

    private:
        using Base = Conditional<
            IsAssignable<ResultOf<F,
                ResultOf<pRC::Chip<Es...>, Vs, decltype(Es)...>...>>,
            Assignable<T, N, Exclude>, View<T, N, Exclude>>;

    public:
        template<class... Xs, If<All<IsSame<Vs, RemoveReference<Xs>>...>> = 0>
        Exclude(F f, Xs &&...args)
            : mF(forward<F>(f))
            , mArgs(forward<Xs>(args)...)
        {
        }

        using Base::operator=;

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices)
        {
            return operator()(Indices<typename Base::Dimension{}>(indices...),
                makeSeriesFor<Index, Es...>(), makeSeriesFor<Index, Vs...>(),
                makeRange<Index, sizeof...(Es), typename Base::Dimension{}>());
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            return operator()(Indices<typename Base::Dimension{}>(indices...),
                makeSeriesFor<Index, Es...>(), makeSeriesFor<Index, Vs...>(),
                makeRange<Index, sizeof...(Es), typename Base::Dimension{}>());
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts)
        {
            return this->call(subscripts);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts) const
        {
            return this->call(subscripts);
        }

    private:
        template<Index... IEs, Index... Ops, Index... IOs>
        constexpr decltype(auto) operator()(
            Indices<typename Base::Dimension{}> const &indices,
            Sequence<Index, IEs...> const, Sequence<Index, Ops...> const,
            Sequence<Index, IOs...> const)
        {
            return mF(chip<Es...>(get<Ops>(mArgs), indices[IEs]...)...)(
                indices[IOs]...);
        }

        template<Index... IEs, Index... Ops, Index... IOs>
        constexpr decltype(auto) operator()(
            Indices<typename Base::Dimension{}> const &indices,
            Sequence<Index, IEs...> const, Sequence<Index, Ops...> const,
            Sequence<Index, IOs...> const) const
        {
            return mF(chip<Es...>(get<Ops>(mArgs), indices[IEs]...)...)(
                indices[IOs]...);
        }

    private:
        F mF;
        tuple<Vs...> mArgs;
    };
}
#endif // pRC_CORE_TENSOR_VIEWS_EXCLUDE_H
