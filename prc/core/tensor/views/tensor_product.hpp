// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_VIEWS_TENSOR_PRODUCT_H
#define pRC_CORE_TENSOR_VIEWS_TENSOR_PRODUCT_H

#include <prc/core/basic/sequence.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, class N, class VA, class VB>
    class TensorProduct : public View<T, N, TensorProduct<T, N, VA, VB>>
    {
        static_assert(IsTensorView<VA>());
        static_assert(IsTensorView<VB>());

    private:
        using Base = View<T, N, TensorProduct>;

    public:
        template<class XA, class XB, If<IsSame<VA, RemoveReference<XA>>> = 0,
            If<IsSame<VB, RemoveReference<XB>>> = 0>
        TensorProduct(XA &&a, XB &&b)
            : mA(forward<XA>(a))
            , mB(forward<XB>(b))
        {
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices)
        {
            return expand(
                       makeSeries<Index, typename VA::Dimension{}>(),
                       [this](auto const &indices,
                           auto const... seq) -> decltype(auto)
                       {
                           return mA(indices[seq]...);
                       },
                       Indices<sizeof...(Is)>(indices...)) *
                expand(
                    makeRange<Index, typename VA::Dimension{},
                        typename Base::Dimension{}>(),
                    [this](auto const &indices, auto const... seq) -> decltype(
                                                                       auto)
                    {
                        return mB(indices[seq]...);
                    },
                    Indices<sizeof...(Is)>(indices...));
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            return expand(
                       makeSeries<Index, typename VA::Dimension{}>(),
                       [this](auto const &indices,
                           auto const... seq) -> decltype(auto)
                       {
                           return mA(indices[seq]...);
                       },
                       Indices<sizeof...(Is)>(indices...)) *
                expand(
                    makeRange<Index, typename VA::Dimension{},
                        typename Base::Dimension{}>(),
                    [this](auto const &indices, auto const... seq) -> decltype(
                                                                       auto)
                    {
                        return mB(indices[seq]...);
                    },
                    Indices<sizeof...(Is)>(indices...));
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts)
        {
            return this->call(subscripts);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts) const
        {
            return this->call(subscripts);
        }

    private:
        VA mA;
        VB mB;
    };
}
#endif // pRC_CORE_TENSOR_VIEWS_TENSOR_PRODUCT_H
