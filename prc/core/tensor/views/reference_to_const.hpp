// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_VIEWS_REFERENCE_TO_CONST_H
#define pRC_CORE_TENSOR_VIEWS_REFERENCE_TO_CONST_H

#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, class N>
    class ReferenceToConst;

    template<class T, Size... Ns>
    ReferenceToConst(Tensor<T, Ns...> const &)
        -> ReferenceToConst<T, Sizes<Ns...>>;

    template<class T, Size... Ns>
    class ReferenceToConst<T, Sizes<Ns...>>
        : public View<T, Sizes<Ns...>, ReferenceToConst<T, Sizes<Ns...>>>
    {
    private:
        using Base = View<T, Sizes<Ns...>, ReferenceToConst>;

    public:
        ReferenceToConst(Tensor<T, Ns...> const &a)
            : mA(a)
        {
        }

        ReferenceToConst(Tensor<T, Ns...> const &&) = delete;

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            return mA(indices...);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts) const
        {
            return mA(subscripts);
        }

        constexpr decltype(auto) operator[](Index const index) const
        {
            return mA[index];
        }

    private:
        Tensor<T, Ns...> const &mA;
    };
}
#endif // pRC_CORE_TENSOR_VIEWS_REFERENCE_TO_CONST_H
