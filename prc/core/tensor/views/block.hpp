// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_VIEWS_BLOCK_H
#define pRC_CORE_TENSOR_VIEWS_BLOCK_H

#include <prc/core/basic/sequence.hpp>
#include <prc/core/log/log.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, class N, class B, class V>
    class Block;

    template<class T, Size... Ns, Size... Bs, class V>
    class Block<T, Sizes<Ns...>, Sizes<Bs...>, V>
        : public Conditional<IsAssignable<V>,
              Assignable<T, Sizes<Ns...>,
                  Block<T, Sizes<Ns...>, Sizes<Bs...>, V>>,
              View<T, Sizes<Ns...>, Block<T, Sizes<Ns...>, Sizes<Bs...>, V>>>
    {
        static_assert(IsTensorView<V>());

    private:
        using Base = Conditional<IsAssignable<V>,
            Assignable<T, Sizes<Ns...>, Block>, View<T, Sizes<Ns...>, Block>>;

    public:
        template<class X, If<IsSame<V, RemoveReference<X>>> = 0>
        Block(X &&a, Subscripts<Bs...> const &offsets)
            : mA(forward<X>(a))
            , mOffsets(offsets)
        {
            if constexpr(cDebugLevel >= DebugLevel::Low)
            {
                if(mOffsets.isOutOfRange())
                {
                    Logging::error("Block offset indices out of range.");
                }
            }
        }

        using Base::operator=;

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices)
        {
            return expand(
                makeSeriesFor<Index, Ns...>(),
                [this](auto const &indices, auto const... seq) -> decltype(auto)
                {
                    return mA((mOffsets[seq] * Ns + indices[seq])...);
                },
                Indices<sizeof...(Is)>(indices...));
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            return expand(
                makeSeriesFor<Index, Ns...>(),
                [this](auto const &indices, auto const... seq) -> decltype(auto)
                {
                    return mA((mOffsets[seq] * Ns + indices[seq])...);
                },
                Indices<sizeof...(Is)>(indices...));
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts)
        {
            return this->call(subscripts);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts) const
        {
            return this->call(subscripts);
        }

    private:
        V mA;
        BEGIN_IGNORE_DIAGNOSTIC_GCC("-Wpedantic")
        Subscripts<Bs...> const mOffsets;
        END_IGNORE_DIAGNOSTIC_GCC
    };
}
#endif // pRC_CORE_TENSOR_VIEWS_BLOCK_H
