// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_VIEWS_PERMUTE_H
#define pRC_CORE_TENSOR_VIEWS_PERMUTE_H

#include <prc/core/basic/sequence.hpp>
#include <prc/core/functors/subscript.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, class N, class P, class V>
    class Permute;

    template<class T, class N, Index... Ps, class V>
    class Permute<T, N, Sequence<Index, Ps...>, V>
        : public Conditional<IsAssignable<V>,
              Assignable<T, N, Permute<T, N, Sequence<Index, Ps...>, V>>,
              View<T, N, Permute<T, N, Sequence<Index, Ps...>, V>>>
    {
        static_assert(IsTensorView<V>());

    private:
        using Base = Conditional<IsAssignable<V>, Assignable<T, N, Permute>,
            View<T, N, Permute>>;

    public:
        template<class X, If<IsSame<V, RemoveReference<X>>> = 0>
        Permute(X &&a)
            : mA(forward<X>(a))
        {
        }

        using Base::operator=;

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices)
        {
            return expand(
                getPermutation(Sequence<Index, Ps...>()),
                [this](auto const &indices, auto const... seq) -> decltype(auto)
                {
                    return mA(indices[seq]...);
                },
                Indices<sizeof...(Is)>(indices...));
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            return expand(
                getPermutation(Sequence<Index, Ps...>()),
                [this](auto const &indices, auto const... seq) -> decltype(auto)
                {
                    return mA(indices[seq]...);
                },
                Indices<sizeof...(Is)>(indices...));
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts)
        {
            return this->call(subscripts);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts) const
        {
            return this->call(subscripts);
        }

        template<class E = IsSubscriptable<V>,
            class L = IsSatisfied<(
                Sequence<Index, Ps...>() == makeSeriesFor<Index, Ps...>())>,
            If<All<E, L>> = 0>
        constexpr decltype(auto) operator[](Index const index)
        {
            return mA[index];
        }

        template<class E = IsSubscriptable<V>,
            class L = IsSatisfied<(
                Sequence<Index, Ps...>() == makeSeriesFor<Index, Ps...>())>,
            If<All<E, L>> = 0>
        constexpr decltype(auto) operator[](Index const index) const
        {
            return mA[index];
        }

    private:
        V mA;
    };
}
#endif // pRC_CORE_TENSOR_VIEWS_PERMUTE_H
