// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_VIEWS_INFLATE_H
#define pRC_CORE_TENSOR_VIEWS_INFLATE_H

#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, class N, class B, class V>
    class Inflate;

    template<class T, class N, Size... Bs, class V>
    class Inflate<T, N, Sizes<Bs...>, V>
        : public View<T, N, Inflate<T, N, Sizes<Bs...>, V>>
    {
        static_assert(IsTensorView<V>());

    private:
        using Base = View<T, N, Inflate>;

    public:
        template<class X, If<IsSame<V, RemoveReference<X>>> = 0>
        Inflate(X &&a)
            : mA(forward<X>(a))
        {
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices)
        {
            return asConst(mA((indices / Bs)...));
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            return mA((indices / Bs)...);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts)
        {
            return this->call(subscripts);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts) const
        {
            return this->call(subscripts);
        }

    private:
        V mA;
    };
}
#endif // pRC_CORE_TENSOR_VIEWS_INFLATE_H
