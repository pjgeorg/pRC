// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_VIEWS_BROADCAST_H
#define pRC_CORE_TENSOR_VIEWS_BROADCAST_H

#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, class N, class B, class V>
    class Broadcast;

    template<class T, Size... Ns, Size... Bs, class V>
    class Broadcast<T, Sizes<Ns...>, Sizes<Bs...>, V>
        : public View<T, Sizes<Ns...>,
              Broadcast<T, Sizes<Ns...>, Sizes<Bs...>, V>>
    {
        static_assert(IsTensorView<V>());

    private:
        using Base = View<T, Sizes<Ns...>, Broadcast>;

    public:
        template<class X, If<IsSame<V, RemoveReference<X>>> = 0>
        Broadcast(X &&a)
            : mA(forward<X>(a))
        {
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices)
        {
            return asConst(mA((indices % (Ns / Bs))...));
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            return mA((indices % (Ns / Bs))...);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts)
        {
            return this->call(subscripts);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts) const
        {
            return this->call(subscripts);
        }

    private:
        V mA;
    };
}
#endif // pRC_CORE_TENSOR_VIEWS_BROADCAST_H
