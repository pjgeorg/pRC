// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_VIEWS_RANDOM_H
#define pRC_CORE_TENSOR_VIEWS_RANDOM_H

#include <prc/core/basic/functions/random.hpp>
#include <prc/core/basic/random.hpp>
#include <prc/core/log/log.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, class N, template<class...> class D>
    class Random : public View<T, N, Random<T, N, D>>
    {
    private:
        using Base = View<T, N, Random>;

    public:
        Random(RandomEngine &rng, D<typename T::Value> &distribution)
            : mRNG(rng)
            , mDistribution(distribution)
        {
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices)
        {
            return operator()(typename Base::Subscripts(indices...));
        }

        constexpr decltype(auto) operator()(
            [[maybe_unused]] typename Base::Subscripts const &subscripts)
        {
            if constexpr(cDebugLevel >= DebugLevel::Mid)
            {
                if(subscripts.isOutOfRange())
                {
                    Logging::error(
                        "Tensor View Random subscripts out of range.");
                }
            }

            return random<T>(mRNG, mDistribution);
        }

        constexpr decltype(auto) operator[]([[maybe_unused]] Index const index)
        {
            if constexpr(cDebugLevel >= DebugLevel::Mid)
            {
                if(!(index < Base::size()))
                {
                    Logging::error("Tensor View Random index out of range.");
                }
            }

            return random<T>(mRNG, mDistribution);
        }

    private:
        RandomEngine &mRNG;
        D<typename T::Value> &mDistribution;
    };
}
#endif // pRC_CORE_TENSOR_VIEWS_RANDOM_H
