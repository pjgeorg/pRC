// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_VIEWS_ASSIGNABLE_H
#define pRC_CORE_TENSOR_VIEWS_ASSIGNABLE_H

#include <prc/core/basic/functions/identity.hpp>
#include <prc/core/basic/functions/unit.hpp>
#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/basic/range.hpp>
#include <prc/core/functors/add.hpp>
#include <prc/core/functors/div.hpp>
#include <prc/core/functors/mul.hpp>
#include <prc/core/functors/sub.hpp>
#include <prc/core/tensor/functions/chip.hpp>
#include <prc/core/tensor/type_traits.hpp>
#include <prc/core/tensor/views/assignable_bool.hpp>

namespace pRC::TensorViews
{
    template<class T, class N, class F>
    class Assignable : public View<T, N, F>
    {
    private:
        using Base = View<T, N, F>;

    public:
        constexpr auto &operator=(Zero<> const)
        {
            return *this = zero<F>();
        }

        constexpr auto &operator=(Unit<> const)
        {
            return *this = unit<F>();
        }

        constexpr auto &operator=(Identity<> const)
        {
            return *this = identity<F>();
        }

        template<class X, class R = RemoveReference<X>,
            If<All<IsConvertible<R, T>,
                IsSatisfied<(typename Base::Dimension() == 0)>>> = 0>
        constexpr auto &operator=(X &&value)
        {
            (*this)() = forward<X>(value);
            return this->self();
        }

        template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
            If<IsSame<typename Base::Sizes, typename R::Sizes>> = 0,
            If<IsConvertible<typename R::Type, T>> = 0>
        constexpr auto &operator=(X &&rhs)
        {
            if constexpr(IsSubscriptable<Base>() && IsSubscriptable<R>())
            {
                range<Base::size()>(
                    [this, &rhs](auto const i)
                    {
                        (*this)[i] = forward<X>(rhs)[i];
                    });
            }
            else
            {
                range<typename Base::Sizes>(
                    [this, &rhs](auto const... indices)
                    {
                        (*this)(indices...) = forward<X>(rhs)(indices...);
                    });
            }
            return this->self();
        }

        template<class X, If<IsInvocable<Add, F, X>> = 0>
        constexpr auto operator+=(X &&rhs)
        {
            this->self() = this->self() + forward<X>(rhs);
            return this->self();
        }

        template<class X, If<IsInvocable<Sub, F, X>> = 0>
        constexpr auto operator-=(X &&rhs)
        {
            this->self() = this->self() - forward<X>(rhs);
            return this->self();
        }

        template<class X, If<IsInvocable<Mul, X, F>> = 0>
        constexpr auto applyOnTheLeft(X &&lhs)
        {
            using U = RemoveReference<X>;

            if constexpr(!IsTensorish<U>())
            {
                this->self() = forward<X>(lhs) * this->self();
                return this->self();
            }

            if constexpr(IsTensorish<U>())
            {
                if constexpr(typename U::Dimension() == 0)
                {
                    this->self() = eval(forward<X>(lhs)) * this->self();
                    return this->self();
                }
            }

            this->self() = eval(forward<X>(lhs) * this->self());
            return this->self();
        }

        template<class X, If<IsInvocable<Mul, F, X>> = 0>
        constexpr auto applyOnTheRight(X &&rhs)
        {
            using U = RemoveReference<X>;

            if constexpr(!IsTensorish<U>())
            {
                this->self() = this->self() * forward<X>(rhs);
                return this->self();
            }

            if constexpr(IsTensorish<U>())
            {
                if constexpr(typename U::Dimension() == 0)
                {
                    this->self() = this->self() * eval(forward<X>(rhs));
                    return this->self();
                }
            }

            this->self() = eval(this->self() * forward<X>(rhs));
            return this->self();
        }

        template<class X, If<IsInvocable<Mul, F, X>> = 0>
        constexpr auto operator*=(X &&rhs)
        {
            return applyOnTheRight(forward<X>(rhs));
        }

        template<class X, If<IsInvocable<Div, F, X>> = 0>
        constexpr auto operator/=(X &&rhs)
        {
            this->self() = this->self() / forward<X>(rhs);
            return this->self();
        }

    protected:
        ~Assignable() = default;
        constexpr Assignable(Assignable const &) = default;
        constexpr Assignable(Assignable &&) = default;
        constexpr Assignable &operator=(Assignable const &) = delete;
        constexpr Assignable &operator=(Assignable &&) = delete;
        constexpr Assignable() = default;
    };
}
#endif // pRC_CORE_TENSOR_VIEWS_ASSIGNABLE_H
