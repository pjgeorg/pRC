// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_VIEWS_VIEW_BOOL_H
#define pRC_CORE_TENSOR_VIEWS_VIEW_BOOL_H

#include <prc/core/basic/crtp.hpp>
#include <prc/core/basic/sequence.hpp>
#include <prc/core/container/subscripts.hpp>
#include <prc/core/functors/subscript.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<Size... Ns, class F>
    class View<Bool, Sizes<Ns...>, F> : public CRTP<F>
    {

    public:
        using Type = Bool;
        template<class C>
        using ChangeType = Tensor<C, Ns...>;

        using Subscripts = pRC::Subscripts<Ns...>;

        using Sizes = pRC::Sizes<Ns...>;
        template<Size... Ss>
        using ChangeSizes = Tensor<Bool, Ss...>;

        using Dimension = typename Sizes::Dimension;

        template<class E = typename Sizes::IsLinearizable, If<E> = 0>
        static constexpr auto size()
        {
            return Sizes::size();
        }

        static constexpr auto size(Index const dimension)
        {
            return Sizes::size(dimension);
        }

        template<class X, class... Is, If<IsConstructible<Bool, X>> = 0,
            If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0>
        static constexpr auto Single(X &&value, Is const... indices)
        {
            return Tensor<Bool, Ns...>::Single(forward<X>(value), indices...);
        }

        template<class X, If<IsConstructible<Bool, X>> = 0>
        static constexpr auto Single(X &&value, Subscripts const &subscripts)
        {
            return Tensor<Bool, Ns...>::Single(forward<X>(value), subscripts);
        }

    public:
        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices)
        {
            return (this->self())(indices...);
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            return (this->self())(indices...);
        }

        constexpr decltype(auto) operator()(Subscripts const &subscripts)
        {
            return (this->self())(subscripts);
        }

        constexpr decltype(auto) operator()(Subscripts const &subscripts) const
        {
            return (this->self())(subscripts);
        }

        template<class V = F, If<IsSubscriptable<V>> = 0>
        constexpr decltype(auto) operator[](Index const index)
        {
            return (this->self())[index];
        }

        template<class V = F, If<IsSubscriptable<V>> = 0>
        constexpr decltype(auto) operator[](Index const index) const
        {
            return (this->self())[index];
        }

        explicit constexpr operator Bool()
        {
            return static_cast<Bool>(eval(*this));
        }

        explicit constexpr operator Bool() const
        {
            return static_cast<Bool>(eval(*this));
        }

    protected:
        ~View() = default;
        constexpr View(View const &) = default;
        constexpr View(View &&) = default;
        constexpr View &operator=(View const &) = delete;
        constexpr View &operator=(View &&) = delete;
        constexpr View()
        {
            static_assert(IsInvocable<F, decltype(Ns)...>());
            static_assert(IsInvocable<F, Subscripts>());
            static_assert(IsBaseOf<View, F>());
        }

        constexpr decltype(auto) call(Subscripts const &subscripts)
        {
            return expand(
                makeSeriesFor<Index, Ns...>(),
                [this](auto const &subscripts, auto const... seq) -> decltype(
                                                                      auto)
                {
                    return (*this)(subscripts[seq]...);
                },
                subscripts);
        }

        constexpr decltype(auto) call(Subscripts const &subscripts) const
        {
            return expand(
                makeSeriesFor<Index, Ns...>(),
                [this](auto const &subscripts, auto const... seq) -> decltype(
                                                                      auto)
                {
                    return (*this)(subscripts[seq]...);
                },
                subscripts);
        }
    };
}
#endif // pRC_CORE_TENSOR_VIEWS_VIEW_BOOL_H
