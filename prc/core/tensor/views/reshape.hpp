// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_VIEWS_RESHAPE_H
#define pRC_CORE_TENSOR_VIEWS_RESHAPE_H

#include <prc/core/functors/subscript.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, class N, class V>
    class Reshape
        : public Conditional<IsAssignable<V>,
              Assignable<T, N, Reshape<T, N, V>>, View<T, N, Reshape<T, N, V>>>
    {
        static_assert(IsTensorView<V>());

    private:
        using Base = Conditional<IsAssignable<V>, Assignable<T, N, Reshape>,
            View<T, N, Reshape>>;

    public:
        template<class X, If<IsSame<V, RemoveReference<X>>> = 0>
        Reshape(X &&a)
            : mA(forward<X>(a))
        {
        }

        using Base::operator=;

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices)
        {
            return (*this)(typename Base::Subscripts(indices...));
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            return (*this)(typename Base::Subscripts(indices...));
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts)
        {
            return mA(typename V::Subscripts(Index(subscripts)));
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts) const
        {
            return mA(typename V::Subscripts(Index(subscripts)));
        }

        template<class E = IsSubscriptable<V>, If<E> = 0>
        constexpr decltype(auto) operator[](Index const index)
        {
            return mA[index];
        }

        template<class E = IsSubscriptable<V>, If<E> = 0>
        constexpr decltype(auto) operator[](Index const index) const
        {
            return mA[index];
        }

    private:
        V mA;
    };
}
#endif // pRC_CORE_TENSOR_VIEWS_RESHAPE_H
