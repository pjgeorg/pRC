// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_VIEWS_CHIP_H
#define pRC_CORE_TENSOR_VIEWS_CHIP_H

#include <prc/core/basic/sequence.hpp>
#include <prc/core/functors/subscript.hpp>
#include <prc/core/log/log.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, class N, Index D, class V>
    class Chip
        : public Conditional<IsAssignable<V>,
              Assignable<T, N, Chip<T, N, D, V>>, View<T, N, Chip<T, N, D, V>>>
    {
        static_assert(IsTensorView<V>());

    private:
        using Base = Conditional<IsAssignable<V>, Assignable<T, N, Chip>,
            View<T, N, Chip>>;

    public:
        template<class X, If<IsSame<V, RemoveReference<X>>> = 0>
        Chip(X &&a, Index const index)
            : mA(forward<X>(a))
            , mIndex(index)
        {
            if constexpr(cDebugLevel >= DebugLevel::Low)
            {
                if(!(mIndex < V::size(D)))
                {
                    Logging::error("Chip index out of range.");
                }
            }
        }

        using Base::operator=;

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices)
        {
            return operator()(Indices<sizeof...(Is)>(indices...),
                makeRange<Index, 0, D>(),
                makeRange<Index, D, typename Base::Dimension{}>());
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            return operator()(Indices<sizeof...(Is)>(indices...),
                makeRange<Index, 0, D>(),
                makeRange<Index, D, typename Base::Dimension{}>());
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts)
        {
            return this->call(subscripts);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts) const
        {
            return this->call(subscripts);
        }

        template<class E = IsSubscriptable<V>,
            class L1 = IsSatisfied<(D == typename Base::Dimension())>,
            class L2 = IsSatisfied<(D == 0)>, If<All<E, Any<L1, L2>>> = 0>
        constexpr decltype(auto) operator[](Index const index)
        {
            if constexpr(D == 0)
            {
                return mA[V::size(0) * index + mIndex];
            }
            else
            {
                return mA[Base::size() * mIndex + index];
            }
        }

        template<class E = IsSubscriptable<V>,
            class L1 = IsSatisfied<(D == typename Base::Dimension())>,
            class L2 = IsSatisfied<(D == 0)>, If<All<E, Any<L1, L2>>> = 0>
        constexpr decltype(auto) operator[](Index const index) const
        {
            if constexpr(D == 0)
            {
                return mA[V::size(0) * index + mIndex];
            }
            else
            {
                return mA[Base::size() * mIndex + index];
            }
        }

    private:
        template<Index... Ss, Index... Es>
        constexpr decltype(auto) operator()(
            Indices<typename Base::Dimension{}> const &indices,
            Sequence<Index, Ss...> const, Sequence<Index, Es...> const)
        {
            return mA(indices[Ss]..., mIndex, indices[Es]...);
        }

        template<Index... Ss, Index... Es>
        constexpr decltype(auto) operator()(
            Indices<typename Base::Dimension{}> const &indices,
            Sequence<Index, Ss...> const, Sequence<Index, Es...> const) const
        {
            return mA(indices[Ss]..., mIndex, indices[Es]...);
        }

    private:
        V mA;
        Index const mIndex;
    };
}
#endif // pRC_CORE_TENSOR_VIEWS_CHIP_H
