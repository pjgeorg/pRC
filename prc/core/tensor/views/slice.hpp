// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_VIEWS_SLICE_H
#define pRC_CORE_TENSOR_VIEWS_SLICE_H

#include <prc/core/basic/sequence.hpp>
#include <prc/core/log/log.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, class N, class O, class V>
    class Slice;

    template<class T, class N, Size... Os, class V>
    class Slice<T, N, Sizes<Os...>, V>
        : public Conditional<IsAssignable<V>,
              Assignable<T, N, Slice<T, N, Sizes<Os...>, V>>,
              View<T, N, Slice<T, N, Sizes<Os...>, V>>>
    {
        static_assert(IsTensorView<V>());

    private:
        using Base = Conditional<IsAssignable<V>, Assignable<T, N, Slice>,
            View<T, N, Slice>>;

    public:
        template<class X, If<IsSame<V, RemoveReference<X>>> = 0>
        Slice(X &&a, Subscripts<Os...> const &offsets)
            : mA(forward<X>(a))
            , mOffsets(offsets)
        {
            if constexpr(cDebugLevel >= DebugLevel::Low)
            {
                if(mOffsets.isOutOfRange())
                {
                    Logging::error("Slice offset indices out of range.");
                }
            }
        }

        using Base::operator=;

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices)
        {
            return expand(
                makeSeries<Index, typename Base::Dimension{}>(),
                [this](auto const &indices, auto const... seq) -> decltype(auto)
                {
                    return mA((mOffsets[seq] + indices[seq])...);
                },
                Indices<sizeof...(Is)>(indices...));
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            return expand(
                makeSeries<Index, typename Base::Dimension{}>(),
                [this](auto const &indices, auto const... seq) -> decltype(auto)
                {
                    return mA((mOffsets[seq] + indices[seq])...);
                },
                Indices<sizeof...(Is)>(indices...));
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts)
        {
            return this->call(subscripts);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts) const
        {
            return this->call(subscripts);
        }

    private:
        V mA;
        Subscripts<Os...> const mOffsets;
    };
}
#endif // pRC_CORE_TENSOR_VIEWS_SLICE_H
