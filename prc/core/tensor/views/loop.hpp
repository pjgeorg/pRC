// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_VIEWS_LOOP_H
#define pRC_CORE_TENSOR_VIEWS_LOOP_H

#include <prc/core/basic/sequence.hpp>
#include <prc/core/functors/subscript.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, class N, class F, class... Vs>
    class Loop
        : public Conditional<IsAssignable<ResultOf<F,
                                 ResultOf<Vs, typename Vs::Subscripts>...>>,
              Assignable<T, N, Loop<T, N, F, Vs...>>,
              View<T, N, Loop<T, N, F, Vs...>>>
    {
        static_assert(All<IsTensorView<Vs>...>());

    private:
        using Base = Conditional<
            IsAssignable<ResultOf<F, ResultOf<Vs, typename Vs::Subscripts>...>>,
            Assignable<T, N, Loop>, View<T, N, Loop>>;

    public:
        template<class... Xs, If<All<IsSame<Vs, RemoveReference<Xs>>...>> = 0>
        Loop(F f, Xs &&...args)
            : mF(forward<F>(f))
            , mArgs(forward<Xs>(args)...)
        {
        }

        using Base::operator=;

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices)
        {
            return operator()(typename Base::Subscripts(indices...));
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            return operator()(typename Base::Subscripts(indices...));
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts)
        {
            check(subscripts);
            return expand(makeSeriesFor<Index, Vs...>(),
                [this, &subscripts](auto const... ops) -> decltype(auto)
                {
                    return mF(get<ops>(mArgs)(subscripts)...);
                });
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts) const
        {
            check(subscripts);
            return expand(makeSeriesFor<Index, Vs...>(),
                [this, &subscripts](auto const... ops) -> decltype(auto)
                {
                    return mF(get<ops>(mArgs)(subscripts)...);
                });
        }

        template<class E = All<IsSubscriptable<Vs>...>, If<E> = 0>
        constexpr decltype(auto) operator[](Index const index)
        {
            check(index);
            return expand(makeSeriesFor<Index, Vs...>(),
                [this, &index](auto const... ops) -> decltype(auto)
                {
                    return mF(get<ops>(mArgs)[index]...);
                });
        }

        template<class E = All<IsSubscriptable<Vs>...>, If<E> = 0>
        constexpr decltype(auto) operator[](Index const index) const
        {
            check(index);
            return expand(makeSeriesFor<Index, Vs...>(),
                [this, &index](auto const... ops) -> decltype(auto)
                {
                    return mF(get<ops>(mArgs)[index]...);
                });
        }

    private:
        static constexpr auto check(
            [[maybe_unused]] typename Base::Subscripts const &subscripts)
        {
            if constexpr(sizeof...(Vs) == 0 && cDebugLevel >= DebugLevel::Mid)
            {
                if(subscripts.isOutOfRange())
                {
                    Logging::error("Tensor View Loop subscripts out of range.");
                }
            }
        }

        static constexpr auto check([[maybe_unused]] Index const index)
        {
            if constexpr(sizeof...(Vs) == 0 && cDebugLevel >= DebugLevel::Mid)
            {
                if(!(index < Base::size()))
                {
                    Logging::error("Tensor View Loop index out of range.");
                }
            }
        }

    private:
        F mF;
        tuple<Vs...> mArgs;
    };
}
#endif // pRC_CORE_TENSOR_VIEWS_LOOP_H
