// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_VIEWS_ASSIGNABLE_BOOL_H
#define pRC_CORE_TENSOR_VIEWS_ASSIGNABLE_BOOL_H

#include <prc/core/basic/functions/identity.hpp>
#include <prc/core/basic/functions/unit.hpp>
#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/basic/range.hpp>
#include <prc/core/functors/subscript.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class N, class F>
    class Assignable<Bool, N, F> : public View<Bool, N, F>
    {
    private:
        using Base = View<Bool, N, F>;

    public:
        constexpr auto &operator=(Zero<> const)
        {
            return *this = zero<F>();
        }

        constexpr auto &operator=(Unit<> const)
        {
            return *this = unit<F>();
        }

        constexpr auto &operator=(Identity<> const)
        {
            return *this = identity<F>();
        }

        template<class X, class R = RemoveReference<X>,
            If<All<IsConvertible<R, Bool>,
                IsSatisfied<(typename Base::Dimension() == 0)>>> = 0>
        constexpr auto &operator=(X &&value)
        {
            (*this)() = forward<X>(value);
            return this->self();
        }

        template<class X, class R = RemoveReference<X>, If<IsTensorish<R>> = 0,
            If<IsSame<typename Base::Sizes, typename R::Sizes>> = 0,
            If<IsConvertible<typename R::Type, Bool>> = 0>
        constexpr auto &operator=(X &&rhs)
        {
            if constexpr(IsSubscriptable<Base>() && IsSubscriptable<R>())
            {
                range<Base::size()>(
                    [this, &rhs](auto const i)
                    {
                        (*this)[i] = forward<X>(rhs)[i];
                    });
            }
            else
            {
                range<typename Base::Sizes>(
                    [this, &rhs](auto const... indices)
                    {
                        (*this)(indices...) = forward<X>(rhs)(indices...);
                    });
            }
            return this->self();
        }

    protected:
        ~Assignable() = default;
        constexpr Assignable(Assignable const &) = default;
        constexpr Assignable(Assignable &&) = default;
        constexpr Assignable &operator=(Assignable const &) = delete;
        constexpr Assignable &operator=(Assignable &&) = delete;
        constexpr Assignable() = default;
    };
}
#endif // pRC_CORE_TENSOR_VIEWS_ASSIGNABLE_BOOL_H
