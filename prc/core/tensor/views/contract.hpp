// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_VIEWS_CONTRACT_H
#define pRC_CORE_TENSOR_VIEWS_CONTRACT_H

#include <prc/core/basic/range.hpp>
#include <prc/core/basic/sequence.hpp>
#include <prc/core/tensor/functions/chip.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, class N, class S1, class S2, class V>
    class ContractUnary;

    template<class T, class N, Index... ILs, Index... IRs, class V>
    class ContractUnary<T, N, Sequence<Index, ILs...>, Sequence<Index, IRs...>,
        V>
        : public View<T, N,
              ContractUnary<T, N, Sequence<Index, ILs...>,
                  Sequence<Index, IRs...>, V>>
    {
        static_assert(IsTensorView<V>());
        static_assert(((V::size(ILs) == V::size(IRs)) && ...));

    private:
        using Base = View<T, N, ContractUnary>;

    public:
        template<class X, If<IsSame<V, RemoveReference<X>>> = 0>
        ContractUnary(X &&a)
            : mA(forward<X>(a))
        {
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices)
        {
            auto c = Add::template Identity<T>();

            range<Sizes<V::size(ILs)...>>(
                [this, &c, indices...](auto const... loop)
                {
                    c += chip<ILs..., IRs...>(mA, loop..., loop...)(indices...);
                });

            return c;
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            auto c = Add::template Identity<T>();

            range<Sizes<V::size(ILs)...>>(
                [this, &c, indices...](auto const... loop)
                {
                    c += chip<ILs..., IRs...>(mA, loop..., loop...)(indices...);
                });

            return c;
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts)
        {
            return this->call(subscripts);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts) const
        {
            return this->call(subscripts);
        }

    private:
        V mA;
    };
}
#endif // pRC_CORE_TENSOR_VIEWS_CONTRACT_H
