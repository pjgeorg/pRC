// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_VIEWS_REVERSE_H
#define pRC_CORE_TENSOR_VIEWS_REVERSE_H

#include <prc/core/functors/subscript.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, class N, class R, class V>
    class Reverse;

    template<class T, Size... Ns, Bool... Rs, class V>
    class Reverse<T, Sizes<Ns...>, Sequence<Bool, Rs...>, V>
        : public Conditional<IsAssignable<V>,
              Assignable<T, Sizes<Ns...>,
                  Reverse<T, Sizes<Ns...>, Sequence<Bool, Rs...>, V>>,
              View<T, Sizes<Ns...>,
                  Reverse<T, Sizes<Ns...>, Sequence<Bool, Rs...>, V>>>
    {
        static_assert(IsTensorView<V>());

    private:
        using Base =
            Conditional<IsAssignable<V>, Assignable<T, Sizes<Ns...>, Reverse>,
                View<T, Sizes<Ns...>, Reverse>>;

    public:
        template<class X, If<IsSame<V, RemoveReference<X>>> = 0>
        Reverse(X &&a)
            : mA(forward<X>(a))
        {
        }

        using Base::operator=;

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices)
        {
            return mA((Rs ? Ns - indices - 1 : indices)...);
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            return mA((Rs ? Ns - indices - 1 : indices)...);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts)
        {
            return this->call(subscripts);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts) const
        {
            return this->call(subscripts);
        }

        template<class E = IsSubscriptable<V>,
            class L = IsSame<Constant<Bool, Rs>...>, If<All<E, L>> = 0>
        constexpr decltype(auto) operator[](Index const index)
        {
            if constexpr((Rs && ...))
            {
                return mA[Base::size() - 1 - index];
            }
            else
            {
                return mA[index];
            }
        }

        template<class E = IsSubscriptable<V>,
            class L = IsSame<Constant<Bool, Rs>...>, If<All<E, L>> = 0>
        constexpr decltype(auto) operator[](Index const index) const
        {
            if constexpr((Rs && ...))
            {
                return mA[Base::size() - 1 - index];
            }
            else
            {
                return mA[index];
            }
        }

    private:
        V mA;
    };
}
#endif // pRC_CORE_TENSOR_VIEWS_REVERSE_H
