// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_VIEWS_REDUCE_H
#define pRC_CORE_TENSOR_VIEWS_REDUCE_H

#include <prc/core/basic/range.hpp>
#include <prc/core/basic/sequence.hpp>
#include <prc/core/functors/subscript.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, class N, class F, class R, class V>
    class Reduce;

    template<class T, class N, class F, Index... Rs, class V>
    class Reduce<T, N, F, Sequence<Index, Rs...>, V>
        : public View<T, N, Reduce<T, N, F, Sequence<Index, Rs...>, V>>
    {
        static_assert(IsTensorView<V>());

    private:
        using Base = View<T, N, Reduce>;

    public:
        template<class X, If<IsSame<V, RemoveReference<X>>> = 0>
        Reduce(X &&a)
            : mA(forward<X>(a))
        {
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices)
        {
            auto c = F::template Identity<T>();

            if constexpr(typename Base::Dimension() == 0)
            {
                if constexpr(IsSubscriptable<V>())
                {
                    range<V::size()>(
                        [this, &c](auto const i)
                        {
                            c = F()(c, mA[i]);
                        });
                }
                else
                {
                    range<typename V::Sizes>(
                        [this, &c](auto const... loop)
                        {
                            c = F()(c, mA(loop...));
                        });
                }
            }
            else
            {
                range<Sizes<V::size(Rs)...>>(
                    [this, &c, indices...](auto const... loop)
                    {
                        c = F()(c, chip<Rs...>(mA, loop...)(indices...));
                    });
            }

            return c;
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            auto c = F::template Identity<T>();

            if constexpr(typename Base::Dimension() == 0)
            {
                if constexpr(IsSubscriptable<V>())
                {
                    range<V::size()>(
                        [this, &c](auto const i)
                        {
                            c = F()(c, mA[i]);
                        });
                }
                else
                {
                    range<typename V::Sizes>(
                        [this, &c](auto const... loop)
                        {
                            c = F()(c, mA(loop...));
                        });
                }
            }
            else
            {
                range<Sizes<V::size(Rs)...>>(
                    [this, &c, indices...](auto const... loop)
                    {
                        c = F()(c, chip<Rs...>(mA, loop...)(indices...));
                    });
            }

            return c;
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts)
        {
            return this->call(subscripts);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts) const
        {
            return this->call(subscripts);
        }

    private:
        V mA;
    };
}
#endif // pRC_CORE_TENSOR_VIEWS_REDUCE_H
