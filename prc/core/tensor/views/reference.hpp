// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_VIEWS_REFERENCE_H
#define pRC_CORE_TENSOR_VIEWS_REFERENCE_H

#include <prc/core/tensor/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, class N>
    class Reference;

    template<class T, Size... Ns>
    Reference(Tensor<T, Ns...> &) -> Reference<T, Sizes<Ns...>>;

    template<class T, Size... Ns>
    class Reference<T, Sizes<Ns...>>
        : public Assignable<T, Sizes<Ns...>, Reference<T, Sizes<Ns...>>>
    {
    private:
        using Base = Assignable<T, Sizes<Ns...>, Reference>;

    public:
        Reference(Tensor<T, Ns...> &a)
            : mA(a)
        {
        }

        using Base::operator=;

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices)
        {
            return mA(indices...);
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == typename Base::Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            return asConst(mA)(indices...);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts)
        {
            return mA(subscripts);
        }

        constexpr decltype(auto) operator()(
            typename Base::Subscripts const &subscripts) const
        {
            return asConst(mA)(subscripts);
        }

        constexpr decltype(auto) operator[](Index const index)
        {
            return mA[index];
        }

        constexpr decltype(auto) operator[](Index const index) const
        {
            return asConst(mA)[index];
        }

    private:
        Tensor<T, Ns...> &mA;
    };
}
#endif // pRC_CORE_TENSOR_VIEWS_REFERENCE_H
