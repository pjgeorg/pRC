// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_VIEWS_VIEW_H
#define pRC_CORE_TENSOR_VIEWS_VIEW_H

#include <prc/core/basic/crtp.hpp>
#include <prc/core/basic/sequence.hpp>
#include <prc/core/complex/type_traits.hpp>
#include <prc/core/container/subscripts.hpp>
#include <prc/core/functors/subscript.hpp>
#include <prc/core/tensor/type_traits.hpp>
#include <prc/core/tensor/views/view_bool.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC::TensorViews
{
    template<class T, Size... Ns, class F>
    class View<T, Sizes<Ns...>, F> : public CRTP<F>
    {
        static_assert(IsValue<T>() || IsComplex<T>(),
            "Tensor<T, Ns..>: T has to be of type Value, Complex, or Bool.");

    public:
        using Type = T;
        template<class C>
        using ChangeType = Tensor<C, Ns...>;

        using Subscripts = pRC::Subscripts<Ns...>;

        using Sizes = pRC::Sizes<Ns...>;
        template<Size... Ss>
        using ChangeSizes = Tensor<T, Ss...>;

        using Dimension = typename Sizes::Dimension;

        using Value = typename T::Value;
        template<class V, If<IsValue<V>> = 0>
        using ChangeValue = Tensor<typename T::template ChangeValue<V>, Ns...>;

        using Signed = typename T::Signed;
        template<Bool R>
        using ChangeSigned =
            Tensor<typename T::template ChangeSigned<R>, Ns...>;

        using Width = typename T::Width;
        template<Size Q>
        using ChangeWidth = Tensor<typename T::template ChangeWidth<Q>, Ns...>;

        using IsComplexified = typename T::IsComplexified;
        using Complexify = Tensor<typename T::Complexify, Ns...>;
        using NonComplex = Tensor<typename T::NonComplex, Ns...>;

        template<class E = typename Sizes::IsLinearizable, If<E> = 0>
        static constexpr auto size()
        {
            return Sizes::size();
        }

        static constexpr auto size(Index const dimension)
        {
            return Sizes::size(dimension);
        }

        template<class X, class... Is, If<IsConstructible<T, X>> = 0,
            If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0>
        static constexpr auto Single(X &&value, Is const... indices)
        {
            return Tensor<T, Ns...>::Single(forward<X>(value), indices...);
        }

        template<class X, If<IsConstructible<T, X>> = 0>
        static constexpr auto Single(X &&value, Subscripts const &subscripts)
        {
            return Tensor<T, Ns...>::Single(forward<X>(value), subscripts);
        }

    public:
        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices)
        {
            return (this->self())(indices...);
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const
        {
            return (this->self())(indices...);
        }

        constexpr decltype(auto) operator()(Subscripts const &subscripts)
        {
            return (this->self())(subscripts);
        }

        constexpr decltype(auto) operator()(Subscripts const &subscripts) const
        {
            return (this->self())(subscripts);
        }

        template<class V = F, If<IsSubscriptable<V>> = 0>
        constexpr decltype(auto) operator[](Index const index)
        {
            return (this->self())[index];
        }

        template<class V = F, If<IsSubscriptable<V>> = 0>
        constexpr decltype(auto) operator[](Index const index) const
        {
            return (this->self())[index];
        }

        template<class E = IsSatisfied<(Dimension() == 0)>, If<E> = 0>
        explicit constexpr operator T()
        {
            return operator()();
        }

        template<class E = IsSatisfied<(Dimension() == 0)>, If<E> = 0>
        explicit constexpr operator T() const
        {
            return operator()();
        }

    protected:
        ~View() = default;
        constexpr View(View const &) = default;
        constexpr View(View &&) = default;
        constexpr View &operator=(View const &) = delete;
        constexpr View &operator=(View &&) = delete;
        constexpr View()
        {
            static_assert(IsInvocable<F, decltype(Ns)...>());
            static_assert(IsInvocable<F, Subscripts>());
            static_assert(IsBaseOf<View, F>());
        }

        constexpr decltype(auto) call(Subscripts const &subscripts)
        {
            return expand(
                makeSeriesFor<Index, Ns...>(),
                [this](auto const &subscripts, auto const... seq) -> decltype(
                                                                      auto)
                {
                    return (*this)(subscripts[seq]...);
                },
                subscripts);
        }

        constexpr decltype(auto) call(Subscripts const &subscripts) const
        {
            return expand(
                makeSeriesFor<Index, Ns...>(),
                [this](auto const &subscripts, auto const... seq) -> decltype(
                                                                      auto)
                {
                    return (*this)(subscripts[seq]...);
                },
                subscripts);
        }
    };
}
#endif // pRC_CORE_TENSOR_VIEWS_VIEW_H
