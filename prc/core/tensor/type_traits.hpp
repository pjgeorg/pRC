// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_TYPE_TRAITS_H
#define pRC_CORE_TENSOR_TYPE_TRAITS_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/container/type_traits.hpp>

namespace pRC
{
    template<class T, Size... Ns>
    class Tensor;

    template<class>
    struct IsTensor : False<>
    {
    };

    template<class T>
    struct IsTensor<T const> : IsTensor<T>
    {
    };

    template<class T, Index... Ns>
    struct IsTensor<Tensor<T, Ns...>> : True<>
    {
    };

    namespace TensorViews
    {
        template<class T, class N, class F>
        class View;

        template<class T, class N, class F>
        class Assignable;
    }

    template<class X, class T, class N, class F>
    static inline constexpr auto isTensorView(
        TensorViews::View<T, N, F> const &&)
    {
        if constexpr(IsReference<X>{})
        {
            return False<>();
        }
        else
        {
            return True<>();
        }
    }

    template<class>
    static inline constexpr auto isTensorView(...)
    {
        return False<>();
    }

    template<class T>
    struct IsTensorView : decltype(pRC::isTensorView<T>(declval<T>()))
    {
    };

    template<class T>
    using IsTensorish = Any<IsTensor<T>, IsTensorView<T>>;
}
#endif // pRC_CORE_TENSOR_TYPE_TRAITS_H
