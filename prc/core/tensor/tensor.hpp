// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_TENSOR_TENSOR_H
#define pRC_CORE_TENSOR_TENSOR_H

#include <prc/core/basic/functions/identity.hpp>
#include <prc/core/basic/functions/unit.hpp>
#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/basic/sequence.hpp>
#include <prc/core/complex/type_traits.hpp>
#include <prc/core/container/array.hpp>
#include <prc/core/functors/add.hpp>
#include <prc/core/functors/div.hpp>
#include <prc/core/functors/mul.hpp>
#include <prc/core/functors/sub.hpp>
#include <prc/core/tensor/bool.hpp>
#include <prc/core/tensor/type_traits.hpp>
#include <prc/core/tensor/views/single.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class F, class T, Size... Ns>
    Tensor(TensorViews::View<T, Sizes<Ns...>, F> const &) -> Tensor<T, Ns...>;

    template<class T, Size... Ns>
    class Tensor
    {
        static_assert((Ns * ... * 1) <= NumericLimits<Size>::max() &&
            typename pRC::Sizes<Ns...>::IsLinearizable());
        static_assert(IsValue<T>() || IsComplex<T>(),
            "Tensor<T, Ns..>: T has to be of type Value, Complex, or Bool.");

    public:
        using Type = T;
        template<class C>
        using ChangeType = Tensor<C, Ns...>;

        using Subscripts = pRC::Subscripts<Ns...>;

        using Sizes = pRC::Sizes<Ns...>;
        template<Size... Ss>
        using ChangeSizes = Tensor<T, Ss...>;

        using Dimension = typename Sizes::Dimension;

        using Value = typename T::Value;
        template<class V, If<IsValue<V>> = 0>
        using ChangeValue = Tensor<typename T::template ChangeValue<V>, Ns...>;

        using Signed = typename T::Signed;
        template<Bool R>
        using ChangeSigned =
            Tensor<typename T::template ChangeSigned<R>, Ns...>;

        using Width = typename T::Width;
        template<Size Q>
        using ChangeWidth = Tensor<typename T::template ChangeWidth<Q>, Ns...>;

        using IsComplexified = typename T::IsComplexified;
        using Complexify = Tensor<typename T::Complexify, Ns...>;
        using NonComplex = Tensor<typename T::NonComplex, Ns...>;

        template<class E = typename Sizes::IsLinearizable, If<E> = 0>
        static constexpr auto size()
        {
            return Sizes::size();
        }

        static constexpr auto size(Index const dimension)
        {
            return Sizes::size(dimension);
        }

        template<class X, class... Is, If<IsConstructible<T, X>> = 0,
            If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0>
        static constexpr auto Single(X &&value, Is const... indices)
        {
            return Single(forward<X>(value), Subscripts(indices...));
        }

        template<class X, If<IsConstructible<T, X>> = 0>
        static constexpr auto Single(X &&value, Subscripts const &subscripts)
        {
            return TensorViews::Single<T, Sizes>(forward<X>(value), subscripts);
        }

    public:
        ~Tensor() = default;
        constexpr Tensor(Tensor const &) = default;
        constexpr Tensor(Tensor &&) = default;
        constexpr Tensor &operator=(Tensor const &) & = default;
        constexpr Tensor &operator=(Tensor &&) & = default;
        constexpr Tensor() = default;

        template<class X,
            If<IsAssignable<TensorViews::Reference<T, Sizes>, X>> = 0>
        constexpr Tensor(X &&other)
        {
            *this = forward<X>(other);
        }

        template<class X,
            If<IsAssignable<TensorViews::Reference<T, Sizes>, X>> = 0>
        constexpr auto &operator=(X &&rhs) &
        {
            view(*this) = forward<X>(rhs);
            return *this;
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) &&
        {
            return move(mData)(indices...);
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const &&
        {
            return move(mData)(indices...);
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) &
        {
            return mData(indices...);
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0>
        constexpr decltype(auto) operator()(Is const... indices) const &
        {
            return mData(indices...);
        }

        constexpr decltype(auto) operator()(Subscripts const &subscripts) &&
        {
            return move(mData)(subscripts);
        }

        constexpr decltype(auto) operator()(
            Subscripts const &subscripts) const &&
        {
            return move(mData)(subscripts);
        }

        constexpr decltype(auto) operator()(Subscripts const &subscripts) &
        {
            return mData(subscripts);
        }

        constexpr decltype(auto) operator()(
            Subscripts const &subscripts) const &
        {
            return mData(subscripts);
        }

        constexpr decltype(auto) operator[](Index const index) &&
        {
            return move(mData)[index];
        }

        constexpr decltype(auto) operator[](Index const index) const &&
        {
            return move(mData)[index];
        }

        constexpr decltype(auto) operator[](Index const index) &
        {
            return mData[index];
        }

        constexpr decltype(auto) operator[](Index const index) const &
        {
            return mData[index];
        }

        template<class X, If<IsInvocable<Add, Tensor &, X>> = 0>
        constexpr auto &operator+=(X &&rhs) &
        {
            return *this = *this + forward<X>(rhs);
        }

        template<class X, If<IsInvocable<Sub, Tensor &, X>> = 0>
        constexpr auto &operator-=(X &&rhs) &
        {
            return *this = *this - forward<X>(rhs);
        }

        template<class X, If<IsInvocable<Mul, X, Tensor &>> = 0>
        constexpr auto &applyOnTheLeft(X &&lhs) &
        {
            view(*this).applyOnTheLeft(forward<X>(lhs));
            return *this;
        }

        template<class X, If<IsInvocable<Mul, Tensor &, X>> = 0>
        constexpr auto &applyOnTheRight(X &&rhs) &
        {
            view(*this).applyOnTheRight(forward<X>(rhs));
            return *this;
        }

        template<class X, If<IsInvocable<Mul, Tensor &, X>> = 0>
        constexpr auto &operator*=(X &&rhs) &
        {
            view(*this) *= forward<X>(rhs);
            return *this;
        }

        template<class X, If<IsInvocable<Div, Tensor &, X>> = 0>
        constexpr auto &operator/=(X &&rhs) &
        {
            return *this = *this / forward<X>(rhs);
        }

        template<class E = IsSatisfied<(Dimension() == 0)>, If<E> = 0>
        explicit constexpr operator T() const
        {
            return operator()();
        }

    private:
        BEGIN_IGNORE_DIAGNOSTIC_GCC("-Wpedantic")
        Array<T, Ns...> mData;
        END_IGNORE_DIAGNOSTIC_GCC
    };
}
#endif // pRC_CORE_TENSOR_TENSOR_H
