// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_CONTAINER_H
#define pRC_CORE_CONTAINER_H

#include <prc/core/container/allocation.hpp>
#include <prc/core/container/array.hpp>
#include <prc/core/container/array_heap.hpp>
#include <prc/core/container/array_scalar.hpp>
#include <prc/core/container/array_stack.hpp>
#include <prc/core/container/deque.hpp>
#include <prc/core/container/functions/equal.hpp>
#include <prc/core/container/functions/not_equal.hpp>
#include <prc/core/container/functions/print.hpp>
#include <prc/core/container/indices.hpp>
#include <prc/core/container/subscripts.hpp>
#include <prc/core/container/type_traits.hpp>

#endif // pRC_CORE_CONTAINER_H
