// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_H
#define pRC_CORE_BASIC_H

#include <prc/core/basic/common.hpp>
#include <prc/core/basic/context.hpp>
#include <prc/core/basic/crtp.hpp>
#include <prc/core/basic/direction.hpp>
#include <prc/core/basic/functions/bit_rotate_left.hpp>
#include <prc/core/basic/functions/bit_rotate_right.hpp>
#include <prc/core/basic/functions/ceil_div.hpp>
#include <prc/core/basic/functions/copy.hpp>
#include <prc/core/basic/functions/equal.hpp>
#include <prc/core/basic/functions/greater.hpp>
#include <prc/core/basic/functions/greater_equal.hpp>
#include <prc/core/basic/functions/identity.hpp>
#include <prc/core/basic/functions/ilog.hpp>
#include <prc/core/basic/functions/ipow.hpp>
#include <prc/core/basic/functions/is_even.hpp>
#include <prc/core/basic/functions/is_identity.hpp>
#include <prc/core/basic/functions/is_odd.hpp>
#include <prc/core/basic/functions/is_power_of_two.hpp>
#include <prc/core/basic/functions/is_unit.hpp>
#include <prc/core/basic/functions/is_zero.hpp>
#include <prc/core/basic/functions/isqrt.hpp>
#include <prc/core/basic/functions/less.hpp>
#include <prc/core/basic/functions/less_equal.hpp>
#include <prc/core/basic/functions/max.hpp>
#include <prc/core/basic/functions/min.hpp>
#include <prc/core/basic/functions/not_equal.hpp>
#include <prc/core/basic/functions/print.hpp>
#include <prc/core/basic/functions/random.hpp>
#include <prc/core/basic/functions/unit.hpp>
#include <prc/core/basic/functions/where.hpp>
#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/basic/identity.hpp>
#include <prc/core/basic/limits.hpp>
#include <prc/core/basic/lock.hpp>
#include <prc/core/basic/position.hpp>
#include <prc/core/basic/random.hpp>
#include <prc/core/basic/range.hpp>
#include <prc/core/basic/recursive_lambda.hpp>
#include <prc/core/basic/sequence.hpp>
#include <prc/core/basic/string.hpp>
#include <prc/core/basic/type_name.hpp>
#include <prc/core/basic/type_traits.hpp>
#include <prc/core/basic/unit.hpp>
#include <prc/core/basic/zero.hpp>

#endif // pRC_CORE_BASIC_H
