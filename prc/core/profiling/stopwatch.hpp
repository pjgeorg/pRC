// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_PROFILING_STOPWATCH_H
#define pRC_CORE_PROFILING_STOPWATCH_H

#include <cstdint>

extern "C"
{
#include <time.h>
}
#include <prc/core/log/log.hpp>
#include <prc/core/profiling/perfevent.hpp>
#include <prc/core/value/float.hpp>
#include <prc/core/value/functions/add.hpp>
#include <prc/core/value/functions/cast.hpp>
#include <prc/core/value/functions/div.hpp>
#include <prc/core/value/functions/sub.hpp>
#include <prc/core/value/zero.hpp>

namespace pRC
{
    static inline Float<64> getTimeInSeconds()
    {
        timespec timeSpec;
        if(clock_gettime(CLOCK_MONOTONIC, &timeSpec))
        {
            Logging::error("Unable to get current time.");
        }
        return static_cast<double>(timeSpec.tv_nsec) / 1e9 + timeSpec.tv_sec;
    }

    class Stopwatch
    {
    private:
        using Integer = UnsignedInteger<64>;
        using Float = pRC::Float<64>;

    public:
        ~Stopwatch() = default;
        Stopwatch(Stopwatch const &) = delete;
        Stopwatch(Stopwatch &&) = default;
        Stopwatch &operator=(Stopwatch const &) = delete;
        Stopwatch &operator=(Stopwatch &&) & = default;

        Stopwatch(Integer const flops = zero(), Integer const bytes = zero())
            : mFlops(flops)
            , mBytes(bytes)
        {
        }

        auto start()
        {
            mSeconds -= getTimeInSeconds();
            mPerfCycles.start();
        }

        auto stop()
        {
            mPerfCycles.stop();
            mSeconds += getTimeInSeconds();
        }

        auto reset()
        {
            mSeconds = zero();
            mPerfCycles.reset();
            mFlops = zero();
            mBytes = zero();
        }

        auto addFlops(Integer const flops)
        {
            mFlops += flops;
        }

        auto addBytes(Integer const bytes)
        {
            mBytes += bytes;
        }

        auto seconds() const
        {
            return mSeconds;
        }

        auto cycles() const
        {
            return mPerfCycles.value();
        }

        auto cyclesPerSecond() const
        {
            return cycles() / seconds();
        }

        auto flops() const
        {
            return mFlops;
        }

        auto flopsPerSecond() const
        {
            return flops() / seconds();
        }

        auto flopsPerCycle() const
        {
            return cast<Float>(flops()) / cycles();
        }

        auto bytes() const
        {
            return mBytes;
        }

        auto bytesPerSecond() const
        {
            return bytes() / seconds();
        }

        auto bytesPerCycle() const
        {
            return cast<Float>(bytes()) / cycles();
        }

    private:
        Float mSeconds = zero();
        Hardware::PerfEvent<Hardware::Events::Cycles> mPerfCycles;
        Integer mFlops;
        Integer mBytes;
    };
}
#endif // pRC_CORE_PROFILING_STOPWATCH_H
