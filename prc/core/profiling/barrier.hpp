// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_PROFILING_BARRIER_H
#define pRC_CORE_PROFILING_BARRIER_H

namespace pRC
{
    template<class T, class... Ts>
    [[gnu::always_inline]] static inline auto optimizationBarrier(
        const T &basic, Ts &...basics)
    {
#if defined(__clang__)
        asm volatile("#MARKER" : : "g"(basic) : "memory");
#else
        asm volatile("#MARKER" : : "i,r,m"(basic) : "memory");
#endif // __clang__

        if constexpr(sizeof...(Ts) == 0)
        {
            return;
        }
        else
        {
            return optimizationBarrier(basics...);
        }
    }
}
#endif // pRC_CORE_PROFILING_BARRIER_H
