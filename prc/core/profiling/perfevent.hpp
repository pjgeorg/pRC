// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_PROFILING_PERFEVENT_H
#define pRC_CORE_PROFILING_PERFEVENT_H

#include <cstdint>
#include <cstring>

extern "C"
{
#include <unistd.h>

#include <linux/perf_event.h>
#include <sys/ioctl.h>
#include <sys/syscall.h>
}

#include <prc/core/log/log.hpp>
#include <prc/core/value/integer.hpp>

namespace pRC::Hardware
{
    enum class Events
    {
        Cycles,
        Instructions,
        CacheReferences,
        CacheMisses,
    };

    template<Events Event>
    class PerfEvent
    {
    private:
        static constexpr auto type()
        {
            if constexpr(Event == Events::Cycles ||
                Event == Events::Instructions ||
                Event == Events::CacheReferences ||
                Event == Events::CacheMisses)
            {
                return PERF_TYPE_HARDWARE;
            }
        }

        static constexpr auto config()
        {
            if constexpr(Event == Events::Cycles)
            {
                return PERF_COUNT_HW_CPU_CYCLES;
            }
            else if constexpr(Event == Events::Instructions)
            {
                return PERF_COUNT_HW_INSTRUCTIONS;
            }
            else if constexpr(Event == Events::CacheReferences)
            {
                return PERF_COUNT_HW_CACHE_REFERENCES;
            }
            else if constexpr(Event == Events::CacheMisses)
            {
                return PERF_COUNT_HW_CACHE_MISSES;
            }
        }

    public:
        PerfEvent()
        {
            struct perf_event_attr pea;
            std::memset(&pea, 0, sizeof(struct perf_event_attr));
            pea.type = type();
            pea.size = sizeof(struct perf_event_attr);
            pea.config = config();
            pea.disabled = 1;
            pea.exclude_kernel = 1;
            pea.exclude_hv = 1;
            pea.inherit = 1;

            mFD = syscall(__NR_perf_event_open, &pea, 0, -1, -1, 0);
            if(mFD == -1)
            {
                Logging::warning("Unable to open perf event descriptor.");
            }
        }
        PerfEvent(PerfEvent &&other)
            : mFD(other.mFD)
            , mValue(other.mValue)
        {
            other.mFD = -1;
        }

        PerfEvent &operator=(PerfEvent &&rhs) &
        {
            mFD = rhs.mFD;
            mValue = rhs.mValue;
            rhs.mFD = -1;
        }

        ~PerfEvent()
        {
            close(mFD);
        }

        PerfEvent(PerfEvent const &) = delete;
        PerfEvent &operator=(PerfEvent const &) = delete;

        auto start()
        {
            if(mFD != -1)
            {
                ioctl(mFD, PERF_EVENT_IOC_RESET, 0);
                ioctl(mFD, PERF_EVENT_IOC_ENABLE, 0);
            }
            return;
        }

        auto stop()
        {
            if(mFD != -1)
            {
                ioctl(mFD, PERF_EVENT_IOC_DISABLE, 0);
                std::uint64_t value;
                if(read(mFD, &value, sizeof(std::uint64_t)) !=
                    sizeof(std::uint64_t))
                {
                    Logging::error("Unable to read perf event counter.");
                }
                mValue += value;
            }
            return;
        }

        auto reset()
        {
            if(mFD != -1)
            {
                ioctl(mFD, PERF_EVENT_IOC_RESET, 0);
                mValue = 0;
            }
        }

        auto value() const
        {
            return Integer(mValue);
        }

    private:
        int mFD = -1;
        std::uint64_t mValue = 0;
    };
}
#endif // pRC_CORE_PROFILING_PERFEVENT_H
