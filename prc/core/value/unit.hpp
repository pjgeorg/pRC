// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_UNIT_H
#define pRC_CORE_VALUE_UNIT_H

#include <prc/core/basic/functions/identity.hpp>
#include <prc/core/value/identity.hpp>

namespace pRC
{
    template<class T>
    struct Unit<T, If<IsValue<T>>>
    {
        constexpr auto operator()()
        {
            return identity<T>();
        }

        template<class X, If<IsConstructible<T, X>> = 0>
        constexpr auto operator()(X &&value)
        {
            return identity<T>(forward<X>(value));
        }
    };
}
#endif // pRC_CORE_VALUE_UNIT_H
