// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_ASSERTS_H
#define pRC_CORE_VALUE_ASSERTS_H

#include <prc/core/value/float.hpp>
#include <prc/core/value/integer.hpp>

namespace pRC
{
    static_assert(sizeof(UnsignedInteger<8>) == 1);
    static_assert(sizeof(UnsignedInteger<16>) == 2);
    static_assert(sizeof(UnsignedInteger<32>) == 4);
    static_assert(sizeof(UnsignedInteger<64>) == 8);

    static_assert(sizeof(SignedInteger<8>) == 1);
    static_assert(sizeof(SignedInteger<16>) == 2);
    static_assert(sizeof(SignedInteger<32>) == 4);
    static_assert(sizeof(SignedInteger<64>) == 8);

    static_assert(sizeof(Float<16>) == 2);
    static_assert(sizeof(Float<32>) == 4);
    static_assert(sizeof(Float<64>) == 8);
}
#endif // pRC_CORE_VALUE_ASSERTS_H
