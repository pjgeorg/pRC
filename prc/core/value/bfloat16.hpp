// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_BFLOAT16_H
#define pRC_CORE_VALUE_BFLOAT16_H

#include <cmath>
#include <cstring>

#include <prc/core/functors/add.hpp>
#include <prc/core/functors/div.hpp>
#include <prc/core/functors/mul.hpp>
#include <prc/core/functors/sub.hpp>
#include <prc/pragma.hpp>

namespace pRC
{
    class BFloat16
    {
    private:
        using Representation = std::uint16_t;
        using Float32 = float;

        static_assert(sizeof(Float32) == 2 * sizeof(Representation));

    public:
        static constexpr auto FromRepresentation(Representation const rep)
        {
            BFloat16 half = {};
            half.mHalf = rep;
            return half;
        }

    public:
        ~BFloat16() = default;
        constexpr BFloat16(BFloat16 const &) = default;
        constexpr BFloat16(BFloat16 &&) = default;
        constexpr BFloat16 &operator=(BFloat16 const &) & = default;
        constexpr BFloat16 &operator=(BFloat16 &&) & = default;
        constexpr BFloat16() = default;

        BFloat16(Float32 const single)
        {
            using std::isnan;
            if(isnan(single))
            {
                mHalf = 0x7FC0;
                return;
            }

#ifdef __FAST_MATH__
            auto src = reinterpret_cast<std::uint16_t const *>(&single);

#    if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
            mHalf = src[0];
#    else
            mHalf = src[1];
#    endif // __BYTE_ORDER__

#else
            std::uint32_t src;
            std::memcpy(&src, &single, sizeof(src));
            auto lsb = (src >> 16) & std::uint32_t(1);
            auto bias = 0x7FFF + lsb;
            src += bias;
            mHalf = static_cast<std::uint16_t>(src >> 16);
#endif // __FAST_MATH__
        }

        operator Float32() const
        {
            Float32 single;

            auto dst = reinterpret_cast<std::uint16_t *>(&single);

#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
            dst[0] = mHalf;
            dst[1] = 0;
#else
            dst[0] = 0;
            dst[1] = mHalf;
#endif // __BYTE_ORDER__

            BEGIN_IGNORE_DIAGNOSTIC_GCC("-Wuninitialized")
            return single;
            END_IGNORE_DIAGNOSTIC_GCC
        }

        constexpr auto isNaN() const
        {
#if !defined(__FINITE_MATH_ONLY__) || __FINITE_MATH_ONLY__ == 0
            return mHalf == 0x7FC0;
#else
            return false;
#endif // ! __FINITE_MATH_ONLY__
        }

        template<class X, If<IsInvocable<Add, BFloat16 &, X>> = 0>
        constexpr auto &operator+=(X &&rhs) &
        {
            return *this = *this + forward<X>(rhs);
        }

        template<class X, If<IsInvocable<Sub, BFloat16 &, X>> = 0>
        constexpr auto &operator-=(X &&rhs) &
        {
            return *this = *this - forward<X>(rhs);
        }

        template<class X, If<IsInvocable<Mul, BFloat16 &, X>> = 0>
        constexpr auto &operator*=(X &&rhs) &
        {
            return *this = *this * forward<X>(rhs);
        }

        template<class X, If<IsInvocable<Div, BFloat16 &, X>> = 0>
        constexpr auto &operator/=(X &&rhs) &
        {
            return *this = *this / forward<X>(rhs);
        }

        constexpr auto operator==(BFloat16 const rhs)
        {
            return mHalf == rhs.mHalf;
        }

        constexpr auto operator!=(BFloat16 const rhs)
        {
            return mHalf != rhs.mHalf;
        }

        auto operator+() const
        {
            return Float32(*this);
        }

        constexpr auto operator-() const
        {
            return FromRepresentation(mHalf ^ 0x8000);
        }

    private:
        Representation mHalf;
    };

    static inline constexpr auto isnan(BFloat16 const a)
    {
        return a.isNaN();
    }
}
#endif // pRC_CORE_VALUE_BFLOAT16_H
