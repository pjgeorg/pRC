// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_COMMON_H
#define pRC_CORE_VALUE_COMMON_H

#include <prc/core/basic/common.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class TA, class TB>
    struct CommonType<TA, TB, If<All<IsSignedInteger<TA>, IsSignedInteger<TB>>>>
    {
        using Type =
            Conditional<IsSatisfied<(sizeof(TA) > sizeof(TB))>, TA, TB>;
    };

    template<class TA, class TB>
    struct CommonType<TA, TB,
        If<All<IsUnsignedInteger<TA>, IsUnsignedInteger<TB>>>>
    {
        using Type =
            Conditional<IsSatisfied<(sizeof(TA) > sizeof(TB))>, TA, TB>;
    };

    template<class TA, class TB>
    struct CommonType<TA, TB,
        If<All<IsSignedInteger<TA>, IsUnsignedInteger<TB>>>>
    {
        using Type =
            Conditional<IsSatisfied<(sizeof(TA) > sizeof(TB))>, TA, TB>;
    };

    template<class TA, class TB>
    struct CommonType<TA, TB,
        If<All<IsUnsignedInteger<TA>, IsSignedInteger<TB>>>>
    {
        using Type =
            Conditional<IsSatisfied<(sizeof(TA) >= sizeof(TB))>, TA, TB>;
    };

    template<class TA, class TB>
    struct CommonType<TA, TB, If<All<IsFloat<TA>, IsFloat<TB>>>>
    {
        using Type =
            Conditional<IsSatisfied<(sizeof(TA) > sizeof(TB))>, TA, TB>;
    };

    template<class TA, class TB>
    struct CommonType<TA, TB, If<All<IsFloat<TA>, IsInteger<TB>>>>
        : CommonType<TA, Float<>>
    {
    };

    template<class TA, class TB>
    struct CommonType<TA, TB, If<All<IsInteger<TA>, IsFloat<TB>>>>
        : CommonType<Float<>, TB>
    {
    };
}
#endif // pRC_CORE_VALUE_COMMON_H
