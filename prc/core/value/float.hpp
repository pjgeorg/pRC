// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FLOAT_H
#define pRC_CORE_VALUE_FLOAT_H

#include <prc/core/basic/functions/identity.hpp>
#include <prc/core/basic/functions/unit.hpp>
#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/complex/type_traits.hpp>
#include <prc/core/functors/add.hpp>
#include <prc/core/functors/div.hpp>
#include <prc/core/functors/mul.hpp>
#include <prc/core/functors/sub.hpp>
#include <prc/core/log/log.hpp>
#include <prc/core/value/bfloat16.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<Size W>
    class Float
    {
    public:
        using Fundamental = Conditional<IsSatisfied<(W == 16)>, BFloat16,
            Conditional<IsSatisfied<(W == 32)>, float,
                Conditional<IsSatisfied<(W == 64)>, double, Undefined>>>;

        static_assert(IsDefined<Fundamental>(),
            "Float<W>: W has to be a supported Width (16, 32, 64).");

        using Value = Float<W>;
        template<class V, If<IsValue<V>> = 0>
        using ChangeValue = V;

        using Signed = True<>;
        template<Bool R>
        using ChangeSigned = Conditional<IsSatisfied<(R)>, Float<W>, Undefined>;

        using Width = pRC::Constant<Size, W>;
        template<Size Q>
        using ChangeWidth = Float<Q>;

        using IsComplexified = False<>;
        using Complexify = Complex<Float<W>>;
        using NonComplex = Float<W>;

    public:
        ~Float() = default;
        constexpr Float(Float const &) = default;
        constexpr Float(Float &&) = default;
        constexpr Float &operator=(Float const &) & = default;
        constexpr Float &operator=(Float &&) & = default;
        constexpr Float() = default;

        template<class U, If<IsConstructible<Fundamental, U>> = 0>
        constexpr Float(U const basic)
            : mValue(basic)
        {
        }

        template<class U, If<IsValue<U>> = 0>
        constexpr Float(U const &value)
            : mValue(value())
        {
        }

        template<class U, If<IsValue<U>> = 0>
        constexpr Float(Complex<U> const &value)
            : Float(value.real())
        {
        }

        constexpr Float(Zero<> const)
            : Float(zero<Float>())
        {
        }

        constexpr Float(Unit<> const)
            : Float(unit<Float>())
        {
        }

        constexpr Float(Identity<> const)
            : Float(identity<Float>())
        {
        }

        template<class U, If<IsAssignable<Fundamental, U>> = 0>
        constexpr auto &operator=(U const basic) &
        {
            mValue = basic;
            return *this;
        }

        template<class U, If<IsValue<U>> = 0>
        constexpr auto &operator=(U const &value) &
        {
            mValue = value();
            return *this;
        }

        template<class U, If<IsValue<U>> = 0>
        constexpr auto &operator=(Complex<U> const &value) &
        {
            return *this = value.real();
        }

        constexpr auto &operator=(Zero<> const) &
        {
            return *this = zero<Float>();
        }

        constexpr auto &operator=(Unit<> const) &
        {
            return *this = unit<Float>();
        }

        constexpr auto &operator=(Identity<> const) &
        {
            return *this = identity<Float>();
        }

        constexpr decltype(auto) operator()() &&
        {
            return move(mValue);
        }

        constexpr decltype(auto) operator()() const &&
        {
            return move(mValue);
        }

        constexpr auto &operator()() &
        {
            return mValue;
        }

        constexpr auto &operator()() const &
        {
            return mValue;
        }

        explicit constexpr operator Fundamental() const
        {
            return mValue;
        }

        template<class X, If<IsInvocable<Add, Float &, X>> = 0>
        constexpr auto &operator+=(X &&rhs) &
        {
            return *this = *this + forward<X>(rhs);
        }

        template<class X, If<IsInvocable<Sub, Float &, X>> = 0>
        constexpr auto &operator-=(X &&rhs) &
        {
            return *this = *this - forward<X>(rhs);
        }

        template<class X, If<IsInvocable<Mul, Float &, X>> = 0>
        constexpr auto &operator*=(X &&rhs) &
        {
            return *this = *this * forward<X>(rhs);
        }

        template<class X, If<IsInvocable<Div, Float &, X>> = 0>
        constexpr auto &operator/=(X &&rhs) &
        {
            return *this = *this / forward<X>(rhs);
        }

    private:
        Fundamental mValue;
    };

    Float(Float<16>::Fundamental const)->Float<16>;
    Float(Float<32>::Fundamental const)->Float<32>;
    Float(Float<64>::Fundamental const)->Float<64>;
}
#endif // pRC_CORE_VALUE_FLOAT_H
