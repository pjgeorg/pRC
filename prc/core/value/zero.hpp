// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_ZERO_H
#define pRC_CORE_VALUE_ZERO_H

#include <prc/core/value/float.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T>
    struct Zero<T, If<IsValue<T>>>
    {
        constexpr T operator()()
        {
            return 0;
        }
    };

    template<>
    struct Zero<Float<16>>
    {
        constexpr Float<16> operator()()
        {
            return BFloat16::FromRepresentation(0x0000);
        }
    };
}
#endif // pRC_CORE_VALUE_ZERO_H
