// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_RANDOM_H
#define pRC_CORE_VALUE_RANDOM_H

#include <prc/core/basic/random.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T, template<class...> class D>
    struct Random<T, D<T>, If<All<IsValue<T>, IsDistribution<D<T>>>>>
    {
    public:
        Random(RandomEngine &rng, D<T> &distribution)
            : mRNG(rng)
            , mDistribution(distribution)
        {
        }

        constexpr auto operator()()
        {
            return mDistribution(mRNG);
        }

    private:
        RandomEngine &mRNG;
        D<T> &mDistribution;
    };
}
#endif // pRC_CORE_VALUE_RANDOM_H
