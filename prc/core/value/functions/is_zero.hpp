// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_IS_ZERO_H
#define pRC_CORE_VALUE_FUNCTIONS_IS_ZERO_H

#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/functors/is_approx.hpp>
#include <prc/core/value/functions/is_approx.hpp>
#include <prc/core/value/limits.hpp>

namespace pRC
{
    template<class X, class R = RemoveReference<X>,
        class TT = typename R::Value, class T = Common<typename R::Value, TT>,
        If<IsValue<TT>> = 0, If<IsDefined<Zero<R>>> = 0,
        If<IsInvocable<Zero<R>>> = 0, If<IsInvocable<IsApprox, X, R>> = 0>
    static inline constexpr auto isZero(
        X &&a, TT const &tolerance = NumericLimits<TT>::tolerance())
    {
        return isApprox(forward<X>(a), zero<R>(), tolerance);
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_IS_ZERO_H
