// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_IMAG_H
#define pRC_CORE_VALUE_FUNCTIONS_IMAG_H

#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T, If<IsValue<T>> = 0>
    static inline constexpr auto imag(T const)
    {
        return zero<T>();
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_IMAG_H
