// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_POW_H
#define pRC_CORE_VALUE_FUNCTIONS_POW_H

#include <cmath>

#include <prc/core/value/float.hpp>
#include <prc/core/value/functions/cast.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class B, class N, If<IsFloat<B>> = 0, If<IsFloat<N>> = 0>
    static inline constexpr auto pow(B const &base, N const &exp)
    {
        return Float(std::pow(base(), exp()));
    }

    template<class B, class N, If<IsInteger<B>> = 0, If<IsFloat<N>> = 0>
    static inline constexpr auto pow(B const &base, N const &exp)
    {
        return pow(cast<Float<>>(base), exp);
    }

    template<class B, class N, If<IsFloat<B>> = 0, If<IsInteger<N>> = 0>
    static inline constexpr auto pow(B const &base, N const &exp)
    {
        return pow(base, cast<Float<>>(exp));
    }

    template<class B, class N, If<IsInteger<B>> = 0, If<IsInteger<N>> = 0>
    static inline constexpr auto pow(B const &base, N const &exp)
    {
        return pow(cast<Float<>>(base), cast<Float<>>(exp));
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_POW_H
