// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_LOG2_H
#define pRC_CORE_VALUE_FUNCTIONS_LOG2_H

#include <cmath>

#include <prc/core/value/float.hpp>
#include <prc/core/value/functions/cast.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T, If<IsFloat<T>> = 0>
    static inline constexpr auto log2(T const &a)
    {
        return Float(std::log2(a()));
    }

    template<class T, If<IsInteger<T>> = 0>
    static inline constexpr auto log2(T const &a)
    {
        return log2(cast<Float<>>(a));
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_LOG2_H
