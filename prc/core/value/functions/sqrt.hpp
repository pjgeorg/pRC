// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_SQRT_H
#define pRC_CORE_VALUE_FUNCTIONS_SQRT_H

#include <cmath>

#include <prc/core/value/float.hpp>
#include <prc/core/value/functions/cast.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T, If<IsFloat<T>> = 0>
    static inline constexpr auto sqrt(T const &a)
    {
        return Float(std::sqrt(a()));
    }

    template<class T, If<IsInteger<T>> = 0>
    static inline constexpr auto sqrt(T const &a)
    {
        return sqrt(cast<Float<>>(a));
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_SQRT_H
