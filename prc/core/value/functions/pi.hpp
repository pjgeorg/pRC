// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_PI_H
#define pRC_CORE_VALUE_FUNCTIONS_PI_H

#include <prc/core/value/float.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T, If<IsFloat<T>> = 0>
    static inline constexpr T pi()
    {
        if constexpr(typename T::Width() == 16)
        {
            return BFloat16::FromRepresentation(0x4049);
        }
        else
        {
            return 3.141592653589793238462643383279502884;
        }
    }

    template<class T, If<IsInteger<T>> = 0>
    static inline constexpr auto pi()
    {
        return pi<Float<>>();
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_PI_H
