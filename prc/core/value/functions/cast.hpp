// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_CAST_H
#define pRC_CORE_VALUE_FUNCTIONS_CAST_H

#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class C, If<IsValue<C>> = 0, class T, If<IsValue<T>> = 0,
        If<IsConstructible<C, typename T::Fundamental>> = 0>
    static inline constexpr auto cast(T const &a)
    {
        return C(a());
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_CAST_H
