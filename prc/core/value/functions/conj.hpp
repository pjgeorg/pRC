// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_CONJ_H
#define pRC_CORE_VALUE_FUNCTIONS_CONJ_H

#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T, If<IsValue<T>> = 0>
    static inline constexpr auto conj(T const &a)
    {
        return a;
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_CONJ_H
