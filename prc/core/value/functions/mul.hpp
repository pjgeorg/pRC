// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_MUL_H
#define pRC_CORE_VALUE_FUNCTIONS_MUL_H

#include <prc/core/value/float.hpp>
#include <prc/core/value/functions/cast.hpp>
#include <prc/core/value/integer.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class TA, class TB, If<IsFloat<TA>> = 0, If<IsFloat<TB>> = 0>
    static inline constexpr auto operator*(TA const &a, TB const &b)
    {
        return Float(a() * b());
    }

    template<class TA, class TB, If<IsInteger<TA>> = 0, If<IsFloat<TB>> = 0>
    static inline constexpr auto operator*(TA const &a, TB const &b)
    {
        return cast<Float<>>(a) * b;
    }

    template<class TA, class TB, If<IsFloat<TA>> = 0, If<IsInteger<TB>> = 0>
    static inline constexpr auto operator*(TA const &a, TB const &b)
    {
        return a * cast<Float<>>(b);
    }

    template<class TA, class TB, If<IsInteger<TA>> = 0, If<IsInteger<TB>> = 0>
    static inline constexpr auto operator*(TA const &a, TB const &b)
    {
        return Integer(a() * b());
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_MUL_H
