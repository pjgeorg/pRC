// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_SQUARE_H
#define pRC_CORE_VALUE_FUNCTIONS_SQUARE_H

#include <prc/core/value/functions/mul.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T, If<IsValue<T>> = 0>
    static inline constexpr auto square(T const &a)
    {
        return a * a;
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_SQUARE_H
