// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_ABSOLUTE_ERROR_H
#define pRC_CORE_VALUE_FUNCTIONS_ABSOLUTE_ERROR_H

#include <prc/core/functors/eval.hpp>
#include <prc/core/functors/norm.hpp>
#include <prc/core/functors/sub.hpp>
#include <prc/core/value/functions/eval.hpp>
#include <prc/core/value/functions/norm.hpp>
#include <prc/core/value/functions/sub.hpp>

namespace pRC
{
    template<class XA, class XB, If<IsInvocable<Sub, XA, XB>> = 0,
        If<IsInvocable<Norm<>, ResultOf<Sub, XA, XB>>> = 0,
        If<IsInvocable<Eval, ResultOf<Norm<>, ResultOf<Sub, XA, XB>>>> = 0>
    static inline constexpr auto absoluteError(XA &&a, XB &&b)
    {
        if constexpr(typename ResultOf<Sub, XA, XB>::Value::Signed() == false)
        {
            return eval(norm(delta(a, b)));
        }
        else
        {
            return eval(norm(a - b));
        }
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_ABSOLUTE_ERROR_H
