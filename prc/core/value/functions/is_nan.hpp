// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_IS_NAN_H
#define pRC_CORE_VALUE_FUNCTIONS_IS_NAN_H

#include <cmath>

#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T, If<IsFloat<T>> = 0>
    static inline constexpr auto isNaN([[maybe_unused]] T const &a)
    {
#if !defined(__FINITE_MATH_ONLY__) || __FINITE_MATH_ONLY__ == 0
        using std::isnan;
        return isnan(a());
#else
        return false;
#endif // ! __FINITE_MATH_ONLY__
    }

    template<class T, If<IsInteger<T>> = 0>
    static inline constexpr auto isNaN(T const &)
    {
        return false;
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_IS_NAN_H
