// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_LOG_H
#define pRC_CORE_VALUE_FUNCTIONS_LOG_H

#include <cmath>

#include <prc/core/value/float.hpp>
#include <prc/core/value/functions/cast.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T, If<IsFloat<T>> = 0>
    static inline constexpr auto log(T const &a)
    {
        return Float(std::log(a()));
    }

    template<class T, If<IsInteger<T>> = 0>
    static inline constexpr auto log(T const &a)
    {
        return log(cast<Float<>>(a));
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_LOG_H
