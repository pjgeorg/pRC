// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_RELATIVE_ERROR_H
#define pRC_CORE_VALUE_FUNCTIONS_RELATIVE_ERROR_H

#include <prc/core/functors/absolute_error.hpp>
#include <prc/core/functors/div.hpp>
#include <prc/core/functors/norm.hpp>
#include <prc/core/value/functions/absolute_error.hpp>
#include <prc/core/value/functions/cast.hpp>
#include <prc/core/value/functions/div.hpp>
#include <prc/core/value/functions/norm.hpp>

namespace pRC
{
    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsFloat<typename RA::Value>> = 0,
        If<IsFloat<typename RB::Value>> = 0,
        If<IsInvocable<AbsoluteError, XA, XB>> = 0,
        If<IsInvocable<Norm<>, XA>> = 0,
        If<IsInvocable<Div, ResultOf<AbsoluteError, XA, XB>,
            ResultOf<Norm<>, XA>>> = 0>
    static inline constexpr auto relativeError(XA &&a, XB &&b)
    {
        return absoluteError(a, b) / norm(a);
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>, If<IsFloat<typename RA::Value>> = 0,
        If<Not<IsFloat<typename RB::Value>>> = 0,
        If<IsInvocable<AbsoluteError, XA, XB>> = 0,
        If<IsInvocable<Norm<>, XA>> = 0,
        If<IsInvocable<Div, ResultOf<AbsoluteError, XA, XB>,
            ResultOf<Norm<>, XA>>> = 0>
    static inline constexpr auto relativeError(XA &&a, XB &&b)
    {
        return relativeError(a, cast<Float<>>(b));
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>,
        If<Not<IsFloat<typename RA::Value>>> = 0,
        If<IsFloat<typename RB::Value>> = 0,
        If<IsInvocable<AbsoluteError, XA, XB>> = 0,
        If<IsInvocable<Norm<>, XA>> = 0,
        If<IsInvocable<Div, ResultOf<AbsoluteError, XA, XB>,
            ResultOf<Norm<>, XA>>> = 0>
    static inline constexpr auto relativeError(XA &&a, XB &&b)
    {
        return relativeError(cast<Float<>>(a), b);
    }

    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>,
        If<Not<IsFloat<typename RA::Value>>> = 0,
        If<Not<IsFloat<typename RB::Value>>> = 0,
        If<IsInvocable<AbsoluteError, XA, XB>> = 0,
        If<IsInvocable<Norm<>, XA>> = 0,
        If<IsInvocable<Div, ResultOf<AbsoluteError, XA, XB>,
            ResultOf<Norm<>, XA>>> = 0>
    static inline constexpr auto relativeError(XA &&a, XB &&b)
    {
        return relativeError(cast<Float<>>(a), cast<Float<>>(b));
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_RELATIVE_ERROR_H
