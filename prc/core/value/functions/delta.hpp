// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_DELTA_H
#define pRC_CORE_VALUE_FUNCTIONS_DELTA_H

#include <prc/core/value/functions/abs.hpp>
#include <prc/core/value/functions/greater_equal.hpp>
#include <prc/core/value/functions/sub.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class TA, class TB, If<IsValue<TA>> = 0, If<IsValue<TB>> = 0,
        If<None<IsUnsignedInteger<TA>, IsUnsignedInteger<TB>>> = 0>
    static inline constexpr auto delta(TA const &a, TB const &b)
    {
        return abs(a - b);
    }

    template<class TA, class TB, If<IsValue<TA>> = 0, If<IsValue<TB>> = 0,
        If<Any<IsUnsignedInteger<TA>, IsUnsignedInteger<TB>>> = 0>
    static inline constexpr auto delta(TA const &a, TB const &b)
    {
        return a >= b ? (a - b) : (b - a);
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_DELTA_H
