// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_MEAN_H
#define pRC_CORE_VALUE_FUNCTIONS_MEAN_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/functors/add.hpp>
#include <prc/core/functors/fold.hpp>
#include <prc/core/value/functions/div.hpp>
#include <prc/core/value/functions/fold.hpp>

namespace pRC
{
    template<class... Xs, If<IsInvocable<Fold<Add>, Xs...>> = 0>
    static inline constexpr auto mean(Xs &&...args)
    {
        using R = RemoveConstReference<ResultOf<Fold<Add>, Xs...>>;
        return fold<Add>(forward<Xs>(args)...) /
            identity<typename R::Value>(sizeof...(args));
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_MEAN_H
