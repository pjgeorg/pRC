// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_EQUAL_H
#define pRC_CORE_VALUE_FUNCTIONS_EQUAL_H

#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class TA, class TB, If<IsValue<TA>> = 0, If<IsValue<TB>> = 0,
        If<Any<IsFloat<TA>, IsFloat<TB>>> = 0>
    static inline constexpr auto operator==(TA const &a, TB const &b)
    {
        return a() == b();
    }

    template<class TA, class TB, If<IsInteger<TA>> = 0, If<IsInteger<TB>> = 0>
    static inline constexpr auto operator==(TA const &a, TB const &b)
    {
        if constexpr(typename TA::Signed() == typename TB::Signed())
        {
            return a() == b();
        }
        else
        {
            if constexpr(typename TA::Signed() == true)
            {
                using UA = typename TA::template ChangeSigned<false>;
                return a < zero() ? false : UA(a) == b;
            }
            else
            {
                using UB = typename TB::template ChangeSigned<false>;
                return b < zero() ? false : a == UB(b);
            }
        }
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_EQUAL_H
