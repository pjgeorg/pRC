// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_FLOOR_H
#define pRC_CORE_VALUE_FUNCTIONS_FLOOR_H

#include <cmath>

#include <prc/core/value/float.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T, If<IsFloat<T>> = 0>
    static inline constexpr auto floor(T const &a)
    {
        return Float(std::floor(a()));
    }

    template<class T, If<IsInteger<T>> = 0>
    static inline constexpr auto floor(T const &a)
    {
        return a;
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_FLOOR_H
