// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_MINUS_H
#define pRC_CORE_VALUE_FUNCTIONS_MINUS_H

#include <prc/core/value/float.hpp>
#include <prc/core/value/integer.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T, If<IsFloat<T>> = 0>
    static inline constexpr auto operator-(T const &a)
    {
        return Float(-a());
    }

    template<class T, If<IsInteger<T>> = 0>
    static inline constexpr auto operator-(T const &a)
    {
        return Integer(-a());
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_MINUS_H
