// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_ATAN2_H
#define pRC_CORE_VALUE_FUNCTIONS_ATAN2_H

#include <cmath>

#include <prc/core/value/float.hpp>
#include <prc/core/value/functions/cast.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class Y, class X, If<IsFloat<Y>> = 0, If<IsFloat<X>> = 0>
    static inline constexpr auto atan2(Y const &y, X const &x)
    {
        return Float(std::atan2(y(), x()));
    }

    template<class Y, class X, If<IsInteger<Y>> = 0, If<IsFloat<X>> = 0>
    static inline constexpr auto atan2(Y const &y, X const &x)
    {
        return atan2(cast<Float<>>(y), x);
    }

    template<class Y, class X, If<IsFloat<Y>> = 0, If<IsInteger<X>> = 0>
    static inline constexpr auto atan2(Y const &y, X const &x)
    {
        return atan2(y, cast<Float<>>(x));
    }

    template<class Y, class X, If<IsInteger<Y>> = 0, If<IsInteger<X>> = 0>
    static inline constexpr auto atan2(Y const &y, X const &x)
    {
        return atan2(cast<Float<>>(y), cast<Float<>>(x));
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_ATAN2_H
