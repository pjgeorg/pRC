// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_SCALAR_PRODUCT_H
#define pRC_CORE_VALUE_FUNCTIONS_SCALAR_PRODUCT_H

#include <prc/core/value/functions/mul.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class TA, class TB, If<IsValue<TA>> = 0, If<IsValue<TB>> = 0>
    static inline constexpr auto scalarProduct(TA const &a, TB const &b)
    {
        return a * b;
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_SCALAR_PRODUCT_H
