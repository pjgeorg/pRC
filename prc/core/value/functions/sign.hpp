// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_SIGN_H
#define pRC_CORE_VALUE_FUNCTIONS_SIGN_H

#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/value/functions/greater.hpp>
#include <prc/core/value/functions/less.hpp>
#include <prc/core/value/functions/minus.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T, If<IsValue<T>> = 0,
        If<IsSatisfied<(typename T::Signed() == true)>> = 0>
    static inline constexpr auto sign(T const &a)
    {
        if(a < zero())
        {
            return -identity<T>();
        }

        if(a > zero())
        {
            return identity<T>();
        }

        return zero<T>();
    }

    template<class T, If<IsValue<T>> = 0,
        If<IsSatisfied<(typename T::Signed() == false)>> = 0>
    static inline constexpr auto sign(T const &a)
    {
        if(a > zero())
        {
            return identity<T>();
        }

        return zero<T>();
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_SIGN_H
