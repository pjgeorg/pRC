// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_PRINT_H
#define pRC_CORE_VALUE_FUNCTIONS_PRINT_H

#include <cstdio>

#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T, If<IsFloat<T>> = 0>
    static inline auto print(T const &value, std::FILE *stream)
    {
        constexpr auto format = []()
        {
            switch(sizeof(T))
            {
                case 2:
                    return "% .4e";
                    ;
                case 4:
                    return "% .8e";
                    ;
                default:
                    return "% .15e";
                    ;
            }
        }();

        std::fprintf(stream, format, static_cast<double>(value()));
    }

    template<class T, If<IsSignedInteger<T>> = 0>
    static inline auto print(T const &value, std::FILE *stream)
    {
        if constexpr(IsSame<typename T::Fundamental, long long>())
        {
            std::fprintf(stream, "% lld", value());
        }
        else if constexpr(IsSame<typename T::Fundamental, long>())
        {
            std::fprintf(stream, "% ld", value());
        }
        else
        {
            std::fprintf(stream, "% d", value());
        }
    }

    template<class T, If<IsUnsignedInteger<T>> = 0>
    static inline auto print(T const &value, std::FILE *stream)
    {
        if constexpr(IsSame<typename T::Fundamental, unsigned long long>())
        {
            std::fprintf(stream, "%llu", value());
        }
        else if constexpr(IsSame<typename T::Fundamental, unsigned long>())
        {
            std::fprintf(stream, "%lu", value());
        }
        else
        {
            std::fprintf(stream, "%u", value());
        }
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_PRINT_H
