// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_ROUND_H
#define pRC_CORE_VALUE_FUNCTIONS_ROUND_H

#include <cmath>

#include <prc/core/basic/functions/ipow.hpp>
#include <prc/core/value/functions/div.hpp>
#include <prc/core/value/functions/mul.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<Size D = 0, class T, If<IsFloat<T>> = 0>
    static inline constexpr auto round(T const &a)
    {
        constexpr T scale = iPow(Size(10), D);
        return Float(std::round((a * scale)())) / scale;
    }

    template<Size D = 0, class T, If<IsInteger<T>> = 0>
    static inline constexpr auto round(T const &a)
    {
        return a;
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_ROUND_H
