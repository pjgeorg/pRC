// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_COMPLEXIFY_H
#define pRC_CORE_VALUE_FUNCTIONS_COMPLEXIFY_H

#include <prc/core/complex/type_traits.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T, If<IsValue<T>> = 0,
        If<IsConstructible<Complex<T>, T>> = 0>
    static inline constexpr auto complexify(T const &a)
    {
        return Complex<T>(a);
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_COMPLEXIFY_H
