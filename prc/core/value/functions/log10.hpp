// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_LOG10_H
#define pRC_CORE_VALUE_FUNCTIONS_LOG10_H

#include <cmath>

#include <prc/core/value/float.hpp>
#include <prc/core/value/functions/cast.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T, If<IsFloat<T>> = 0>
    static inline constexpr auto log10(T const &a)
    {
        return Float(std::log10(a()));
    }

    template<class T, If<IsInteger<T>> = 0>
    static inline constexpr auto log10(T const &a)
    {
        return log10(cast<Float<>>(a));
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_LOG10_H
