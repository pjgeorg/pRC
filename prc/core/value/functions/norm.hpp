// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_NORM_H
#define pRC_CORE_VALUE_FUNCTIONS_NORM_H

#include <prc/core/value/functions/abs.hpp>
#include <prc/core/value/functions/square.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<Index P = 2, Index Q = P, class T, If<IsValue<T>> = 0>
    static inline constexpr auto norm(T const &a)
    {
        if constexpr(P == Q)
        {
            return abs(a);
        }
        else if constexpr(P == 2 && Q == 1)
        {
            return square(a);
        }
        else if constexpr(Q == 0)
        {
            return norm<P, P>(a);
        }
        else
        {
            static_assert(P != P, "Unsupported p-norm.");
        }
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_NORM_H
