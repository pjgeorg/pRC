// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_LESS_EQUAL_H
#define pRC_CORE_VALUE_FUNCTIONS_LESS_EQUAL_H

#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class TA, class TB, If<IsValue<TA>> = 0, If<IsValue<TB>> = 0>
    static inline constexpr auto operator<=(TA const &a, TB const &b)
    {
        return !(a > b);
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_LESS_EQUAL_H
