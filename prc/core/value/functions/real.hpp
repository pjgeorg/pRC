// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_REAL_H
#define pRC_CORE_VALUE_FUNCTIONS_REAL_H

#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class X, If<IsValue<RemoveReference<X>>> = 0>
    static inline constexpr X real(X &&a)
    {
        return forward<X>(a);
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_REAL_H
