// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_ABS_H
#define pRC_CORE_VALUE_FUNCTIONS_ABS_H

#include <prc/core/value/functions/greater_equal.hpp>
#include <prc/core/value/functions/minus.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T, If<IsValue<T>> = 0,
        If<IsSatisfied<(typename T::Signed() == true)>> = 0>
    static inline constexpr auto abs(T const &a)
    {
        return (a >= zero()) ? a : -a;
    }

    template<class T, If<IsValue<T>> = 0,
        If<IsSatisfied<(typename T::Signed() == false)>> = 0>
    static inline constexpr auto abs(T const &a)
    {
        return a;
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_ABS_H
