// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_FOLD_H
#define pRC_CORE_VALUE_FUNCTIONS_FOLD_H

#include <prc/core/basic/direction.hpp>
#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<class F, Bool E = false, Direction D = Direction::Forwards>
    struct Fold;

    template<class F, Bool E = false, Direction D = Direction::Forwards,
        If<IsSatisfied<(
            D == Direction::Forwards || D == Direction::Backwards)>> = 0,
        class XA, If<IsInvocable<F, XA, XA>> = 0>
    static inline constexpr Conditional<IsSatisfied<E>,
        decltype(eval(declval<XA>())), XA>
    fold(XA &&a)
    {
        if constexpr(E)
        {
            return eval(forward<XA>(a));
        }
        else
        {
            return forward<XA>(a);
        }
    }

    template<class F, Bool E = false, Direction D = Direction::Forwards,
        If<IsSatisfied<(
            D == Direction::Forwards || D == Direction::Backwards)>> = 0,
        class XA, class XB, If<IsInvocable<F, XA, XB>> = 0>
    static inline constexpr auto fold(XA &&a, XB &&b)
    {
        if constexpr(E)
        {
            return eval(F()(forward<XA>(a), forward<XB>(b)));
        }
        else
        {
            return F()(forward<XA>(a), forward<XB>(b));
        }
    }

    template<class F, Bool E = false, Direction D = Direction::Forwards,
        class XA, class XB, class... Xs,
        If<IsSatisfied<(sizeof...(Xs) > 0)>> = 0,
        If<IsSatisfied<(
            D == Direction::Forwards || D == Direction::Backwards)>> = 0,
        If<IsInvocable<F, XA, XB>> = 0,
        If<IsInvocable<Fold<F, E, D>, ResultOf<Fold<F, E, D>, XA, XB> const &,
            Xs...>> = 0>
    static inline constexpr auto fold(XA &&a, XB &&b, Xs &&...args)
    {
        if constexpr(D == Direction::Forwards)
        {
            auto const c = [&a, &b]()
            {
                if constexpr(E)
                {
                    return eval(F()(forward<XA>(a), forward<XB>(b)));
                }
                else
                {
                    return F()(forward<XA>(a), forward<XB>(b));
                }
            };

            return fold<F, E, D>(c(), forward<Xs>(args)...);
        }
        if constexpr(D == Direction::Backwards)
        {
            auto const c = [&b, &args...]()
            {
                return fold<F, E, D>(forward<XB>(b), forward<Xs>(args)...);
            };

            if constexpr(E)
            {
                return eval(F()(forward<XA>(a), c()));
            }
            else
            {
                return F()(forward<XA>(a), c());
            }
        }
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_FOLD_H
