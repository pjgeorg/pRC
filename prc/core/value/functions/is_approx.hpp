// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_IS_APPROX_H
#define pRC_CORE_VALUE_FUNCTIONS_IS_APPROX_H

#include <prc/core/basic/functions/min.hpp>
#include <prc/core/functors/absolute_error.hpp>
#include <prc/core/functors/norm.hpp>
#include <prc/core/value/functions/absolute_error.hpp>
#include <prc/core/value/functions/norm.hpp>
#include <prc/core/value/limits.hpp>

namespace pRC
{
    template<class XA, class XB, class RA = RemoveReference<XA>,
        class RB = RemoveReference<XB>,
        class TT = Common<typename RA::Value, typename RB::Value>,
        class T = Common<typename RA::Value, typename RB::Value, TT>,
        If<IsValue<TT>> = 0, If<IsInvocable<AbsoluteError, XA, XB>> = 0,
        If<IsInvocable<Norm<>, XA>> = 0, If<IsInvocable<Norm<>, XB>> = 0,
        If<IsConstructible<T, ResultOf<AbsoluteError, XA, XB>>> = 0,
        If<IsConstructible<T, ResultOf<Norm<>, XA>>> = 0,
        If<IsConstructible<T, ResultOf<Norm<>, XB>>> = 0>
    static inline constexpr auto isApprox(
        XA &&a, XB &&b, TT const &tolerance = NumericLimits<TT>::tolerance())
    {
        auto const absError = static_cast<T>(absoluteError(a, b));

        auto const normA = static_cast<T>(norm(a));
        auto const normB = static_cast<T>(norm(b));

        if(min(normA, normB) > NumericLimits<TT>::epsilon())
        {
            return absError <= tolerance * min(normA, normB);
        }
        else
        {
            return absError <= tolerance * max(normA, normB);
        }
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_IS_APPROX_H
