// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_FUNCTIONS_ARG_H
#define pRC_CORE_VALUE_FUNCTIONS_ARG_H

#include <prc/core/value/float.hpp>
#include <prc/core/value/functions/cast.hpp>
#include <prc/core/value/functions/pi.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T, If<IsFloat<T>> = 0>
    static inline constexpr auto arg(T const &a)
    {
        if(a < zero())
        {
            return pi<T>;
        }
        else
        {
            return zero<T>();
        }
    }

    template<class T, If<IsInteger<T>> = 0>
    static inline constexpr auto arg(T const &a)
    {
        return arg(cast<Float<>>(a));
    }
}
#endif // pRC_CORE_VALUE_FUNCTIONS_ARG_H
