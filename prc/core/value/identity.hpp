// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_IDENTITY_H
#define pRC_CORE_VALUE_IDENTITY_H

#include <prc/core/value/float.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T>
    struct Identity<T, If<IsValue<T>>>
    {
        constexpr T operator()()
        {
            return 1;
        }

        template<class X, If<IsConstructible<T, X>> = 0>
        constexpr T operator()(X &&value)
        {
            return forward<X>(value);
        }
    };

    template<>
    struct Identity<Float<16>>
    {
        constexpr Float<16> operator()()
        {
            return BFloat16::FromRepresentation(0x3F80);
        }

        template<class X, If<IsConstructible<Float<16>, X>> = 0>
        constexpr Float<16> operator()(X &&value)
        {
            return forward<X>(value);
        }
    };
}
#endif // pRC_CORE_VALUE_IDENTITY_H
