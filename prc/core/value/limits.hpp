// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_LIMITS_H
#define pRC_CORE_VALUE_LIMITS_H

#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/basic/limits.hpp>
#include <prc/core/value/float.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T>
    struct NumericLimits<T, If<IsValue<T>>>
    {
    private:
        using F = typename T::Fundamental;

    public:
        static constexpr Size digits()
        {
            return std::numeric_limits<F>::digits;
        }

        static constexpr T min()
        {
            return std::numeric_limits<F>::min();
        }

        static constexpr T max()
        {
            return std::numeric_limits<F>::max();
        }

        static constexpr T lowest()
        {
            return std::numeric_limits<F>::lowest();
        }

        static constexpr T epsilon()
        {
            return std::numeric_limits<F>::epsilon();
        }

        static constexpr T tolerance()
        {
            if constexpr(IsFloat<T>())
            {
                if constexpr(typename T::Width() == 64)
                {
                    return 1e-12;
                }
                if constexpr(typename T::Width() == 32)
                {
                    return 1e-5f;
                }
                else
                {
                    return zero<T>();
                }
            }
        }
    };

    template<>
    struct NumericLimits<Float<16>>
    {
        static constexpr Size digits()
        {
            return 7;
        }

        static constexpr Float<16> min()
        {
            return BFloat16::FromRepresentation(0x0080);
        }

        static constexpr Float<16> max()
        {
            return BFloat16::FromRepresentation(0x7F7F);
        }

        static constexpr Float<16> lowest()
        {
            return BFloat16::FromRepresentation(0xFF7F);
        }

        static constexpr Float<16> epsilon()
        {
            return BFloat16::FromRepresentation(0x3C00);
        }

        static constexpr Float<16> tolerance()
        {
            return BFloat16::FromRepresentation(0x3C24);
        }
    };
}
#endif // pRC_CORE_VALUE_LIMITS_H
