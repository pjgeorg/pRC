// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_INTEGER_H
#define pRC_CORE_VALUE_INTEGER_H

#include <prc/core/basic/functions/identity.hpp>
#include <prc/core/basic/functions/unit.hpp>
#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/complex/type_traits.hpp>
#include <prc/core/functors/add.hpp>
#include <prc/core/functors/div.hpp>
#include <prc/core/functors/mod.hpp>
#include <prc/core/functors/mul.hpp>
#include <prc/core/functors/sub.hpp>
#include <prc/core/log/log.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<Bool S, Size W>
    class Integer
    {
    public:
        using Fundamental = Conditional<IsSatisfied<(S)>,
            Conditional<IsSatisfied<(W == 8)>, std::int8_t,
                Conditional<IsSatisfied<(W == 16)>, std::int16_t,
                    Conditional<IsSatisfied<(W == 32)>, std::int32_t,
                        Conditional<IsSatisfied<(W == 64)>, std::int64_t,
                            Undefined>>>>,
            Conditional<IsSatisfied<(W == 8)>, std::uint8_t,
                Conditional<IsSatisfied<(W == 16)>, std::uint16_t,
                    Conditional<IsSatisfied<(W == 32)>, std::uint32_t,
                        Conditional<IsSatisfied<(W == 64)>, std::uint64_t,
                            Undefined>>>>>;

        static_assert(IsDefined<Fundamental>(),
            "Integer<S, W>: W has to be a supported Width (8, 16, 32, 64).");

        using Value = Integer<S, W>;
        template<class V, If<IsValue<V>> = 0>
        using ChangeValue = V;

        using Signed = pRC::Constant<Bool, S>;
        template<Bool R>
        using ChangeSigned = Integer<R, W>;

        using Width = pRC::Constant<Size, W>;
        template<Size Q>
        using ChangeWidth = Integer<S, Q>;

        using IsComplexified = False<>;
        using Complexify = Complex<Integer<S, W>>;
        using NonComplex = Integer<S, W>;

    public:
        ~Integer() = default;
        constexpr Integer(Integer const &) = default;
        constexpr Integer(Integer &&) = default;
        constexpr Integer &operator=(Integer const &) & = default;
        constexpr Integer &operator=(Integer &&) & = default;
        constexpr Integer() = default;

        template<class U, If<IsConstructible<Fundamental, U>> = 0>
        constexpr Integer(U const basic)
            : mValue(basic)
        {
        }

        template<class U, If<IsValue<U>> = 0>
        constexpr Integer(U const &value)
            : mValue(value())
        {
        }

        template<class U, If<IsValue<U>> = 0>
        constexpr Integer(Complex<U> const &value)
            : Integer(value.real())
        {
        }

        constexpr Integer(Zero<> const)
            : Integer(zero<Integer>())
        {
        }

        constexpr Integer(Unit<> const)
            : Integer(unit<Integer>())
        {
        }

        constexpr Integer(Identity<> const)
            : Integer(identity<Integer>())
        {
        }

        template<class U, If<IsAssignable<Fundamental, U>> = 0>
        constexpr auto &operator=(U const basic) &
        {
            mValue = basic;
            return *this;
        }

        template<class U, If<IsValue<U>> = 0>
        constexpr auto &operator=(U const &value) &
        {
            mValue = value();
            return *this;
        }

        template<class U, If<IsValue<U>> = 0>
        constexpr auto &operator=(Complex<U> const &value) &
        {
            return *this = value.real();
        }

        constexpr auto &operator=(Zero<> const) &
        {
            return *this = zero<Integer>();
        }

        constexpr auto &operator=(Unit<> const) &
        {
            return *this = unit<Integer>();
        }

        constexpr auto &operator=(Identity<> const) &
        {
            return *this = identity<Integer>();
        }

        constexpr decltype(auto) operator()() &&
        {
            return move(mValue);
        }

        constexpr decltype(auto) operator()() const &&
        {
            return move(mValue);
        }

        constexpr auto &operator()() &
        {
            return mValue;
        }

        constexpr auto &operator()() const &
        {
            return mValue;
        }

        explicit constexpr operator Fundamental() const
        {
            return mValue;
        }

        template<class X, If<IsInvocable<Add, Integer &, X>> = 0>
        constexpr auto &operator+=(X &&rhs) &
        {
            return *this = *this + forward<X>(rhs);
        }

        template<class X, If<IsInvocable<Sub, Integer &, X>> = 0>
        constexpr auto &operator-=(X &&rhs) &
        {
            return *this = *this - forward<X>(rhs);
        }

        template<class X, If<IsInvocable<Mul, Integer &, X>> = 0>
        constexpr auto &operator*=(X &&rhs) &
        {
            return *this = *this * forward<X>(rhs);
        }

        template<class X, If<IsInvocable<Div, Integer &, X>> = 0>
        constexpr auto &operator/=(X &&rhs) &
        {
            return *this = *this / forward<X>(rhs);
        }

        template<class X, If<IsInvocable<Mod, Integer &, X>> = 0>
        constexpr auto &operator%=(X &&rhs) &
        {
            return *this = *this % forward<X>(rhs);
        }

    private:
        Fundamental mValue;
    };

    Integer(Integer<true, 8>::Fundamental const)->Integer<true, 8>;
    Integer(Integer<true, 16>::Fundamental const)->Integer<true, 16>;
    Integer(Integer<true, 32>::Fundamental const)->Integer<true, 32>;
    Integer(Integer<true, 64>::Fundamental const)->Integer<true, 64>;
    Integer(Integer<false, 8>::Fundamental const)->Integer<false, 8>;
    Integer(Integer<false, 16>::Fundamental const)->Integer<false, 16>;
    Integer(Integer<false, 32>::Fundamental const)->Integer<false, 32>;
    Integer(Integer<false, 64>::Fundamental const)->Integer<false, 64>;
}
#endif // pRC_CORE_VALUE_INTEGER_H
