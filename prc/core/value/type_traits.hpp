// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_VALUE_TYPE_TRAITS_H
#define pRC_CORE_VALUE_TYPE_TRAITS_H

#include <prc/config.hpp>
#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<Size Width = DEFAULT_FLOAT>
    class Float;

    template<class>
    struct IsFloat : False<>
    {
    };

    template<class T>
    struct IsFloat<T const> : IsFloat<T>
    {
    };

    template<Size W>
    struct IsFloat<Float<W>> : True<>
    {
    };

    template<Bool S = true, Size Width = sizeof(int) * 8>
    class Integer;

    template<Size W = sizeof(int) * 8>
    using SignedInteger = Integer<true, W>;

    template<class>
    struct IsSignedInteger : False<>
    {
    };

    template<class T>
    struct IsSignedInteger<T const> : IsSignedInteger<T>
    {
    };

    template<Size W>
    struct IsSignedInteger<SignedInteger<W>> : True<>
    {
    };

    template<Size W = sizeof(int) * 8>
    using UnsignedInteger = Integer<false, W>;

    template<class>
    struct IsUnsignedInteger : False<>
    {
    };

    template<class T>
    struct IsUnsignedInteger<T const> : IsUnsignedInteger<T>
    {
    };

    template<Size W>
    struct IsUnsignedInteger<UnsignedInteger<W>> : True<>
    {
    };

    template<class T>
    using IsInteger = Any<IsSignedInteger<T>, IsUnsignedInteger<T>>;

    template<class T>
    using IsValue = Any<IsFloat<T>, IsInteger<T>>;
}
#endif // pRC_CORE_VALUE_TYPE_TRAITS_H
