// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_CONTAINER_INDICES_H
#define pRC_CORE_CONTAINER_INDICES_H

#include <prc/core/container/type_traits.hpp>

namespace pRC
{
    template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0>
    Indices(Is const...) -> Indices<sizeof...(Is)>;

    template<Size N>
    class Indices
    {
    public:
        static constexpr auto size()
        {
            return N;
        }

    public:
        ~Indices() = default;
        constexpr Indices() = default;
        constexpr Indices(Indices const &) = default;
        constexpr Indices(Indices &&) = default;
        constexpr Indices &operator=(Indices const &) & = default;
        constexpr Indices &operator=(Indices &&) & = default;

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == N)>> = 0>
        constexpr Indices(Is const... indices)
            : mIndices(indices...)
        {
        }

        constexpr decltype(auto) operator[](Index const dimension) &&
        {
            return move(mIndices)[dimension];
        }

        constexpr decltype(auto) operator[](Index const dimension) const &&
        {
            return move(mIndices)[dimension];
        }

        constexpr decltype(auto) operator[](Index const dimension) &
        {
            return mIndices[dimension];
        }

        constexpr decltype(auto) operator[](Index const dimension) const &
        {
            return mIndices[dimension];
        }

    private:
        BEGIN_IGNORE_DIAGNOSTIC_GCC("-Wpedantic")
        StackArray<Index, N> const mIndices = {};
        END_IGNORE_DIAGNOSTIC_GCC
    };
}
#endif // pRC_CORE_CONTAINER_INDICES_H
