// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_CONTAINER_ALLOCATION_H
#define pRC_CORE_CONTAINER_ALLOCATION_H

#include <cstddef>
#include <cstdlib>
#include <new>

#include <prc/config.hpp>
#include <prc/core/basic/functions/is_power_of_two.hpp>
#include <prc/core/basic/type_traits.hpp>
#include <prc/core/log/log.hpp>
#include <prc/pragma.hpp>

namespace pRC
{
    enum class Allocation
    {
        Stack,
        Heap,
    };

    static inline constexpr auto isProperAlignment(Size const alignment)
    {
        if(alignment == 0 || !isPowerOfTwo(alignment))
        {
            return false;
        }
        return true;
    }

    template<Size S, Size P = alignof(std::max_align_t)>
    static inline constexpr auto alignment()
    {
        static_assert(
            isProperAlignment(P), "Proposed alignment is no proper alignment.");

        if constexpr(S == 0)
        {
            return P;
        }
        else if constexpr(P >= cHugepageSizeByte)
        {
            return P;
        }
        else
        {
            if constexpr(S % (P * 2) == 0)
            {
                return alignment<S, P * 2>();
            }
            else
            {
                return P;
            }
        }
    }

    static inline auto alloc(
        Size size, Size const alignment = alignof(std::max_align_t))
    {
        if constexpr(cDebugLevel >= DebugLevel::Low)
        {
            if(!isProperAlignment(alignment))
            {
                Logging::error("Alignment is no proper alignment.");
            }
        }

        auto p = std::aligned_alloc(alignment, size);
        if(p == nullptr)
        {
            Logging::error("Unable to allocate memory.");
        }

        if constexpr(cDebugLevel >= DebugLevel::Low)
        {
            if(reinterpret_cast<std::uintptr_t>(p) % alignment != 0)
            {
                Logging::error(
                    "Allocated memory is not aligned to alignment "
                    "requirement.");
            }
        }

        return p;
    }

    static inline auto dealloc(void *ptr)
    {
        std::free(ptr);
    }
}

BEGIN_IGNORE_DIAGNOSTIC_CLANG("-Winline-new-delete")

inline void *operator new(std::size_t size)
{
    return operator new(
        size, static_cast<std::align_val_t>(alignof(std::max_align_t)));
}

inline void *operator new[](std::size_t size)
{
    return operator new(size);
}

inline void *operator new(std::size_t size, std::align_val_t alignment)
{
    return pRC::alloc(size, static_cast<pRC::Size>(alignment));
}

inline void *operator new[](std::size_t size, std::align_val_t alignment)
{
    return operator new(size, alignment);
}

inline void *operator new(
    std::size_t size, [[maybe_unused]] std::nothrow_t const &) noexcept
{
    return operator new(size);
}

inline void *operator new[](
    std::size_t size, [[maybe_unused]] std::nothrow_t const &) noexcept
{
    return operator new[](size);
}

inline void *operator new(std::size_t size, std::align_val_t alignment,
    [[maybe_unused]] std::nothrow_t const &) noexcept
{
    return operator new(size, alignment);
}

inline void *operator new[](std::size_t size, std::align_val_t alignment,
    [[maybe_unused]] std::nothrow_t const &) noexcept
{
    return operator new[](size, alignment);
}

inline void operator delete(void *ptr) noexcept
{
    return pRC::dealloc(ptr);
}

inline void operator delete[](void *ptr) noexcept
{
    return operator delete(ptr);
}

inline void operator delete(void *ptr, std::align_val_t) noexcept
{
    return operator delete(ptr);
}

inline void operator delete[](void *ptr, std::align_val_t) noexcept
{
    return operator delete[](ptr);
}

inline void operator delete(void *ptr, std::size_t)
{
    return operator delete(ptr);
}

inline void operator delete[](void *ptr, std::size_t)
{
    return operator delete[](ptr);
}

inline void operator delete(void *ptr, std::size_t, std::align_val_t)
{
    return operator delete(ptr);
}

inline void operator delete[](void *ptr, std::size_t, std::align_val_t)
{
    return operator delete[](ptr);
}

inline void operator delete(void *ptr, std::nothrow_t const &) noexcept
{
    return operator delete(ptr);
}

inline void operator delete[](void *ptr, std::nothrow_t const &) noexcept
{
    return operator delete[](ptr);
}

inline void operator delete(void *ptr, std::nothrow_t const &, std::align_val_t)
{
    return operator delete(ptr);
}

inline void operator delete[](
    void *ptr, std::nothrow_t const &, std::align_val_t)
{
    return operator delete[](ptr);
}

END_IGNORE_DIAGNOSTIC_CLANG
#endif // pRC_CORE_CONTAINER_ALLOCATION_H
