// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_CONTAINER_ARRAY_HEAP_H
#define pRC_CORE_CONTAINER_ARRAY_HEAP_H

#include <prc/core/basic/range.hpp>
#include <prc/core/basic/sequence.hpp>
#include <prc/core/container/allocation.hpp>
#include <prc/core/container/subscripts.hpp>
#include <prc/core/container/type_traits.hpp>
#include <prc/core/log/log.hpp>
#include <prc/pragma.hpp>

namespace pRC
{
    template<class T, Size N>
    class CommonArray<Allocation::Heap, T, N>
    {
    public:
        using Allocation = Constant<pRC::Allocation, pRC::Allocation::Heap>;
        using Type = T;
        using Sizes = pRC::Sizes<N>;
        using Dimension = typename Sizes::Dimension;

        static constexpr auto size()
        {
            return Sizes::size();
        }

        static constexpr auto size(Index const dimension)
        {
            return Sizes::size(dimension);
        }

        static constexpr auto indexToSubscripts(Index const index)
        {
            return Subscripts<N>(index);
        }

        static constexpr auto subscriptsToIndex(Index const subscripts)
        {
            return subscripts;
        }

        static constexpr auto subscriptsToIndex(Subscripts<N> const &subscripts)
        {
            return Index(subscripts);
        }

    private:
        BEGIN_IGNORE_DIAGNOSTIC_GCC("-Wpedantic")
        BEGIN_IGNORE_DIAGNOSTIC_CLANG("-Wzero-length-array")
        template<class R, Size S>
        using CArray = R[S];
        END_IGNORE_DIAGNOSTIC_CLANG
        END_IGNORE_DIAGNOSTIC_GCC

    public:
        ~CommonArray()
        {
            deallocate();
        }

        CommonArray()
        {
            allocate();
        }

        CommonArray(CommonArray const &other)
        {
            allocate();
            *this = other;
        }

        CommonArray(CommonArray &&other)
            : mData(other.mData)
        {
            other.mData = nullptr;
        }

        auto &operator=(CommonArray const &rhs) &
        {
            return operator=<Allocation{}, T>(rhs);
        }

        auto &operator=(CommonArray &&rhs) &
        {
            deallocate();
            mData = rhs.mData;
            rhs.mData = nullptr;

            return *this;
        }

        template<pRC::Allocation B, class R, If<IsConvertible<R, T>> = 0>
        constexpr CommonArray(CommonArray<B, R, N> const &other)
        {
            allocate();
            *this = other;
        }

        template<class R, If<IsConvertible<R, T>> = 0>
        constexpr CommonArray(CArray<R, N> const &other)
            : CommonArray(makeSeries<Index, N>(), other)
        {
        }

        template<class... Rs, If<All<IsConvertible<Rs, T>...>> = 0,
            If<IsSatisfied<(sizeof...(Rs) == N)>> = 0>
        constexpr CommonArray(Rs const &...values)
            : CommonArray(makeSeriesFor<Index, Rs...>(), values...)
        {
        }

        template<pRC::Allocation B, class R, If<IsConvertible<R, T>> = 0>
        constexpr auto &operator=(CommonArray<B, R, N> const &rhs) &
        {
            range<size()>(
                [this, &rhs](auto const i)
                {
                    operator[](i) = rhs[i];
                });
            return *this;
        }

        template<class R, If<IsConvertible<R, T>> = 0>
        constexpr auto &operator=(CArray<R, N> const &rhs) &
        {
            range<size()>(
                [this, &rhs](auto const i)
                {
                    operator[](i) = rhs[i];
                });
            return *this;
        }

        constexpr decltype(auto) operator()(Index const subscript) &&
        {
            return move(*this)[subscript];
        }

        constexpr decltype(auto) operator()(Index const subscript) const &&
        {
            return move(*this)[subscript];
        }

        constexpr decltype(auto) operator()(Index const subscript) &
        {
            return operator[](subscript);
        }

        constexpr decltype(auto) operator()(Index const subscript) const &
        {
            return operator[](subscript);
        }

        constexpr decltype(auto) operator()(Subscripts<N> const &subscripts) &&
        {
            return move(*this)[Index(subscripts)];
        }

        constexpr decltype(auto) operator()(
            Subscripts<N> const &subscripts) const &&
        {
            return move(*this)[Index(subscripts)];
        }

        constexpr decltype(auto) operator()(Subscripts<N> const &subscripts) &
        {
            return operator[](Index(subscripts));
        }

        constexpr decltype(auto) operator()(
            Subscripts<N> const &subscripts) const &
        {
            return operator[](Index(subscripts));
        }

        constexpr decltype(auto) operator[](Index const index) &&
        {
            validate();
            return move(*mData)[index];
        }

        constexpr decltype(auto) operator[](Index const index) const &&
        {
            validate();
            return move(*mData)[index];
        }

        constexpr decltype(auto) operator[](Index const index) &
        {
            validate();
            return (*mData)[index];
        }

        constexpr decltype(auto) operator[](Index const index) const &
        {
            validate();
            return (*mData)[index];
        }

        constexpr auto data() && = delete;
        constexpr auto data() const && = delete;

        constexpr auto data() &
        {
            return mData->data();
        }

        constexpr auto data() const &
        {
            return mData->data();
        }

    private:
        template<Index... Is, class R, If<IsConvertible<R, T>> = 0,
            If<IsSatisfied<(sizeof...(Is) == N)>> = 0>
        constexpr CommonArray(
            Sequence<Index, Is...> const, CArray<R, N> const &other)
            : CommonArray(Sequence<Index, Is...>(), other[Is]...)
        {
        }

        template<Index... Is, class... Rs, If<All<IsConvertible<Rs, T>...>> = 0,
            If<IsSatisfied<((sizeof...(Rs) == N) && (sizeof...(Is) == N))>> = 0>
        constexpr CommonArray(Sequence<Index, Is...> const, Rs const &...values)
        {
            allocate();
            ((operator[](Is) = values), ...);
        }

        auto allocate()
        {
            if constexpr(cDebugLevel >= DebugLevel::Low)
            {
                if(mData != nullptr)
                {
                    Logging::error("Heap memory has already been allocated.");
                }
            }

            mData = new(std::nothrow) StackArray<T, N>;
        }

        auto deallocate()
        {
            if(mData != nullptr)
            {
                delete mData;
                mData = nullptr;
            }
        }

        constexpr auto validate() const
        {
            if constexpr(cDebugLevel >= DebugLevel::Mid)
            {
                if(mData == nullptr)
                {
                    Logging::error(
                        "Accessing invalidated dynamically allocated memory.");
                }
            }
        }

    private:
        StackArray<T, N> *mData = nullptr;
    };
}
#endif // pRC_CORE_CONTAINER_ARRAY_HEAP_H
