// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_CONTAINER_FUNCTIONS_PRINT_H
#define pRC_CORE_CONTAINER_FUNCTIONS_PRINT_H

#include <prc/core/basic/range.hpp>
#include <prc/core/basic/type_name.hpp>
#include <prc/core/container/allocation.hpp>
#include <prc/core/container/type_traits.hpp>

namespace pRC
{
    template<class S>
    static inline auto print(Allocation const alloc, S &&stream)
    {
        switch(alloc)
        {
            case Allocation::Stack:
                print("pRC::Allocation::Stack", stream);
                break;
            case Allocation::Heap:
                print("pRC::Allocation::Heap", stream);
                break;
            default:
                print("pRC::Allocation::Unknown", stream);
                break;
        }
    }
    template<Size... Ns, class S>
    static inline auto print(Subscripts<Ns...> const &a, S &&stream)
    {
        print(name<Subscripts<Ns...>>(), stream);

        print("(", stream);
        if constexpr(sizeof...(Ns) > 0)
        {
            print(a[0], stream);

            expand(makeRange<Index, 1, sizeof...(Ns)>(),
                [&a, &stream](auto const... seq)
                {
                    ((print(", ", stream), print(a[seq], stream)), ...);
                });
        }
        print(")", stream);
    }

    template<Size N, class S>
    static inline auto print(Indices<N> const &a, S &&stream)
    {
        print(name<Indices<N>>(), stream);

        print("(", stream);
        if constexpr(N > 0)
        {
            print(a[0], stream);

            expand(makeRange<Index, 1, N>(),
                [&a, &stream](auto const... seq)
                {
                    ((print(", ", stream), print(a[seq], stream)), ...);
                });
        }
        print(")", stream);
    }

    template<Allocation A, class T, Size... Ns, class S>
    static inline auto print(CommonArray<A, T, Ns...> const &arg, S &&stream)
    {
        print(name<CommonArray<A, T, Ns...>>(), stream);
        print(":\n", stream);
        range<Sizes<Ns...>>(
            [&arg, &stream](auto const... indices)
            {
                print(Subscripts<Ns...>(indices...), stream);

                print(": ", stream);
                print(arg(indices...), stream);
                print('\n', stream);
            });
    }
}
#endif // pRC_CORE_CONTAINER_FUNCTIONS_PRINT_H
