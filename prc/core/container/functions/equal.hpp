// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_CONTAINER_FUNCTIONS_EQUAL_H
#define pRC_CORE_CONTAINER_FUNCTIONS_EQUAL_H

#include <prc/core/container/type_traits.hpp>
#include <prc/core/functors/equal.hpp>

namespace pRC
{
    template<Allocation A1, Allocation A2, class TA, class TB, Size... Ns,
        If<IsInvocable<Equal, TA, TB>> = 0>
    static inline constexpr auto operator==(
        CommonArray<A1, TA, Ns...> const &lhs,
        CommonArray<A2, TB, Ns...> const &rhs)
    {
        for(Index i = 0; i < Sizes<Ns...>::size(); ++i)
        {
            if(lhs[i] != rhs[i])
            {
                return false;
            }
        }
        return true;
    }
}
#endif // pRC_CORE_CONTAINER_FUNCTIONS_EQUAL_H
