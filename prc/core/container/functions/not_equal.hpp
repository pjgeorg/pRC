// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_CONTAINER_FUNCTIONS_NOT_EQUAL_H
#define pRC_CORE_CONTAINER_FUNCTIONS_NOT_EQUAL_H

#include <prc/core/container/functions/equal.hpp>
#include <prc/core/container/type_traits.hpp>

namespace pRC
{
    template<Allocation A1, Allocation A2, class TA, class TB, Size... Ns>
    static inline constexpr auto operator!=(
        CommonArray<A1, TA, Ns...> const &lhs,
        CommonArray<A2, TB, Ns...> const &rhs)
    {
        return !(lhs == rhs);
    }
}
#endif // pRC_CORE_CONTAINER_FUNCTIONS_NOT_EQUAL_H
