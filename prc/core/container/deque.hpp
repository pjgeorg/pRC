// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_CONTAINER_DEQUE_H
#define pRC_CORE_CONTAINER_DEQUE_H

#include <prc/core/basic/functions/min.hpp>
#include <prc/core/container/type_traits.hpp>
#include <prc/core/log/log.hpp>
#include <prc/pragma.hpp>

namespace pRC
{
    template<class T, Size N>
    class Deque
    {
    public:
        using Type = T;
        using Space = Constant<Size, N>;

    public:
        ~Deque() = default;
        constexpr Deque(Deque const &) = default;
        constexpr Deque(Deque &&) = default;
        constexpr Deque &operator=(Deque const &) & = default;
        constexpr Deque &operator=(Deque &&) & = default;
        constexpr Deque() = default;

        constexpr auto size() const
        {
            return mSize;
        }

        constexpr decltype(auto) front(Index const position = 0) &&
        {
            return move(*this)[position];
        }

        constexpr decltype(auto) front(Index const position = 0) const &&
        {
            return move(*this)[position];
        }

        constexpr decltype(auto) front(Index const position = 0) &
        {
            return operator[](position);
        }

        constexpr decltype(auto) front(Index const position = 0) const &
        {
            return operator[](position);
        }

        constexpr decltype(auto) back(Index const position = 0) &&
        {
            return move(*this)[mSize - position - 1];
        }

        constexpr decltype(auto) back(Index const position = 0) const &&
        {
            return move(*this)[mSize - position - 1];
        }

        constexpr decltype(auto) back(Index const position = 0) &
        {
            return operator[](mSize - position - 1);
        }

        constexpr decltype(auto) back(Index const position = 0) const &
        {
            return operator[](mSize - position - 1);
        }

        constexpr auto &clear() &
        {
            mSize = 0;
            mNext = 0;
            return *this;
        }

        template<class R, If<IsConvertible<R, T>> = 0>
        constexpr auto pushFront(R const &element) &&
        {
            updatePushFront();
            front() = element;
            return move(*this);
        }

        template<class R, If<IsConvertible<R, T>> = 0>
        constexpr auto &pushFront(R const &element) &
        {
            updatePushFront();
            front() = element;
            return *this;
        }

        constexpr auto pushFront(T &&element) &&
        {
            updatePushFront();
            front() = move(element);
            return move(*this);
        }

        constexpr auto &pushFront(T &&element) &
        {
            updatePushFront();
            front() = move(element);
            return *this;
        }

        template<class... Args, If<IsConstructible<T, Args...>> = 0>
        constexpr auto emplaceFront(Args &&...args) &&
        {
            updatePushFront();
            front() = T(forward<Args>(args)...);
            return move(*this);
        }

        template<class... Args, If<IsConstructible<T, Args...>> = 0>
        constexpr auto &emplaceFront(Args &&...args) &
        {
            updatePushFront();
            front() = T(forward<Args>(args)...);
            return *this;
        }

        constexpr auto &popFront() &
        {
            updatePopFront();
            return *this;
        }

        template<class R, If<IsConvertible<R, T>> = 0>
        constexpr auto pushBack(R const &element) &&
        {
            updatePushBack();
            back() = element;
            return move(*this);
        }

        template<class R, If<IsConvertible<R, T>> = 0>
        constexpr auto &pushBack(R const &element) &
        {
            updatePushBack();
            back() = element;
            return *this;
        }

        constexpr auto pushBack(T &&element) &&
        {
            updatePushBack();
            back() = move(element);
            return move(*this);
        }

        constexpr auto &pushBack(T &&element) &
        {
            updatePushBack();
            back() = move(element);
            return *this;
        }

        template<class... Args, If<IsConstructible<T, Args...>> = 0>
        constexpr auto emplaceBack(Args &&...args) &&
        {
            updatePushBack();
            back() = T(forward<Args>(args)...);
            return move(*this);
        }

        template<class... Args, If<IsConstructible<T, Args...>> = 0>
        constexpr auto &emplaceBack(Args &&...args) &
        {
            updatePushBack();
            back() = T(forward<Args>(args)...);
            return *this;
        }

        constexpr auto &popBack() &
        {
            updatePopBack();
            return *this;
        }

    private:
        constexpr decltype(auto) operator[](Index const position) &&
        {
            return move(mData)[getIndex(position)];
        }

        constexpr decltype(auto) operator[](Index const position) const &&
        {
            return move(mData)[getIndex(position)];
        }

        constexpr decltype(auto) operator[](Index const position) &
        {
            return mData[getIndex(position)];
        }

        constexpr decltype(auto) operator[](Index const position) const &
        {
            return mData[getIndex(position)];
        }

        constexpr auto getIndex(Index const position) const
        {
            if constexpr(cDebugLevel >= DebugLevel::Mid)
            {
                if(position >= mSize)
                {
                    Logging::error("Deque out of range.");
                }
            }

            if constexpr(N != 0)
            {
                return (mNext + N - 1 - position) % N;
            }
            else
            {
                return mNext;
            }
        }

        constexpr auto updatePushFront()
        {
            if constexpr(N != 0)
            {
                mSize = min(mSize + 1, N);
                mNext = (mNext + 1) % N;
            }
        }

        constexpr auto updatePopFront()
        {
            if constexpr(cDebugLevel >= DebugLevel::Mid)
            {
                if(mSize == 0)
                {
                    Logging::error("Deque is empty.");
                }
            }

            if constexpr(N != 0)
            {
                mSize = mSize - 1;
                mNext = (mNext - 1) % N;
            }
        }

        constexpr auto updatePushBack()
        {
            if constexpr(N != 0)
            {
                if(mSize == N)
                {
                    mNext = (mNext - 1) % N;
                }
                mSize = min(mSize + 1, N);
            }
        }

        constexpr auto updatePopBack()
        {
            if constexpr(cDebugLevel >= DebugLevel::Mid)
            {
                if(mSize == 0)
                {
                    Logging::error("Deque is empty.");
                }
            }

            if constexpr(N != 0)
            {
                mSize = mSize - 1;
            }
        }

    private:
        BEGIN_IGNORE_DIAGNOSTIC_GCC("-Wpedantic")
        StackArray<T, N> mData;
        END_IGNORE_DIAGNOSTIC_GCC

        Size mSize = 0;
        Index mNext = 0;
    };
}
#endif // pRC_CORE_CONTAINER_DEQUE_H
