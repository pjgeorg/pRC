// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_CONTAINER_ARRAY_SCALAR_H
#define pRC_CORE_CONTAINER_ARRAY_SCALAR_H

#include <prc/core/basic/sequence.hpp>
#include <prc/core/container/array_stack.hpp>
#include <prc/core/container/subscripts.hpp>
#include <prc/core/container/type_traits.hpp>

namespace pRC
{
    template<Allocation A, class T>
    class CommonArray<A, T>
    {
    public:
        using Allocation = Constant<pRC::Allocation, A>;
        using Type = T;
        using Sizes = pRC::Sizes<>;
        using Dimension = typename Sizes::Dimension;

        static constexpr auto size()
        {
            return Sizes::size();
        }

        static constexpr auto size(Index const dimension) = delete;

        static constexpr auto indexToSubscripts(Index const index)
        {
            return Subscripts<>(index);
        }

        static constexpr auto subscriptsToIndex()
        {
            return Index(Subscripts<>());
        }

        static constexpr auto subscriptsToIndex(Subscripts<> const &subscripts)
        {
            return Index(subscripts);
        }

    public:
        ~CommonArray() = default;
        constexpr CommonArray() = default;

        constexpr CommonArray(CommonArray const &) = default;
        constexpr CommonArray(CommonArray &&) = default;

        template<pRC::Allocation B, class R, If<IsConvertible<R, T>> = 0>
        constexpr CommonArray(CommonArray<B, R> const &other)
        {
            *this = other;
        }

        template<class X, If<IsConvertible<X, T>> = 0>
        constexpr CommonArray(X &&value)
            : mData(forward<X>(value))
        {
        }

        constexpr CommonArray &operator=(CommonArray const &) & = default;
        constexpr CommonArray &operator=(CommonArray &&) & = default;

        template<pRC::Allocation B, class R, If<IsConvertible<R, T>> = 0>
        constexpr auto &operator=(CommonArray<B, R> const &rhs) &
        {
            operator()() = rhs();
            return *this;
        }

        template<class X, If<IsConvertible<X, T>> = 0>
        constexpr auto &operator=(X &&value) &
        {
            operator()() = forward<X>(value);
            return *this;
        }

        constexpr decltype(auto) operator()() &&
        {
            return move(*this)[0];
        }

        constexpr decltype(auto) operator()() const &&
        {
            return move(*this)[0];
        }

        constexpr decltype(auto) operator()() &
        {
            return operator[](0);
        }

        constexpr decltype(auto) operator()() const &
        {
            return operator[](0);
        }

        constexpr decltype(auto) operator()(Subscripts<> const &) &&
        {
            return move(*this)();
        }

        constexpr decltype(auto) operator()(Subscripts<> const &) const &&
        {
            return move(*this)();
        }

        constexpr decltype(auto) operator()(Subscripts<> const &) &
        {
            return operator()();
        }

        constexpr decltype(auto) operator()(Subscripts<> const &) const &
        {
            return operator()();
        }

        constexpr decltype(auto) operator[](Index const index) &&
        {
            return move(mData)[index];
        }

        constexpr decltype(auto) operator[](Index const index) const &&
        {
            return move(mData)[index];
        }

        constexpr decltype(auto) operator[](Index const index) &
        {
            return mData[index];
        }

        constexpr decltype(auto) operator[](Index const index) const &
        {
            return mData[index];
        }

        constexpr auto data() && = delete;
        constexpr auto data() const && = delete;

        constexpr auto data() &
        {
            return mData.data();
        }

        constexpr auto data() const &
        {
            return mData.data();
        }

    private:
        CommonArray<A, T, 1> mData;
    };
}
#endif // pRC_CORE_CONTAINER_ARRAY_SCALAR_H
