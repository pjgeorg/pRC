// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_CONTAINER_SUBSCRIPTS_H
#define pRC_CORE_CONTAINER_SUBSCRIPTS_H

#include <prc/core/basic/sequence.hpp>
#include <prc/core/container/type_traits.hpp>
#include <prc/core/functors/add.hpp>
#include <prc/core/functors/div.hpp>
#include <prc/core/functors/mod.hpp>
#include <prc/core/functors/mul.hpp>
#include <prc/core/functors/sub.hpp>
#include <prc/core/log/log.hpp>
#include <prc/pragma.hpp>

namespace pRC
{
    template<Size... Ns>
    class Subscripts
    {
    public:
        using Sizes = pRC::Sizes<Ns...>;
        using Dimension = typename Sizes::Dimension;

        template<class E = typename Sizes::IsLinearizable, If<E> = 0>
        static constexpr auto size()
        {
            return Sizes::size();
        }

        static constexpr auto size(Index const dimension)
        {
            return Sizes::size(dimension);
        }

    public:
        ~Subscripts() = default;
        constexpr Subscripts() = default;
        constexpr Subscripts(Subscripts const &) = default;
        constexpr Subscripts(Subscripts &&) = default;
        constexpr Subscripts &operator=(Subscripts const &) & = default;
        constexpr Subscripts &operator=(Subscripts &&) & = default;

        template<class... Ss, If<All<IsConvertible<Ss, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Ss) == Dimension())>> = 0>
        explicit constexpr Subscripts(Ss const... subscripts)
            : mSubscripts(subscripts...)
        {
        }

        template<Index D = Dimension(), If<IsSatisfied<(D == 0)>> = 0>
        explicit constexpr Subscripts(Index const index)
        {
            check(index);
        }

        template<Index D = Dimension(), If<IsSatisfied<(D > 1)>> = 0,
            class E = typename Sizes::IsLinearizable, If<E> = 0>
        explicit constexpr Subscripts(Index const index)
        {
            check(index);

            expand(
                makeSeries<Index, Dimension{} - 1>(),
                [this](auto index, auto const... seq)
                {
                    operator[](0) = index % size(0);
                    ((operator[](seq + 1) =
                             (index /= size(seq)) % size(seq + 1)),
                        ...);
                },
                index);
        }

        template<class E = typename Sizes::IsLinearizable, If<E> = 0>
        explicit constexpr operator Index() const
        {
            if constexpr(cDebugLevel >= DebugLevel::Low)
            {
                if(isOutOfRange())
                {
                    Logging::error("Subscripts out of range.");
                }
            }

            return expand(
                makeSeries<Index, Dimension{}>(),
                [](auto subscripts, auto const... seq)
                {
                    [[maybe_unused]] Index scale = 1;
                    ((subscripts[seq] *= scale, scale *= size(seq)), ...);
                    return (subscripts[seq] + ... + 0);
                },
                mSubscripts);
        }

        constexpr decltype(auto) operator[](Index const dimension) &&
        {
            return move(mSubscripts)[dimension];
        }

        constexpr decltype(auto) operator[](Index const dimension) const &&
        {
            return move(mSubscripts)[dimension];
        }

        constexpr decltype(auto) operator[](Index const dimension) &
        {
            return mSubscripts[dimension];
        }

        constexpr decltype(auto) operator[](Index const dimension) const &
        {
            return mSubscripts[dimension];
        }

        constexpr auto isOutOfRange() const
        {
            return expand(makeSeries<Index, Dimension{}>(),
                [this](auto const... seq)
                {
                    return !((this->operator[](seq) < size(seq)) && ...);
                });
        }

        constexpr decltype(auto) periodize() &
        {
            return *this %= Sizes();
        }

        template<class X, If<IsInvocable<Add, Subscripts &, X>> = 0>
        constexpr auto &operator+=(X &&rhs) &
        {
            return *this = *this + forward<X>(rhs);
        }

        template<class X, If<IsInvocable<Sub, Subscripts &, X>> = 0>
        constexpr auto &operator-=(X &&rhs) &
        {
            return *this = *this - forward<X>(rhs);
        }

        template<class X, If<IsInvocable<Mul, Subscripts &, X>> = 0>
        constexpr auto &operator*=(X &&rhs) &
        {
            return *this = *this * forward<X>(rhs);
        }

        template<class X, If<IsInvocable<Div, Subscripts &, X>> = 0>
        constexpr auto &operator/=(X &&rhs) &
        {
            return *this = *this / forward<X>(rhs);
        }

        template<class X, If<IsInvocable<Mod, Subscripts &, X>> = 0>
        constexpr auto &operator%=(X &&rhs) &
        {
            return *this = *this % forward<X>(rhs);
        }

    private:
        constexpr auto check([[maybe_unused]] Index const index)
        {
            if constexpr(cDebugLevel >= DebugLevel::Low)
            {
                if(!(index < size()))
                {
                    Logging::error("Index out of range.");
                }
            }
        }

    private:
        BEGIN_IGNORE_DIAGNOSTIC_GCC("-Wpedantic")
        StackArray<Index, Dimension{}> mSubscripts;
        END_IGNORE_DIAGNOSTIC_GCC
    };

    template<Size... Ns>
    static inline constexpr auto operator+(
        Subscripts<Ns...> const &lhs, Subscripts<Ns...> const &rhs)
    {
        return expand(makeSeriesFor<Index, Ns...>(),
            [&lhs, &rhs](auto const... seq)
            {
                return Subscripts<Ns...>((lhs[seq] + rhs[seq])...);
            });
    }

    template<Size... Ns>
    static inline constexpr auto operator-(
        Subscripts<Ns...> const &lhs, Subscripts<Ns...> const &rhs)
    {
        return expand(makeSeriesFor<Index, Ns...>(),
            [&lhs, &rhs](auto const... seq)
            {
                return Subscripts<Ns...>((lhs[seq] - rhs[seq])...);
            });
    }

    template<Size... Ns, Size... Ss,
        If<IsSatisfied<(sizeof...(Ns) == sizeof...(Ss))>> = 0>
    static inline constexpr auto operator*(
        Subscripts<Ns...> const &lhs, Sizes<Ss...> const)
    {
        return expand(makeSeriesFor<Index, Ns...>(),
            [&lhs](auto const... seq)
            {
                return Subscripts<(
                    Sizes<Ns...>::size(seq) * Sizes<Ss...>::size(seq))...>(
                    (lhs[seq] * Sizes<Ss...>::size(seq))...);
            });
    }

    template<Size... Ns, Size... Ss,
        If<IsSatisfied<(sizeof...(Ns) == sizeof...(Ss))>> = 0>
    static inline constexpr auto operator*(
        Sizes<Ss...> const, Subscripts<Ns...> const &rhs)
    {
        return rhs * Sizes<Ss...>();
    }

    template<Size... Ns, Size... Ss,
        If<IsSatisfied<(sizeof...(Ns) == sizeof...(Ss))>> = 0>
    static inline constexpr auto operator/(
        Subscripts<Ns...> const &lhs, Sizes<Ss...> const)
    {
        return expand(makeSeriesFor<Index, Ns...>(),
            [&lhs](auto const... seq)
            {
                return Subscripts<(
                    Sizes<Ns...>::size(seq) / Sizes<Ss...>::size(seq))...>(
                    (lhs[seq] / Sizes<Ss...>::size(seq))...);
            });
    }

    template<Size... Ns, Size... Ss,
        If<IsSatisfied<(sizeof...(Ns) == sizeof...(Ss))>> = 0>
    static inline constexpr auto operator%(
        Subscripts<Ns...> const &lhs, Sizes<Ss...> const)
    {
        return expand(makeSeriesFor<Index, Ns...>(),
            [&lhs](auto const... seq)
            {
                return Subscripts<Ss...>(
                    (lhs[seq] % Sizes<Ss...>::size(seq))...);
            });
    }

    template<Size... Ns>
    static inline constexpr auto operator==(
        Subscripts<Ns...> const &lhs, Subscripts<Ns...> const &rhs)
    {
        return expand(makeSeriesFor<Index, Ns...>(),
            [&lhs, &rhs](auto const... seq)
            {
                return ((lhs[seq] == rhs[seq]) && ...);
            });
    }

    template<Size... Ns>
    static inline constexpr auto operator!=(
        Subscripts<Ns...> const &lhs, Subscripts<Ns...> const &rhs)
    {
        return !(lhs == rhs);
    }

    template<Index... Ps, Size... Ns,
        If<IsSatisfied<(sizeof...(Ps) == sizeof...(Ns))>> = 0,
        If<IsSatisfied<(max(Ps...) < sizeof...(Ns))>> = 0,
        If<IsUnique<Constant<Index, Ps>...>> = 0>
    static inline constexpr auto permute(Subscripts<Ns...> const &arg)
    {
        return expand(permute<Ps...>(makeSeriesFor<Index, Ns...>()),
            [&arg](auto const... seq)
            {
                return Subscripts<Sizes<Ns...>::size(seq)...>(arg[seq]...);
            });
    }

    template<Size... Ns>
    static inline constexpr auto reverse(Subscripts<Ns...> const &arg)
    {
        return expand(reverse(makeSeriesFor<Index, Ns...>()),
            [&arg](auto const... seq)
            {
                return permute<seq...>(arg);
            });
    }

    template<Direction D, Size Step, Size... Ns,
        If<True<decltype(rotate<D, Step>(makeSeriesFor<Index, Ns...>()))>> = 0>
    static inline constexpr auto rotate(Subscripts<Ns...> const &arg)
    {
        return expand(rotate<D, Step>(makeSeriesFor<Index, Ns...>()),
            [&arg](auto const... seq)
            {
                return permute<seq...>(arg);
            });
    }

    template<Index... Ds, Size... Ns,
        If<True<decltype(chip<Ds...>(Sequence<Index, Ns...>()))>> = 0>
    static inline constexpr auto chip(Subscripts<Ns...> const &arg)
    {
        return expand(chip<Ds...>(makeSeriesFor<Index, Ns...>()),
            [&arg](auto const... seq)
            {
                return Subscripts<Sizes<Ns...>::size(seq)...>(arg[seq]...);
            });
    }
}
#endif // pRC_CORE_CONTAINER_SUBSCRIPTS_H
