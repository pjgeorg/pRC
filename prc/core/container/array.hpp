// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_CONTAINER_ARRAY_H
#define pRC_CORE_CONTAINER_ARRAY_H

#include <prc/core/basic/range.hpp>
#include <prc/core/basic/sequence.hpp>
#include <prc/core/container/array_heap.hpp>
#include <prc/core/container/array_scalar.hpp>
#include <prc/core/container/array_stack.hpp>
#include <prc/core/container/subscripts.hpp>
#include <prc/core/container/type_traits.hpp>

namespace pRC
{
    template<Allocation A, class T, Size N, Size... Ns>
    class CommonArray<A, T, N, Ns...>
    {
        static_assert((Ns * ... * N) <= NumericLimits<Size>::max() &&
            typename pRC::Sizes<N, Ns...>::IsLinearizable());

    public:
        using Allocation = Constant<pRC::Allocation, A>;
        using Type = T;
        using Sizes = pRC::Sizes<N, Ns...>;
        using Dimension = typename Sizes::Dimension;

        template<class E = typename Sizes::IsLinearizable, If<E> = 0>
        static constexpr auto size()
        {
            return Sizes::size();
        }

        static constexpr auto size(Index const dimension)
        {
            return Sizes::size(dimension);
        }

        template<class E = IsConstructible<Subscripts<N, Ns...>, Index>,
            If<E> = 0>
        static constexpr auto indexToSubscripts(Index const index)
        {
            return Subscripts<N, Ns...>(index);
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == Dimension())>> = 0,
            class E = All<IsConstructible<Subscripts<N, Ns...>, Index>,
                IsConstructible<Index, Subscripts<N, Ns...>>>,
            If<E> = 0>
        static constexpr auto subscriptsToIndex(Is const... subscripts)
        {
            return Index(Subscripts<N, Ns...>(subscripts...));
        }

        template<class E = IsConstructible<Index, Subscripts<N, Ns...>>,
            If<E> = 0>
        static constexpr auto subscriptsToIndex(
            Subscripts<N, Ns...> const &subscripts)
        {
            return Index(subscripts);
        }

    public:
        ~CommonArray() = default;
        constexpr CommonArray() = default;

        constexpr CommonArray(CommonArray const &other)
        {
            *this = other;
        }

        template<pRC::Allocation B = Allocation(),
            If<IsSatisfied<B == pRC::Allocation::Stack>> = 0>
        constexpr CommonArray(StackArray<T, N, Ns...> &&other)
        {
            operator=<Allocation{}, T>(other);
        }

        template<pRC::Allocation B = Allocation(),
            If<IsSatisfied<B == pRC::Allocation::Heap>> = 0>
        constexpr CommonArray(HeapArray<T, N, Ns...> &&other)
            : mData(move(other.mData))
        {
        }

        template<pRC::Allocation B, class R, If<IsConvertible<R, T>> = 0>
        constexpr CommonArray(CommonArray<B, R, N, Ns...> const &other)
        {
            *this = other;
        }

        constexpr CommonArray &operator=(CommonArray const &rhs) &
        {
            return operator=<A, T>(rhs);
        }

        constexpr CommonArray &operator=(CommonArray &&rhs) &
        {
            if constexpr(Allocation() == pRC::Allocation::Stack)
            {
                range<size()>(
                    [this, &rhs](auto const i)
                    {
                        (*this)[i] = move(rhs[i]);
                    });
            }
            else
            {
                mData = move(rhs.mData);
            }
            return *this;
        }

        template<pRC::Allocation B, class R, If<IsConvertible<R, T>> = 0>
        constexpr auto &operator=(CommonArray<B, R, N, Ns...> const &rhs) &
        {
            range<size()>(
                [this, &rhs](auto const i)
                {
                    (*this)[i] = rhs[i];
                });
            return *this;
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == sizeof...(Ns))>> = 0>
        constexpr decltype(auto) operator()(
            Index const subscript, Is const... subscripts) &&
        {
            return move(mData)(subscripts...)[subscript];
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == sizeof...(Ns))>> = 0>
        constexpr decltype(auto) operator()(
            Index const subscript, Is const... subscripts) const &&
        {
            return move(mData)(subscripts...)[subscript];
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == sizeof...(Ns))>> = 0>
        constexpr decltype(auto) operator()(
            Index const subscript, Is const... subscripts) &
        {
            return mData(subscripts...)[subscript];
        }

        template<class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<IsSatisfied<(sizeof...(Is) == sizeof...(Ns))>> = 0>
        constexpr decltype(auto) operator()(
            Index const subscript, Is const... subscripts) const &
        {
            return mData(subscripts...)[subscript];
        }

        constexpr decltype(auto) operator()(
            Subscripts<N, Ns...> const &subscripts) &&
        {
            return expand(
                makeSeries<Index, Dimension{}>(),
                [this](auto const &subscripts, auto const... seq) -> decltype(
                                                                      auto)
                {
                    return move(*this)(subscripts[seq]...);
                },
                subscripts);
        }

        constexpr decltype(auto) operator()(
            Subscripts<N, Ns...> const &subscripts) const &&
        {
            return expand(
                makeSeries<Index, Dimension{}>(),
                [this](auto const &subscripts, auto const... seq) -> decltype(
                                                                      auto)
                {
                    return move(*this)(subscripts[seq]...);
                },
                subscripts);
        }

        constexpr decltype(auto) operator()(
            Subscripts<N, Ns...> const &subscripts) &
        {
            return expand(
                makeSeries<Index, Dimension{}>(),
                [this](auto const &subscripts, auto const... seq) -> decltype(
                                                                      auto)
                {
                    return this->operator()(subscripts[seq]...);
                },
                subscripts);
        }

        constexpr decltype(auto) operator()(
            Subscripts<N, Ns...> const &subscripts) const &
        {
            return expand(
                makeSeries<Index, Dimension{}>(),
                [this](auto const &subscripts, auto const... seq) -> decltype(
                                                                      auto)
                {
                    return this->operator()(subscripts[seq]...);
                },
                subscripts);
        }

        constexpr decltype(auto) operator[](Index const index) &&
        {
            checkFlattenable();
            return move(
                *reinterpret_cast<StackArray<T, size()> *>(data()))[index];
        }

        constexpr decltype(auto) operator[](Index const index) const &&
        {
            checkFlattenable();
            return move(*reinterpret_cast<StackArray<T, size()> const *>(
                data()))[index];
        }

        constexpr decltype(auto) operator[](Index const index) &
        {
            checkFlattenable();
            return (*reinterpret_cast<StackArray<T, size()> *>(data()))[index];
        }

        constexpr decltype(auto) operator[](Index const index) const &
        {
            checkFlattenable();
            return (*reinterpret_cast<StackArray<T, size()> const *>(
                data()))[index];
        }

        constexpr auto data() && = delete;
        constexpr auto data() const && = delete;

        constexpr auto data() &
        {
            return mData.data();
        }

        constexpr auto data() const &
        {
            return mData.data();
        }

    private:
        static constexpr auto checkFlattenable()
        {
            if constexpr((sizeof(StackArray<StackArray<T, N>, Ns...>) !=
                                 sizeof(StackArray<T, size()>) &&
                             size() != 0) ||
                !IsStandardLayout<CommonArray<A, StackArray<T, N>, Ns...>>() ||
                !IsStandardLayout<StackArray<T, size()>>() ||
                offsetof(CommonArray, mData) != 0)
            {
                static_assert(False<T>(),
                    "Unable to flatten multi-dimensional array. Must be some "
                    "weird underlying type.");
            }
        }

    private:
        BEGIN_IGNORE_DIAGNOSTIC_GCC("-Wpedantic")
        CommonArray<A, StackArray<T, N>, Ns...> mData;
        END_IGNORE_DIAGNOSTIC_GCC
    };
}
#endif // pRC_CORE_CONTAINER_ARRAY_H
