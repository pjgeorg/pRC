// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_CONTAINER_TYPE_TRAITS_H
#define pRC_CORE_CONTAINER_TYPE_TRAITS_H

#include <prc/config.hpp>
#include <prc/core/basic/type_traits.hpp>
#include <prc/core/container/allocation.hpp>

namespace pRC
{
    template<Size N>
    class Indices;

    template<class>
    struct IsIndices : False<>
    {
    };

    template<class T>
    struct IsIndices<T const> : IsIndices<T>
    {
    };

    template<Size N>
    struct IsIndices<Indices<N>> : True<>
    {
    };

    template<Size... Ns>
    class Subscripts;

    template<class>
    struct IsSubscripts : False<>
    {
    };

    template<class T>
    struct IsSubscripts<T const> : IsSubscripts<T>
    {
    };

    template<Size... Ns>
    struct IsSubscripts<Subscripts<Ns...>> : True<>
    {
    };

    template<Allocation, class, Size...>
    class CommonArray;

    template<class T, Size... Ns>
    using StackArray = CommonArray<Allocation::Stack, T, Ns...>;

    template<class T, Size... Ns>
    using HeapArray = CommonArray<Allocation::Heap, T, Ns...>;

    template<class T, Size... Ns>
    using Array = Conditional<
        IsSatisfied<((Ns * ... * 1) * sizeof(T) > cHugepageSizeByte)>,
        HeapArray<T, Ns...>, StackArray<T, Ns...>>;

    template<class>
    struct IsArray : False<>
    {
    };

    template<class T>
    struct IsArray<T const> : IsArray<T>
    {
    };

    template<Allocation A, class T, Size... Ns>
    struct IsArray<CommonArray<A, T, Ns...>> : True<>
    {
    };

    template<class, Size>
    class Deque;

    template<class>
    struct IsDeque : False<>
    {
    };

    template<class T>
    struct IsDeque<T const> : IsDeque<T>
    {
    };

    template<class T, Size N>
    struct IsDeque<Deque<T, N>> : True<>
    {
    };
}
#endif // pRC_CORE_CONTAINER_TYPE_TRAITS_H
