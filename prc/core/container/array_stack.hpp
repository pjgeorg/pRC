// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_CONTAINER_ARRAY_STACK_H
#define pRC_CORE_CONTAINER_ARRAY_STACK_H

#include <prc/core/basic/range.hpp>
#include <prc/core/basic/sequence.hpp>
#include <prc/core/container/allocation.hpp>
#include <prc/core/container/subscripts.hpp>
#include <prc/core/container/type_traits.hpp>
#include <prc/core/log/log.hpp>
#include <prc/pragma.hpp>

namespace pRC
{
    template<class T, Size N>
    CommonArray(T const (&)[N]) -> CommonArray<Allocation::Stack, T, N>;

    template<class T, Size N>
    class CommonArray<Allocation::Stack, T, N>
    {
    public:
        using Allocation = Constant<pRC::Allocation, pRC::Allocation::Stack>;
        using Type = T;
        using Sizes = pRC::Sizes<N>;
        using Dimension = typename Sizes::Dimension;

        static constexpr auto size()
        {
            return Sizes::size();
        }

        static constexpr auto size(Index const dimension)
        {
            return Sizes::size(dimension);
        }

        static constexpr auto indexToSubscripts(Index const index)
        {
            return Subscripts<N>(index);
        }

        static constexpr auto subscriptsToIndex(Index const subscripts)
        {
            return subscripts;
        }

        static constexpr auto subscriptsToIndex(Subscripts<N> const &subscripts)
        {
            return Index(subscripts);
        }

    private:
        BEGIN_IGNORE_DIAGNOSTIC_GCC("-Wpedantic")
        BEGIN_IGNORE_DIAGNOSTIC_CLANG("-Wzero-length-array")
        template<class R, Size S>
        using CArray = R[S];
        END_IGNORE_DIAGNOSTIC_CLANG
        END_IGNORE_DIAGNOSTIC_GCC

        static constexpr auto check([[maybe_unused]] Index const index)
        {
            if constexpr(cDebugLevel >= DebugLevel::Mid)
            {
                if(!(index < size()))
                {
                    Logging::error("Array index out of range.");
                }
            }
        }

    public:
        ~CommonArray() = default;
        constexpr CommonArray(CommonArray const &) = default;
        constexpr CommonArray(CommonArray &&) = default;
        constexpr CommonArray &operator=(CommonArray const &) & = default;
        constexpr CommonArray &operator=(CommonArray &&) & = default;
        constexpr CommonArray() = default;

        template<pRC::Allocation B, class R, If<IsConvertible<R, T>> = 0>
        constexpr CommonArray(CommonArray<B, R, N> const &other)
        {
            *this = other;
        }

        template<class R, If<IsConvertible<R, T>> = 0>
        constexpr CommonArray(CArray<R, N> const &other)
            : CommonArray(makeSeries<Index, N>(), other)
        {
        }

        template<class... Rs, If<All<IsConvertible<Rs, T>...>> = 0,
            If<IsSatisfied<(sizeof...(Rs) == N)>> = 0>
        constexpr CommonArray(Rs const &...values)
            : mData{static_cast<T>(values)...}
        {
        }

        template<pRC::Allocation B, class R, If<IsConvertible<R, T>> = 0>
        constexpr auto &operator=(CommonArray<B, R, N> const &rhs) &
        {
            range<size()>(
                [this, &rhs](auto const i)
                {
                    operator[](i) = rhs[i];
                });
            return *this;
        }

        template<class R, If<IsConvertible<R, T>> = 0>
        constexpr auto &operator=(CArray<R, N> const &rhs) &
        {
            range<size()>(
                [this, &rhs](auto const i)
                {
                    operator[](i) = rhs[i];
                });
            return *this;
        }

        constexpr decltype(auto) operator()(Index const subscript) &&
        {
            return move(*this)[subscript];
        }

        constexpr decltype(auto) operator()(Index const subscript) const &&
        {
            return move(*this)[subscript];
        }

        constexpr decltype(auto) operator()(Index const subscript) &
        {
            return operator[](subscript);
        }

        constexpr decltype(auto) operator()(Index const subscript) const &
        {
            return operator[](subscript);
        }

        constexpr decltype(auto) operator()(Subscripts<N> const &subscripts) &&
        {
            return move(*this)[Index(subscripts)];
        }

        constexpr decltype(auto) operator()(
            Subscripts<N> const &subscripts) const &&
        {
            return move(*this)[Index(subscripts)];
        }

        constexpr decltype(auto) operator()(Subscripts<N> const &subscripts) &
        {
            return operator[](Index(subscripts));
        }

        constexpr decltype(auto) operator()(
            Subscripts<N> const &subscripts) const &
        {
            return operator[](Index(subscripts));
        }

        constexpr decltype(auto) operator[](Index const index) &&
        {
            check(index);
            return move(mData[index]);
        }

        constexpr decltype(auto) operator[](Index const index) const &&
        {
            check(index);
            return move(mData[index]);
        }

        constexpr auto &operator[](Index const index) &
        {
            check(index);
            return mData[index];
        }

        constexpr auto &operator[](Index const index) const &
        {
            check(index);
            return mData[index];
        }

        constexpr auto data() && = delete;
        constexpr auto data() const && = delete;

        constexpr auto data() &
        {
            return mData;
        }

        constexpr auto data() const &
        {
            return mData;
        }

    private:
        template<Index... Is, class R, If<IsConvertible<R, T>> = 0,
            If<IsSatisfied<(sizeof...(Is) == N)>> = 0>
        constexpr CommonArray(
            Sequence<Index, Is...> const, CArray<R, N> const &other)
            : mData{other[Is]...}
        {
        }

    private:
        BEGIN_IGNORE_DIAGNOSTIC_GCC("-Wpedantic")
        alignas(alignment<sizeof(T) * N, alignof(T)>()) CArray<T, N> mData;
        END_IGNORE_DIAGNOSTIC_GCC
    };
}
#endif // pRC_CORE_CONTAINER_ARRAY_STACK_H
