// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_LOG_IO_H
#define pRC_CORE_LOG_IO_H

#include <cstdio>

#include <prc/config.hpp>
#include <prc/core/basic/lock.hpp>
#include <prc/core/log/print.hpp>

namespace pRC::Logging
{
    static inline Mutex Mutex;

    template<LogLevel L = LogLevel::Error, Bool E = false, class X, class... Xs>
    static inline void log(X &&arg, Xs &&...args)
    {
        if constexpr(cLogLevel >= L)
        {
            auto stream = []()
            {
                if constexpr(E)
                {
                    return stderr;
                }
                else
                {
                    return stdout;
                }
            }();

            Lock const lock(Mutex);

            print(forward<X>(arg), stream);

            ((print(' ', stream), print(forward<Xs>(args), stream)), ...);

            print('\n', stream);

            std::fflush(stream);
        }
    }
}
#endif // pRC_CORE_LOG_IO_H
