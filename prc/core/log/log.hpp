// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_LOG_LOG_H
#define pRC_CORE_LOG_LOG_H

#include <cstdlib>

#include <prc/config.hpp>
#include <prc/core/log/io.hpp>

namespace pRC::Logging
{
    template<class... Xs>
    [[noreturn]] static inline void error(Xs &&...args)
    {
        log<LogLevel::Error, true>("Error:", forward<Xs>(args)...);
        std::abort();
    }

    template<class... Xs>
    static inline void warning(Xs &&...args)
    {
        return log<LogLevel::Warning>("Warning:", forward<Xs>(args)...);
    }

    template<class... Xs>
    static inline void info(Xs &&...args)
    {
        return log<LogLevel::Info>("Info:", forward<Xs>(args)...);
    }

    template<class... Xs>
    static inline void debug(Xs &&...args)
    {
        return log<LogLevel::Debug>("Debug:", forward<Xs>(args)...);
    }

    template<class... Xs>
    static inline void trace(Xs &&...args)
    {
        return log<LogLevel::Trace>("Trace:", forward<Xs>(args)...);
    }
}
#endif // pRC_CORE_LOG_LOG_H
