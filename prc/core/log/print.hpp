// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_LOG_PRINT_H
#define pRC_CORE_LOG_PRINT_H

#include <cstdio>
#include <string>

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<class T>
    static inline constexpr auto name();

    template<auto T>
    static inline constexpr auto name();

    template<class T, T V, class S>
    static inline auto print(Constant<T, V> const, S &&stream)
    {
        print("pRC::Constant<" + name<T>() + ", " + name<V>() + ">", stream);
    }

    template<class T, If<IsSame<T, Bool>> = 0>
    static inline auto print(T const value, std::FILE *stream)
    {
        if(value)
        {
            std::fprintf(stream, "True");
        }
        else
        {
            std::fprintf(stream, "False");
        }
    }

    template<class T, If<IsIntegral<T>> = 0>
    static inline auto print(T const value, std::FILE *stream)
    {
        if constexpr(IsSame<T, int>())
        {
            std::fprintf(stream, "% d", value);
        }
        else if constexpr(IsSame<T, short>())
        {
            std::fprintf(stream, "% hd", value);
        }
        else if constexpr(IsSame<T, long>())
        {
            std::fprintf(stream, "% ld", value);
        }
        else if constexpr(IsSame<T, long long>())
        {
            std::fprintf(stream, "% lld", value);
        }
        else if constexpr(IsSame<T, unsigned>())
        {
            std::fprintf(stream, "%u", value);
        }
        else if constexpr(IsSame<T, unsigned short>())
        {
            std::fprintf(stream, "%hu", value);
        }
        else if constexpr(IsSame<T, unsigned long>())
        {
            std::fprintf(stream, "%lu", value);
        }
        else if constexpr(IsSame<T, unsigned long long>())
        {
            std::fprintf(stream, "%llu", value);
        }
        else if constexpr(IsSame<T, char>() || IsSame<T, unsigned char>() ||
            IsSame<T, signed char>())
        {
            std::fputc(value, stream);
        }
    }

    static inline auto print(char const *const string, std::FILE *stream)
    {
        std::fputs(string, stream);
    }

    static inline auto print(std::string const &string, std::FILE *stream)
    {
        print(string.c_str(), stream);
    }
}
#endif // pRC_CORE_LOG_PRINT_H
