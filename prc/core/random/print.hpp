// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_RANDOM_PRINT_H
#define pRC_CORE_RANDOM_PRINT_H

#include <prc/core/basic/functions/print.hpp>
#include <prc/core/random/seq.hpp>

namespace pRC
{
    template<Size N, class S>
    static inline auto print(SeedSequence<N> const &seq, S &&stream)
    {
        print(name<SeedSequence<N>>(), stream);
        print(":", stream);
        for(Index i = 0; i < N; ++i)
        {
            print(" ", stream);
            print(seq.param()[i], stream);
        }
    }
}
#endif // pRC_CORE_RANDOM_PRINT_H
