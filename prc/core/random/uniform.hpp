// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_RANDOM_UNIFORM_H
#define pRC_CORE_RANDOM_UNIFORM_H

#include <prc/core/basic/functions/identity.hpp>
#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/log/log.hpp>
#include <prc/core/random/canonical.hpp>
#include <prc/core/random/type_traits.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T>
    class UniformDistribution<T, If<Any<IsFloat<T>, IsInteger<T>>>>
    {
    public:
        constexpr explicit UniformDistribution(
            T const a = zero(),
            T const b =
                []()
            {
                if constexpr(IsFloat<T>())
                {
                    return identity<T>();
                }
                else
                {
                    return NumericLimits<T>::max();
                }
            }())
            : mA(a)
            , mB(b)
        {
            if constexpr(IsFloat<T>())
            {
                if(mA >= mB)
                {
                    Logging::error(
                        "Parameter a of uniform real distribution must be less "
                        "than parameter b. Range of values is [a,b).");
                }
            }
            else
            {
                if(mA > mB)
                {
                    Logging::error(
                        "Parameter a of uniform integer distribution must be "
                        "less than or equal to parameter b. Range of values is "
                        "[a,b].");
                }
            }
        }

        constexpr auto reset() {}

        constexpr auto &a() const
        {
            return mA;
        }

        constexpr auto &b() const
        {
            return mB;
        }

        constexpr auto min() const
        {
            return a();
        }

        constexpr auto max() const
        {
            return b();
        }

        template<class URNG>
        constexpr auto operator()(URNG &rng)
        {
            if constexpr(IsFloat<T>())
            {
                return (b() - a()) * generateCanonical<T>(rng) + a();
            }
            else
            {
                using UInt = Common<typename URNG::result_type,
                    typename T::template ChangeSigned<false>>;

                constexpr auto range = [](auto const a, auto const b)
                {
                    if constexpr(typename T::Signed())
                    {
                        if(a < 0)
                        {
                            return UInt(b) + UInt(-a);
                        }
                    }
                    return UInt(b) - UInt(a);
                };

                auto const r = range(a(), b());
                if(r == 0)
                {
                    return a();
                }
                constexpr auto R = range(URNG::min(), URNG::max());

                UInt basic;
                if(R == r)
                {
                    basic = UInt(rng()) - URNG::min();
                }
                else if(R > r)
                {
                    auto const scaling = R / (r + UInt(1));
                    auto const past = (r + UInt(1)) * scaling;
                    do
                    {
                        basic = UInt(rng()) - URNG::min();
                    }
                    while(basic >= past);
                    basic /= scaling;
                }
                else
                {
                    if constexpr(R != NumericLimits<UInt>::max())
                    {
                        UInt tmp;
                        do
                        {
                            tmp = ((R + 1) *
                                   operator()(rng, param_type(0, r / (R + 1))));
                            basic = tmp + (UInt(rng()) - URNG::min());
                        }
                        while(basic > r || basic < tmp);
                    }
                    else
                    {
                        Logging::error(
                            "Found the impossible basic R < r of same type "
                            "with R being the maximum limit.");
                    }
                }

                return basic + a();
            }
        }

    private:
        T mA;
        T mB;
    };

    template<class T>
    constexpr auto operator==(
        UniformDistribution<T> const &lhs, UniformDistribution<T> const &rhs)
    {
        return lhs.a() == rhs.a() && lhs.b() == rhs.b();
    }

    template<class T>
    constexpr auto operator!=(
        UniformDistribution<T> const &lhs, UniformDistribution<T> const &rhs)
    {
        return !(lhs == rhs);
    }
}
#endif // pRC_CORE_RANDOM_UNIFORM_H
