// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_RANDOM_TYPE_TRAITS_H
#define pRC_CORE_RANDOM_TYPE_TRAITS_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<Size Rounds, class U = Seed>
    class Threefry;

    using RandomEngine = Threefry<20>;

    template<class>
    struct IsRandomEngine : False<>
    {
    };

    template<class T>
    struct IsRandomEngine<T const> : IsRandomEngine<T>
    {
    };

    template<Size R, class U>
    struct IsRandomEngine<Threefry<R, U>> : True<>
    {
    };

    template<class>
    struct IsDistribution : False<>
    {
    };

    template<class T>
    struct IsDistribution<T const> : IsDistribution<T>
    {
    };

    template<class T, class = If<>>
    class LaplaceDistribution;

    template<class T>
    struct IsDistribution<LaplaceDistribution<T>> : True<>
    {
    };

    template<class T, class = If<>>
    class UniformDistribution;

    template<class T, class V>
    struct IsDistribution<UniformDistribution<T, V>> : True<>
    {
    };

    template<class T, class = If<>>
    class GaussianDistribution;

    template<class T, class V>
    struct IsDistribution<GaussianDistribution<T, V>> : True<>
    {
    };
}
#endif // pRC_CORE_RANDOM_TYPE_TRAITS_H
