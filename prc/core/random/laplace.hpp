// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_RANDOM_LAPLACE_H
#define pRC_CORE_RANDOM_LAPLACE_H

#include <prc/core/basic/functions/identity.hpp>
#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/log/log.hpp>
#include <prc/core/random/canonical.hpp>
#include <prc/core/random/type_traits.hpp>
#include <prc/core/value/functions/log.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T>
    class LaplaceDistribution<T, If<IsFloat<T>>>
    {
    public:
        constexpr explicit LaplaceDistribution(
            T const mu = zero(), T const b = identity())
            : mMu(mu)
            , mB(b)
        {
            if(!(mB > zero()))
            {
                Logging::error(
                    "Parameter b of Laplace distribution must be positive.");
            }
        }

        constexpr auto reset() {}

        constexpr auto &mu() const
        {
            return mMu;
        }

        constexpr auto &b() const
        {
            return mB;
        }

        constexpr auto min() const
        {
            return NumericLimits<T>::lowest();
        }

        constexpr auto max() const
        {
            return NumericLimits<T>::max();
        }

        template<class URNG>
        constexpr auto operator()(URNG &rng)
        {
            auto const q = generateCanonical<T>(rng);
            if(q < T(0.5))
            {
                return b() * log(T(2) * q) + mu();
            }
            else
            {
                return -b() * log(T(2) - q * T(2)) + mu();
            }
        }

    private:
        T mMu;
        T mB;
    };

    template<class T>
    constexpr auto operator==(
        LaplaceDistribution<T> const &lhs, LaplaceDistribution<T> const &rhs)
    {
        return lhs.mu() == rhs.mu() && lhs.b() == rhs.b();
    }

    template<class T>
    constexpr auto operator!=(
        LaplaceDistribution<T> const &lhs, LaplaceDistribution<T> const &rhs)
    {
        return !(lhs == rhs);
    }
}
#endif // pRC_CORE_RANDOM_LAPLACE_H
