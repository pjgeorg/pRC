// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_COMPLEX_H
#define pRC_CORE_COMPLEX_COMPLEX_H

#include <prc/core/basic/common.hpp>
#include <prc/core/basic/functions/identity.hpp>
#include <prc/core/basic/functions/unit.hpp>
#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/complex/type_traits.hpp>
#include <prc/core/functors/add.hpp>
#include <prc/core/functors/div.hpp>
#include <prc/core/functors/mul.hpp>
#include <prc/core/functors/sub.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T, If<IsFloat<T>> = 0>
    Complex(T const &) -> Complex<T>;
    template<class TA, class TB, If<All<IsFloat<TA>, IsFloat<TB>>> = 0>
    Complex(TA const &, TB const &) -> Complex<Common<TA, TB>>;

    template<class T>
    class Complex
    {
        static_assert(IsFloat<T>(), "Complex<T>: T has to be of type Float.");

    public:
        using Type = T;
        template<class C, If<IsFloat<C>> = 0>
        using ChangeType = Complex<C>;

        using Value = typename T::Value;
        template<class V, If<IsValue<V>> = 0>
        using ChangeValue = Complex<typename T::template ChangeValue<V>>;

        using Signed = typename T::Signed;
        template<Bool R>
        using ChangeSigned = Complex<typename T::template ChangeSigned<R>>;

        using Width = typename T::Width;
        template<Size Q>
        using ChangeWidth = Complex<typename T::template ChangeWidth<Q>>;

        using IsComplexified = True<>;
        using Complexify = Complex<T>;
        using NonComplex = T;

    public:
        ~Complex() = default;
        constexpr Complex(Complex const &) = default;
        constexpr Complex(Complex &&) = default;
        constexpr Complex &operator=(Complex const &) & = default;
        constexpr Complex &operator=(Complex &&) & = default;
        Complex() = default;

        template<class R, If<IsConvertible<R, T>> = 0>
        Complex(Complex<R> const &other)
            : mReal(other.real())
            , mImag(other.imag())
        {
        }

        template<class R, If<IsConvertible<R, T>> = 0>
        constexpr Complex(R const &real, R const &imag)
            : mReal(real)
            , mImag(imag)
        {
        }

        template<class R, If<IsConvertible<R, T>> = 0>
        Complex(R const &real)
            : mReal(real)
            , mImag(zero())
        {
        }

        constexpr Complex(Zero<> const)
            : Complex(zero<Complex>())
        {
        }

        constexpr Complex(Unit<> const)
            : Complex(unit<Complex>())
        {
        }

        constexpr Complex(Identity<> const)
            : Complex(identity<Complex>())
        {
        }

        template<class R, If<IsConvertible<R, T>> = 0>
        constexpr auto &operator=(Complex<R> const &rhs) &
        {
            mReal = rhs.real();
            mImag = rhs.imag();
            return *this;
        }

        template<class R, If<IsConvertible<R, T>> = 0>
        constexpr auto &operator=(R const &real) &
        {
            mReal = real;
            mImag = zero();
            return *this;
        }

        constexpr auto &operator=(Zero<> const) &
        {
            return *this = zero<Complex>();
        }

        constexpr auto &operator=(Unit<> const) &
        {
            return *this = unit<Complex>();
        }

        constexpr auto &operator=(Identity<> const) &
        {
            return *this = identity<Complex>();
        }

        constexpr decltype(auto) real() &&
        {
            return move(mReal);
        }

        constexpr decltype(auto) real() const &&
        {
            return move(mReal);
        }

        constexpr auto &real() &
        {
            return mReal;
        }

        constexpr auto &real() const &
        {
            return mReal;
        }

        constexpr decltype(auto) imag() &&
        {
            return move(mImag);
        }

        constexpr decltype(auto) imag() const &&
        {
            return move(mImag);
        }

        constexpr auto &imag() &
        {
            return mImag;
        }

        constexpr auto &imag() const &
        {
            return mImag;
        }

        template<class X, If<IsInvocable<Add, Complex &, X>> = 0>
        constexpr auto &operator+=(X &&rhs) &
        {
            return *this = *this + forward<X>(rhs);
        }

        template<class X, If<IsInvocable<Sub, Complex &, X>> = 0>
        constexpr auto &operator-=(X &&rhs) &
        {
            return *this = *this - forward<X>(rhs);
        }

        template<class X, If<IsInvocable<Mul, Complex &, X>> = 0>
        constexpr auto &operator*=(X &&rhs) &
        {
            return *this = *this * forward<X>(rhs);
        }

        template<class X, If<IsInvocable<Div, Complex &, X>> = 0>
        constexpr auto &operator/=(X &&rhs) &
        {
            return *this = *this / forward<X>(rhs);
        }

    private:
        T mReal;
        T mImag;
    };
}
#endif // pRC_CORE_COMPLEX_COMPLEX_H
