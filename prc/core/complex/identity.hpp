// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_IDENTITY_H
#define pRC_CORE_COMPLEX_IDENTITY_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T>
    struct Identity<T, If<IsComplex<T>>>
    {
        constexpr auto operator()()
        {
            using U = typename T::Type;
            return Complex<U>(identity<U>(), zero<U>());
        }

        template<class X, If<IsConstructible<T, X>> = 0>
        constexpr auto operator()(X &&value)
        {
            using U = typename T::Type;
            return Complex<U>(forward<X>(value), zero<U>());
        }
    };
}
#endif // pRC_CORE_COMPLEX_IDENTITY_H
