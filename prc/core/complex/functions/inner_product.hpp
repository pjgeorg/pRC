// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_INNER_PRODUCT_H
#define pRC_CORE_COMPLEX_FUNCTIONS_INNER_PRODUCT_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/complex/functions/conj.hpp>
#include <prc/core/complex/functions/scalar_product.hpp>
#include <prc/core/functors/conj.hpp>
#include <prc/core/functors/scalar_product.hpp>

namespace pRC
{
    template<class TA, class TB, If<IsInvocable<Conj, Complex<TA>>> = 0,
        If<IsInvocable<ScalarProduct, Complex<TA>, Complex<TB>>> = 0>
    static inline constexpr auto innerProduct(
        Complex<TA> const &a, Complex<TB> const &b)
    {
        return scalarProduct(conj(a), b);
    }

    template<class TA, class TB, If<IsValue<TB>> = 0,
        If<IsInvocable<Conj, Complex<TA>>> = 0,
        If<IsInvocable<ScalarProduct, Complex<TA>, TB>> = 0>
    static inline constexpr auto innerProduct(Complex<TA> const &a, TB const &b)
    {
        return scalarProduct(conj(a), b);
    }

    template<class TA, class TB, If<IsValue<TA>> = 0,
        If<IsInvocable<ScalarProduct, TA, Complex<TB>>> = 0>
    static inline constexpr auto innerProduct(TA const &a, Complex<TB> const &b)
    {
        return scalarProduct(a, b);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_INNER_PRODUCT_H
