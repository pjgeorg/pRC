// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_ACOS_H
#define pRC_CORE_COMPLEX_FUNCTIONS_ACOS_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/complex/functions/asin.hpp>
#include <prc/core/value/functions/pi.hpp>

namespace pRC
{
    template<class T>
    static inline constexpr auto acos(Complex<T> const &a)
    {
        auto const t = asin(a);
        auto const real = pi<T>() / identity<T>(2) - t.real();
        auto const imag = t.imag();

        return Complex(real, imag);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_ACOS_H
