// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_POW_H
#define pRC_CORE_COMPLEX_FUNCTIONS_POW_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/complex/functions/exp.hpp>
#include <prc/core/complex/functions/log.hpp>

namespace pRC
{
    template<class X, class Y>
    static inline constexpr auto pow(Complex<X> const &x, Complex<Y> const &y)
    {
        return exp(y * log(x));
    }

    template<class X, class Y, If<IsValue<Y>> = 0>
    static inline constexpr auto pow(Complex<X> const &x, Y const &y)
    {
        return exp(y * log(x));
    }

    template<class X, class Y, If<IsValue<X>> = 0>
    static inline constexpr auto pow(X const &x, Complex<Y> const &y)
    {
        return exp(y * log(x));
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_POW_H
