// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_COS_H
#define pRC_CORE_COMPLEX_FUNCTIONS_COS_H

#include <prc/core/complex/complex.hpp>

namespace pRC
{
    template<class T>
    static inline constexpr auto cos(Complex<T> const &a)
    {
        auto const real = cos(a.real()) * cosh(a.imag());
        auto const imag = -sin(a.real()) * sinh(a.imag());

        return Complex(real, imag);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_COS_H
