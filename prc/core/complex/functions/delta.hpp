// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_DELTA_H
#define pRC_CORE_COMPLEX_FUNCTIONS_DELTA_H

#include <prc/core/complex/type_traits.hpp>
#include <prc/core/functors/norm.hpp>

namespace pRC
{
    template<class TA, class TB, If<IsInvocable<Sub, TA, TB>> = 0>
    static inline constexpr auto delta(
        Complex<TA> const &a, Complex<TB> const &b)
    {
        auto const real = delta(a.real(), b.real());
        auto const imag = delta(a.imag(), b.imag());

        return abs(Complex(real, imag));
    }

    template<class TA, class TB, If<IsValue<TB>> = 0,
        If<IsInvocable<Sub, TA, TB>> = 0>
    static inline constexpr auto delta(Complex<TA> const &a, TB const &b)
    {
        auto const real = delta(a.real(), b.real());
        auto const imag = a.imag();

        return abs(Complex(real, imag));
    }

    template<class TA, class TB, If<IsValue<TA>> = 0,
        If<IsInvocable<Sub, TA, TB>> = 0>
    static inline constexpr auto delta(TA const &a, Complex<TB> const &b)
    {
        auto const real = delta(a.real(), b.real());
        auto const imag = b.imag();

        return abs(Complex(real, imag));
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_DELTA_H
