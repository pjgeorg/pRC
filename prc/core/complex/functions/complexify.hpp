// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_COMPLEXIFY_H
#define pRC_CORE_COMPLEX_FUNCTIONS_COMPLEXIFY_H

#include <prc/core/complex/type_traits.hpp>

namespace pRC
{
    template<class X, If<IsComplex<RemoveReference<X>>> = 0>
    static inline constexpr X complexify(X &&a)
    {
        return forward<X>(a);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_COMPLEXIFY_H
