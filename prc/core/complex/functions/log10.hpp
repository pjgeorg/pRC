// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_LOG10_H
#define pRC_CORE_COMPLEX_FUNCTIONS_LOG10_H

#include <prc/core/complex/functions/log.hpp>
#include <prc/core/complex/type_traits.hpp>
#include <prc/core/functors/log.hpp>

namespace pRC
{
    template<class T, If<IsInvocable<Log, Complex<T>>> = 0>
    static inline constexpr auto log10(Complex<T> const &a)
    {
        return log(a) / log(identity<T>(10));
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_LOG10_H
