// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_DIV_H
#define pRC_CORE_COMPLEX_FUNCTIONS_DIV_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/complex/functions/norm.hpp>
#include <prc/core/functors/div.hpp>
#include <prc/core/value/functions/rcp.hpp>

namespace pRC
{
    template<class TA, class TB>
    static inline constexpr auto operator/(
        Complex<TA> const &a, Complex<TB> const &b)
    {
        auto const denominator = rcp(norm<2, 1>(b));

        auto const real =
            (a.real() * b.real() + a.imag() * b.imag()) * denominator;
        auto const imag =
            (a.imag() * b.real() - a.real() * b.imag()) * denominator;

        return Complex(real, imag);
    }

    template<class TA, class TB, If<IsValue<TB>> = 0,
        If<IsInvocable<Div, TA, TB>> = 0>
    static inline constexpr auto operator/(Complex<TA> const &a, TB const &b)
    {
        auto const real = a.real() / b;
        auto const imag = a.imag() / b;

        return Complex(real, imag);
    }

    template<class TA, class TB, If<IsValue<TA>> = 0>
    static inline constexpr auto operator/(TA const &a, Complex<TB> const &b)
    {
        auto const denominator = rcp(norm<2, 1>(b));

        auto const real = a * b.real() * denominator;
        auto const imag = -a * b.imag() * denominator;

        return Complex(real, imag);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_DIV_H
