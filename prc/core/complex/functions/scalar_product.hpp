// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_SCALAR_PRODUCT_H
#define pRC_CORE_COMPLEX_FUNCTIONS_SCALAR_PRODUCT_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/complex/functions/mul.hpp>

namespace pRC
{
    template<class TA, class TB,
        If<IsInvocable<Mul, Complex<TA>, Complex<TB>>> = 0>
    static inline constexpr auto scalarProduct(
        Complex<TA> const &a, Complex<TB> const &b)
    {
        return a * b;
    }

    template<class TA, class TB, If<IsValue<TB>> = 0,
        If<IsInvocable<Mul, Complex<TA>, TB>> = 0>
    static inline constexpr auto scalarProduct(
        Complex<TA> const &a, TB const &b)
    {
        return a * b;
    }

    template<class TA, class TB, If<IsValue<TA>> = 0,
        If<IsInvocable<Mul, TA, Complex<TB>>> = 0>
    static inline constexpr auto scalarProduct(
        TA const &a, Complex<TB> const &b)
    {
        return a * b;
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_SCALAR_PRODUCT_H
