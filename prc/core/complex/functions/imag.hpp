// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_IMAG_H
#define pRC_CORE_COMPLEX_FUNCTIONS_IMAG_H

#include <prc/core/complex/complex.hpp>

namespace pRC
{
    template<class X, If<IsComplex<RemoveReference<X>>> = 0>
    static inline constexpr decltype(auto) imag(X &&a)
    {
        return forward<X>(a).imag();
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_IMAG_H
