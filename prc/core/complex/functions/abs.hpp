// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_ABS_H
#define pRC_CORE_COMPLEX_FUNCTIONS_ABS_H

#include <prc/core/complex/type_traits.hpp>
#include <prc/core/functors/norm.hpp>

namespace pRC
{
    template<class T, If<IsInvocable<Norm<>, Complex<T>>> = 0>
    static inline constexpr auto abs(Complex<T> const &a)
    {
        return norm(a);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_ABS_H
