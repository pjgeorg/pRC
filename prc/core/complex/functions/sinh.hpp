// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_SINH_H
#define pRC_CORE_COMPLEX_FUNCTIONS_SINH_H

#include <prc/core/complex/complex.hpp>

namespace pRC
{
    template<class T>
    static inline constexpr auto sinh(Complex<T> const &a)
    {
        auto const real = sinh(a.real()) * cos(a.imag());
        auto const imag = cosh(a.real()) * sin(a.imag());

        return Complex(real, imag);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_SINH_H
