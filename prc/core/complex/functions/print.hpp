// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_PRINT_H
#define pRC_CORE_COMPLEX_FUNCTIONS_PRINT_H

#include <prc/core/complex/complex.hpp>

namespace pRC
{
    template<class T, class S, If<IsComplex<T>> = 0>
    static inline auto print(T const &value, S &&stream)
    {
        print('(', stream);
        print(value.real(), stream);
        print(',', stream);
        print(value.imag(), stream);
        print(')', stream);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_PRINT_H
