// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_NORM_H
#define pRC_CORE_COMPLEX_FUNCTIONS_NORM_H

#include <prc/core/complex/type_traits.hpp>

namespace pRC
{
    template<Index P = 2, Index Q = P, class T>
    static inline constexpr auto norm(Complex<T> const &a)
    {
        if constexpr(P == 1 && Q == 1)
        {
            return abs(a.real()) + abs(a.imag());
        }
        else if constexpr(P == 2 && Q == 1)
        {
            return a.real() * a.real() + a.imag() * a.imag();
        }
        else if constexpr(P == 2 && Q == 2)
        {
            return sqrt(norm<2, 1>(a));
        }
        else if constexpr(P != Q && Q == 0)
        {
            return norm<P, P>(a);
        }
        else
        {
            static_assert(P != P, "Unsupported p-norm.");
        }
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_NORM_H
