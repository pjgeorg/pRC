// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_CONJ_H
#define pRC_CORE_COMPLEX_FUNCTIONS_CONJ_H

#include <prc/core/complex/complex.hpp>

namespace pRC
{
    template<class T>
    static inline constexpr auto conj(Complex<T> const &a)
    {
        return Complex(a.real(), -a.imag());
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_CONJ_H
