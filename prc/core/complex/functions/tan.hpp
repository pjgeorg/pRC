// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_TAN_H
#define pRC_CORE_COMPLEX_FUNCTIONS_TAN_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/complex/functions/cos.hpp>
#include <prc/core/complex/functions/div.hpp>
#include <prc/core/complex/functions/sin.hpp>

namespace pRC
{
    template<class T>
    static inline constexpr auto tan(Complex<T> const &a)
    {
        return sin(a) / cos(a);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_TAN_H
