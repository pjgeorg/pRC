// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_SIGN_H
#define pRC_CORE_COMPLEX_FUNCTIONS_SIGN_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/complex/functions/abs.hpp>
#include <prc/core/functors/sign.hpp>

namespace pRC
{
    template<class T, If<IsInvocable<Sign, T>> = 0>
    static inline constexpr auto sign(Complex<T> const &a)
    {
        if(a.real() == zero() && a.imag() == zero())
        {
            return zero<Complex<T>>();
        }
        else
        {
            return a / abs(a);
        }
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_SIGN_H
