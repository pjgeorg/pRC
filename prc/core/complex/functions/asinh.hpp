// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_ASINH_H
#define pRC_CORE_COMPLEX_FUNCTIONS_ASINH_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/complex/functions/log.hpp>
#include <prc/core/complex/functions/sqrt.hpp>

namespace pRC
{
    template<class T>
    static inline constexpr auto asinh(Complex<T> const &a)
    {
        auto const real =
            (a.real() - a.imag()) * (a.real() + a.imag()) + unit<T>();
        auto const imag = identity<T>(2) * a.real() * a.imag();

        return log(sqrt(Complex(real, imag)) + a);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_ASINH_H
