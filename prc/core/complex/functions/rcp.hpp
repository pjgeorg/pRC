// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_RCP_H
#define pRC_CORE_COMPLEX_FUNCTIONS_RCP_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/complex/functions/norm.hpp>
#include <prc/core/value/functions/rcp.hpp>

namespace pRC
{
    template<class T>
    static inline constexpr auto rcp(Complex<T> const &b)
    {
        auto const denominator = rcp(norm<2, 1>(b));
        auto const real = b.real() * denominator;
        auto const imag = -b.imag() * denominator;

        return Complex(real, imag);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_RCP_H
