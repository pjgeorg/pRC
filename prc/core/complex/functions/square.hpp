// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_SQUARE_H
#define pRC_CORE_COMPLEX_FUNCTIONS_SQUARE_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/value/functions/abs.hpp>
#include <prc/core/value/functions/sqrt.hpp>

namespace pRC
{
    template<class T, If<IsInvocable<Mul, T, T>> = 0,
        If<IsInvocable<Sub, T, T>> = 0>
    static inline constexpr auto square(Complex<T> const &a)
    {
        auto const real = a.real() * a.real() - a.imag() * a.imag();
        auto const imag = identity<T>(2) * a.real() * a.imag();

        return Complex(real, imag);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_SQUARE_H
