// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_ACOSH_H
#define pRC_CORE_COMPLEX_FUNCTIONS_ACOSH_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/complex/functions/log.hpp>
#include <prc/core/complex/functions/sqrt.hpp>

namespace pRC
{
    template<class T>
    static inline constexpr auto acosh(Complex<T> const &a)
    {
        return identity<T>(2) *
            log(sqrt(identity<T>(0.5) * (a + unit<T>())) +
                sqrt(identity<T>(0.5) * (a - unit<T>())));
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_ACOSH_H
