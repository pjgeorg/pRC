// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_TRUNC_H
#define pRC_CORE_COMPLEX_FUNCTIONS_TRUNC_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/functors/trunc.hpp>

namespace pRC
{
    template<class T, If<IsInvocable<Trunc, T>> = 0>
    static inline constexpr auto trunc(Complex<T> const &a)
    {
        return Complex(trunc(a.real()), trunc(a.imag()));
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_TRUNC_H
