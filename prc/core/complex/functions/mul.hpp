// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_MUL_H
#define pRC_CORE_COMPLEX_FUNCTIONS_MUL_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/functors/mul.hpp>

namespace pRC
{
    template<class TA, class TB, If<IsInvocable<Mul, TA, TB>> = 0,
        If<IsInvocable<Add, TA, TB>> = 0, If<IsInvocable<Sub, TA, TB>> = 0>
    static inline constexpr auto operator*(
        Complex<TA> const &a, Complex<TB> const &b)
    {
        auto const real = a.real() * b.real() - a.imag() * b.imag();
        auto const imag = a.real() * b.imag() + a.imag() * b.real();

        return Complex(real, imag);
    }

    template<class TA, class TB, If<IsValue<TB>> = 0,
        If<IsInvocable<Mul, TA, TB>> = 0>
    static inline constexpr auto operator*(Complex<TA> const &a, TB const &b)
    {
        auto const real = a.real() * b;
        auto const imag = a.imag() * b;

        return Complex(real, imag);
    }

    template<class TA, class TB, If<IsValue<TA>> = 0,
        If<IsInvocable<Mul, TA, TB>> = 0>
    static inline constexpr auto operator*(TA const &a, Complex<TB> const &b)
    {
        auto const real = a * b.real();
        auto const imag = a * b.imag();

        return Complex(real, imag);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_MUL_H
