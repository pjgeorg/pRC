// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_TANH_H
#define pRC_CORE_COMPLEX_FUNCTIONS_TANH_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/complex/functions/cosh.hpp>
#include <prc/core/complex/functions/sinh.hpp>

namespace pRC
{
    template<class T>
    static inline constexpr auto tanh(Complex<T> const &a)
    {
        return sinh(a) / cosh(a);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_TANH_H
