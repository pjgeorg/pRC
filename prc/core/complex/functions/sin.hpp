// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_SIN_H
#define pRC_CORE_COMPLEX_FUNCTIONS_SIN_H

#include <prc/core/complex/complex.hpp>

namespace pRC
{
    template<class T>
    static inline constexpr auto sin(Complex<T> const &a)
    {
        auto const real = sin(a.real()) * cosh(a.imag());
        auto const imag = cos(a.real()) * sinh(a.imag());

        return Complex(real, imag);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_SIN_H
