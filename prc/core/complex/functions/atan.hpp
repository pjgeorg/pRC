// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_ATAN_H
#define pRC_CORE_COMPLEX_FUNCTIONS_ATAN_H

#include <prc/core/complex/complex.hpp>

namespace pRC
{
    template<class T>
    static inline constexpr auto atan(Complex<T> const &a)
    {
        auto const rs = a.real() * a.real();
        auto const t = unit<T>() - rs - a.imag() * a.imag();

        auto num = a.imag() + unit<T>();
        auto den = a.imag() - unit<T>();

        num = rs + num * num;
        den = rs + den * den;

        auto const real =
            identity<T>(0.5) * atan2(identity<T>(2) * a.real(), t);
        auto const imag = identity<T>(0.25) * log(num / den);

        return Complex(real, imag);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_ATAN_H
