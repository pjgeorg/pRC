// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_ATANH_H
#define pRC_CORE_COMPLEX_FUNCTIONS_ATANH_H

#include <prc/core/complex/complex.hpp>

namespace pRC
{
    template<class T>
    static inline constexpr auto atanh(Complex<T> const &a)
    {
        auto const is = a.imag() * a.imag();
        auto const t = unit<T>() - is - a.real() * a.real();

        auto num = unit<T>() + a.real();
        auto den = unit<T>() - a.real();

        num = is + num * num;
        den = is + den * den;

        auto const real = identity<T>(0.25) * (log(num) - log(den));
        auto const imag =
            identity<T>(0.5) * atan2(identity<T>(2) * a.imag(), t);

        return Complex(real, imag);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_ATANH_H
