// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_PLUS_H
#define pRC_CORE_COMPLEX_FUNCTIONS_PLUS_H

#include <prc/core/complex/complex.hpp>

namespace pRC
{
    template<class T>
    static inline constexpr auto operator+(Complex<T> const &a)
    {
        auto const real = +a.real();
        auto const imag = +a.imag();

        return Complex(real, imag);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_PLUS_H
