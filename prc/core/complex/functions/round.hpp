// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_ROUND_H
#define pRC_CORE_COMPLEX_FUNCTIONS_ROUND_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/functors/round.hpp>

namespace pRC
{
    template<Size D = 0, class T, If<IsInvocable<Round<D>, T>> = 0>
    static inline constexpr auto round(Complex<T> const &a)
    {
        return Complex(round<D>(a.real()), round<D>(a.imag()));
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_ROUND_H
