// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_ARG_H
#define pRC_CORE_COMPLEX_FUNCTIONS_ARG_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/functors/atan2.hpp>

namespace pRC
{
    template<class T, If<IsInvocable<Atan2, T, T>> = 0>
    static inline constexpr auto arg(Complex<T> const &a)
    {
        return atan2(a.imag(), a.real());
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_ARG_H
