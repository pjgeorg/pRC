// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_CAST_H
#define pRC_CORE_COMPLEX_FUNCTIONS_CAST_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/functors/cast.hpp>

namespace pRC
{
    template<class C, If<IsValue<C>> = 0, class T,
        If<IsInvocable<Cast<C>, T>> = 0>
    static inline constexpr auto cast(Complex<T> const &a)
    {
        return Complex<C>(a);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_CAST_H
