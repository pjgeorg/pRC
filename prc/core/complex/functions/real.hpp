// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_REAL_H
#define pRC_CORE_COMPLEX_FUNCTIONS_REAL_H

#include <prc/core/complex/complex.hpp>

namespace pRC
{
    template<class X, If<IsComplex<RemoveReference<X>>> = 0>
    static inline constexpr decltype(auto) real(X &&a)
    {
        return forward<X>(a).real();
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_REAL_H
