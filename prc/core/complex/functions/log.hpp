// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_LOG_H
#define pRC_CORE_COMPLEX_FUNCTIONS_LOG_H

#include <prc/core/complex/complex.hpp>

namespace pRC
{
    template<class T>
    static inline constexpr auto log(Complex<T> const &a)
    {
        auto const real = log(abs(a));
        auto const imag = arg(a);

        return Complex(real, imag);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_LOG_H
