// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_EQUAL_H
#define pRC_CORE_COMPLEX_FUNCTIONS_EQUAL_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/functors/equal.hpp>

namespace pRC
{
    template<class TA, class TB, If<IsInvocable<Equal, TA, TB>> = 0>
    static inline constexpr auto operator==(
        Complex<TA> const &a, Complex<TB> const &b)
    {
        return (a.real() == b.real()) && (a.imag() == b.imag());
    }

    template<class TA, class TB, If<IsValue<TB>> = 0,
        If<IsInvocable<Equal, TA, TB>> = 0>
    static inline constexpr auto operator==(Complex<TA> const &a, TB const &b)
    {
        return (a.real() == b) && (a.imag() == zero());
    }

    template<class TA, class TB, If<IsValue<TA>> = 0,
        If<IsInvocable<Equal, TA, TB>> = 0>
    static inline constexpr auto operator==(TA const &a, Complex<TB> const &b)
    {
        return (a == b.real()) && (zero() == b.imag());
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_EQUAL_H
