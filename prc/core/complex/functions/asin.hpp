// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_ASIN_H
#define pRC_CORE_COMPLEX_FUNCTIONS_ASIN_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/complex/functions/asinh.hpp>
#include <prc/core/functors/asinh.hpp>

namespace pRC
{
    template<class T, If<IsInvocable<Asinh, Complex<T>>> = 0>
    static inline constexpr auto asin(Complex<T> const &a)
    {
        auto c = Complex(-a.imag(), a.real());
        c = asinh(c);
        return Complex(c.imag(), -c.real());
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_ASIN_H
