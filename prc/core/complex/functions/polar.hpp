// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_POLAR_H
#define pRC_CORE_COMPLEX_FUNCTIONS_POLAR_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/functors/cos.hpp>
#include <prc/core/functors/mul.hpp>
#include <prc/core/functors/sin.hpp>

namespace pRC
{
    template<class TA, class TB, If<All<IsValue<TA>, IsValue<TB>>> = 0,
        If<IsInvocable<Cos, TB>> = 0, If<IsInvocable<Sin, TB>> = 0,
        If<IsInvocable<Mul, TA, TB>> = 0>
    static inline constexpr auto polar(TA const &rho, TB const &theta)
    {
        auto const real = rho * cos(theta);
        auto const imag = rho * sin(theta);

        return Complex(real, imag);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_POLAR_H
