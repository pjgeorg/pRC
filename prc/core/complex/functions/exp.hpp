// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_FUNCTIONS_EXP_H
#define pRC_CORE_COMPLEX_FUNCTIONS_EXP_H

#include <prc/core/complex/functions/polar.hpp>
#include <prc/core/complex/type_traits.hpp>

namespace pRC
{
    template<class T>
    static inline constexpr auto exp(Complex<T> const &a)
    {
        auto const rho = exp(a.real());
        auto const theta = a.imag();

        return polar(rho, theta);
    }
}
#endif // pRC_CORE_COMPLEX_FUNCTIONS_EXP_H
