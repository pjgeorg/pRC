// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_COMMON_H
#define pRC_CORE_COMPLEX_COMMON_H

#include <prc/core/basic/common.hpp>
#include <prc/core/complex/type_traits.hpp>
#include <prc/core/value/common.hpp>

namespace pRC
{
    template<class T, class U>
    struct CommonType<Complex<T>, Complex<U>, If<HasCommon<T, U>>>
    {
        using Type = Complex<Common<T, U>>;
    };

    template<class T, class U>
    struct CommonType<Complex<T>, U, If<All<IsValue<U>, HasCommon<T, U>>>>
    {
        using Type = Complex<Common<T, U>>;
    };

    template<class T, class U>
    struct CommonType<T, Complex<U>, If<All<IsValue<T>, HasCommon<T, U>>>>
    {
        using Type = Complex<Common<T, U>>;
    };
}
#endif // pRC_CORE_COMPLEX_COMMON_H
