// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_TYPE_TRAITS_H
#define pRC_CORE_COMPLEX_TYPE_TRAITS_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<class T>
    class Complex;

    template<class>
    struct IsComplex : False<>
    {
    };

    template<class T>
    struct IsComplex<T const> : IsComplex<T>
    {
    };

    template<class T>
    struct IsComplex<Complex<T>> : True<>
    {
    };
}
#endif // pRC_CORE_COMPLEX_TYPE_TRAITS_H
