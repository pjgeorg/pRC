// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_COMPLEX_ZERO_H
#define pRC_CORE_COMPLEX_ZERO_H

#include <prc/core/complex/complex.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class T>
    struct Zero<T, If<IsComplex<T>>>
    {
        constexpr auto operator()()
        {
            using U = typename T::Type;
            return Complex<U>(zero<U>(), zero<U>());
        }
    };
}
#endif // pRC_CORE_COMPLEX_ZERO_H
