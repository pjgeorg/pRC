// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_TANH_H
#define pRC_CORE_FUNCTORS_TANH_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Tanh
    {
        template<class X, If<True<decltype(tanh(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return tanh(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_TANH_H
