// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_LOG2_H
#define pRC_CORE_FUNCTORS_LOG2_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Log2
    {
        template<class X, If<True<decltype(log2(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return log2(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_LOG2_H
