// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_LOWER_TRIANGULAR_H
#define pRC_CORE_FUNCTORS_LOWER_TRIANGULAR_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct LowerTriangular
    {
        template<class X, If<True<decltype(lowerTriangular(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return lowerTriangular(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_LOWER_TRIANGULAR_H
