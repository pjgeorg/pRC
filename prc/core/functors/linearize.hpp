// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_LINEARIZE_H
#define pRC_CORE_FUNCTORS_LINEARIZE_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Linearize
    {
        template<class X, If<True<decltype(linearize(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return linearize(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_LINEARIZE_H
