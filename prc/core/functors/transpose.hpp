// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_TRANSPOSE_H
#define pRC_CORE_FUNCTORS_TRANSPOSE_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Transpose
    {
        template<class X, If<True<decltype(transpose(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return transpose(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_TRANSPOSE_H
