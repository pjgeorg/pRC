// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_PERMUTE_H
#define pRC_CORE_FUNCTORS_PERMUTE_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/functions/permute.hpp>

namespace pRC
{
    template<Index... Ps>
    struct Permute
    {
        template<class X, If<True<decltype(permute<Ps...>(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return permute<Ps...>(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_PERMUTE_H
