// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_ACOSH_H
#define pRC_CORE_FUNCTORS_ACOSH_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Acosh
    {
        template<class X, If<True<decltype(acosh(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return acosh(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_ACOSH_H
