// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_REDUCE_H
#define pRC_CORE_FUNCTORS_REDUCE_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/functions/reduce.hpp>

namespace pRC
{
    template<class F, Index... Is>
    struct Reduce
    {
        template<class X,
            If<True<decltype(reduce<F, Is...>(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return reduce<F, Is...>(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_REDUCE_H
