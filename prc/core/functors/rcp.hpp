// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_RCP_H
#define pRC_CORE_FUNCTORS_RCP_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Rcp
    {
        template<class X, If<True<decltype(rcp(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return rcp(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_RCP_H
