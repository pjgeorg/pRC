// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_DELTA_H
#define pRC_CORE_FUNCTORS_DELTA_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Delta
    {
        template<class XA, class XB,
            If<True<decltype(delta(declval<XA>(), declval<XB>()))>> = 0>
        constexpr decltype(auto) operator()(XA &&a, XB &&b) const
        {
            return delta(forward<XA>(a), forward<XB>(b));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_DELTA_H
