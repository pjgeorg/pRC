// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_EXP_H
#define pRC_CORE_FUNCTORS_EXP_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Exp
    {
        template<class X, If<True<decltype(exp(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return exp(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_EXP_H
