// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_EXCLUDE_H
#define pRC_CORE_FUNCTORS_EXCLUDE_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/functions/exclude.hpp>

namespace pRC
{
    template<class F, Index... Es>
    struct Exclude
    {
        template<class... Xs,
            If<True<decltype(exclude<F, Es...>(declval<Xs>()...))>> = 0>
        constexpr decltype(auto) operator()(Xs &&...args) const
        {
            return exclude<F, Es...>(forward<Xs>(args)...);
        }
    };
}
#endif // pRC_CORE_FUNCTORS_EXCLUDE_H
