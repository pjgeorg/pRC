// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_SLICE_H
#define pRC_CORE_FUNCTORS_SLICE_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/functions/slice.hpp>

namespace pRC
{
    template<Size... Es>
    struct Slice
    {
        template<class X, class... Os,
            If<True<decltype(slice<Es...>(declval<X>(), declval<Os>()...))>> =
                0>
        constexpr decltype(auto) operator()(X &&a, Os const... offsets) const
        {
            return slice<Es...>(forward<X>(a), offsets...);
        }
    };
}
#endif // pRC_CORE_FUNCTORS_SLICE_H
