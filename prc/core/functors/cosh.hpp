// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_COSH_H
#define pRC_CORE_FUNCTORS_COSH_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Cosh
    {
        template<class X, If<True<decltype(cosh(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return cosh(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_COSH_H
