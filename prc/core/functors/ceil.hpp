// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_CEIL_H
#define pRC_CORE_FUNCTORS_CEIL_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Ceil
    {
        template<class X, If<True<decltype(ceil(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return ceil(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_CEIL_H
