// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_LOG_H
#define pRC_CORE_FUNCTORS_LOG_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Log
    {
        template<class X, If<True<decltype(log(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return log(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_LOG_H
