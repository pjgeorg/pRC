// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_CONJ_H
#define pRC_CORE_FUNCTORS_CONJ_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Conj
    {
        template<class X, If<True<decltype(conj(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return conj(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_CONJ_H
