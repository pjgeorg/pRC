// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_IS_APPROX_H
#define pRC_CORE_FUNCTORS_IS_APPROX_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct IsApprox
    {
        template<class XA, class XB,
            If<True<decltype(isApprox(declval<XA>(), declval<XB>()))>> = 0>
        constexpr decltype(auto) operator()(XA &&a, XB &&b) const
        {
            return isApprox(forward<XA>(a), forward<XB>(b));
        }

        template<class XA, class XB, class XT,
            If<True<decltype(
                isApprox(declval<XA>(), declval<XB>(), declval<XT>()))>> = 0>
        constexpr decltype(auto) operator()(
            XA &&a, XB &&b, XT &&tolerance) const
        {
            return isApprox(
                forward<XA>(a), forward<XB>(b), forward<XT>(tolerance));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_IS_APPROX_H
