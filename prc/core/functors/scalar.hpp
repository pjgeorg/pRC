// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_SCALAR_H
#define pRC_CORE_FUNCTORS_SCALAR_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Scalar
    {
        template<class X, If<True<decltype(scalar(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return scalar(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_SCALAR_H
