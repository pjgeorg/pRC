// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_SQUARE_H
#define pRC_CORE_FUNCTORS_SQUARE_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Square
    {
        template<class X, If<True<decltype(square(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return square(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_SQUARE_H
