// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_LOOP_H
#define pRC_CORE_FUNCTORS_LOOP_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/functions/loop.hpp>

namespace pRC
{
    template<class F>
    struct Loop
    {
        template<class... Xs, If<True<decltype(loop<F>(declval<Xs>()...))>> = 0>
        constexpr decltype(auto) operator()(Xs &&...args) const
        {
            return loop<F>(forward<Xs>(args)...);
        }
    };
}
#endif // pRC_CORE_FUNCTORS_LOOP_H
