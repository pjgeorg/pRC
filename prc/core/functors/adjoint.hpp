// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_ADJOINT_H
#define pRC_CORE_FUNCTORS_ADJOINT_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Adjoint
    {
        template<class X, If<True<decltype(adjoint(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return adjoint(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_ADJOINT_H
