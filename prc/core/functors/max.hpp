// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_MAX_H
#define pRC_CORE_FUNCTORS_MAX_H

#include <prc/core/basic/functions/max.hpp>
#include <prc/core/basic/type_traits.hpp>
#include <prc/core/value/limits.hpp>

namespace pRC
{
    struct Max
    {
        template<class T>
        static constexpr auto Identity()
        {
            return unit<T>(NumericLimits<typename T::Value>::lowest());
        }

        template<class... Xs, If<True<decltype(max(declval<Xs>()...))>> = 0>
        constexpr decltype(auto) operator()(Xs &&...args) const
        {
            return max(forward<Xs>(args)...);
        }
    };
}
#endif // pRC_CORE_FUNCTORS_MAX_H
