// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_SINH_H
#define pRC_CORE_FUNCTORS_SINH_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Sinh
    {
        template<class X, If<True<decltype(sinh(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return sinh(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_SINH_H
