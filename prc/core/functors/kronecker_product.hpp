// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_KRONECKER_PRODUCT_H
#define pRC_CORE_FUNCTORS_KRONECKER_PRODUCT_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct KroneckerProduct
    {
        template<class XA, class XB,
            If<True<decltype(kroneckerProduct(declval<XA>(), declval<XB>()))>> =
                0>
        constexpr decltype(auto) operator()(XA &&a, XB &&b) const
        {
            return kroneckerProduct(forward<XA>(a), forward<XB>(b));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_KRONECKER_PRODUCT_H
