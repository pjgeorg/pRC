// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_REVERSE_H
#define pRC_CORE_FUNCTORS_REVERSE_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/functions/reverse.hpp>

namespace pRC
{
    template<Bool... Rs>
    struct Reverse
    {
        template<class X, If<True<decltype(reverse<Rs...>(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return reverse<Rs...>(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_REVERSE_H
