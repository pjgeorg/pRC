// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_ATAN2_H
#define pRC_CORE_FUNCTORS_ATAN2_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Atan2
    {
        template<class XA, class XB,
            If<True<decltype(atan2(declval<XA>(), declval<XB>()))>> = 0>
        constexpr decltype(auto) operator()(XA &&a, XB &&b) const
        {
            return atan2(forward<XA>(a), forward<XB>(b));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_ATAN2_H
