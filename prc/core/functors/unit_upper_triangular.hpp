// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_UNIT_UPPER_TRIANGULAR_H
#define pRC_CORE_FUNCTORS_UNIT_UPPER_TRIANGULAR_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct UnitUpperTriangular
    {
        template<class X,
            If<True<decltype(unitUpperTriangular(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return unitUpperTriangular(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_UNIT_UPPER_TRIANGULAR_H
