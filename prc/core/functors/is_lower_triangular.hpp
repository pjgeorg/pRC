// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_IS_LOWER_TRIANGULAR_H
#define pRC_CORE_FUNCTORS_IS_LOWER_TRIANGULAR_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct IsLowerTriangular
    {
        template<class X,
            If<True<decltype(isLowerTriangular(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return isLowerTriangular(forward<X>(a));
        }

        template<class X, class XT,
            If<True<decltype(isLowerTriangular(declval<X>(), declval<XT>()))>> =
                0>
        constexpr decltype(auto) operator()(X &&a, XT &&tolerance) const
        {
            return isLowerTriangular(forward<X>(a), forward<XT>(tolerance));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_IS_LOWER_TRIANGULAR_H
