// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_FOLDING_H
#define pRC_CORE_FUNCTORS_FOLDING_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/functions/folding.hpp>

namespace pRC
{
    template<Position P>
    struct Folding
    {
        template<class X, If<True<decltype(folding<P>(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return folding<P>(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_FOLDING_H
