// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_FOLD_H
#define pRC_CORE_FUNCTORS_FOLD_H

#include <prc/core/basic/direction.hpp>
#include <prc/core/basic/type_traits.hpp>
#include <prc/core/value/functions/fold.hpp>

namespace pRC
{
    template<class F, Bool E, Direction D>
    struct Fold
    {
        template<class... Xs,
            If<True<decltype(fold<F, E, D>(declval<Xs>()...))>> = 0>
        constexpr decltype(auto) operator()(Xs &&...args) const
        {
            return fold<F, E, D>(forward<Xs>(args)...);
        }
    };
}
#endif // pRC_CORE_FUNCTORS_FOLD_H
