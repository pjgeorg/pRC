// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_TRANSFORM_H
#define pRC_CORE_FUNCTORS_TRANSFORM_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/operator/functions/transform.hpp>

namespace pRC
{
    template<Operator::Transform T = Operator::Transform::None>
    struct Transform
    {
        template<class X, If<True<decltype(transform<T>(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return transform<T>(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_TRANSFORM_H
