// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_COMPLEXIFY_H
#define pRC_CORE_FUNCTORS_COMPLEXIFY_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Complexify
    {
        template<class X, If<True<decltype(complexify(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return complexify(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_COMPLEXIFY_H
