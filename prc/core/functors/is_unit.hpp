// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_IS_UNIT_H
#define pRC_CORE_FUNCTORS_IS_UNIT_H

#include <prc/core/basic/functions/is_unit.hpp>
#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct IsUnit
    {
        template<class X, If<True<decltype(isUnit(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return isUnit(forward<X>(a));
        }

        template<class X, class XT,
            If<True<decltype(isUnit(declval<X>(), declval<XT>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a, XT &&tolerance) const
        {
            return isUnit(forward<X>(a), forward<XT>(tolerance));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_IS_UNIT_H
