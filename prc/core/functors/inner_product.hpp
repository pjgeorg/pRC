// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_INNER_PRODUCT_H
#define pRC_CORE_FUNCTORS_INNER_PRODUCT_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct InnerProduct
    {
        template<class XA, class XB,
            If<True<decltype(innerProduct(declval<XA>(), declval<XB>()))>> = 0>
        constexpr decltype(auto) operator()(XA &&a, XB &&b) const
        {
            return innerProduct(forward<XA>(a), forward<XB>(b));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_INNER_PRODUCT_H
