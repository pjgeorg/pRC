// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_ROUND_H
#define pRC_CORE_FUNCTORS_ROUND_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/value/functions/round.hpp>

namespace pRC
{
    template<Size D = 0>
    struct Round
    {
        template<class X, If<True<decltype(round<D>(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return round<D>(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_ROUND_H
