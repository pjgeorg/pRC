// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_CAST_H
#define pRC_CORE_FUNCTORS_CAST_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/value/functions/cast.hpp>

namespace pRC
{
    template<class C>
    struct Cast
    {
        template<class X, If<True<decltype(cast<C>(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return cast<C>(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_CAST_H
