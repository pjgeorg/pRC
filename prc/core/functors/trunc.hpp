// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_TRUNC_H
#define pRC_CORE_FUNCTORS_TRUNC_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Trunc
    {
        template<class X, If<True<decltype(trunc(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return trunc(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_TRUNC_H
