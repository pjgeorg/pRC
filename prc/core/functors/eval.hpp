// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_EVAL_H
#define pRC_CORE_FUNCTORS_EVAL_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Eval
    {
        template<class X, If<True<decltype(eval(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return eval(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_EVAL_H
