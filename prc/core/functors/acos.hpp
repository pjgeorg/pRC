// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_ACOS_H
#define pRC_CORE_FUNCTORS_ACOS_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Acos
    {
        template<class X, If<True<decltype(acos(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return acos(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_ACOS_H
