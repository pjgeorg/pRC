// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_BACKWARDS_H
#define pRC_CORE_FUNCTORS_BACKWARDS_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Backwards
    {
        template<class X, If<True<decltype(backwards(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return backwards(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_BACKWARDS_H
