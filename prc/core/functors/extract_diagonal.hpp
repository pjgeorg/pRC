// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_EXTRACT_DIAGONAL_H
#define pRC_CORE_FUNCTORS_EXTRACT_DIAGONAL_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct ExtractDiagonal
    {
        template<class X, If<True<decltype(extractDiagonal(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return extractDiagonal(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_EXTRACT_DIAGONAL_H
