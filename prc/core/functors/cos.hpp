// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_COS_H
#define pRC_CORE_FUNCTORS_COS_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Cos
    {
        template<class X, If<True<decltype(cos(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return cos(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_COS_H
