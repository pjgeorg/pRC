// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_CONTRACT_H
#define pRC_CORE_FUNCTORS_CONTRACT_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/functions/contract.hpp>

namespace pRC
{
    template<Index... Is>
    struct Contract
    {
        template<class X, If<True<decltype(contract<Is...>(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return contract<Is...>(forward<X>(a));
        }

        template<class XA, class XB,
            If<True<decltype(contract<Is...>(declval<XA>(), declval<XB>()))>> =
                0>
        constexpr decltype(auto) operator()(XA &&a, XB &&b) const
        {
            return contract<Is...>(forward<XA>(a), forward<XB>(b));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_CONTRACT_H
