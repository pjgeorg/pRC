// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_TRACE_H
#define pRC_CORE_FUNCTORS_TRACE_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Trace
    {
        template<class X, If<True<decltype(trace(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return trace(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_TRACE_H
