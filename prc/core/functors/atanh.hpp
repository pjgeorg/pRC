// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_ATANH_H
#define pRC_CORE_FUNCTORS_ATANH_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Atanh
    {
        template<class X, If<True<decltype(atanh(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return atanh(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_ATANH_H
