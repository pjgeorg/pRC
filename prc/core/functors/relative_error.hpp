// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_RELATIVE_ERROR_H
#define pRC_CORE_FUNCTORS_RELATIVE_ERROR_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct RelativeError
    {
        template<class XA, class XB,
            If<True<decltype(relativeError(declval<XA>(), declval<XB>()))>> = 0>
        constexpr decltype(auto) operator()(XA &&a, XB &&b) const
        {
            return relativeError(forward<XA>(a), forward<XB>(b));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_RELATIVE_ERROR_H
