// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_MATRICIZE_H
#define pRC_CORE_FUNCTORS_MATRICIZE_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Matricize
    {
        template<class X, If<True<decltype(matricize(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return matricize(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_MATRICIZE_H
