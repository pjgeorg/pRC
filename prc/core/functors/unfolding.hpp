// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_UNFOLDING_H
#define pRC_CORE_FUNCTORS_UNFOLDING_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/functions/unfolding.hpp>

namespace pRC
{
    template<Index K>
    struct Unfolding
    {
        template<class X, If<True<decltype(unfolding<K>(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return unfolding<K>(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_UNFOLDING_H
