// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_BROADCAST_H
#define pRC_CORE_FUNCTORS_BROADCAST_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/functions/broadcast.hpp>

namespace pRC
{
    template<Size... Bs>
    struct Broadcast
    {
        template<class X,
            If<True<decltype(broadcast<Bs...>(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return broadcast<Bs...>(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_BROADCAST_H
