// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_ABS_H
#define pRC_CORE_FUNCTORS_ABS_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Abs
    {
        template<class X, If<True<decltype(abs(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return abs(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_ABS_H
