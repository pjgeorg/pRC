// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_BLOCK_H
#define pRC_CORE_FUNCTORS_BLOCK_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/functions/block.hpp>

namespace pRC
{
    template<Size... Bs>
    struct Block
    {
        template<class X, class... Os,
            If<True<decltype(block<Bs...>(declval<X>(), declval<Os>()...))>> =
                0>
        constexpr decltype(auto) operator()(X &&a, Os const... offsets) const
        {
            return block<Bs...>(forward<X>(a), offsets...);
        }
    };
}
#endif // pRC_CORE_FUNCTORS_BLOCK_H
