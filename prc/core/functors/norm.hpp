// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_NORM_H
#define pRC_CORE_FUNCTORS_NORM_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/value/functions/norm.hpp>

namespace pRC
{
    template<Index P = 2, Index Q = P>
    struct Norm
    {
        template<class X, If<True<decltype(norm<P, Q>(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return norm<P, Q>(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_NORM_H
