// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_RESTRICT_H
#define pRC_CORE_FUNCTORS_RESTRICT_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/operator/functions/restrict.hpp>

namespace pRC
{
    template<Operator::Restrict R = Operator::Restrict::None>
    struct Restrict
    {
        template<class X, If<True<decltype(restrict<R>(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return restrict<R>(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_RESTRICT_H
