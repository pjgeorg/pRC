// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_TAN_H
#define pRC_CORE_FUNCTORS_TAN_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Tan
    {
        template<class X, If<True<decltype(tan(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return tan(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_TAN_H
