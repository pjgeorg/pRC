// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_IS_UPPER_TRIANGULAR_H
#define pRC_CORE_FUNCTORS_IS_UPPER_TRIANGULAR_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct IsUpperTriangular
    {
        template<class X,
            If<True<decltype(isUpperTriangular(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return isUpperTriangular(forward<X>(a));
        }

        template<class X, class XT,
            If<True<decltype(isUpperTriangular(declval<X>(), declval<XT>()))>> =
                0>
        constexpr decltype(auto) operator()(X &&a, XT &&tolerance) const
        {
            return isUpperTriangular(forward<X>(a), forward<XT>(tolerance));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_IS_UPPER_TRIANGULAR_H
