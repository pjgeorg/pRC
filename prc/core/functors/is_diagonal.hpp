// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_IS_DIAGONAL_H
#define pRC_CORE_FUNCTORS_IS_DIAGONAL_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct IsDiagonal
    {
        template<class X, If<True<decltype(isDiagonal(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return isDiagonal(forward<X>(a));
        }

        template<class X, class XT,
            If<True<decltype(isDiagonal(declval<X>(), declval<XT>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a, XT &&tolerance) const
        {
            return isDiagonal(forward<X>(a), forward<XT>(tolerance));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_IS_DIAGONAL_H
