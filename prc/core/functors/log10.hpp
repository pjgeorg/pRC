// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_LOG10_H
#define pRC_CORE_FUNCTORS_LOG10_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Log10
    {
        template<class X, If<True<decltype(log10(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return log10(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_LOG10_H
