// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_IS_OFF_DIAGONAL_H
#define pRC_CORE_FUNCTORS_IS_OFF_DIAGONAL_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct IsOffDiagonal
    {
        template<class X, If<True<decltype(isOffDiagonal(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return isOffDiagonal(forward<X>(a));
        }

        template<class X, class XT,
            If<True<decltype(isOffDiagonal(declval<X>(), declval<XT>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a, XT &&tolerance) const
        {
            return isOffDiagonal(forward<X>(a), forward<XT>(tolerance));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_IS_OFF_DIAGONAL_H
