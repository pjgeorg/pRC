// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_MEAN_H
#define pRC_CORE_FUNCTORS_MEAN_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Mean
    {
        template<class... Xs, If<True<decltype(mean(declval<Xs>()...))>> = 0>
        constexpr decltype(auto) operator()(Xs &&...args) const
        {
            return mean(forward<Xs>(args)...);
        }
    };
}
#endif // pRC_CORE_FUNCTORS_MEAN_H
