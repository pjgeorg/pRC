// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_APPLY_H
#define pRC_CORE_FUNCTORS_APPLY_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/operator/functions/apply.hpp>

namespace pRC
{
    template<Operator::Transform T = Operator::Transform::None,
        Operator::Restrict R = Operator::Restrict::None,
        Operator::Hint H = Operator::Hint::None>
    struct Apply
    {
        template<class XA, class XB,
            If<True<decltype(apply<T, R, H>(declval<XA>(), declval<XB>()))>> =
                0>
        constexpr decltype(auto) operator()(XA &&a, XB &&b) const
        {
            return apply<T, R, H>(forward<XA>(a), forward<XB>(b));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_APPLY_H
