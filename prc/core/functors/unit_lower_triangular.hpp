// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_UNIT_LOWER_TRIANGULAR_H
#define pRC_CORE_FUNCTORS_UNIT_LOWER_TRIANGULAR_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct UnitLowerTriangular
    {
        template<class X,
            If<True<decltype(unitLowerTriangular(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return unitLowerTriangular(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_UNIT_LOWER_TRIANGULAR_H
