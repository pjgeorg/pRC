// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_IS_ZERO_H
#define pRC_CORE_FUNCTORS_IS_ZERO_H

#include <prc/core/basic/functions/is_zero.hpp>
#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct IsZero
    {
        template<class X, If<True<decltype(isZero(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return isZero(forward<X>(a));
        }

        template<class X, class XT,
            If<True<decltype(isZero(declval<X>(), declval<XT>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a, XT &&tolerance) const
        {
            return isZero(forward<X>(a), forward<XT>(tolerance));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_IS_ZERO_H
