// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_OFF_DIAGONAL_H
#define pRC_CORE_FUNCTORS_OFF_DIAGONAL_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct OffDiagonal
    {
        template<class X, If<True<decltype(offDiagonal(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return offDiagonal(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_OFF_DIAGONAL_H
