// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_TENSOR_PRODUCT_H
#define pRC_CORE_FUNCTORS_TENSOR_PRODUCT_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct TensorProduct
    {
        template<class XA, class XB,
            If<True<decltype(tensorProduct(declval<XA>(), declval<XB>()))>> = 0>
        constexpr decltype(auto) operator()(XA &&a, XB &&b) const
        {
            return tensorProduct(forward<XA>(a), forward<XB>(b));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_TENSOR_PRODUCT_H
