// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_LOGICAL_AND_H
#define pRC_CORE_FUNCTORS_LOGICAL_AND_H

#include <prc/core/basic/functions/identity.hpp>
#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct LogicalAnd
    {
        template<class T, If<IsInvocable<Identity<T>>> = 0>
        static constexpr auto Identity()
        {
            return identity<T>();
        }

        template<class XA, class XB,
            If<True<decltype(declval<XA>() && declval<XB>())>> = 0>
        constexpr decltype(auto) operator()(XA &&a, XB &&b) const
        {
            return forward<XA>(a) && forward<XB>(b);
        }
    };
}
#endif // pRC_CORE_FUNCTORS_LOGICAL_AND_H
