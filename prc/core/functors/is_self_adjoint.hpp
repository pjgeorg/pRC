// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_IS_SELF_ADJOINT_H
#define pRC_CORE_FUNCTORS_IS_SELF_ADJOINT_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct IsSelfAdjoint
    {
        template<class X, If<True<decltype(isSelfAdjoint(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return isSelfAdjoint(forward<X>(a));
        }

        template<class X, class XT,
            If<True<decltype(isSelfAdjoint(declval<X>(), declval<XT>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a, XT &&tolerance) const
        {
            return isSelfAdjoint(forward<X>(a), forward<XT>(tolerance));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_IS_SELF_ADJOINT_H
