// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_SQRT_H
#define pRC_CORE_FUNCTORS_SQRT_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Sqrt
    {
        template<class X, If<True<decltype(sqrt(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return sqrt(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_SQRT_H
