// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_DIAGONAL_H
#define pRC_CORE_FUNCTORS_DIAGONAL_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Diagonal
    {
        template<class X, If<True<decltype(diagonal(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return diagonal(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_DIAGONAL_H
