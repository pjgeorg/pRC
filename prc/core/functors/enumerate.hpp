// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_ENUMERATE_H
#define pRC_CORE_FUNCTORS_ENUMERATE_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<class F>
    struct Enumerate
    {
        template<class... Xs,
            If<True<decltype(enumerate<F>(declval<Xs>()...))>> = 0>
        constexpr decltype(auto) operator()(Xs &&...args) const
        {
            return enumerate<F>(forward<Xs>(args)...);
        }
    };
}
#endif // pRC_CORE_FUNCTORS_ENUMERATE_H
