// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_WHERE_H
#define pRC_CORE_FUNCTORS_WHERE_H

#include <prc/core/basic/functions/where.hpp>
#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Where
    {
        template<class XE, class XA, class XB,
            If<True<decltype(
                where(declval<XE>(), declval<XA>(), declval<XB>()))>> = 0>
        constexpr decltype(auto) operator()(XE &&e, XA &&a, XB &&b) const
        {
            return where(forward<XE>(e), forward<XA>(a), forward<XB>(b));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_WHERE_H
