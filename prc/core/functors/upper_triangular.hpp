// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_UPPER_TRIANGULAR_H
#define pRC_CORE_FUNCTORS_UPPER_TRIANGULAR_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct UpperTriangular
    {
        template<class X, If<True<decltype(upperTriangular(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return upperTriangular(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_UPPER_TRIANGULAR_H
