// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_IS_UNIT_LOWER_TRIANGULAR_H
#define pRC_CORE_FUNCTORS_IS_UNIT_LOWER_TRIANGULAR_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct IsUnitLowerTriangular
    {
        template<class X,
            If<True<decltype(isUnitLowerTriangular(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return isUnitLowerTriangular(forward<X>(a));
        }

        template<class X, class XT,
            If<True<decltype(
                isUnitLowerTriangular(declval<X>(), declval<XT>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a, XT &&tolerance) const
        {
            return isUnitLowerTriangular(forward<X>(a), forward<XT>(tolerance));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_IS_UNIT_LOWER_TRIANGULAR_H
