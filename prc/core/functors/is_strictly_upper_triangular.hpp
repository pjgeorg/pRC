// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_IS_STRICTLY_UPPER_TRIANGULAR_H
#define pRC_CORE_FUNCTORS_IS_STRICTLY_UPPER_TRIANGULAR_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct IsStrictlyUpperTriangular
    {
        template<class X,
            If<True<decltype(isStrictlyUpperTriangular(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return isStrictlyUpperTriangular(forward<X>(a));
        }

        template<class X, class XT,
            If<True<decltype(
                isStrictlyUpperTriangular(declval<X>(), declval<XT>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a, XT &&tolerance) const
        {
            return isStrictlyUpperTriangular(
                forward<X>(a), forward<XT>(tolerance));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_IS_STRICTLY_UPPER_TRIANGULAR_H
