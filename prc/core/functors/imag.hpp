// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_IMAG_H
#define pRC_CORE_FUNCTORS_IMAG_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Imag
    {
        template<class X, If<True<decltype(imag(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return imag(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_IMAG_H
