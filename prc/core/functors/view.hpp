// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_VIEW_H
#define pRC_CORE_FUNCTORS_VIEW_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/functions/view.hpp>

namespace pRC
{
    struct View
    {
        template<class X, If<True<decltype(view(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return view(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_VIEW_H
