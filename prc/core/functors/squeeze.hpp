// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_SQUEEZE_H
#define pRC_CORE_FUNCTORS_SQUEEZE_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Squeeze
    {
        template<class X, If<True<decltype(squeeze(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return squeeze(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_SQUEEZE_H
