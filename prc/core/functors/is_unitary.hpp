// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_IS_UNITARY_H
#define pRC_CORE_FUNCTORS_IS_UNITARY_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct IsUnitary
    {
        template<class X, If<True<decltype(isUnitary(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return isUnitary(forward<X>(a));
        }

        template<class X, class XT,
            If<True<decltype(isUnitary(declval<X>(), declval<XT>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a, XT &&tolerance) const
        {
            return isUnitary(forward<X>(a), forward<XT>(tolerance));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_IS_UNITARY_H
