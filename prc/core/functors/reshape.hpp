// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_RESHAPE_H
#define pRC_CORE_FUNCTORS_RESHAPE_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/functions/reshape.hpp>

namespace pRC
{
    template<Size... Ns>
    struct Reshape
    {
        template<class X, If<True<decltype(reshape<Ns...>(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return reshape<Ns...>(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_RESHAPE_H
