// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_ASIN_H
#define pRC_CORE_FUNCTORS_ASIN_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Asin
    {
        template<class X, If<True<decltype(asin(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return asin(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_ASIN_H
