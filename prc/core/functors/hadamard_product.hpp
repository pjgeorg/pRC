// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_HADAMARD_PRODUCT_H
#define pRC_CORE_FUNCTORS_HADAMARD_PRODUCT_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct HadamardProduct
    {
        template<class XA, class XB,
            If<True<decltype(hadamardProduct(declval<XA>(), declval<XB>()))>> =
                0>
        constexpr decltype(auto) operator()(XA &&a, XB &&b) const
        {
            return hadamardProduct(forward<XA>(a), forward<XB>(b));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_HADAMARD_PRODUCT_H
