// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_MIN_H
#define pRC_CORE_FUNCTORS_MIN_H

#include <prc/core/basic/functions/min.hpp>
#include <prc/core/basic/type_traits.hpp>
#include <prc/core/value/limits.hpp>

namespace pRC
{
    struct Min
    {
        template<class T>
        static constexpr auto Identity()
        {
            return unit<T>(NumericLimits<typename T::Value>::max());
        }

        template<class... Xs, If<True<decltype(min(declval<Xs>()...))>> = 0>
        constexpr decltype(auto) operator()(Xs &&...args) const
        {
            return min(forward<Xs>(args)...);
        }
    };
}
#endif // pRC_CORE_FUNCTORS_MIN_H
