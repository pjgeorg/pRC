// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_ROTATE_H
#define pRC_CORE_FUNCTORS_ROTATE_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/functions/rotate.hpp>

namespace pRC
{
    template<Direction D, Size S = 1>
    struct Rotate
    {
        template<class X, If<True<decltype(rotate<D, S>(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return rotate<D, S>(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_ROTATE_H
