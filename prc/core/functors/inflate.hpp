// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_INFLATE_H
#define pRC_CORE_FUNCTORS_INFLATE_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/functions/inflate.hpp>

namespace pRC
{
    template<Size... Is>
    struct Inflate
    {
        template<class X, If<True<decltype(inflate<Is...>(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return inflate<Is...>(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_INFLATE_H
