// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_ARG_H
#define pRC_CORE_FUNCTORS_ARG_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Arg
    {
        template<class X, If<True<decltype(arg(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return arg(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_ARG_H
