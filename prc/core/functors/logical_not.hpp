// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_LOGICAL_NOT_H
#define pRC_CORE_FUNCTORS_LOGICAL_NOT_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct LogicalNot
    {
        template<class X, If<True<decltype(!declval<X>())>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return !forward<X>(a);
        }
    };
}
#endif // pRC_CORE_FUNCTORS_LOGICAL_NOT_H
