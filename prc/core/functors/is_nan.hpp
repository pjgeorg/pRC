// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_IS_NAN_H
#define pRC_CORE_FUNCTORS_IS_NAN_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct IsNaN
    {
        template<class X, If<True<decltype(isNaN(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return isNaN(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_IS_NAN_H
