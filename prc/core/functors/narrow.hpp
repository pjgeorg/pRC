// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_NARROW_H
#define pRC_CORE_FUNCTORS_NARROW_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/functions/narrow.hpp>

namespace pRC
{
    template<class F, Index... Ns>
    struct Narrow
    {
        template<class... Xs,
            If<True<decltype(narrow<F, Ns...>(declval<Xs>()...))>> = 0>
        constexpr decltype(auto) operator()(Xs &&...args) const
        {
            return narrow<F, Ns...>(forward<Xs>(args)...);
        }
    };
}
#endif // pRC_CORE_FUNCTORS_NARROW_H
