// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_FROM_DIAGONAL_H
#define pRC_CORE_FUNCTORS_FROM_DIAGONAL_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/operator/functions/from_diagonal.hpp>

namespace pRC
{
    template<Size... Ds>
    struct FromDiagonal
    {
        template<class X,
            If<True<decltype(fromDiagonal<Ds...>(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return fromDiagonal<Ds...>(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_FROM_DIAGONAL_H
