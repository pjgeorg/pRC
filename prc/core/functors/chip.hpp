// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_CHIP_H
#define pRC_CORE_FUNCTORS_CHIP_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/functions/chip.hpp>

namespace pRC
{
    template<Index... Ds>
    struct Chip
    {
        template<class X, class... Is, If<All<IsConvertible<Is, Index>...>> = 0,
            If<True<decltype(
                chip<Ds...>(declval<X>(), declval<Is const>()...))>> = 0>
        constexpr decltype(auto) operator()(X &&a, Is const... indices) const
        {
            return chip<Ds...>(forward<X>(a), indices...);
        }
    };
}
#endif // pRC_CORE_FUNCTORS_CHIP_H
