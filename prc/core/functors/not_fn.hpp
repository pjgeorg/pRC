// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_NOT_FN_H
#define pRC_CORE_FUNCTORS_NOT_FN_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/functors/logical_not.hpp>

namespace pRC
{
    template<class F>
    struct NotFn
    {
        template<class... Xs,
            If<True<decltype(LogicalNot()(F()(declval<Xs>()...)))>> = 0>
        constexpr decltype(auto) operator()(Xs &&...args) const
        {
            return LogicalNot()(F()(forward<Xs>(args)...));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_NOT_FN_H
