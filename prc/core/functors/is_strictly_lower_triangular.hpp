// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_IS_STRICTLY_LOWER_TRIANGULAR_H
#define pRC_CORE_FUNCTORS_IS_STRICTLY_LOWER_TRIANGULAR_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct IsStrictlyLowerTriangular
    {
        template<class X,
            If<True<decltype(isStrictlyLowerTriangular(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return isStrictlyLowerTriangular(forward<X>(a));
        }

        template<class X, class XT,
            If<True<decltype(
                isStrictlyLowerTriangular(declval<X>(), declval<XT>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a, XT &&tolerance) const
        {
            return isStrictlyLowerTriangular(
                forward<X>(a), forward<XT>(tolerance));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_IS_STRICTLY_LOWER_TRIANGULAR_H
