// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_STRIDE_H
#define pRC_CORE_FUNCTORS_STRIDE_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/functions/stride.hpp>

namespace pRC
{
    template<Size... Ss>
    struct Stride
    {
        template<class X, class... Os,
            If<True<decltype(stride<Ss...>(declval<X>(), declval<Os>()...))>> =
                0>
        constexpr decltype(auto) operator()(X &&a, Os const... offsets) const
        {
            return stride<Ss...>(forward<X>(a), offsets...);
        }
    };
}
#endif // pRC_CORE_FUNCTORS_STRIDE_H
