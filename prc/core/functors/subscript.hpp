// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_SUBSCRIPT_H
#define pRC_CORE_FUNCTORS_SUBSCRIPT_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct Subscript
    {
        template<class X,
            If<True<decltype(declval<X>()[declval<Index const>()])>> = 0>
        constexpr decltype(auto) operator()(X &&a, Index const index) const
        {
            return forward<X>(a)[index];
        }
    };

    template<class T>
    using IsSubscriptable = IsInvocable<Subscript, T, Index>;
}
#endif // pRC_CORE_FUNCTORS_SUBSCRIPT_H
