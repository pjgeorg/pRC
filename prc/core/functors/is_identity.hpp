// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_IS_IDENTITY_H
#define pRC_CORE_FUNCTORS_IS_IDENTITY_H

#include <prc/core/basic/functions/is_identity.hpp>
#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    struct IsIdentity
    {
        template<class X, If<True<decltype(isIdentity(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return isIdentity(forward<X>(a));
        }

        template<class X, class XT,
            If<True<decltype(isIdentity(declval<X>(), declval<XT>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a, XT &&tolerance) const
        {
            return isIdentity(forward<X>(a), forward<XT>(tolerance));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_IS_IDENTITY_H
