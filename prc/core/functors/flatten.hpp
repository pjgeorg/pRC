// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_FUNCTORS_FLATTEN_H
#define pRC_CORE_FUNCTORS_FLATTEN_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/tensor/functions/flatten.hpp>

namespace pRC
{
    template<Index K>
    struct Flatten
    {
        template<class X, If<True<decltype(flatten<K>(declval<X>()))>> = 0>
        constexpr decltype(auto) operator()(X &&a) const
        {
            return flatten<K>(forward<X>(a));
        }
    };
}
#endif // pRC_CORE_FUNCTORS_FLATTEN_H
