// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_LOG_H
#define pRC_CORE_LOG_H

#include <prc/core/log/io.hpp>
#include <prc/core/log/log.hpp>
#include <prc/core/log/print.hpp>

#endif // pRC_CORE_LOG_H
