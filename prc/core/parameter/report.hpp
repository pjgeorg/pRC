// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_PARAMETER_REPORT_H
#define pRC_CORE_PARAMETER_REPORT_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/log/log.hpp>
#include <prc/core/parameter/parameter.hpp>

namespace pRC
{
    template<class... Ps>
    static inline constexpr auto report(Ps &&...parameters)
    {
        auto const print = [](auto const &parameter)
        {
            Logging::log("   ", parameter.name() + ":", parameter.value());
        };

        Logging::log("Compile Time Parameters:");

        auto const printCompileTime = [&print](auto const &parameter)
        {
            if constexpr(typename RemoveReference<decltype(
                             parameter)>::Context() == Context::CompileTime)
            {
                print(parameter);
            }
        };
        (printCompileTime(parameters), ...);

        Logging::log("");
        Logging::log("Run Time Parameters:");

        auto const printRunTime = [&print](auto const &parameter)
        {
            if constexpr(typename RemoveReference<decltype(
                             parameter)>::Context() == Context::RunTime)
            {
                print(parameter);
            }
        };
        (printRunTime(parameters), ...);

        Logging::log("");
    }
}
#endif // pRC_CORE_PARAMETER_REPORT_H
