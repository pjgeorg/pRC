// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_PARAMETER_PARSE_H
#define pRC_CORE_PARAMETER_PARSE_H

#include <cstring>

#include <prc/core/basic/type_name.hpp>
#include <prc/core/basic/type_traits.hpp>
#include <prc/core/log/log.hpp>
#include <prc/core/parameter/parameter.hpp>
#include <prc/core/parameter/report.hpp>

namespace pRC
{
    template<class N, class D, class... Ps,
        If<True<decltype(String(declval<N>()))>> = 0,
        If<True<decltype(String(declval<D>()))>> = 0>
    static inline constexpr auto parse(int const argc,
        char const *const *const argv, N &&appName, D &&appDescription,
        Ps &...parameters)
    {
        Logging::log(appName);
        Logging::log("   ", appDescription);
        Logging::log("");

        for(int i = 1; i < argc; ++i)
        {
            if(std::strcmp(argv[i], "--help") == 0)
            {
                help(parameters...);
                std::exit(0);
            }

            auto const parse = [&argc, &argv, &i](auto &parameter)
            {
                if constexpr(typename RemoveReference<decltype(
                                 parameter)>::Context() == Context::RunTime)
                {
                    if(std::strcmp(argv[i], parameter.argument().cString()) ==
                        0)
                    {
                        ++i;
                        if(i >= argc)
                        {
                            auto const msg = "Missing value for parameter " +
                                parameter.name() + ": " + parameter.argument() +
                                " <" +
                                name<typename RemoveReference<decltype(
                                    parameter)>::Type>() +
                                ">";
                            Logging::error(msg);
                        }
                        parameter.parseValue(argv[i]);
                    }
                }
            };

            (parse(parameters), ...);
        }

        auto allSet = true;
        auto const check = [&allSet](auto const &parameter)
        {
            if constexpr(typename RemoveReference<decltype(
                             parameter)>::Context() == Context::RunTime)
            {
                if(!parameter.isSet())
                {
                    Logging::log("Missing parameter:", parameter.name() + ":",
                        parameter.argument(),
                        "<" +
                            name<typename RemoveReference<decltype(
                                parameter)>::Type>() +
                            ">");

                    allSet = false;
                }
            }
        };

        (check(parameters), ...);

        if(!allSet)
        {
            Logging::error("Some parameters have not been set!");
        }

        return report(parameters...);
    }
}
#endif // pRC_CORE_PARAMETER_PARSE_H
