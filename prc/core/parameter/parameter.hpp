// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_PARAMETER_PARAMETER_H
#define pRC_CORE_PARAMETER_PARAMETER_H

#include <cstdlib>
#include <cstring>

#include <prc/core/basic/context.hpp>
#include <prc/core/basic/string.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<Context C, class T, Size SA, Size SN, Size SD>
    class Parameter;

    template<class T, Size SA, Size SN, Size SD>
    class Parameter<Context::CompileTime, T, SA, SN, SD>
    {
    public:
        using Context = Constant<pRC::Context, pRC::Context::CompileTime>;
        using Type = T;

    public:
        template<class A, class N, class D,
            If<IsConstructible<String<SA>, A>> = 0,
            If<IsConstructible<String<SN>, N>> = 0,
            If<IsConstructible<String<SD>, D>> = 0>
        Parameter(A &&argument, N &&name, D &&description, T const &value)
            : mArgument(forward<A>(argument))
            , mName(forward<N>(name))
            , mDescription(forward<D>(description))
            , mValue(value)
        {
        }

        constexpr auto &argument() const
        {
            return mArgument;
        }

        constexpr auto &name() const
        {
            return mName;
        }

        constexpr auto &description() const
        {
            return mDescription;
        }

        constexpr auto &value() const
        {
            return mValue;
        }

        constexpr auto operator()() const
        {
            return value();
        }

        constexpr operator T() const
        {
            return value();
        }

    private:
        String<SA> const mArgument;
        String<SN> const mName;
        String<SD> const mDescription;
        T mValue;
    };

    template<class T, Size SA, Size SN, Size SD>
    class Parameter<Context::RunTime, T, SA, SN, SD>
    {
    public:
        using Context = Constant<pRC::Context, pRC::Context::RunTime>;
        using Type = T;

    public:
        template<class A, class N, class D,
            If<IsConstructible<String<SA>, A>> = 0,
            If<IsConstructible<String<SN>, N>> = 0,
            If<IsConstructible<String<SD>, D>> = 0>
        Parameter(A &&argument, N &&name, D &&description)
            : mArgument(forward<A>(argument))
            , mName(forward<N>(name))
            , mDescription(forward<D>(description))
        {
        }

        template<class A, class N, class D,
            If<IsConstructible<String<SA>, A>> = 0,
            If<IsConstructible<String<SN>, N>> = 0,
            If<IsConstructible<String<SD>, D>> = 0>
        Parameter(
            A &&argument, N &&name, D &&description, T const &defaultValue)
            : mArgument(forward<A>(argument))
            , mName(forward<N>(name))
            , mDescription(forward<D>(description))
            , mValue(defaultValue)
            , mSet(true)
        {
        }

        template<class A, class N, class D,
            If<IsConstructible<String<SA>, A>> = 0,
            If<IsConstructible<String<SN>, N>> = 0,
            If<IsConstructible<String<SD>, D>> = 0>
        Parameter(A &&argument, N &&name, D &&description,
            T (*convert)(char const *const))
            : mArgument(forward<A>(argument))
            , mName(forward<N>(name))
            , mDescription(forward<D>(description))
            , mConvert(convert)
        {
        }

        template<class A, class N, class D,
            If<IsConstructible<String<SA>, A>> = 0,
            If<IsConstructible<String<SN>, N>> = 0,
            If<IsConstructible<String<SD>, D>> = 0>
        Parameter(A &&argument, N &&name, D &&description,
            T (*convert)(char const *const), T const &defaultValue)
            : mArgument(forward<A>(argument))
            , mName(forward<N>(name))
            , mDescription(forward<D>(description))
            , mValue(defaultValue)
            , mSet(true)
            , mConvert(convert)
        {
        }

        constexpr auto &argument() const
        {
            return mArgument;
        }

        constexpr auto &name() const
        {
            return mName;
        }

        constexpr auto &description() const
        {
            return mDescription;
        }

        constexpr auto &value() const
        {
            if(!mSet)
            {
                Logging::error("Accessing non-set value of parameter", name());
            }
            return mValue;
        }

        constexpr auto &isSet() const
        {
            return mSet;
        }

        constexpr auto parseValue(char const *const value)
        {
            mValue = mConvert(value);
            mSet = true;
        }

        constexpr auto operator()() const
        {
            return value();
        }

        constexpr operator T() const
        {
            return value();
        }

    private:
        String<SA> const mArgument;
        String<SN> const mName;
        String<SD> const mDescription;
        T mValue;
        Bool mSet = false;

        T(*mConvert)
        (char const *const) = [](char const *const value) -> T
        {
            if constexpr(IsFloat<T>())
            {
                return std::atof(value);
            }
            else if constexpr(IsIntegral<T>() || IsInteger<T>())
            {
                return std::atoll(value);
            }
            else if constexpr(IsBool<T>())
            {
                return std::strcmp(value, "true") == 0;
            }
            else
            {
                return value;
            }
        };
    };

    template<Context C, class T, class A, class N, class D,
        If<True<decltype(String(declval<A>()))>> = 0,
        If<True<decltype(String(declval<N>()))>> = 0,
        If<True<decltype(String(declval<D>()))>> = 0,
        If<IsSatisfied<(C == Context::CompileTime || C == Context::RunTime)>> =
            0>
    static inline constexpr auto parameter(
        A &&argument, N &&name, D &&description, T const &defaultValue)
    {
        using SA = decltype(String(declval<A>()));
        using SN = decltype(String(declval<N>()));
        using SD = decltype(String(declval<D>()));

        return Parameter<C, T, SA::size(), SN::size(), SD::size()>(
            forward<A>(argument), forward<N>(name), forward<D>(description),
            defaultValue);
    }

    template<Context C, class T, class A, class N, class D,
        If<True<decltype(String(declval<A>()))>> = 0,
        If<True<decltype(String(declval<N>()))>> = 0,
        If<True<decltype(String(declval<D>()))>> = 0,
        If<IsSatisfied<(C == Context::RunTime)>> = 0>
    static inline constexpr auto parameter(
        A &&argument, N &&name, D &&description)
    {
        using SA = decltype(String(declval<A>()));
        using SN = decltype(String(declval<N>()));
        using SD = decltype(String(declval<D>()));

        return Parameter<C, T, SA::size(), SN::size(), SD::size()>(
            forward<A>(argument), forward<N>(name), forward<D>(description));
    }

    template<Context C, class T, class A, class N, class D,
        If<True<decltype(String(declval<A>()))>> = 0,
        If<True<decltype(String(declval<N>()))>> = 0,
        If<True<decltype(String(declval<D>()))>> = 0,
        If<IsSatisfied<(C == Context::RunTime)>> = 0>
    static inline constexpr auto parameter(A &&argument, N &&name,
        D &&description, T (*convert)(char const *const))
    {
        using SA = decltype(String(declval<A>()));
        using SN = decltype(String(declval<N>()));
        using SD = decltype(String(declval<D>()));

        return Parameter<C, T, SA::size(), SN::size(), SD::size()>(
            forward<A>(argument), forward<N>(name), forward<D>(description),
            convert);
    }

    template<Context C, class T, class A, class N, class D,
        If<True<decltype(String(declval<A>()))>> = 0,
        If<True<decltype(String(declval<N>()))>> = 0,
        If<True<decltype(String(declval<D>()))>> = 0,
        If<IsSatisfied<(C == Context::RunTime)>> = 0>
    static inline constexpr auto parameter(A &&argument, N &&name,
        D &&description, T (*convert)(char const *const), T const &defaultValue)
    {
        using SA = decltype(String(declval<A>()));
        using SN = decltype(String(declval<N>()));
        using SD = decltype(String(declval<D>()));

        return Parameter<C, T, SA::size(), SN::size(), SD::size()>(
            forward<A>(argument), forward<N>(name), forward<D>(description),
            convert, defaultValue);
    }
}
#endif // pRC_CORE_PARAMETER_PARAMETER_H
