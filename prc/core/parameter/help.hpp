// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_PARAMETER_HELP_H
#define pRC_CORE_PARAMETER_HELP_H

#include <prc/core/basic/type_name.hpp>
#include <prc/core/basic/type_traits.hpp>
#include <prc/core/log/log.hpp>
#include <prc/core/parameter/parameter.hpp>

namespace pRC
{
    template<class... Ps>
    static inline constexpr auto help(Ps &&...parameters)
    {
        Logging::log("Compile Time Parameters:");

        auto const printCompileTime = [](auto const &parameter)
        {
            if constexpr(typename RemoveReference<decltype(
                             parameter)>::Context() == Context::CompileTime)
            {
                Logging::log("   ", parameter.name() + ":",
                    parameter.argument() + "=<" +
                        name<typename RemoveReference<decltype(
                            parameter)>::Type>() +
                        ">");
                if(parameter.description().size())
                {
                    Logging::log("       ", parameter.description());
                }
            }
        };
        (printCompileTime(parameters), ...);

        Logging::log("");
        Logging::log("Run Time Parameters:");

        auto const printRunTime = [](auto const &parameter)
        {
            if constexpr(typename RemoveReference<decltype(
                             parameter)>::Context() == Context::RunTime)
            {
                Logging::log("   ", parameter.name() + ":",
                    parameter.argument(),
                    "<" +
                        name<typename RemoveReference<decltype(
                            parameter)>::Type>() +
                        ">");
                if(parameter.description().size())
                {
                    Logging::log("       ", parameter.description());
                }
            }
        };
        (printRunTime(parameters), ...);

        Logging::log("");
    }
}
#endif // pRC_CORE_PARAMETER_HELP_H
