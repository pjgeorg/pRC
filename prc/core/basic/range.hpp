// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_RANGE_H
#define pRC_CORE_BASIC_RANGE_H

#include <prc/core/basic/context.hpp>
#include <prc/core/basic/direction.hpp>
#include <prc/core/basic/sequence.hpp>
#include <prc/core/basic/type_traits.hpp>
#include <prc/core/functors/mul.hpp>

namespace pRC
{
    template<Context C, Size I, Size L, Direction D = Direction::Forwards,
        Size S = 1, class F, class... Xs>
    [[gnu::always_inline]] static inline constexpr auto range(
        F &&f, Xs &&...args)
    {
        if constexpr(C == Context::CompileTime)
        {
            expand(makeRange<Index, I, L, D, S>(),
                [&f, &args...](auto const... seq)
                {
                    (f(args..., seq), ...);
                });
        }
        if constexpr(C == Context::RunTime)
        {
            if constexpr(I < L)
            {
                for(Index i = I; i < L; i += S)
                {
                    if constexpr(D == Direction::Forwards)
                    {
                        f(forward<Xs>(args)..., i);
                    }
                    if constexpr(D == Direction::Backwards)
                    {
                        f(forward<Xs>(args)..., L + I - S - i);
                    }
                }
            }
        }
    }

    template<Size I, Size L, Direction D = Direction::Forwards, Size S = 1,
        class F, class... Xs>
    [[gnu::always_inline]] static inline constexpr auto range(
        F &&f, Xs &&...args)
    {
        if constexpr((L - I) / S <= 8)
        {
            range<Context::CompileTime, I, L, D, S>(
                forward<F>(f), forward<Xs>(args)...);
        }
        else
        {
            range<Context::RunTime, I, L, D, S>(
                forward<F>(f), forward<Xs>(args)...);
        }
    }

    template<Context C, Size L, Direction D = Direction::Forwards, Size S = 1,
        class F, class... Xs>
    [[gnu::always_inline]] static inline constexpr auto range(
        F &&f, Xs &&...args)
    {
        range<C, 0, L, D, S>(forward<F>(f), forward<Xs>(args)...);
    }

    template<Size L, Direction D = Direction::Forwards, Size S = 1, class F,
        class... Xs>
    [[gnu::always_inline]] static inline constexpr auto range(
        F &&f, Xs &&...args)
    {
        range<0, L, D, S>(forward<F>(f), forward<Xs>(args)...);
    }

    template<Context C, class I, class L, Direction O, class D,
        class S = decltype(
            makeConstantSequence<Size, typename I::Dimension{}, 1>()),
        class F, class... Xs, If<IsSizes<I>> = 0, If<IsSizes<L>> = 0,
        If<IsSequence<D>> = 0, If<IsSizes<S>> = 0,
        If<IsSame<Direction, typename D::Type>> = 0,
        If<IsSame<typename I::Dimension, typename L::Dimension,
            typename D::Dimension, typename S::Dimension>> = 0,
        If<IsSatisfied<((L() - I()) % S() == Constant<Size, 0>())>> = 0,
        If<IsSatisfied<(
            O == Direction::LeftToRight || O == Direction::RightToLeft)>> = 0>
    [[gnu::always_inline]] static inline constexpr auto range(
        F &&f, Xs &&...args)
    {
        if constexpr(typename L::Dimension() > 0)
        {
            if constexpr(O == Direction::RightToLeft)
            {
                auto g = [&f, &args...](auto const... indices)
                {
                    constexpr auto N = typename L::Dimension();

                    expand(
                        reverse(makeSeries<Index, N>()),
                        [&f, &args...](auto const &indices, auto const... seq)
                        {
                            f(forward<Xs>(args)..., get<seq>(indices)...);
                        },
                        tuple(indices...));
                };

                using RI = decltype(reverse(I()));
                using RL = decltype(reverse(L()));
                using RD = decltype(reverse(D()));
                using RS = decltype(reverse(S()));

                range<C, RI, RL, Direction::LeftToRight, RD, RS>(g);
            }
            if constexpr(O == Direction::LeftToRight)
            {
                auto g = [&f, &args...](auto const... indices)
                {
                    range<C, I::size(0), L::size(0), D::size(0), S::size(0)>(
                        [&f, &args..., &indices...](auto const i)
                        {
                            f(forward<Xs>(args)..., i, indices...);
                        });
                };

                using NI = decltype(chip<0>(I()));
                using NL = decltype(chip<0>(L()));
                using ND = decltype(chip<0>(D()));
                using NS = decltype(chip<0>(S()));
                range<C, NI, NL, Direction::LeftToRight, ND, NS>(g);
            }
        }
        else
        {
            forward<F>(f)(forward<Xs>(args)...);
        }
    }

    template<Context C, class I, class L, Direction O = Direction::LeftToRight,
        class S = decltype(
            makeConstantSequence<Size, typename I::Dimension{}, 1>()),
        class D = decltype(makeConstantSequence<Direction,
            typename I::Dimension{}, Direction::Forwards>()),
        class F, class... Xs, If<IsSizes<I>> = 0, If<IsSizes<L>> = 0,
        If<IsSizes<S>> = 0, If<IsSequence<D>> = 0,
        If<IsSame<Direction, typename D::Type>> = 0,
        If<IsSame<typename I::Dimension, typename L::Dimension,
            typename S::Dimension, typename D::Dimension>> = 0,
        If<IsSatisfied<((L() - I()) % S() == Constant<Size, 0>())>> = 0,
        If<IsSatisfied<(
            O == Direction::LeftToRight || O == Direction::RightToLeft)>> = 0>
    [[gnu::always_inline]] static inline constexpr auto range(
        F &&f, Xs &&...args)
    {
        range<C, I, L, O, D, S>(forward<F>(f), forward<Xs>(args)...);
    }

    template<Context C, class L, Direction O, class D,
        class S = decltype(
            makeConstantSequence<Size, typename L::Dimension{}, 1>()),
        class F, class... Xs, If<IsSizes<L>> = 0, If<IsSequence<D>> = 0,
        If<IsSizes<S>> = 0, If<IsSame<Direction, typename D::Type>> = 0,
        If<IsSame<typename L::Dimension, typename D::Dimension,
            typename S::Dimension>> = 0,
        If<IsSatisfied<(L() % S() == Constant<Size, 0>())>> = 0,
        If<IsSatisfied<(
            O == Direction::LeftToRight || O == Direction::RightToLeft)>> = 0>
    [[gnu::always_inline]] static inline constexpr auto range(
        F &&f, Xs &&...args)
    {
        using I =
            decltype(makeConstantSequence<Size, typename L::Dimension{}, 0>());
        range<C, I, L, O, D, S>(forward<F>(f), forward<Xs>(args)...);
    }

    template<Context C, class L, Direction O = Direction::LeftToRight,
        class S = decltype(
            makeConstantSequence<Size, typename L::Dimension{}, 1>()),
        class D = decltype(makeConstantSequence<Direction,
            typename L::Dimension{}, Direction::Forwards>()),
        class F, class... Xs, If<IsSizes<L>> = 0, If<IsSizes<S>> = 0,
        If<IsSequence<D>> = 0, If<IsSame<Direction, typename D::Type>> = 0,
        If<IsSame<typename L::Dimension, typename S::Dimension,
            typename D::Dimension>> = 0,
        If<IsSatisfied<(L() % S() == Constant<Size, 0>())>> = 0,
        If<IsSatisfied<(
            O == Direction::LeftToRight || O == Direction::RightToLeft)>> = 0>
    [[gnu::always_inline]] static inline constexpr auto range(
        F &&f, Xs &&...args)
    {
        using I =
            decltype(makeConstantSequence<Size, typename L::Dimension{}, 0>());
        range<C, I, L, O, D, S>(forward<F>(f), forward<Xs>(args)...);
    }

    template<class I, class L, Direction O, class D,
        class S = decltype(
            makeConstantSequence<Size, typename I::Dimension{}, 1>()),
        class F, class... Xs, If<IsSizes<I>> = 0, If<IsSizes<L>> = 0,
        If<IsSequence<D>> = 0, If<IsSizes<S>> = 0,
        If<IsSame<Direction, typename D::Type>> = 0,
        If<IsSame<typename I::Dimension, typename L::Dimension,
            typename D::Dimension, typename S::Dimension>> = 0,
        If<IsSatisfied<((L() - I()) % S() == Constant<Size, 0>())>> = 0,
        If<IsSatisfied<(
            O == Direction::LeftToRight || O == Direction::RightToLeft)>> = 0>
    [[gnu::always_inline]] static inline constexpr auto range(
        F &&f, Xs &&...args)
    {
        if constexpr(typename L::Dimension() == 0)
        {
            range<Context::CompileTime, I, L, O, D, S>(
                forward<F>(f), forward<Xs>(args)...);
        }
        else if constexpr(reduce<Mul>((L() - I()) / S()) <= 8)
        {
            range<Context::CompileTime, I, L, O, D, S>(
                forward<F>(f), forward<Xs>(args)...);
        }
        else
        {
            range<Context::RunTime, I, L, O, D, S>(
                forward<F>(f), forward<Xs>(args)...);
        }
    }

    template<class I, class L, Direction O = Direction::LeftToRight,
        class S = decltype(
            makeConstantSequence<Size, typename I::Dimension{}, 1>()),
        class D = decltype(makeConstantSequence<Direction,
            typename I::Dimension{}, Direction::Forwards>()),
        class F, class... Xs, If<IsSizes<I>> = 0, If<IsSizes<L>> = 0,
        If<IsSizes<S>> = 0, If<IsSequence<D>> = 0,
        If<IsSame<Direction, typename D::Type>> = 0,
        If<IsSame<typename I::Dimension, typename L::Dimension,
            typename S::Dimension, typename D::Dimension>> = 0,
        If<IsSatisfied<((L() - I()) % S() == Constant<Size, 0>())>> = 0,
        If<IsSatisfied<(
            O == Direction::LeftToRight || O == Direction::RightToLeft)>> = 0>
    [[gnu::always_inline]] static inline constexpr auto range(
        F &&f, Xs &&...args)
    {
        range<I, L, O, D, S>(forward<F>(f), forward<Xs>(args)...);
    }

    template<class L, Direction O, class D,
        class S = decltype(
            makeConstantSequence<Size, typename L::Dimension{}, 1>()),
        class F, class... Xs, If<IsSizes<L>> = 0, If<IsSequence<D>> = 0,
        If<IsSizes<S>> = 0, If<IsSame<Direction, typename D::Type>> = 0,
        If<IsSame<typename L::Dimension, typename D::Dimension,
            typename S::Dimension>> = 0,
        If<IsSatisfied<(L() % S() == Constant<Size, 0>())>> = 0,
        If<IsSatisfied<(
            O == Direction::LeftToRight || O == Direction::RightToLeft)>> = 0>
    [[gnu::always_inline]] static inline constexpr auto range(
        F &&f, Xs &&...args)
    {
        using I =
            decltype(makeConstantSequence<Size, typename L::Dimension{}, 0>());
        range<I, L, O, D, S>(forward<F>(f), forward<Xs>(args)...);
    }

    template<class L, Direction O = Direction::LeftToRight,
        class S = decltype(
            makeConstantSequence<Size, typename L::Dimension{}, 1>()),
        class D = decltype(makeConstantSequence<Direction,
            typename L::Dimension{}, Direction::Forwards>()),
        class F, class... Xs, If<IsSizes<L>> = 0, If<IsSizes<S>> = 0,
        If<IsSequence<D>> = 0, If<IsSame<Direction, typename D::Type>> = 0,
        If<IsSame<typename L::Dimension, typename S::Dimension,
            typename D::Dimension>> = 0,
        If<IsSatisfied<(L() % S() == Constant<Size, 0>())>> = 0,
        If<IsSatisfied<(
            O == Direction::LeftToRight || O == Direction::RightToLeft)>> = 0>
    [[gnu::always_inline]] static inline constexpr auto range(
        F &&f, Xs &&...args)
    {
        using I =
            decltype(makeConstantSequence<Size, typename L::Dimension{}, 0>());
        range<I, L, O, D, S>(forward<F>(f), forward<Xs>(args)...);
    }
}
#endif // pRC_CORE_BASIC_RANGE_H
