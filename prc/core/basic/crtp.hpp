// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_CRTP_H
#define pRC_CORE_BASIC_CRTP_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<class T>
    class CRTP
    {
    public:
        constexpr operator T() const
        {
            return self();
        }

        using Derived = T;

    protected:
        ~CRTP() = default;
        constexpr CRTP(CRTP const &) = default;
        constexpr CRTP(CRTP &&) = default;
        constexpr CRTP &operator=(CRTP const &) & = default;
        constexpr CRTP &operator=(CRTP &&) & = default;
        constexpr CRTP()
        {
            static_assert(IsBaseOf<CRTP, T>());
        }

    public:
        constexpr decltype(auto) self() &
        {
            return static_cast<T &>(*this);
        }

        constexpr decltype(auto) self() const &
        {
            return static_cast<T const &>(*this);
        }
    };
}
#endif // pRC_CORE_BASIC_CRTP_H
