// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_STRING_H
#define pRC_CORE_BASIC_STRING_H

#include <prc/core/basic/range.hpp>
#include <prc/core/basic/sequence.hpp>
#include <prc/core/basic/type_traits.hpp>
#include <prc/core/container/type_traits.hpp>

namespace pRC
{
    String(char const &)->String<1>;
    template<Size N>
    String(char const (&)[N]) -> String<N - 1>;

    template<Size N>
    class String
    {
    public:
        static constexpr auto size()
        {
            return N;
        }

        static constexpr auto empty()
        {
            return !size();
        }

    private:
        using Type = char;

        static constexpr auto check([[maybe_unused]] Index const index)
        {
            if constexpr(cDebugLevel >= DebugLevel::Low)
            {
                if(!(index <= size()))
                {
                    Logging::error("String index out of range.");
                }
            }
        }

    public:
        ~String() = default;
        constexpr String(String const &) = default;
        constexpr String(String &&) = default;
        constexpr String &operator=(String const &) & = default;
        constexpr String &operator=(String &&) & = default;
        constexpr String()
            : mData{}
        {
        }

        constexpr String(char const (&string)[N + 1])
            : String(makeSeries<Index, N + 1>(), string)
        {
        }

        template<class... Ts,
            If<All<IsSame<char, Ts...>,
                IsSatisfied<(sizeof...(Ts) == size())>>> = 0>
        constexpr String(Ts const &...chars)
            : mData{chars..., '\0'}
        {
        }

        constexpr decltype(auto) operator[](Index const index) &&
        {
            check(index);
            return move(mData)[index];
        }

        constexpr decltype(auto) operator[](Index const index) const &&
        {
            check(index);
            return move(mData)[index];
        }

        constexpr decltype(auto) operator[](Index const index) &
        {
            check(index);
            return mData[index];
        }

        constexpr decltype(auto) operator[](Index const index) const &
        {
            check(index);
            return mData[index];
        }

        constexpr auto find(char const character) const
        {
            for(Index i = 0; i < size(); ++i)
            {
                if(mData[i] == character)
                {
                    return i;
                }
            }
            return N;
        }

        template<Index P, Index L,
            If<IsSatisfied<P + L<N>> = 0> constexpr auto substring() const
        {
            String<L> sub;
            range<L>(
                [this, &sub](auto const i)
                {
                    sub[i] = mData[P + i];
                });

            return sub;
        }

        constexpr auto cString() && = delete;
        constexpr auto cString() const && = delete;

        constexpr auto cString() &
        {
            return mData.data();
        }

        constexpr auto cString() const &
        {
            return mData.data();
        }

    private:
        template<Index... Is, If<IsSatisfied<(sizeof...(Is) == N + 1)>> = 0>
        constexpr String(
            Sequence<Index, Is...> const, char const (&string)[N + 1])
            : mData{string[Is]...}
        {
        }

    private:
        StackArray<Type, size() + 1> mData;
    };

    template<Size M, Size N>
    static inline constexpr auto operator+(
        String<M> const &lhs, String<N> const &rhs)
    {
        String<M + N> concat;
        range<M>(
            [&concat, &lhs](auto const i)
            {
                concat[i] = lhs[i];
            });
        range<N>(
            [&concat, &rhs](auto const i)
            {
                concat[M + i] = rhs[i];
            });

        return concat;
    }

    template<Size M, Size N>
    static inline constexpr auto operator+(
        String<M> const &lhs, char const (&rhs)[N])
    {
        return lhs + String(rhs);
    }

    template<Size M, Size N>
    static inline constexpr auto operator+(
        char const (&lhs)[M], String<N> const &rhs)
    {
        return String(lhs) + rhs;
    }
}
#endif // pRC_CORE_BASIC_STRING_H
