// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_IDENTITY_H
#define pRC_CORE_BASIC_IDENTITY_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<class T>
    struct Identity<T, If<IsBool<T>>>
    {
        constexpr T operator()()
        {
            return true;
        }

        template<class X, If<IsConstructible<T, X>> = 0>
        constexpr T operator()(X &&value)
        {
            return forward<X>(value);
        }
    };

    template<class T>
    struct Identity<T, If<IsIntegral<T>>>
    {
        constexpr T operator()()
        {
            return 1;
        }

        template<class X, If<IsConstructible<T, X>> = 0>
        constexpr T operator()(X &&value)
        {
            return forward<X>(value);
        }
    };

    template<>
    struct Identity<Void<>>
    {
        template<class T, If<Any<IsBool<T>, IsIntegral<T>>> = 0>
        constexpr operator T() const
        {
            return Identity<T>()();
        }
    };
}
#endif // pRC_CORE_BASIC_IDENTITY_H
