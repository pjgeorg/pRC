// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_POSITION_H
#define pRC_CORE_BASIC_POSITION_H

namespace pRC
{
    enum class Position
    {
        Left,
        Right,
        Front,
        Back,
    };

    static inline constexpr auto operator!(Position const P)
    {
        switch(P)
        {
            case Position::Left:
                return Position::Right;
            case Position::Right:
                return Position::Left;
            case Position::Front:
                return Position::Back;
            case Position::Back:
                return Position::Front;
        }
    }
}
#endif // pRC_CORE_BASIC_POSITION_H
