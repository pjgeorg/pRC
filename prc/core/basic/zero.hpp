// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_ZERO_H
#define pRC_CORE_BASIC_ZERO_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<class T>
    struct Zero<T, If<IsBool<T>>>
    {
        constexpr T operator()()
        {
            return false;
        }
    };

    template<class T>
    struct Zero<T, If<IsIntegral<T>>>
    {
        constexpr T operator()()
        {
            return 0;
        }
    };

    template<>
    struct Zero<Void<>>
    {
        template<class T, If<Any<IsBool<T>, IsIntegral<T>>> = 0>
        constexpr operator T() const
        {
            return Zero<T>()();
        }
    };
}
#endif // pRC_CORE_BASIC_ZERO_H
