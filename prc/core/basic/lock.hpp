// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_LOCK_H
#define pRC_CORE_BASIC_LOCK_H

#include <mutex>

namespace pRC
{
    using Mutex = std::mutex;

    template<class Mutex>
    class Lock
    {
    public:
        explicit Lock(Mutex &mutex)
            : mMutex(mutex)
        {
            mMutex.lock();
        }

        ~Lock()
        {
            mMutex.unlock();
        }

        Lock(Lock const &) = delete;
        Lock &operator=(Lock const &) = delete;

    private:
        Mutex &mMutex;
    };
}
#endif // pRC_CORE_BASIC_LOCK_H
