// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_UNIT_H
#define pRC_CORE_BASIC_UNIT_H

#include <prc/core/basic/identity.hpp>
#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<class T>
    struct Unit<T, If<Any<IsBool<T>, IsIntegral<T>>>> : public Identity<T>
    {
    };

    template<>
    struct Unit<Void<>>
    {
        template<class T, If<Any<IsBool<T>, IsIntegral<T>>> = 0>
        constexpr operator T() const
        {
            return Unit<T>()();
        }
    };
}
#endif // pRC_CORE_BASIC_UNIT_H
