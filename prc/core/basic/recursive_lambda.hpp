// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_RECURSIVE_LAMBDA_H
#define pRC_CORE_BASIC_RECURSIVE_LAMBDA_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<class L>
    class RecursiveLambda
    {
    public:
        RecursiveLambda(RecursiveLambda const &) = delete;
        RecursiveLambda &operator=(RecursiveLambda const &) = delete;

        template<class X, If<IsConstructible<L, X>> = 0>
        constexpr RecursiveLambda(X &&lambda)
            : mLambda(std::forward<X>(lambda))
        {
        }

        template<auto... Ps, class... Xs>
        constexpr decltype(auto) operator()(Xs &&...args) const
        {
            return mLambda.template operator()<Ps...>(
                *this, forward<Xs>(args)...);
        }

        template<auto... Ps, class... Xs>
        constexpr decltype(auto) operator()(Xs &&...args)
        {
            return mLambda.template operator()<Ps...>(
                *this, forward<Xs>(args)...);
        }

    private:
        L mLambda;
    };

    template<class X>
    RecursiveLambda(X &&) -> RecursiveLambda<RemoveReference<X>>;
}
#endif // pRC_CORE_BASIC_RECURSIVE_LAMBDA_H
