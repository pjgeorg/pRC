// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_COMMON_H
#define pRC_CORE_BASIC_COMMON_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<class TA, class TB>
    struct DetectCommonType
    {
        template<class S>
        struct Success
        {
            using Type = S;
        };

        struct Failure
        {
        };

        template<class U1, class U2>
        static Success<RemoveConstReference<decltype(
            false ? declval<U1>() : declval<U2>())>>
        test(int);

        template<class, class>
        static Failure test(...);

        using Type = decltype(test<TA, TB>(0));
    };

    template<class TA, class TB, class = If<>>
    struct CommonType : DetectCommonType<TA, TB>::Type
    {
    };

    template<class T, class... Ts>
    struct CommonTypes : CommonTypes<typename CommonTypes<T, Ts>::Type...>
    {
    };

    template<class TA, class TB>
    struct CommonTypes<TA, TB> : CommonType<TA, TB>
    {
    };

    template<class T>
    struct CommonTypes<T> : CommonTypes<T, T>
    {
    };

    template<class... Ts>
    using Common = typename CommonTypes<Ts...>::Type;

    template<class... Ts>
    using HasCommon = IsDetected<Common, Ts...>;
}
#endif // pRC_CORE_BASIC_COMMON_H
