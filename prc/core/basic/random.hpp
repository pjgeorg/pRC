// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_RANDOM_H
#define pRC_CORE_BASIC_RANDOM_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/random/type_traits.hpp>

namespace pRC
{
    template<class T, class D, class = If<>>
    struct Random;
}
#endif // pRC_CORE_BASIC_RANDOM_H
