// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_DIRECTION_H
#define pRC_CORE_BASIC_DIRECTION_H

namespace pRC
{
    enum class Direction
    {
        Leftwards,
        RightToLeft = Leftwards,
        Rightwards,
        LeftToRight = Rightwards,
        Upwards,
        Up = Upwards,
        Downwards,
        Down = Downwards,
        Forwards,
        Forward = Forwards,
        Backwards,
        Backward = Backwards,
    };

    static inline constexpr auto reverse(Direction const D)
    {
        switch(D)
        {
            case Direction::Leftwards:
                return Direction::Rightwards;
            case Direction::Rightwards:
                return Direction::Leftwards;
            case Direction::Upwards:
                return Direction::Downwards;
            case Direction::Downwards:
                return Direction::Upwards;
            case Direction::Forwards:
                return Direction::Backwards;
            case Direction::Backwards:
                return Direction::Forwards;
        }
    }
}
#endif // pRC_CORE_BASIC_DIRECTION_H
