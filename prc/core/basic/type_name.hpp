// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_TYPE_NAME_H
#define pRC_CORE_BASIC_TYPE_NAME_H

#include <prc/core/basic/string.hpp>

namespace pRC
{
    template<class T>
    static inline constexpr auto name()
    {
        constexpr String s = __PRETTY_FUNCTION__;
        constexpr auto offset = s.find('=') + 2;
        constexpr auto length = s.size() - offset - 1;

        return s.template substring<offset, length>();
    }

    template<auto T>
    static inline constexpr auto name()
    {
        constexpr String s = __PRETTY_FUNCTION__;
        constexpr auto offset = s.find('=') + 2;
        constexpr auto length = s.size() - offset - 1;

        return s.template substring<offset, length>();
    }
}
#endif // pRC_CORE_BASIC_TYPE_NAME_H
