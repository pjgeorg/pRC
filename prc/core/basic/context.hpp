// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_CONTEXT_H
#define pRC_CORE_BASIC_CONTEXT_H

namespace pRC
{
    enum class Context
    {
        CompileTime,
        RunTime,
    };
}
#endif // pRC_CORE_BASIC_CONTEXT_H
