// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_LIMITS_H
#define pRC_CORE_BASIC_LIMITS_H

#include <limits>

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<class T, class = If<>>
    struct NumericLimits;

    template<class T>
    struct NumericLimits<T const> : public NumericLimits<T>
    {
    };

    template<class T>
    struct NumericLimits<T, If<IsIntegral<T>>>
    {
        static constexpr Size digits()
        {
            return std::numeric_limits<T>::digits;
        }

        static constexpr auto min()
        {
            return std::numeric_limits<T>::min();
        }

        static constexpr auto max()
        {
            return std::numeric_limits<T>::max();
        }

        static constexpr auto lowest()
        {
            return std::numeric_limits<T>::lowest();
        }
    };
}
#endif // pRC_CORE_BASIC_LIMITS_H
