// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_FUNCTIONS_CEIL_DIV_H
#define pRC_CORE_BASIC_FUNCTIONS_CEIL_DIV_H

#include <prc/core/basic/common.hpp>
#include <prc/core/basic/functions/unit.hpp>
#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<class TA, class TB, If<IsUnsignedIntegral<TA>> = 0,
        If<IsUnsignedIntegral<TB>> = 0>
    static inline constexpr auto ceilDiv(TA const a, TB const b)
    {
        using T = Common<TA, TB>;
        return a % b != zero() ? a / b + unit<T>() : a / b;
    }
}
#endif // pRC_CORE_BASIC_FUNCTIONS_CEIL_DIV_H
