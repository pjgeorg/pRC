// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_FUNCTIONS_IDENTITY_H
#define pRC_CORE_BASIC_FUNCTIONS_IDENTITY_H

#include <prc/core/basic/identity.hpp>

namespace pRC
{
    template<class T, If<IsDefined<Identity<T>>> = 0,
        If<IsConstructible<Identity<T>>> = 0>
    static inline constexpr auto identity()
    {
        return Identity<T>()();
    }

    static inline constexpr auto identity()
    {
        return Identity<Void<>>();
    }

    template<class T, class X, If<IsDefined<Identity<T>>> = 0,
        If<IsConstructible<Identity<T>>> = 0,
        If<IsInvocable<Identity<T>, X>> = 0>
    static inline constexpr auto identity(X &&value)
    {
        return Identity<T>()(forward<X>(value));
    }
}
#endif // pRC_CORE_BASIC_FUNCTIONS_IDENTITY_H
