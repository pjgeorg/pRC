// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_FUNCTIONS_IS_ODD_H
#define pRC_CORE_BASIC_FUNCTIONS_IS_ODD_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<class T, If<IsIntegral<T>> = 0>
    static inline constexpr auto isOdd(T const a)
    {
        return a % T(2) != 0;
    }

    template<class T, T V, If<IsIntegral<T>> = 0>
    static inline constexpr auto isOdd(Constant<T, V> const)
    {
        return V % T(2) != 0;
    }
}
#endif // pRC_CORE_BASIC_FUNCTIONS_IS_ODD_H
