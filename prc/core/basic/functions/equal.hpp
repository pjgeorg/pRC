// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_FUNCTIONS_EQUAL_H
#define pRC_CORE_BASIC_FUNCTIONS_EQUAL_H

#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<class X>
    static inline constexpr auto operator==(X &&a, Zero<> const)
    {
        return forward<X>(a) == zero<RemoveReference<X>>();
    }

    template<class X>
    static inline constexpr auto operator==(Zero<> const, X &&b)
    {
        return zero<RemoveReference<X>>() == forward<X>(b);
    }

    static inline constexpr auto operator==(Zero<> const, Zero<> const)
    {
        return true;
    }
}
#endif // pRC_CORE_BASIC_FUNCTIONS_EQUAL_H
