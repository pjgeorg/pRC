// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_FUNCTIONS_IPOW_H
#define pRC_CORE_BASIC_FUNCTIONS_IPOW_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<class B, class N, If<IsIntegral<B>> = 0,
        If<IsUnsignedIntegral<N>> = 0>
    static inline constexpr B iPow(B const base, N const exp)
    {
        return exp == B(0) ? B(1) : base * iPow(base, exp - N(1));
    }
}
#endif // pRC_CORE_BASIC_FUNCTIONS_IPOW_H
