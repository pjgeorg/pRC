// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_FUNCTIONS_WHERE_H
#define pRC_CORE_BASIC_FUNCTIONS_WHERE_H

#include <prc/core/basic/common.hpp>
#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<class TE, class T, If<IsBool<TE>> = 0>
    static inline constexpr T where(TE const e, T &&a, T &&b)
    {
        return e ? a : b;
    }

    template<class TE, class T, If<IsBool<TE>> = 0>
    static inline constexpr T const &where(TE const e, T const &a, T &b)
    {
        return e ? a : b;
    }

    template<class TE, class T, If<IsBool<TE>> = 0>
    static inline constexpr T const &where(TE const e, T &a, T const &b)
    {
        return e ? a : b;
    }

    template<class TE, class XA, class XB, If<IsBool<TE>> = 0>
    static inline constexpr auto where(TE const e, XA &&a, XB &&b)
    {
        using T = Common<RemoveReference<XA>, RemoveReference<XB>>;
        return where(e, T(forward<XA>(a)), T(forward<XB>(b)));
    }
}
#endif // pRC_CORE_BASIC_FUNCTIONS_WHERE_H
