// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_FUNCTIONS_IS_ZERO_H
#define pRC_CORE_BASIC_FUNCTIONS_IS_ZERO_H

#include <prc/core/basic/functions/zero.hpp>

namespace pRC
{
    template<class T, If<IsIntegral<T>> = 0>
    static inline constexpr auto isZero(T const a)
    {
        return a == zero();
    }

    template<class T, T V, If<IsIntegral<T>> = 0>
    static inline constexpr auto isZero(Constant<T, V> const)
    {
        return V == zero();
    }
}
#endif // pRC_CORE_BASIC_FUNCTIONS_IS_ZERO_H
