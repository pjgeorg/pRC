// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_FUNCTIONS_IS_POWER_OF_TWO_H
#define pRC_CORE_BASIC_FUNCTIONS_IS_POWER_OF_TWO_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<class T, If<IsIntegral<T>> = 0>
    static inline constexpr auto isPowerOfTwo(T const v)
    {
        return (v > 0) && !(v & (v - T(1)));
    }
}
#endif // pRC_CORE_BASIC_FUNCTIONS_IS_POWER_OF_TWO_H
