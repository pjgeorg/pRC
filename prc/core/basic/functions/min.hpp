// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_FUNCTIONS_MIN_H
#define pRC_CORE_BASIC_FUNCTIONS_MIN_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/functors/less.hpp>
#include <prc/core/functors/where.hpp>

namespace pRC
{
    template<class X>
    static inline constexpr X min(X &&a)
    {
        return forward<X>(a);
    }

    template<class XA, class XB, If<IsInvocable<Less, XA, XB>> = 0>
    static inline constexpr auto min(XA &&a, XB &&b)
    {
        return where(a < b, forward<XA>(a), forward<XB>(b));
    }

    template<class XA, class XB, class... Xs, If<IsInvocable<Less, XA, XB>> = 0,
        If<All<IsInvocable<Less,
            ResultOf<Where, ResultOf<Less, XA, XB>, XA, XB>, Xs>...>> = 0>
    static inline constexpr auto min(XA &&a, XB &&b, Xs &&...args)
    {
        return min(min(forward<XA>(a), forward<XB>(b)), forward<Xs>(args)...);
    }
}
#endif // pRC_CORE_BASIC_FUNCTIONS_MIN_H
