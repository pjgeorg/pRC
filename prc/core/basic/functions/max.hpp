// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_FUNCTIONS_MAX_H
#define pRC_CORE_BASIC_FUNCTIONS_MAX_H

#include <prc/core/basic/type_traits.hpp>
#include <prc/core/functors/greater.hpp>
#include <prc/core/functors/where.hpp>

namespace pRC
{
    template<class X>
    static inline constexpr X max(X &&a)
    {
        return forward<X>(a);
    }

    template<class XA, class XB, If<IsInvocable<Greater, XA, XB>> = 0>
    static inline constexpr auto max(XA &&a, XB &&b)
    {
        return where(a > b, forward<XA>(a), forward<XB>(b));
    }

    template<class XA, class XB, class... Xs,
        If<IsInvocable<Greater, XA, XB>> = 0,
        If<All<IsInvocable<Greater,
            ResultOf<Where, ResultOf<Greater, XA, XB>, XA, XB>, Xs>...>> = 0>
    static inline constexpr auto max(XA &&a, XB &&b, Xs &&...args)
    {
        return max(max(forward<XA>(a), forward<XB>(b)), forward<Xs>(args)...);
    }
}
#endif // pRC_CORE_BASIC_FUNCTIONS_MAX_H
