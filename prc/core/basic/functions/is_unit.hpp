// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_FUNCTIONS_IS_UNIT_H
#define pRC_CORE_BASIC_FUNCTIONS_IS_UNIT_H

#include <prc/core/basic/functions/unit.hpp>

namespace pRC
{
    template<class T, If<IsIntegral<T>> = 0>
    static inline constexpr auto isUnit(T const a)
    {
        return a == unit();
    }

    template<class T, T V, If<IsIntegral<T>> = 0>
    static inline constexpr auto isUnit(Constant<T, V> const)
    {
        return V == unit();
    }
}
#endif // pRC_CORE_BASIC_FUNCTIONS_IS_UNIT_H
