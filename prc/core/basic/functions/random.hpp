// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_FUNCTIONS_RANDOM_H
#define pRC_CORE_BASIC_FUNCTIONS_RANDOM_H

#include <prc/core/basic/random.hpp>

namespace pRC
{
    template<class T, class D,
        If<IsConstructible<Random<T, D>, RandomEngine &, D &>> = 0>
    static inline constexpr auto random(RandomEngine &rng, D &distribution)
    {
        return Random<T, D>(rng, distribution)();
    }
}
#endif // pRC_CORE_BASIC_FUNCTIONS_RANDOM_H
