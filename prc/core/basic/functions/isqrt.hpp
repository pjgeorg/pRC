// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_FUNCTIONS_ISQRT_H
#define pRC_CORE_BASIC_FUNCTIONS_ISQRT_H

#include <prc/core/basic/recursive_lambda.hpp>
#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<class T, If<IsUnsignedIntegral<T>> = 0>
    static inline constexpr T iSqrt(T const a)
    {
        return RecursiveLambda(
            [&a](auto const &self, T const low, T const high)
            {
                if(low == high)
                {
                    return low;
                }
                else
                {
                    auto const mid = (low + high + T(1)) / T(2);

                    if(a / mid < mid)
                        return self(low, mid - T(1));
                    else
                        return self(mid, high);
                }
            })(T(0), a / T(2) + T(1));
    }
}
#endif // pRC_CORE_BASIC_FUNCTIONS_ISQRT_H
