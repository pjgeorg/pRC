// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_FUNCTIONS_PRINT_H
#define pRC_CORE_BASIC_FUNCTIONS_PRINT_H

#include <cstdio>

#include <prc/core/basic/context.hpp>
#include <prc/core/basic/direction.hpp>
#include <prc/core/basic/range.hpp>
#include <prc/core/basic/sequence.hpp>
#include <prc/core/basic/type_name.hpp>
#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<Size N>
    static inline auto print(String<N> const &string, std::FILE *stream)
    {
        range<0, N>(
            [&string, &stream](auto const i)
            {
                if(string[i] != '\0')
                {
                    std::fputc(string[i], stream);
                }
            });
    }

    template<class T, T... Is, class S>
    static inline auto print(Sequence<T, Is...> const, S &&stream)
    {
        print(name<Sequence<T, Is...>>(), stream);
    }

    template<Size... Ns, class S>
    static inline auto print(Sizes<Ns...> const, S &&stream)
    {
        print(name<Sizes<Ns...>>(), stream);
    }

    template<class S>
    static inline auto print(Direction const direction, S &&stream)
    {
        switch(direction)
        {
            case Direction::Leftwards:
                print("pRC::Direction::Leftwards", stream);
                break;
            case Direction::Rightwards:
                print("pRC::Direction::Rightwards", stream);
                break;
            case Direction::Upwards:
                print("pRC::Direction::Upwards", stream);
                break;
            case Direction::Downwards:
                print("pRC::Direction::Downwards", stream);
                break;
            case Direction::Forwards:
                print("pRC::Direction::Forwards", stream);
                break;
            case Direction::Backwards:
                print("pRC::Direction::Backwards", stream);
                break;
            default:
                print("pRC::Direction::Unknown", stream);
                break;
        }
    }

    template<class S>
    static inline auto print(Context const context, S &&stream)
    {
        switch(context)
        {
            case Context::CompileTime:
                print("pRC::Context::CompileTime", stream);
                break;
            case Context::RunTime:
                print("pRC::Context::RunTime", stream);
                break;
            default:
                print("pRC::Context::Unknown", stream);
                break;
        }
    }
}
#endif // pRC_CORE_BASIC_FUNCTIONS_PRINT_H
