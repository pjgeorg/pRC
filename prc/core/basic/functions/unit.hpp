// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_FUNCTIONS_UNIT_H
#define pRC_CORE_BASIC_FUNCTIONS_UNIT_H

#include <prc/core/basic/unit.hpp>

namespace pRC
{
    template<class T, If<IsDefined<Unit<T>>> = 0,
        If<IsConstructible<Unit<T>>> = 0, If<IsInvocable<Unit<T>>> = 0>
    static inline constexpr auto unit()
    {
        return Unit<T>()();
    }

    static inline constexpr auto unit()
    {
        return Unit<Void<>>();
    }

    template<class T, class X, If<IsDefined<Unit<T>>> = 0,
        If<IsConstructible<Unit<T>>> = 0, If<IsInvocable<Unit<T>, X>> = 0>
    static inline constexpr auto unit(X &&value)
    {
        return Unit<T>()(forward<X>(value));
    }
}
#endif // pRC_CORE_BASIC_FUNCTIONS_UNIT_H
