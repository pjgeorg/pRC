// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_FUNCTIONS_BIT_ROTATE_LEFT_H
#define pRC_CORE_BASIC_FUNCTIONS_BIT_ROTATE_LEFT_H

#include <prc/core/basic/limits.hpp>
#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<class TA, class TB, If<IsUnsignedIntegral<TA>> = 0,
        If<IsUnsignedIntegral<TB>> = 0>
    static inline constexpr auto bitRotateLeft(TA const value, TB count)
    {
        count %= NumericLimits<TA>::digits();

        if(count == 0)
        {
            return value;
        }

        return (value << count) |
            (value >> (NumericLimits<TA>::digits() - count));
    }
}
#endif // pRC_CORE_BASIC_FUNCTIONS_BIT_ROTATE_LEFT_H
