// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_FUNCTIONS_ZERO_H
#define pRC_CORE_BASIC_FUNCTIONS_ZERO_H

#include <prc/core/basic/zero.hpp>

namespace pRC
{
    template<class T, If<IsDefined<Zero<T>>> = 0,
        If<IsConstructible<Zero<T>>> = 0, If<IsInvocable<Zero<T>>> = 0>
    static inline constexpr auto zero()
    {
        return Zero<T>()();
    }

    static inline constexpr auto zero()
    {
        return Zero<Void<>>();
    }
}
#endif // pRC_CORE_BASIC_FUNCTIONS_ZERO_H
