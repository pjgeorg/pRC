// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_FUNCTIONS_IS_IDENTITY_H
#define pRC_CORE_BASIC_FUNCTIONS_IS_IDENTITY_H

#include <prc/core/basic/functions/identity.hpp>

namespace pRC
{
    template<class T, If<IsIntegral<T>> = 0>
    static inline constexpr auto isIdentity(T const a)
    {
        return a == identity();
    }

    template<class T, T V, If<IsIntegral<T>> = 0>
    static inline constexpr auto isIdentity(Constant<T, V> const)
    {
        return V == identity();
    }
}
#endif // pRC_CORE_BASIC_FUNCTIONS_IS_IDENTITY_H
