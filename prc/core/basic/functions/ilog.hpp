// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_FUNCTIONS_ILOG_H
#define pRC_CORE_BASIC_FUNCTIONS_ILOG_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
    template<Size B, class T, If<IsUnsignedIntegral<T>> = 0>
    static inline constexpr T iLog(T const a)
    {
        return (a < B) ? T(0) : T(1) + iLog<B>(T(a / B));
    }
}
#endif // pRC_CORE_BASIC_FUNCTIONS_ILOG_H
