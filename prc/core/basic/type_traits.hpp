// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_BASIC_TYPE_TRAITS_H
#define pRC_CORE_BASIC_TYPE_TRAITS_H

#include <cstdint>
#include <functional>
#include <type_traits>
#include <utility>

#include <prc/std.hpp>

namespace pRC
{
    template<class... Ts>
    using Void = void;

    using Bool = bool;

    using Size = std::size_t;
    using Index = Size;

    using Seed = std::uint32_t;

    using std::declval;
    using std::forward;
    using std::get;
    using std::move;
    using std::swap;
    using std::tie;
    using std::tuple;

    template<class T, T V>
    using Constant = std::integral_constant<T, V>;

    template<class...>
    struct True : Constant<Bool, Bool(true)>
    {
    };

    template<class...>
    struct False : Constant<Bool, Bool(false)>
    {
    };

    template<class T>
    using IsReference = std::is_reference<T>;

    template<class T>
    using IsPointer = std::is_pointer<T>;

    template<class T>
    using IsConst = std::is_const<T>;

    template<class T>
    using RemoveReference = std::remove_reference_t<T>;

    template<class T>
    using RemoveConst = std::remove_const_t<T>;

    template<class T>
    using RemoveConstReference = RemoveConst<RemoveReference<T>>;

    template<class T>
    using AddConst = std::add_const_t<T>;

    template<class B = True<>>
    using If = std::enable_if_t<B{}, int>;

    template<Bool B>
    using IsSatisfied = Constant<Bool, B>;

    template<class B>
    using Not = std::negation<B>;

    template<class... Bs>
    using All = std::conjunction<Bs...>;

    template<class... Bs>
    using Any = std::disjunction<Bs...>;

    template<class... Bs>
    using None = All<Not<Bs>...>;

    class Undefined;

    template<class T, auto = sizeof(T)>
    True<> isDefined(T *);

    template<class T, auto = sizeof(T), If<Not<IsPointer<T>>> = 0>
    True<> isDefined(T &&);

    False<> isDefined(...);

    template<class T>
    using IsDefined = decltype(isDefined(declval<T *>()));

    template<class... Ts>
    struct IsSame;

    template<class T>
    struct IsSame<T> : True<>
    {
    };

    template<class T>
    struct IsSame<T, T> : True<>
    {
    };

    template<class T, class U>
    struct IsSame<T, U> : False<>
    {
    };

    template<class T, class U, class... Ts>
    struct IsSame<T, U, Ts...> : All<IsSame<T, U>, IsSame<T, Ts>...>
    {
    };

    template<class T, class... Ts>
    struct IsUnique : All<Not<IsSame<T, Ts>>..., IsUnique<Ts...>>
    {
    };
    template<class T>
    struct IsUnique<T> : True<>
    {
    };

    template<class B, class T, class F>
    using Conditional = std::conditional_t<B{}, T, F>;

    template<class F, class... Args>
    using IsInvocable = std::is_invocable<F, Args...>;

    template<class R, class F, class... Args>
    using IsInvocableResult = std::is_invocable_r<R, F, Args...>;

    template<class F, class... Args>
    using ResultOf = std::invoke_result_t<F, Args...>;

    template<class T, class... Args>
    using IsConstructible = std::is_constructible<T, Args...>;

    template<class T, class U = T>
    using IsAssignable = std::is_assignable<T, U>;

    template<class F, class T>
    using IsConvertible = std::is_convertible<F, T>;

    template<class B, class D>
    using IsBaseOf = std::is_base_of<B, D>;

    template<class T>
    using IsStandardLayout = std::is_standard_layout<T>;

    template<class AlwaysVoid, template<class...> class Op, class... Args>
    struct Detector : False<>
    {
    };

    template<template<class...> class Op, class... Args>
    struct Detector<Void<Op<Args...>>, Op, Args...> : True<>
    {
    };

    template<template<class...> class Op, class... Args>
    using IsDetected = Detector<void, Op, Args...>;

    template<class T>
    using IsEnum = std::is_enum<T>;

    template<class T>
    using UnderlyingType = std::underlying_type_t<T>;

    template<class T>
    using IsBool = IsSame<RemoveConst<T>, Bool>;

    template<class T>
    using IsIntegral = All<std::is_integral<T>, Not<IsBool<T>>>;

    template<class T>
    using IsSignedIntegral = All<IsIntegral<T>, std::is_signed<T>>;

    template<class T>
    using IsUnsignedIntegral = All<IsIntegral<T>, std::is_unsigned<T>>;

    template<class T>
    using IsSize = IsSame<RemoveConst<T>, Size>;

    template<class T>
    using IsIndex = IsSame<RemoveConst<T>, Index>;

    template<class T>
    using IsSeed = IsSame<RemoveConst<T>, Seed>;

    static_assert(IsSame<Size, Index>());
    static_assert(IsUnsignedIntegral<Size>());
    static_assert(IsUnsignedIntegral<Seed>());

    template<class... Xs>
    static inline constexpr auto forwardAsTuple(Xs &&...args)
    {
        return tuple<Xs &&...>(forward<Xs>(args)...);
    }

    template<class T>
    AddConst<T> &asConst(T &a)
    {
        return a;
    }

    template<class T>
    AddConst<T> asConst(T &&a)
    {
        return a;
    }

    template<class T, T... Seq>
    class Sequence;

    template<class>
    struct IsSequence : False<>
    {
    };

    template<class T>
    struct IsSequence<T const> : IsSequence<T>
    {
    };

    template<class T, T... Seq>
    struct IsSequence<Sequence<T, Seq...>> : True<>
    {
    };

    template<Size... Ns>
    using Sizes = Sequence<Size, Ns...>;

    template<class T>
    using IsSizes = All<IsSequence<T>, IsSame<Size, typename T::Type>>;

    template<Size N>
    class String;

    template<class>
    struct IsString : False<>
    {
    };

    template<class T>
    struct IsString<T const> : IsString<T>
    {
    };

    template<Size N>
    struct IsString<String<N>> : True<>
    {
    };

    template<class T = Void<>, class = If<>>
    struct Identity;

    template<class T = Void<>, class = If<>>
    struct Unit;

    template<class T = Void<>, class = If<>>
    struct Zero;
}
#endif // pRC_CORE_BASIC_TYPE_TRAITS_H
