// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_STD_H
#define pRC_STD_H

#ifndef NO_COMPILER_CHECKS

#    if __cplusplus > 201703L
#    else
#error A C++ compiler with full C++17 support and partial C++20 support is required. To disable compiler checks, define NO_COMPILER_CHECKS.
#    endif // __cplusplus

#    if __cpp_generic_lambdas >= 201707L
#    else
#        error C++20 Feature 'Familiar template syntax for generic lambdas' is required. To disable compiler checks, define NO_COMPILER_CHECKS.
#    endif // __cpp_generic_lambdas

#endif // NO_COMPILER_CHECKS

#endif // pRC_STD_H
