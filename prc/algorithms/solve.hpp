// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_ALGORITHMS_SOLVE_H
#define pRC_ALGORITHMS_SOLVE_H

#include <prc/core/basic/common.hpp>
#include <prc/core/basic/functions/zero.hpp>
#include <prc/core/basic/limits.hpp>
#include <prc/core/tensor/operator/hint.hpp>
#include <prc/core/tensor/operator/restrict.hpp>
#include <prc/core/tensor/operator/transform.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<Operator::Transform T = Operator::Transform::None,
        Operator::Restrict R = Operator::Restrict::None,
        Operator::Hint H = Operator::Hint::None, class Solver, class XA,
        class XB, If<IsInvocable<Solver, XA, XB>> = 0>
    static inline constexpr decltype(auto) solve(
        Solver &&solver, XA &&A, XB &&b)
    {
        return forward<Solver>(solver).template operator()<T, R, H>(
            forward<XA>(A), forward<XB>(b));
    }

    template<Operator::Transform T = Operator::Transform::None,
        Operator::Restrict R = Operator::Restrict::None,
        Operator::Hint H = Operator::Hint::None, class Solver, class XA,
        class XB, class XX, If<IsInvocable<Solver, XA, XB, XX>> = 0>
    static inline constexpr decltype(auto) solve(
        Solver &&solver, XA &&A, XB &&b, XX &&x0)
    {
        return forward<Solver>(solver).template operator()<T, R, H>(
            forward<XA>(A), forward<XB>(b), forward<XX>(x0));
    }

    template<Operator::Transform T = Operator::Transform::None,
        Operator::Restrict R = Operator::Restrict::None,
        Operator::Hint H = Operator::Hint::None, class Solver, class XA,
        class XB, class VT, If<IsInvocable<Solver, XA, XB, VT>> = 0>
    static inline constexpr decltype(auto) solve(
        Solver &&solver, XA &&A, XB &&b, VT const &tolerance)
    {
        return forward<Solver>(solver).template operator()<T, R, H>(
            forward<XA>(A), forward<XB>(b), tolerance);
    }

    template<Operator::Transform T = Operator::Transform::None,
        Operator::Restrict R = Operator::Restrict::None,
        Operator::Hint H = Operator::Hint::None, class Solver, class XA,
        class XB, class XX, class VT,
        If<IsInvocable<Solver, XA, XB, XX, VT>> = 0>
    static inline constexpr decltype(auto) solve(
        Solver &&solver, XA &&A, XB &&b, XX &&x0, VT const &tolerance)
    {
        return forward<Solver>(solver).template operator()<T, R, H>(
            forward<XA>(A), forward<XB>(b), forward<XX>(x0), tolerance);
    }

    template<class Solver, Operator::Transform T = Operator::Transform::None,
        Operator::Restrict R = Operator::Restrict::None,
        Operator::Hint H = Operator::Hint::None, class XA, class XB,
        If<IsInvocable<Solver, XA, XB>> = 0>
    static inline constexpr decltype(auto) solve(XA &&A, XB &&b)
    {
        return solve<T, R, H>(Solver(), forward<XA>(A), forward<XB>(b));
    }

    template<class Solver, Operator::Transform T = Operator::Transform::None,
        Operator::Restrict R = Operator::Restrict::None,
        Operator::Hint H = Operator::Hint::None, class XA, class XB, class XX,
        If<IsInvocable<Solver, XA, XB, XX>> = 0>
    static inline constexpr decltype(auto) solve(XA &&A, XB &&b, XX &&x0)
    {
        return solve<T, R, H>(
            Solver(), forward<XA>(A), forward<XB>(b), forward<XX>(x0));
    }

    template<class Solver, Operator::Transform T = Operator::Transform::None,
        Operator::Restrict R = Operator::Restrict::None,
        Operator::Hint H = Operator::Hint::None, class XA, class XB, class VT,
        If<IsInvocable<Solver, XA, XB, VT>> = 0>
    static inline constexpr decltype(auto) solve(
        XA &&A, XB &&b, VT const &tolerance)
    {
        return solve<T, R, H>(
            Solver(), forward<XA>(A), forward<XB>(b), tolerance);
    }

    template<class Solver, Operator::Transform T = Operator::Transform::None,
        Operator::Restrict R = Operator::Restrict::None,
        Operator::Hint H = Operator::Hint::None, class XA, class XB, class XX,
        class VT, If<IsInvocable<Solver, XA, XB, XX, VT>> = 0>
    static inline constexpr decltype(auto) solve(
        XA &&A, XB &&b, XX &&x0, VT const &tolerance)
    {
        return solve<T, R, H>(Solver(), forward<XA>(A), forward<XB>(b),
            forward<XX>(x0), tolerance);
    }
}
#endif // pRC_ALGORITHMS_SOLVE_H
