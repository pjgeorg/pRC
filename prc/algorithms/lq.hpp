// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_ALGORITHMS_LQ_H
#define pRC_ALGORITHMS_LQ_H

#include <prc/algorithms/qr.hpp>
#include <prc/core/tensor/operator/functions/adjoint.hpp>
#include <prc/core/tensor/type_traits.hpp>

namespace pRC
{
    template<Size B = 32, class X, class R = RemoveReference<X>,
        If<IsTensorish<R>> = 0, If<IsFloat<typename R::Value>> = 0,
        If<IsSatisfied<(typename R::Dimension() == 2)>> = 0>
    static inline constexpr auto lq(X &&input)
    {
        auto const [q, r] = qr<B>(adjoint(forward<X>(input)));
        return tuple(eval(adjoint(r)), eval(adjoint(q)));
    }
}
#endif // pRC_ALGORITHMS_LQ_H
