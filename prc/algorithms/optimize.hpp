// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_ALGORITHMS_OPTIMIZE_H
#define pRC_ALGORITHMS_OPTIMIZE_H

#include <prc/core/functors/eval.hpp>
#include <prc/core/tensor/type_traits.hpp>
#include <prc/core/value/limits.hpp>
#include <prc/core/value/type_traits.hpp>

namespace pRC
{
    template<class Optimizer, class XX, class RX = RemoveReference<XX>,
        class TX = typename RX::Type, class VX = typename TX::Value,
        If<IsTensorish<RX>> = 0,
        class RXE = RemoveConstReference<ResultOf<Eval, XX>>, class FF,
        If<IsInvocable<FF, RXE const &, RXE &>> = 0,
        If<IsFloat<ResultOf<FF, RXE const &, RXE &>>> = 0, class FC,
        If<IsInvocable<FC, RXE>> = 0, class VT = VX,
        If<All<IsFloat<VX>, IsFloat<VT>>> = 0,
        If<IsInvocable<Optimizer, XX, FF, FC, VT>> = 0>
    static inline constexpr auto optimize(Optimizer &&optimizer, XX &&x,
        FF &&function, FC &&callback,
        VT const &tolerance = NumericLimits<VT>::tolerance())
    {
        return forward<Optimizer>(optimizer)(forward<XX>(x),
            forward<FF>(function), forward<FC>(callback), tolerance);
    }

    template<class Optimizer, class XX, class RX = RemoveReference<XX>,
        class TX = typename RX::Type, class VX = typename TX::Value,
        If<IsTensorish<RX>> = 0,
        class RXE = RemoveConstReference<ResultOf<Eval, XX>>, class FF,
        If<IsInvocable<FF, RXE const &, RXE &>> = 0,
        If<IsFloat<ResultOf<FF, RXE const &, RXE &>>> = 0, class VT = VX,
        If<All<IsFloat<VX>, IsFloat<VT>>> = 0,
        If<IsInvocable<Optimizer, XX, FF, void(RXE const), VT>> = 0>
    static inline constexpr auto optimize(Optimizer &&optimizer, XX &&x,
        FF &&function, VT const &tolerance = NumericLimits<VT>::tolerance())
    {
        return optimize(
            forward<Optimizer>(optimizer), forward<XX>(x),
            forward<FF>(function),
            [](auto &&)
            {
            },
            tolerance);
    }

    template<class Optimizer, class XX, class RX = RemoveReference<XX>,
        class TX = typename RX::Type, class VX = typename TX::Value,
        If<IsTensorish<RX>> = 0,
        class RXE = RemoveConstReference<ResultOf<Eval, XX>>, class FF,
        If<IsInvocable<FF, RXE const &, RXE &>> = 0,
        If<IsFloat<ResultOf<FF, RXE const &, RXE &>>> = 0, class FC,
        If<IsInvocable<FC, RXE>> = 0, class VT = VX,
        If<All<IsFloat<VX>, IsFloat<VT>>> = 0,
        If<IsInvocable<Optimizer, XX, FF, FC, VT>> = 0>
    static inline constexpr auto optimize(XX &&x, FF &&function, FC &&callback,
        VT const &tolerance = NumericLimits<VT>::tolerance())
    {
        return optimize(Optimizer(), forward<XX>(x), forward<FF>(function),
            forward<FC>(callback), tolerance);
    }

    template<class Optimizer, class XX, class RX = RemoveReference<XX>,
        class TX = typename RX::Type, class VX = typename TX::Value,
        If<IsTensorish<RX>> = 0,
        class RXE = RemoveConstReference<ResultOf<Eval, XX>>, class FF,
        If<IsInvocable<FF, RXE const &, RXE &>> = 0,
        If<IsFloat<ResultOf<FF, RXE const &, RXE &>>> = 0, class VT = VX,
        If<All<IsFloat<VX>, IsFloat<VT>>> = 0,
        If<IsInvocable<Optimizer, XX, FF, void(RXE const), VT>> = 0>
    static inline constexpr auto optimize(XX &&x, FF &&function,
        VT const &tolerance = NumericLimits<VT>::tolerance())
    {
        return optimize(
            Optimizer(), forward<XX>(x), forward<FF>(function), tolerance);
    }
}
#endif // pRC_ALGORITHMS_OPTIMIZE_H
