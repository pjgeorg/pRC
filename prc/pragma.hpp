// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_PRAGMA_H
#define pRC_PRAGMA_H

#define DO_PRAGMA(x) _Pragma(#x)

#if defined(__clang__)

#    define BEGIN_IGNORE_DIAGNOSTIC(warning) \
        _Pragma("clang diagnostic push") \
            DO_PRAGMA(GCC diagnostic ignored warning)

#    define END_IGNORE_DIAGNOSTIC _Pragma("clang diagnostic pop")

#    define BEGIN_IGNORE_DIAGNOSTIC_GCC(warning)
#    define END_IGNORE_DIAGNOSTIC_GCC

#    define BEGIN_IGNORE_DIAGNOSTIC_CLANG(warning) \
        BEGIN_IGNORE_DIAGNOSTIC(warning)
#    define END_IGNORE_DIAGNOSTIC_CLANG END_IGNORE_DIAGNOSTIC

#elif defined(__GNUC__)

#    define BEGIN_IGNORE_DIAGNOSTIC(warning) \
        _Pragma("GCC diagnostic push") DO_PRAGMA(GCC diagnostic ignored warning)

#    define END_IGNORE_DIAGNOSTIC _Pragma("GCC diagnostic pop")

#    define BEGIN_IGNORE_DIAGNOSTIC_GCC(warning) \
        BEGIN_IGNORE_DIAGNOSTIC(warning)
#    define END_IGNORE_DIAGNOSTIC_GCC END_IGNORE_DIAGNOSTIC

#    define BEGIN_IGNORE_DIAGNOSTIC_CLANG(warning)
#    define END_IGNORE_DIAGNOSTIC_CLANG

#else

#    define BEGIN_IGNORE_DIAGNOSTIC(warning)
#    define END_IGNORE_DIAGNOSTIC

#    define BEGIN_IGNORE_DIAGNOSTIC_GCC(warning)
#    define END_IGNORE_DIAGNOSTIC_GCC

#    define BEGIN_IGNORE_DIAGNOSTIC_CLANG(warning)
#    define END_IGNORE_DIAGNOSTIC_CLANG

#endif

#endif // pRC_PRAGMA_H
