// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_TENSOR_TRAIN_H
#define pRC_TENSOR_TRAIN_H

#include <prc/config.hpp>
#include <prc/core.hpp>
#include <prc/std.hpp>

#include <prc/tensor_train/algorithms.hpp>
#include <prc/tensor_train/common.hpp>
#include <prc/tensor_train/operator.hpp>
#include <prc/tensor_train/tensor.hpp>

#endif // pRC_TENSOR_TRAIN_H
