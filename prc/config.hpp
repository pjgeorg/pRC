// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CONFIG_H
#define pRC_CONFIG_H

#include <prc/core/basic/type_traits.hpp>

namespace pRC
{
#ifndef DEFAULT_FLOAT
#    define DEFAULT_FLOAT 64
#endif // DEFAULT_FLOAT

#if DEFAULT_FLOAT == 16
#    error \
        "Can not set default floating-point type to Float<16>." \
        "Half-precision support is limited to storage only."
#endif // DEFAULT_FLOAT

    enum class LogLevel
    {
        Error,
        Warning,
        Info,
        Debug,
        Trace,
    };

#ifdef LOG_LEVEL
    constexpr auto cLogLevel = LogLevel::LOG_LEVEL;
#else
    constexpr auto cLogLevel = LogLevel::Info;
#endif // LOG_LEVEL

    enum class DebugLevel
    {
        None,
        Low,
        Mid,
        High,
    };

#ifdef DEBUG_LEVEL
    constexpr auto cDebugLevel = DebugLevel::DEBUG_LEVEL;
#else
    constexpr auto cDebugLevel = DebugLevel::Low;
#endif // DEBUG_LEVEL

    constexpr Size cHugepageSizeByte = 2 * 1024 * 1024;
    constexpr Size cPageSizeByte = 4 * 1024;
    constexpr Size cCacheLineSizeByte = 64;
    constexpr Size cSimdSizeByte =
#if defined __AVX512F__
        64;
#elif defined __AVX__
        32;
#else
        16;
#endif
}
#endif // pRC_CONFIG_H
