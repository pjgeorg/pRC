// SPDX-License-Identifier: BSD-2-Clause

#ifndef pRC_CORE_H
#define pRC_CORE_H

#include <prc/config.hpp>
#include <prc/pragma.hpp>
#include <prc/std.hpp>

#include <prc/core/basic.hpp>
#include <prc/core/complex.hpp>
#include <prc/core/container.hpp>
#include <prc/core/functors.hpp>
#include <prc/core/log.hpp>
#include <prc/core/parameter.hpp>
#include <prc/core/profiling.hpp>
#include <prc/core/random.hpp>
#include <prc/core/tensor.hpp>
#include <prc/core/value.hpp>

#endif // pRC_CORE_H
